﻿//=============================================================//
//                                                             //
//                  DataLib::PluginLoader.cs                   //
//   -------------------------------------------------------   //
//  Copyright © 2011 Anthony Mushrow, SKGenius Development     //
//  All rights reserved                                        //
//                                                             //
//  This code is licensed under The Code Project Open License  //
//  (CPOL) See http://www.codeproject.com/info/cpol10.aspx or  //
//  LICENSE.TXT for more information                           //
//                                                             //
//=============================================================//
using System;
using System.Collections.Generic;
using System.Text;
using System.IO;
using System.Reflection;
using System.Runtime.InteropServices;
using DataLibInterfaces;

namespace DataLib
{
	public class Plugin : ITypeContainer
	{
		public override string Name
		{
			get { return m_attribs.Name; }
		}

		public override string Description
		{
			get { return m_attribs.Description; }
		}

		public string AssemblyFile
		{
			get { return m_asmPath; }
		}

		public override Guid GUID
		{
			get { return m_type.GUID; }
		}

		public Plugin(DataTypeAttribute attribs, string AssemblyPath, Type type)
		{
			m_attribs = attribs;
			m_asmPath = AssemblyPath;
			m_type = type;
		}

		public override string ToString()
		{
			return m_attribs.Name;
		}

		public override IDataType CreateInstance()
		{
			return (IDataType)Activator.CreateInstance(m_type);
		}

		private DataTypeAttribute m_attribs;
		private string m_asmPath;
		private Type m_type;
	}

	public static class PluginLoader
	{
		public static List<Plugin> FindDataPlugins(string asmFile)
		{
			List<Plugin> list = new List<Plugin>();

			try
			{
				Assembly asm = Assembly.LoadFrom(asmFile);
				Type[] allTypes = asm.GetExportedTypes();

				foreach (Type t in allTypes)
				{
					DataTypeAttribute[] attributes = (DataTypeAttribute[])t.GetCustomAttributes(typeof(DataTypeAttribute), false);
					for (int i = 0; i < attributes.Length; i++)
					{
						FileInfo fi = new FileInfo(asmFile);
						Plugin ip = new Plugin(attributes[i], fi.Name, t);
						list.Add(ip);
					}
				}
			}
			catch (System.BadImageFormatException)
			{
				//It would seem this is not really a DLL we can use
			}

			return list;
		}

		public static List<Plugin> FindDataPlugins(Assembly asm)
		{
			List<Plugin> list = new List<Plugin>();

			try
			{
				Type[] allTypes = asm.GetExportedTypes();

				foreach (Type t in allTypes)
				{
					DataTypeAttribute[] attributes = (DataTypeAttribute[])t.GetCustomAttributes(typeof(DataTypeAttribute), false);
					for (int i = 0; i < attributes.Length; i++)
					{
						FileInfo fi = new FileInfo(asm.Location);
						Plugin ip = new Plugin(attributes[i], fi.Name, t);
						list.Add(ip);
					}
				}
			}
			catch (System.BadImageFormatException)
			{
				//It would seem this is not really a DLL we can use
			}

			return list;
		}
	}
}
