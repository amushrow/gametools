﻿//=============================================================//
//                                                             //
//                    DataLib::SpriteData.cs                   //
//   -------------------------------------------------------   //
//  Copyright © 2011 Anthony Mushrow, SKGenius Development     //
//  All rights reserved                                        //
//                                                             //
//  This code is licensed under The Code Project Open License  //
//  (CPOL) See http://www.codeproject.com/info/cpol10.aspx or  //
//  LICENSE.TXT for more information                           //
//                                                             //
//=============================================================//
using System;
using System.Collections.Generic;
using System.Text;
using System.IO;

namespace DataLib
{
	public struct Shape
	{
		public float X;
		public float Y;
		public float Width;
		public float Height;
		public float cX;
		public float cY;
	}

	public class SpriteData
	{
		private Shape[] m_frameCoords;
		private int m_numFrames;
		private float m_frameInterval;
		private float m_width;
		private float m_height;
		private string m_name;
		private string m_texName;
		private string m_texPath;

		public SpriteData()
		{
			m_width = 0;
			m_height = 0;
			m_numFrames = 0;
			m_frameInterval = 0.0166667f;
			m_frameCoords = null;
			m_name = "DefaultName";
			m_texName = "";
			m_texPath = "";
		}

		public SpriteData Clone()
		{
			SpriteData data = new SpriteData();
			data.Name = m_name;
			data.TexName = m_texName;
			data.TexPath = m_texPath;
			data.FrameInterval = m_frameInterval;
			data.Height = m_height;
			data.Width = m_width;
			data.SetFrameCoords(m_numFrames, m_frameCoords);

			return data;
		}

		public void WriteToStream(Stream output)
		{
			StreamWrapper sw = new StreamWrapper(output);

			sw.WriteString(m_name);
			sw.WriteString(m_texName);
			sw.WriteString(m_texPath);
			sw.WriteFloat(m_width);
			sw.WriteFloat(m_height);
			sw.WriteFloat(m_frameInterval);
			sw.WriteInt(m_numFrames);

			for (int i = 0; i < m_numFrames; i++)
			{
				sw.WriteFloat(m_frameCoords[i].cX);
				sw.WriteFloat(m_frameCoords[i].cY);
				sw.WriteFloat(m_frameCoords[i].X);
				sw.WriteFloat(m_frameCoords[i].Y);
				sw.WriteFloat(m_frameCoords[i].Width);
				sw.WriteFloat(m_frameCoords[i].Height);
			}
		}

		public static SpriteData ReadFromStream(Stream input)
		{
			StreamWrapper sw = new StreamWrapper(input);

			SpriteData data = new SpriteData();
			data.Name = sw.ReadString();
			data.TexName = sw.ReadString();
			data.TexPath = sw.ReadString();
			data.Width = sw.ReadFloat();
			data.Height = sw.ReadFloat();
			data.FrameInterval = sw.ReadFloat();

			int numFrames = sw.ReadInt();
			Shape[] shapes = new Shape[numFrames];

			for (int i = 0; i < numFrames; i++)
			{
				shapes[i].cX = sw.ReadFloat();
				shapes[i].cY = sw.ReadFloat();
				shapes[i].X = sw.ReadFloat();
				shapes[i].Y = sw.ReadFloat();
				shapes[i].Width = sw.ReadFloat();
				shapes[i].Height = sw.ReadFloat();
			}

			data.SetFrameCoords(numFrames, shapes);

			return data;
		}

		public string Name
		{
			get { return m_name; }
			set { m_name = value; }
		}

		public string TexName
		{
			get { return m_texName; }
			set { m_texName = value; }
		}

		public string TexPath
		{
			get { return m_texPath; }
			set { m_texPath = value; }
		}

		public float Width
		{
			get { return m_width; }
			set { m_width = value; }
		}

		public float Height
		{
			get { return m_height; }
			set { m_height = value; }
		}

		public float FrameInterval
		{
			get { return m_frameInterval; }
			set { m_frameInterval = value; }
		}

		public int NumFrames
		{
			get { return m_numFrames; }
		}

		public Shape[] FrameCoords
		{
			get { return m_frameCoords; }
		}

		public void SetFrameCoords(int numFrames, Shape[] coords)
		{
			m_numFrames = numFrames;
			m_frameCoords = new Shape[numFrames];
			for (int i = 0; i < numFrames; i++)
			{
				m_frameCoords[i] = coords[i];
			}
		}

		public void SetNumFrameCoords(int numFrames)
		{
			Shape[] newData = new Shape[numFrames];
			for (int i = 0; i < m_numFrames && i < numFrames; i++)
			{
				newData[i] = m_frameCoords[i];
			}

			m_frameCoords = newData;
			m_numFrames = numFrames;
		}
	}
}
