﻿//=============================================================//
//                                                             //
//                  DataLib::StreamWrapper.cs                  //
//   -------------------------------------------------------   //
//  Copyright © 2011 Anthony Mushrow, SKGenius Development     //
//  All rights reserved                                        //
//                                                             //
//  This code is licensed under The Code Project Open License  //
//  (CPOL) See http://www.codeproject.com/info/cpol10.aspx or  //
//  LICENSE.TXT for more information                           //
//                                                             //
//=============================================================//
using System;
using System.Collections.Generic;
using System.Text;
using System.IO;

namespace DataLib
{
	public class StreamWrapper
	{
		private Stream m_stream;
		public StreamWrapper(Stream stream)
		{
			m_stream = stream;
		}

		public void Close()
		{
			m_stream.Close();
		}

		public void WriteInt(int value)
		{
			byte[] buffer = BitConverter.GetBytes(value);
			m_stream.Write(buffer, 0, buffer.Length);
		}


		public int ReadInt()
		{
			byte[] buffer = new byte[4];
			m_stream.Read(buffer, 0, 4);

			return BitConverter.ToInt32(buffer, 0);
		}

		public void WriteFloat(float value)
		{
			byte[] buffer = BitConverter.GetBytes(value);
			m_stream.Write(buffer, 0, buffer.Length);
		}

		public float ReadFloat()
		{
			byte[] buffer = new byte[4];
			m_stream.Read(buffer, 0, 4);

			return BitConverter.ToSingle(buffer, 0);
		}

		public void WriteLong(Int64 value)
		{
			byte[] buffer = BitConverter.GetBytes(value);
			m_stream.Write(buffer, 0, buffer.Length);
		}

		public Int64 ReadLong()
		{
			byte[] buffer = new byte[8];
			m_stream.Read(buffer, 0, 8);

			return BitConverter.ToInt64(buffer, 0);
		}

		public void WriteBool(bool value)
		{
			byte[] buffer = BitConverter.GetBytes(value);
			m_stream.Write(buffer, 0, buffer.Length);
		}

		public bool ReadBool()
		{
			byte[] buffer = new byte[1];
			m_stream.Read(buffer, 0, 1);

			return BitConverter.ToBoolean(buffer, 0);
		}

		public void WriteString(string value)
		{
			byte[] buffer = System.Text.UnicodeEncoding.Unicode.GetBytes(value);
			WriteInt(buffer.Length);
			m_stream.Write(buffer, 0, buffer.Length);
		}

		public string ReadString()
		{
			byte[] buffer = new byte[4];
			m_stream.Read(buffer, 0, 4);
			int length = BitConverter.ToInt32(buffer, 0);
			buffer = new byte[length];
			m_stream.Read(buffer, 0, length);

			return System.Text.UnicodeEncoding.Unicode.GetString(buffer, 0, length);
		}
	}
}
