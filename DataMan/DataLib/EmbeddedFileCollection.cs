﻿//=============================================================//
//                                                             //
//              DataLib::EmbeddedFileCollection.cs             //
//   -------------------------------------------------------   //
//  Copyright © 2011 Anthony Mushrow, SKGenius Development     //
//  All rights reserved                                        //
//                                                             //
//  This code is licensed under The Code Project Open License  //
//  (CPOL) See http://www.codeproject.com/info/cpol10.aspx or  //
//  LICENSE.TXT for more information                           //
//                                                             //
//=============================================================//
using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;

namespace DataLib
{
	class InternalFileCollection : IEnumerable
	{
		public InternalFileCollection()
		{
			m_files = new List<EmbeddedFile>();
			m_topId = 1;
			m_rebuildFile = false;
		}

		public bool RebuildRequired
		{
			get { return m_rebuildFile; }
		}

		public int FileCount
		{
			get { return m_files.Count; }
		}

		public void Clear()
		{
			m_rebuildFile = false;
			foreach (EmbeddedFile file in m_files)
			{
				//Flag that we will need to rewrite the whole file
				if (file.Embedded)
				{
					m_rebuildFile = true;
					break;
				}
			}
			m_files.Clear();

			m_topId = 1;
		}

		public void Add(EmbeddedFile newFile)
		{
			foreach (EmbeddedFile file in m_files)
			{
				if (file.UID == newFile.UID)
				{
					newFile.SetNewUID(m_topId++);
				}
			}

			if (newFile.UID >= m_topId)
				m_topId = newFile.UID + 1;

			m_files.Add(newFile);
		}

		public int Add(string FileName, string InternalName)
		{
			System.IO.FileInfo fi = new System.IO.FileInfo(FileName);
			EmbeddedFile newFile = new EmbeddedFile(m_topId);
			newFile.Embedded = false;
			newFile.FileName = FileName;
			newFile.InternalName = InternalName;
			newFile.OriginalLength = fi.Length;
			newFile.FileAttributes = (int)fi.Attributes;
			newFile.CreationTimeUTC = fi.CreationTimeUtc.ToFileTime();
			newFile.ModifiedTimeUTC = fi.LastWriteTimeUtc.ToFileTime();
			newFile.AccessTimeUTC = fi.LastAccessTimeUtc.ToFileTime();

			m_files.Add(newFile);

			return m_topId++;
		}

		public void Remove(int UID)
		{
			foreach (EmbeddedFile file in m_files)
			{
				if (file.UID == UID)
				{
					if (file.Embedded)
						m_rebuildFile = true;
					m_files.Remove(file);
					break;
				}
			}
		}

		public bool Rename(int UID, string NewName)
		{
			bool success = false;
			foreach (EmbeddedFile file in m_files)
			{
				if (file.UID == UID)
				{
					file.InternalName = NewName;
					success = true;
					break;
				}
			}

			return success;
		}

		public IEnumerator GetEnumerator()
		{
			return m_files.GetEnumerator();
		}

		private List<EmbeddedFile> m_files;
		private int m_topId;
		private bool m_rebuildFile;
	}

	public class FileCollection : IEnumerable
	{
		public class File
		{
			internal File(EmbeddedFile source)
			{
				m_source = source;
			}

			public string Name
			{
				get { return m_source.InternalName; }
			}

			public int UID
			{
				get { return m_source.UID; }
			}

			public bool Embedded
			{
				get { return m_source.Embedded; }
			}

			public long CompressedLength
			{
				get { return m_source.CompressedLength; }
			}

			public long OriginalLength
			{
				get { return m_source.OriginalLength; }
			}

			public long CreationTimeUTC
			{
				get { return m_source.CreationTimeUTC; }
			}

			public long AccessTimeUTC
			{
				get { return m_source.AccessTimeUTC; }
			}

			public long ModifiedTimeUTC
			{
				get { return m_source.ModifiedTimeUTC; }
			}

			public int FileAttributes
			{
				get { return m_source.FileAttributes; }
			}

			private EmbeddedFile m_source;
		}

		
		internal FileCollection(InternalFileCollection intern)
		{
			m_internal = intern;
			m_exposedFiles = new List<File>();
		}

		public int Count
		{
			get { return m_internal.FileCount; }
		}
		public int Add(string FilePath, string InternalName)
		{
			return m_internal.Add(FilePath, InternalName);
		}

		public void Remove(int uid)
		{
			m_internal.Remove(uid);
		}

		public void Rename(int uid, string newName)
		{
			m_internal.Rename(uid, newName);
		}

		public File Find(int UID)
		{
			File found = null;
			
			foreach (EmbeddedFile search in m_internal)
			{
				if (search.UID == UID)
				{
					found = new File(search);
					break;
				}
			}

			return found;
		}

		public void Clear()
		{
			m_internal.Clear();
		}

		public IEnumerator GetEnumerator()
		{
			m_exposedFiles.Clear();
			foreach (EmbeddedFile file in m_internal)
			{
				File exposed = new File(file);
				m_exposedFiles.Add(exposed);
			}

			return m_exposedFiles.GetEnumerator();
		}

		private InternalFileCollection m_internal;
		private List<File> m_exposedFiles;
	}

	internal class EmbeddedFile
	{
		public EmbeddedFile(int ID)
		{
			m_ID = ID;
			DataStart = 0;
			CompressedLength = 0;
			OriginalLength = 0;
			Embedded = false;
			FileName = String.Empty;
			InternalName = String.Empty;
		}

		public void SetNewUID(int uid)
		{
			m_ID = uid;
		}

		public long DataStart;
		public long CompressedLength;
		public long OriginalLength;

		public long CreationTimeUTC;
		public long ModifiedTimeUTC;
		public long AccessTimeUTC;
		public int FileAttributes;

		public bool Embedded;
		public string FileName;
		public string InternalName;

		public int UID
		{
			get { return m_ID; }
		}

		private int m_ID;
	}
}
