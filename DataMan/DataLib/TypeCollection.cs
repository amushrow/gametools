﻿//=============================================================//
//                                                             //
//                  DataLib::TypeCollection.cs                 //
//   -------------------------------------------------------   //
//  Copyright © 2011 Anthony Mushrow, SKGenius Development     //
//  All rights reserved                                        //
//                                                             //
//  This code is licensed under The Code Project Open License  //
//  (CPOL) See http://www.codeproject.com/info/cpol10.aspx or  //
//  LICENSE.TXT for more information                           //
//                                                             //
//=============================================================//
using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Text;
using DataLibInterfaces;

namespace DataLib
{
	public class TypeCollection : IEnumerable
	{
		public TypeCollection()
		{
			m_stdTypes = new List<ITypeContainer>();
			m_custTypes = new List<ITypeContainer>();
			m_extTypes = new List<ITypeContainer>();
			m_allTypes = new List<ITypeContainer>();

			foreach (Type t in DataLib.StandardTypes.TypeCollection)
			{
				DataTypeAttribute[] attributes = (DataTypeAttribute[])t.GetCustomAttributes(typeof(DataTypeAttribute), false);
				for (int i = 0; i < attributes.Length; i++)
				{
					StandardTypeContainer tp = new StandardTypeContainer(attributes[i].Name, attributes[i].Description, t);
					m_stdTypes.Add(tp);
					m_allTypes.Add(tp);
				}
			}
		}

		public ReadOnlyCollection<ITypeContainer> Standard
		{
			get { return m_stdTypes.AsReadOnly(); }
		}

		public ReadOnlyCollection<ITypeContainer> Custom
		{
			get { return m_custTypes.AsReadOnly(); }
		}

		public ReadOnlyCollection<ITypeContainer> Extended
		{
			get { return m_extTypes.AsReadOnly(); }
		}

		public void Add(ITypeContainer newType)
		{
			bool exists = false;

			foreach (ITypeContainer type in m_allTypes)
			{
				if (type.GUID == newType.GUID)
				{
					exists = true;
					break;
				}
			}

			if (!exists)
			{
				if (newType is CustomTypeContainer)
					m_custTypes.Add(newType);
				else
					m_extTypes.Add(newType);

				m_allTypes.Add(newType);
			}
		}

		public void Remove(ITypeContainer container)
		{
			m_stdTypes.Remove(container);
			m_custTypes.Remove(container);
			m_extTypes.Remove(container);
			m_allTypes.Remove(container);
		}

        public void Clear()
		{
            Clear(true);
        }

		public void Clear(bool extended)
		{
			foreach (ITypeContainer container in m_custTypes)
				m_allTypes.Remove(container);
			m_custTypes.Clear();

            if(extended) 
            {
			    foreach (ITypeContainer container in m_extTypes)
				    m_allTypes.Remove(container);
			    m_extTypes.Clear();
            }
		}

		public bool FindType(Guid GUID, out ITypeContainer Container)
		{
			Container = null;
			foreach (ITypeContainer type in this)
			{
				if (type.GUID == GUID)
				{
					Container = type;
					break;
				}
			}

			return (Container != null);
		}

		public int Count
		{
			get { return m_allTypes.Count; }
		}

		public ITypeContainer this[int index]
		{
			get { return m_allTypes[index]; }
		}

		//--IEnumerable
		public IEnumerator GetEnumerator()
		{
			return m_allTypes.GetEnumerator();
		}
		//--

		private List<ITypeContainer> m_stdTypes;
		private List<ITypeContainer> m_extTypes;
		private List<ITypeContainer> m_custTypes;
		private List<ITypeContainer> m_allTypes;
	}
}
