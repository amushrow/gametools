﻿//=============================================================//
//                                                             //
//                  DataLib::CustomDataType.cs                 //
//   -------------------------------------------------------   //
//  Copyright © 2011 Anthony Mushrow, SKGenius Development     //
//  All rights reserved                                        //
//                                                             //
//  This code is licensed under The Code Project Open License  //
//  (CPOL) See http://www.codeproject.com/info/cpol10.aspx or  //
//  LICENSE.TXT for more information                           //
//                                                             //
//=============================================================//
using System;
using System.Collections.Generic;
using System.Collections;
using System.Text;
using System.IO;
using DataLibInterfaces;

namespace DataLib
{
	public class PropID
	{
		public string	Name;
		public Guid		GUID;
		public UInt16	ID;
	}

	public class CustomDataTypeDefinition
	{
		public Guid GUID;
		public string TypeName;
		public string Description;
		public List<PropID> Properties;

		public void WriteStream(Stream s)
		{
			StreamWrapper sw = new StreamWrapper(s);

			byte[] data = GUID.ToByteArray();
			s.Write(data, 0, data.Length);

			sw.WriteString(TypeName);
			sw.WriteString(Description);
			sw.WriteInt(Properties.Count);
			foreach (PropID prop in Properties)
			{
				sw.WriteString(prop.Name);
				
				data = prop.GUID.ToByteArray();
				s.Write(data, 0, data.Length);

				data = BitConverter.GetBytes(prop.ID);
				s.Write(data, 0, 2);
			}
		}

		public static CustomDataTypeDefinition ReadStream(Stream s)
		{
			CustomDataTypeDefinition customType = new CustomDataTypeDefinition();
			StreamWrapper sw = new StreamWrapper(s);
			
			byte[] data = new byte[16];
			s.Read(data, 0, 16);
			customType.GUID = new Guid(data);
			customType.TypeName = sw.ReadString();
			customType.Description = sw.ReadString();
			int count = sw.ReadInt();
			customType.Properties = new List<PropID>(count);
			for (int i = 0; i < count; i++)
			{
				PropID newProp = new PropID();
				newProp.Name = sw.ReadString();
				s.Read(data, 0, 16);
				newProp.GUID = new Guid(data);
				s.Read(data, 0, 2);
				newProp.ID = BitConverter.ToUInt16(data, 0);

				customType.Properties.Add(newProp);
			}

			return customType;
		}
	}

	public class CustomDataType : IDataType
	{
		public CustomDataType(CustomDataTypeDefinition Def, TypeCollection AvailableTypes)
		{
			m_definition = Def;
			m_typeName = Def.TypeName;
			m_properties = new List<PropertyDescription>();
			m_internalPropDesc = new EditablePropertyDescription(this);
			m_internalPropDesc.Name = TypeName;
			m_internalPropDesc.TextEdit = false;
			m_internalPropDesc.EditDialog = false;

			m_propDesc = new PropertyDescription(m_internalPropDesc);

			
			foreach (PropID propdef in Def.Properties)
			{
				//--Find type and create instance
				IDataType instance = null;
				foreach (ITypeContainer typeContainer in AvailableTypes)
				{
					if (typeContainer.GUID == propdef.GUID)
						instance = typeContainer.CreateInstance();
				}
				if (instance == null)
				{
					instance = new UnknownDataType(propdef.GUID);
				}
				
				instance.ID = propdef.ID;
				instance.Name = propdef.Name;
				m_properties.Add(instance.Property);
				//--
			}
		}

		public string TypeName
		{
			get { return m_typeName; }
		}

		public Guid TypeGUID
		{
			get { return m_definition.GUID; }
		}

		public string Name
		{
			get { return m_internalPropDesc.Name; }
			set { m_internalPropDesc.Name = value; }
		}

		public string StringValue
		{
			get { return String.Format("[{0}]", m_typeName); }
		}

		public bool SetValue(string val)
		{
			return false;
		}

		public PropertyDescription Property
		{
			get { return m_propDesc; }
		}

		public int ID
		{
			get;
			set;
		}

		public IEnumerable<PropertyDescription> GetSubProperties()
		{
			List<PropertyDescription> props = new List<PropertyDescription>();
			foreach (PropertyDescription prop in m_properties)
			{
				props.Add(prop.DataType.Property);
			}
			return props;
		}

		public void ShowEditForm()
		{
			throw new NotImplementedException();
		}

		public void WriteStream(Stream s)
		{
			MemoryStream ms = new MemoryStream();
			StreamWrapper sw = new StreamWrapper(s);
			foreach (PropertyDescription property in m_properties)
			{
				ms.SetLength(0);
				property.DataType.WriteStream(ms);
				sw.WriteInt((int)ms.Length);
				ms.WriteTo(s);
			}
		}

		public void ReadStream(Stream s)
		{
			MemoryStream ms;
			StreamWrapper sw = new StreamWrapper(s);
			foreach (PropertyDescription property in m_properties)
			{
				try
				{
					int length = sw.ReadInt();
					if (length > 0)
					{
						byte[] data = new byte[length];
						s.Read(data, 0, length);
						ms = new MemoryStream(data);

						if (property.DataType is UnknownDataType)
						{
							(property.DataType as UnknownDataType).SetData(data);
						}
						else
						{
							property.DataType.ReadStream(ms);
						}
					}
				}
				catch (ArgumentException)
				{
					//No more data left in the stream we have
					break;
				}
			}
		}

		public void Rebuild(TypeCollection AvailableTypes)
		{
			m_typeName = m_definition.TypeName;
			List<PropertyDescription> oldProps = m_properties;
			m_properties = new List<PropertyDescription>();


			foreach (PropID propdef in m_definition.Properties)
			{
				bool alreadyExists = false;
				foreach (PropertyDescription property in oldProps)
				{
					if (property.DataType.ID == propdef.ID)
					{
						alreadyExists = true;
						property.DataType.Name = propdef.Name;
						m_properties.Add(property);
						break;
					}
				}

				//--Find type and create instance
				if (!alreadyExists)
				{
					IDataType instance = null;
					foreach (ITypeContainer typeContainer in AvailableTypes)
					{
						if (typeContainer.GUID == propdef.GUID)
							instance = typeContainer.CreateInstance();
					}
					if (instance == null)
					{
						instance = new UnknownDataType(propdef.GUID);
					}
				
					instance.ID = propdef.ID;
					instance.Name = propdef.Name;
					m_properties.Add(instance.Property);
				}
				//--
			}

            //--Rebuild any children
            foreach(PropertyDescription prop in m_properties)
            {
                CustomDataType data = prop.DataType as CustomDataType;
                if(data != null)
                    data.Rebuild(AvailableTypes);
            }
            //--
		}

		private CustomDataTypeDefinition m_definition;
		private PropertyDescription m_propDesc;
		private EditablePropertyDescription m_internalPropDesc;
		private string m_typeName;
		private List<PropertyDescription> m_properties;
	}

	class UnknownDataType : IDataType
	{
		public UnknownDataType(Guid GUID)
		{
			m_guid = GUID;
			m_name = "Unknown";
			m_data = new byte[0];

			EditablePropertyDescription propDesc = new EditablePropertyDescription(this);
			propDesc.Name = TypeName;
			propDesc.TextEdit = false;
			propDesc.EditDialog = false;
			m_propDesc = new PropertyDescription(propDesc);
		}

		public void SetData(byte[] data)
		{
			m_data = (byte[])data.Clone();
		}

		public string TypeName
		{
			get { return "UnknownDataType"; }
		}

		public Guid TypeGUID
		{
			get { return m_guid; }
		}

		public string Name
		{
			get { return m_name; }
			set { m_name = value; }
		}

		public string StringValue
		{
			get { return "[Unknown]"; }
		}

		public bool SetValue(string val)
		{
			return false;
		}

		public PropertyDescription Property
		{
			get { return m_propDesc; }
		}

		public int ID
		{
			get;
			set;
		}

		public IEnumerable<PropertyDescription> GetSubProperties()
		{
			List<PropertyDescription> props = new List<PropertyDescription>();
			return props;
		}

		public void ShowEditForm()
		{
			throw new NotImplementedException();
		}

		public void WriteStream(Stream s)
		{
			s.Write(m_data, 0, m_data.Length);
		}

		public void ReadStream(Stream s)
		{
			
		}

		private Guid m_guid;
		private string m_name;
		private byte[] m_data;
		private PropertyDescription m_propDesc;
	}
}
