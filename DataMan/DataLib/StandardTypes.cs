﻿//=============================================================//
//                                                             //
//                  DataLib::StandardTypes.cs                  //
//   -------------------------------------------------------   //
//  Copyright © 2011 Anthony Mushrow, SKGenius Development     //
//  All rights reserved                                        //
//                                                             //
//  This code is licensed under The Code Project Open License  //
//  (CPOL) See http://www.codeproject.com/info/cpol10.aspx or  //
//  LICENSE.TXT for more information                           //
//                                                             //
//=============================================================//
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Text;
using System.IO;
using System.Runtime.InteropServices;
using System.Windows.Forms;
using DataLibInterfaces;

namespace DataLib
{
	public static class StandardTypes
	{
		static StandardTypes()
		{
			Type[] types = new Type[13];
			types[0] = typeof(Boolean);
			types[1] = typeof(Unsigned8);
			types[2] = typeof(Signed8);
			types[3] = typeof(Unsigned16);
			types[4] = typeof(Signed16);
			types[5] = typeof(Unsigned32);
			types[6] = typeof(Signed32);
			types[7] = typeof(Unsigned64);
			types[8] = typeof(Signed64);
			types[9] = typeof(SinglePrecision);
			types[10] = typeof(DoublePrecision);
			types[11] = typeof(Text);
			types[12] = typeof(Sprite);

			m_stdTypes = new ReadOnlyCollection<Type>(types);
		}

		public static ReadOnlyCollection<Type> TypeCollection
		{
			get { return m_stdTypes; }
		}
		private static ReadOnlyCollection<Type> m_stdTypes;
	}

	[Guid("E9B9875C-FA7D-4224-A30F-7F4DB77E9A59")]
	[DataTypeAttribute("Boolean", "Boolean value")]
	public class Boolean : IDataType
	{
		public Boolean()
		{
			m_internalPropDesc = new EditablePropertyDescription(this);
			m_internalPropDesc.Name = TypeName;
			m_internalPropDesc.TextEdit = true;
			m_internalPropDesc.EditDialog = false;

			m_propDesc = new PropertyDescription(m_internalPropDesc);
		}

		//--Interface Methods
		public string TypeName
		{
			get { return "Boolean"; }
		}

		public Guid TypeGUID
		{
			get { return this.GetType().GUID; }
		}

		public string Name
		{
			get { return m_internalPropDesc.Name; }
			set { m_internalPropDesc.Name = value; }
		}

		public string StringValue
		{
			get
			{
				if (m_val)
					return "True";
				else
					return "False";
			}
		}

		public bool SetValue(string val)
		{
			bool result = true;

			if (String.Compare(val, "true", true) == 0)
				m_val = true;
			else if (String.Compare(val, "false", true) == 0)
				m_val = false;
			else
				result = false;

			return result;
		}

		public PropertyDescription Property
		{
			get { return m_propDesc; }
		}

		public int ID
		{
			get;
			set;
		}

		public IEnumerable<PropertyDescription> GetSubProperties()
		{
			return new PropertyDescription[0];
		}

		public void ShowEditForm()
		{
			throw new NotImplementedException();
		}

		public void WriteStream(Stream s)
		{
			byte val = (byte)(m_val ? 1 : 0);
			s.WriteByte(val);
		}

		public void ReadStream(Stream s)
		{
			int val = s.ReadByte();
			m_val = (val != 0);
		}
		//--

		public bool Value
		{
			get { return m_val; }
			set { m_val = value; }
		}

		private bool m_val = false;
		private PropertyDescription m_propDesc;
		private EditablePropertyDescription m_internalPropDesc;
	}

	[Guid("5FBF54F9-2960-4B04-91D5-008432151111")]
	[DataTypeAttribute("Byte", "8-bit unsigned integer")]
	public class Unsigned8 : IDataType
	{
		public Unsigned8()
		{
			m_internalPropDesc = new EditablePropertyDescription(this);
			m_internalPropDesc.Name = TypeName;
			m_internalPropDesc.TextEdit = true;
			m_internalPropDesc.EditDialog = false;

			m_propDesc = new PropertyDescription(m_internalPropDesc);
		}

		//--Interface Methods
		public string TypeName
		{
			get { return "Byte"; }
		}

		public Guid TypeGUID
		{
			get { return this.GetType().GUID; }
		}

		public string Name
		{
			get { return m_internalPropDesc.Name; }
			set { m_internalPropDesc.Name = value; }
		}

		public string StringValue
		{
			get
			{
				string text;
				if (m_representHex)
					text = "0x" + m_val.ToString("X2");
				else
					text = m_val.ToString();

				return text;
			}
		}

		public bool SetValue(string val)
		{
			bool result = false;
			Byte newVal;

			if (val.StartsWith("0x", true, System.Globalization.CultureInfo.InvariantCulture))
			{
				val = val.Remove(0, 2);
				result = Byte.TryParse(val, System.Globalization.NumberStyles.HexNumber, System.Globalization.CultureInfo.InvariantCulture, out newVal);
				if (result)
				{
					m_representHex = true;
					m_val = newVal;
				}
			}
			else
			{
				result = Byte.TryParse(val, out newVal);
				if (result)
				{
					m_representHex = false;
					m_val = newVal;
				}
			}

			return result;
		}

		public PropertyDescription Property
		{
			get { return m_propDesc; }
		}

		public int ID
		{
			get;
			set;
		}

		public IEnumerable<PropertyDescription> GetSubProperties()
		{
			return new PropertyDescription[0];
		}

		public void ShowEditForm()
		{
			throw new NotImplementedException();
		}

		public void WriteStream(Stream s)
		{
			s.WriteByte(m_val);
			byte val = (byte)(m_representHex ? 1 : 0);
			s.WriteByte(val);
		}

		public void ReadStream(Stream s)
		{
			m_val = (Byte)s.ReadByte();
			int val = s.ReadByte();
			m_representHex = val == 0 ? false : true;
		}
		//--

		public Byte Value
		{
			get { return m_val; }
			set { m_val = value; }
		}

		private bool m_representHex = false;
		private Byte m_val = 0;
		private PropertyDescription m_propDesc;
		private EditablePropertyDescription m_internalPropDesc;
	}

	[Guid("3CAD0EB0-86E8-4484-B20A-1444A4B6A978")]
	[DataTypeAttribute("SByte", "8-bit signed integer")]
	public class Signed8 : IDataType
	{
		public Signed8()
		{
			m_internalPropDesc = new EditablePropertyDescription(this);
			m_internalPropDesc.Name = TypeName;
			m_internalPropDesc.TextEdit = true;
			m_internalPropDesc.EditDialog = false;

			m_propDesc = new PropertyDescription(m_internalPropDesc);
		}

		//--Interface Methods
		public string TypeName
		{
			get { return "SByte"; }
		}

		public Guid TypeGUID
		{
			get { return this.GetType().GUID; }
		}

		public string Name
		{
			get { return m_internalPropDesc.Name; }
			set { m_internalPropDesc.Name = value; }
		}

		public string StringValue
		{
			get
			{
				string text;
				if (m_representHex)
					text = "0x" + m_val.ToString("X2");
				else
					text = m_val.ToString();

				return text;
			}
		}

		public bool SetValue(string val)
		{
			bool result = false;
			SByte newVal;

			if (val.StartsWith("0x", true, System.Globalization.CultureInfo.InvariantCulture))
			{
				val = val.Remove(0, 2);
				result = SByte.TryParse(val, System.Globalization.NumberStyles.HexNumber, System.Globalization.CultureInfo.InvariantCulture, out newVal);
				if (result)
				{
					m_representHex = true;
					m_val = newVal;
				}
			}
			else
			{
				result = SByte.TryParse(val, out newVal);
				if (result)
				{
					m_representHex = false;
					m_val = newVal;
				}
			}

			return result;
		}

		public PropertyDescription Property
		{
			get { return m_propDesc; }
		}

		public int ID
		{
			get;
			set;
		}

		public IEnumerable<PropertyDescription> GetSubProperties()
		{
			return new PropertyDescription[0];
		}

		public void ShowEditForm()
		{
			throw new NotImplementedException();
		}

		public void WriteStream(Stream s)
		{
			s.WriteByte((Byte)m_val);
			byte val = (byte)(m_representHex ? 1 : 0);
			s.WriteByte(val);
		}

		public void ReadStream(Stream s)
		{
			m_val = (SByte)s.ReadByte();
			int val = s.ReadByte();
			m_representHex = val == 0 ? false : true;
		}
		//--

		public SByte Value
		{
			get { return m_val; }
			set { m_val = value; }
		}

		private bool m_representHex = false;
		private SByte m_val = 0;
		private PropertyDescription m_propDesc;
		private EditablePropertyDescription m_internalPropDesc;
	}

	[Guid("5293574C-8AA6-46E0-952A-30DDAE8AC168")]
	[DataTypeAttribute("UInt16", "16-bit unsigned integer")]
	public class Unsigned16 : IDataType
	{
		public Unsigned16()
		{
			m_internalPropDesc = new EditablePropertyDescription(this);
			m_internalPropDesc.Name = TypeName;
			m_internalPropDesc.TextEdit = true;
			m_internalPropDesc.EditDialog = false;

			m_propDesc = new PropertyDescription(m_internalPropDesc);
		}

		//--Interface Methods
		public string TypeName
		{
			get { return "UInt16"; }
		}

		public Guid TypeGUID
		{
			get { return this.GetType().GUID; }
		}

		public string Name
		{
			get { return m_internalPropDesc.Name; }
			set { m_internalPropDesc.Name = value; }
		}

		public string StringValue
		{
			get
			{
				string text;
				if (m_representHex)
					text = "0x" + m_val.ToString("X4");
				else
					text = m_val.ToString();

				return text;
			}
		}

		public bool SetValue(string val)
		{
			bool result = false;
			UInt16 newVal;

			if (val.StartsWith("0x", true, System.Globalization.CultureInfo.InvariantCulture))
			{
				val = val.Remove(0, 2);
				result = UInt16.TryParse(val, System.Globalization.NumberStyles.HexNumber, System.Globalization.CultureInfo.InvariantCulture, out newVal);
				if (result)
				{
					m_representHex = true;
					m_val = newVal;
				}
			}
			else
			{
				result = UInt16.TryParse(val, out newVal);
				if (result)
				{
					m_representHex = false;
					m_val = newVal;
				}
			}

			return result;
		}

		public PropertyDescription Property
		{
			get { return m_propDesc; }
		}

		public int ID
		{
			get;
			set;
		}

		public IEnumerable<PropertyDescription> GetSubProperties()
		{
			return new PropertyDescription[0];
		}

		public void ShowEditForm()
		{
			throw new NotImplementedException();
		}

		public void WriteStream(Stream s)
		{
			byte[] data = BitConverter.GetBytes(m_val);
			s.Write(data, 0, data.Length);
			byte val = (byte)(m_representHex ? 1 : 0);
			s.WriteByte(val);
		}

		public void ReadStream(Stream s)
		{
			Byte[] data = new Byte[2];
			s.Read(data, 0, 2);
			m_val = BitConverter.ToUInt16(data, 0);
			int val = s.ReadByte();
			m_representHex = val == 0 ? false : true;
		}
		//--

		public UInt16 Value
		{
			get { return m_val; }
			set { m_val = value; }
		}

		private bool m_representHex = false;
		private UInt16 m_val = 0;
		private PropertyDescription m_propDesc;
		private EditablePropertyDescription m_internalPropDesc;
	}

	[Guid("0874170E-6BF6-4125-8F04-A1587261FDD1")]
	[DataTypeAttribute("Int16", "16-bit signed integer")]
	public class Signed16 : IDataType
	{
		public Signed16()
		{
			m_internalPropDesc = new EditablePropertyDescription(this);
			m_internalPropDesc.Name = TypeName;
			m_internalPropDesc.TextEdit = true;
			m_internalPropDesc.EditDialog = false;

			m_propDesc = new PropertyDescription(m_internalPropDesc);
		}

		//--Interface Methods
		public string TypeName
		{
			get { return "Int16"; }
		}

		public Guid TypeGUID
		{
			get { return this.GetType().GUID; }
		}

		public string Name
		{
			get { return m_internalPropDesc.Name; }
			set { m_internalPropDesc.Name = value; }
		}

		public string StringValue
		{
			get
			{
				string text;
				if (m_representHex)
					text = "0x" + m_val.ToString("X4");
				else
					text = m_val.ToString();

				return text;
			}
		}

		public bool SetValue(string val)
		{
			bool result = false;
			Int16 newVal;

			if (val.StartsWith("0x", true, System.Globalization.CultureInfo.InvariantCulture))
			{
				val = val.Remove(0, 2);
				result = Int16.TryParse(val, System.Globalization.NumberStyles.HexNumber, System.Globalization.CultureInfo.InvariantCulture, out newVal);
				if (result)
				{
					m_representHex = true;
					m_val = newVal;
				}
			}
			else
			{
				result = Int16.TryParse(val, out newVal);
				if (result)
				{
					m_representHex = false;
					m_val = newVal;
				}
			}

			return result;
		}

		public PropertyDescription Property
		{
			get { return m_propDesc; }
		}

		public int ID
		{
			get;
			set;
		}

		public IEnumerable<PropertyDescription> GetSubProperties()
		{
			return new PropertyDescription[0];
		}

		public void ShowEditForm()
		{
			throw new NotImplementedException();
		}

		public void WriteStream(Stream s)
		{
			byte[] data = BitConverter.GetBytes(m_val);
			s.Write(data, 0, data.Length);
			byte val = (byte)(m_representHex ? 1 : 0);
			s.WriteByte(val);
		}

		public void ReadStream(Stream s)
		{
			Byte[] data = new Byte[2];
			s.Read(data, 0, 2);
			m_val = BitConverter.ToInt16(data, 0);
			int val = s.ReadByte();
			m_representHex = val == 0 ? false : true;
		}
		//--

		public Int16 Value
		{
			get { return m_val; }
			set { m_val = value; }
		}

		private bool m_representHex = false;
		private Int16 m_val = 0;
		private PropertyDescription m_propDesc;
		private EditablePropertyDescription m_internalPropDesc;
	}

	[Guid("242DA566-B7BA-4624-8CC2-9D019E94796F")]
	[DataTypeAttribute("UInt32", "32-bit unsigned integer")]
	public class Unsigned32 : IDataType
	{
		public Unsigned32()
		{
			m_internalPropDesc = new EditablePropertyDescription(this);
			m_internalPropDesc.Name = TypeName;
			m_internalPropDesc.TextEdit = true;
			m_internalPropDesc.EditDialog = false;

			m_propDesc = new PropertyDescription(m_internalPropDesc);
		}

		//--Interface Methods
		public string TypeName
		{
			get { return "UInt32"; }
		}

		public Guid TypeGUID
		{
			get { return this.GetType().GUID; }
		}

		public string Name
		{
			get { return m_internalPropDesc.Name; }
			set { m_internalPropDesc.Name = value; }
		}

		public string StringValue
		{
			get
			{
				string text;
				if (m_representHex)
					text = "0x" + m_val.ToString("X8");
				else
					text = m_val.ToString();

				return text;
			}
		}

		public bool SetValue(string val)
		{
			bool result = false;
			UInt32 newVal;

			if (val.StartsWith("0x", true, System.Globalization.CultureInfo.InvariantCulture))
			{
				val = val.Remove(0, 2);
				result = UInt32.TryParse(val, System.Globalization.NumberStyles.HexNumber, System.Globalization.CultureInfo.InvariantCulture, out newVal);
				if (result)
				{
					m_representHex = true;
					m_val = newVal;
				}
			}
			else
			{
				result = UInt32.TryParse(val, out newVal);
				if (result)
				{
					m_representHex = false;
					m_val = newVal;
				}
			}

			return result;
		}

		public PropertyDescription Property
		{
			get { return m_propDesc; }
		}
		
		public int ID
		{
			get;
			set;
		}

		public IEnumerable<PropertyDescription> GetSubProperties()
		{
			return new PropertyDescription[0];
		}

		public void ShowEditForm()
		{
			throw new NotImplementedException();
		}

		public void WriteStream(Stream s)
		{
			byte[] data = BitConverter.GetBytes(m_val);
			s.Write(data, 0, data.Length);
			byte val = (byte)(m_representHex ? 1 : 0);
			s.WriteByte(val);
		}

		public void ReadStream(Stream s)
		{
			Byte[] data = new Byte[4];
			s.Read(data, 0, 4);
			m_val = BitConverter.ToUInt32(data, 0);
			int val = s.ReadByte();
			m_representHex = val == 0 ? false : true;
		}
		//--

		public UInt32 Value
		{
			get { return m_val; }
			set { m_val = value; }
		}

		private bool m_representHex = false;
		private UInt32 m_val = 0;
		private PropertyDescription m_propDesc;
		private EditablePropertyDescription m_internalPropDesc;
	}

	[Guid("DDDA40EE-6554-45BD-9DDF-CD191CC10EAE")]
	[DataTypeAttribute("Int32", "32-bit signed integer")]
	public class Signed32 : IDataType
	{
		public Signed32()
		{
			m_internalPropDesc = new EditablePropertyDescription(this);
			m_internalPropDesc.Name = TypeName;
			m_internalPropDesc.TextEdit = true;
			m_internalPropDesc.EditDialog = false;

			m_propDesc = new PropertyDescription(m_internalPropDesc);
		}

		//--Interface Methods
		public string TypeName
		{
			get { return "Int32"; }
		}

		public Guid TypeGUID
		{
			get { return this.GetType().GUID; }
		}

		public string Name
		{
			get { return m_internalPropDesc.Name; }
			set { m_internalPropDesc.Name = value; }
		}

		public string StringValue
		{
			get
			{
				string text;
				if (m_representHex)
					text = "0x" + m_val.ToString("X8");
				else
					text = m_val.ToString();

				return text;
			}
		}

		public bool SetValue(string val)
		{
			bool result = false;
			Int32 newVal;

			if (val.StartsWith("0x", true, System.Globalization.CultureInfo.InvariantCulture))
			{
				val = val.Remove(0, 2);
				result = Int32.TryParse(val, System.Globalization.NumberStyles.HexNumber, System.Globalization.CultureInfo.InvariantCulture, out newVal);
				if (result)
				{
					m_representHex = true;
					m_val = newVal;
				}
			}
			else
			{
				result = Int32.TryParse(val, out newVal);
				if (result)
				{
					m_representHex = false;
					m_val = newVal;
				}
			}

			return result;
		}

		public PropertyDescription Property
		{
			get { return m_propDesc; }
		}

		public int ID
		{
			get;
			set;
		}

		public IEnumerable<PropertyDescription> GetSubProperties()
		{
			return new PropertyDescription[0];
		}

		public void ShowEditForm()
		{
			throw new NotImplementedException();
		}

		public void WriteStream(Stream s)
		{
			byte[] data = BitConverter.GetBytes(m_val);
			s.Write(data, 0, data.Length);
			byte val = (byte)(m_representHex ? 1 : 0);
			s.WriteByte(val);
		}

		public void ReadStream(Stream s)
		{
			Byte[] data = new Byte[4];
			s.Read(data, 0, 4);
			m_val = BitConverter.ToInt32(data, 0);
			int val = s.ReadByte();
			m_representHex = val == 0 ? false : true;
		}
		//--

		public Int32 Value
		{
			get { return m_val; }
			set { m_val = value; }
		}

		private bool m_representHex = false;
		private Int32 m_val = 0;
		private PropertyDescription m_propDesc;
		private EditablePropertyDescription m_internalPropDesc;
	}

	[Guid("65042023-B366-4AC6-A959-FE03F0AD0C9E")]
	[DataTypeAttribute("UInt64", "64-bit unsigned integer")]
	public class Unsigned64 : IDataType
	{
		public Unsigned64()
		{
			m_internalPropDesc = new EditablePropertyDescription(this);
			m_internalPropDesc.Name = TypeName;
			m_internalPropDesc.TextEdit = true;
			m_internalPropDesc.EditDialog = false;

			m_propDesc = new PropertyDescription(m_internalPropDesc);
		}

		//--Interface Methods
		public string TypeName
		{
			get { return "UInt64"; }
		}

		public Guid TypeGUID
		{
			get { return this.GetType().GUID; }
		}

		public string Name
		{
			get { return m_internalPropDesc.Name; }
			set { m_internalPropDesc.Name = value; }
		}

		public string StringValue
		{
			get
			{
				string text;
				if (m_representHex)
					text = "0x" + m_val.ToString("X16");
				else
					text = m_val.ToString();

				return text;
			}
		}

		public bool SetValue(string val)
		{
			bool result = false;
			UInt64 newVal;

			if (val.StartsWith("0x", true, System.Globalization.CultureInfo.InvariantCulture))
			{
				val = val.Remove(0, 2);
				result = UInt64.TryParse(val, System.Globalization.NumberStyles.HexNumber, System.Globalization.CultureInfo.InvariantCulture, out newVal);
				if (result)
				{
					m_representHex = true;
					m_val = newVal;
				}
			}
			else
			{
				result = UInt64.TryParse(val, out newVal);
				if (result)
				{
					m_representHex = false;
					m_val = newVal;
				}
			}

			return result;
		}

		public PropertyDescription Property
		{
			get { return m_propDesc; }
		}

		public int ID
		{
			get;
			set;
		}

		public IEnumerable<PropertyDescription> GetSubProperties()
		{
			return new PropertyDescription[0];
		}

		public void ShowEditForm()
		{
			throw new NotImplementedException();
		}

		public void WriteStream(Stream s)
		{
			byte[] data = BitConverter.GetBytes(m_val);
			s.Write(data, 0, data.Length);
			byte val = (byte)(m_representHex ? 1 : 0);
			s.WriteByte(val);
		}

		public void ReadStream(Stream s)
		{
			Byte[] data = new Byte[8];
			s.Read(data, 0, 8);
			m_val = BitConverter.ToUInt64(data, 0);
			int val = s.ReadByte();
			m_representHex = val == 0 ? false : true;
		}
		//--

		public UInt64 Value
		{
			get { return m_val; }
			set { m_val = value; }
		}

		private bool m_representHex = false;
		private UInt64 m_val = 0;
		private PropertyDescription m_propDesc;
		private EditablePropertyDescription m_internalPropDesc;
	}

	[Guid("47B8BA1C-D424-4D53-9786-99FF886BFAA0")]
	[DataTypeAttribute("Int64", "64-bit signed integer")]
	public class Signed64 : IDataType
	{
		public Signed64()
		{
			m_internalPropDesc = new EditablePropertyDescription(this);
			m_internalPropDesc.Name = TypeName;
			m_internalPropDesc.TextEdit = true;
			m_internalPropDesc.EditDialog = false;

			m_propDesc = new PropertyDescription(m_internalPropDesc);
		}

		//--Interface Methods
		public string TypeName
		{
			get { return "Int64"; }
		}

		public Guid TypeGUID
		{
			get { return this.GetType().GUID; }
		}

		public string Name
		{
			get { return m_internalPropDesc.Name; }
			set { m_internalPropDesc.Name = value; }
		}

		public string StringValue
		{
			get
			{
				string text;
				if (m_representHex)
					text = "0x" + m_val.ToString("X16");
				else
					text = m_val.ToString();

				return text;
			}
		}

		public bool SetValue(string val)
		{
			bool result = false;
			Int64 newVal;

			if (val.StartsWith("0x", true, System.Globalization.CultureInfo.InvariantCulture))
			{
				val = val.Remove(0, 2);
				result = Int64.TryParse(val, System.Globalization.NumberStyles.HexNumber, System.Globalization.CultureInfo.InvariantCulture, out newVal);
				if (result)
				{
					m_representHex = true;
					m_val = newVal;
				}
			}
			else
			{
				result = Int64.TryParse(val, out newVal);
				if (result)
				{
					m_representHex = false;
					m_val = newVal;
				}
			}

			return result;
		}

		public PropertyDescription Property
		{
			get { return m_propDesc; }
		}

		public int ID
		{
			get;
			set;
		}

		public IEnumerable<PropertyDescription> GetSubProperties()
		{
			return new PropertyDescription[0];
		}

		public void ShowEditForm()
		{
			throw new NotImplementedException();
		}

		public void WriteStream(Stream s)
		{
			byte[] data = BitConverter.GetBytes(m_val);
			s.Write(data, 0, data.Length);
			byte val = (byte)(m_representHex ? 1 : 0);
			s.WriteByte(val);
		}

		public void ReadStream(Stream s)
		{
			Byte[] data = new Byte[8];
			s.Read(data, 0, 8);
			m_val = BitConverter.ToInt64(data, 0);
			int val = s.ReadByte();
			m_representHex = val == 0 ? false : true;
		}
		//--

		public Int64 Value
		{
			get { return m_val; }
			set { m_val = value; }
		}

		private bool m_representHex = false;
		private Int64 m_val = 0;
		private PropertyDescription m_propDesc;
		private EditablePropertyDescription m_internalPropDesc;
	}

	[Guid("F4D8AE27-C914-4EC3-8D25-F341AC24D5B0")]
	[DataTypeAttribute("Float", "Single-precision floating-point number")]
	public class SinglePrecision : IDataType
	{
		public SinglePrecision()
		{
			m_internalPropDesc = new EditablePropertyDescription(this);
			m_internalPropDesc.Name = TypeName;
			m_internalPropDesc.TextEdit = true;
			m_internalPropDesc.EditDialog = false;

			m_propDesc = new PropertyDescription(m_internalPropDesc);
		}

		//--Interface Methods
		public string TypeName
		{
			get { return "Float"; }
		}

		public Guid TypeGUID
		{
			get { return this.GetType().GUID; }
		}

		public string Name
		{
			get { return m_internalPropDesc.Name; }
			set { m_internalPropDesc.Name = value; }
		}

		public string StringValue
		{
			get { return m_val.ToString(); }
		}

		public bool SetValue(string val)
		{
			Single newVal;
			if (Single.TryParse(val, out newVal))
			{ 
				m_val = newVal;
				return true;
			}
			else
			{
				return false;
			}
		}

		public PropertyDescription Property
		{
			get { return m_propDesc; }
		}

		public int ID
		{
			get;
			set;
		}

		public IEnumerable<PropertyDescription> GetSubProperties()
		{
			return new PropertyDescription[0];
		}

		public void ShowEditForm()
		{
			throw new NotImplementedException();
		}

		public void WriteStream(Stream s)
		{
			byte[] data = BitConverter.GetBytes(m_val);
			s.Write(data, 0, data.Length);
		}

		public void ReadStream(Stream s)
		{
			Byte[] data = new Byte[4];
			s.Read(data, 0, 4);
			m_val = BitConverter.ToSingle(data, 0);
		}
		//--

		public Single Value
		{
			get { return m_val; }
			set { m_val = value; }
		}

		private Single m_val = 0;
		private PropertyDescription m_propDesc;
		private EditablePropertyDescription m_internalPropDesc;
	}

	[Guid("4CB9D9B9-DC49-40F4-8CF8-0983BA895204")]
	[DataTypeAttribute("Double", "Double-precision floating-point number")]
	public class DoublePrecision : IDataType
	{
		public DoublePrecision()
		{
			m_internalPropDesc = new EditablePropertyDescription(this);
			m_internalPropDesc.Name = TypeName;
			m_internalPropDesc.TextEdit = true;
			m_internalPropDesc.EditDialog = false;

			m_propDesc = new PropertyDescription(m_internalPropDesc);
		}

		//--Interface Methods
		public string TypeName
		{
			get { return "Double"; }
		}

		public Guid TypeGUID
		{
			get { return this.GetType().GUID; }
		}

		public string Name
		{
			get { return m_internalPropDesc.Name; }
			set { m_internalPropDesc.Name = value; }
		}

		public string StringValue
		{
			get { return m_val.ToString(); }
		}

		public bool SetValue(string val)
		{
			Double newVal;
			if (Double.TryParse(val, out newVal))
			{ 
				m_val = newVal;
				return true;
			}
			else
			{
				return false;
			}
		}

		public PropertyDescription Property
		{
			get { return m_propDesc; }
		}

		public int ID
		{
			get;
			set;
		}

		public IEnumerable<PropertyDescription> GetSubProperties()
		{
			return new PropertyDescription[0];
		}

		public void ShowEditForm()
		{
			throw new NotImplementedException();
		}

		public void WriteStream(Stream s)
		{
			byte[] data = BitConverter.GetBytes(m_val);
			s.Write(data, 0, data.Length);
		}

		public void ReadStream(Stream s)
		{
			Byte[] data = new Byte[8];
			s.Read(data, 0, 8);
			m_val = BitConverter.ToDouble(data, 0);
		}
		//--

		public Double Value
		{
			get { return m_val; }
			set { m_val = value; }
		}

		private Double m_val = 0;
		private PropertyDescription m_propDesc;
		private EditablePropertyDescription m_internalPropDesc;
	}

	[Guid("DBEEB451-E406-47F9-8BDE-72BA94038AB7")]
	[DataTypeAttribute("String", "Array of Unicode characters")]
	public class Text : IDataType
	{
		public Text()
		{
			m_internalPropDesc = new EditablePropertyDescription(this);
			m_internalPropDesc.Name = TypeName;
			m_internalPropDesc.TextEdit = true;
			m_internalPropDesc.EditDialog = true;

			m_propDesc = new PropertyDescription(m_internalPropDesc);
		}

		//--Interface Methods
		public string TypeName
		{
			get { return "String"; }
		}

		public Guid TypeGUID
		{
			get { return this.GetType().GUID; }
		}

		public string Name
		{
			get { return m_internalPropDesc.Name; }
			set { m_internalPropDesc.Name = value; }
		}

		public string StringValue
		{
			get { return m_val; }
		}

		public bool SetValue(string val)
		{
			m_val = val;
			return true;
		}

		public PropertyDescription Property
		{
			get { return m_propDesc; }
		}

		public int ID
		{
			get;
			set;
		}

		public IEnumerable<PropertyDescription> GetSubProperties()
		{
			return new PropertyDescription[0];
		}

		public void ShowEditForm()
		{
			TypeEditors.TextEditBox textEdit = new TypeEditors.TextEditBox();
			textEdit.TextData = m_val;
			DialogResult dr = textEdit.ShowDialog();
			if (dr == DialogResult.OK)
			{
				m_val = textEdit.TextData;
			}
		}

		public void WriteStream(Stream s)
		{
			byte[] data = System.Text.UnicodeEncoding.Unicode.GetBytes(m_val);
			byte[] len = BitConverter.GetBytes(data.Length);
			s.Write(len, 0, 4);
			s.Write(data, 0, data.Length);
		}

		public void ReadStream(Stream s)
		{
			Byte[] len = new Byte[4];
			s.Read(len, 0, 4);
			int Length = BitConverter.ToInt32(len, 0);
			Byte[] data = new Byte[Length];
			s.Read(data, 0, Length);

			m_val = System.Text.UnicodeEncoding.Unicode.GetString(data);
		}
		//--

		public String Value
		{
			get { return m_val; }
			set { m_val = value; }
		}

		private String m_val = "";
		private PropertyDescription m_propDesc;
		private EditablePropertyDescription m_internalPropDesc;
	}

	[Guid("CB9EC996-4A46-4EDE-8ACE-6E287D5C3368")]
	[DataTypeAttribute("Sprite", "2D animated sprite")]
	public class Sprite : IDataType
	{
		public Sprite()
		{
			m_data = new SpriteData();
			m_internalPropDesc = new EditablePropertyDescription(this);
			m_internalPropDesc.Name = TypeName;
			m_internalPropDesc.TextEdit = false;
			m_internalPropDesc.EditDialog = true;

			m_propDesc = new PropertyDescription(m_internalPropDesc);
		}

		//--Interface Methods
		public string TypeName
		{
			get { return "Sprite"; }
		}

		public Guid TypeGUID
		{
			get { return this.GetType().GUID; }
		}

		public string Name
		{
			get { return m_internalPropDesc.Name; }
			set { m_internalPropDesc.Name = value; }
		}

		public string StringValue
		{
			get { return "[Sprite]"; }
		}

		public bool SetValue(string val)
		{
			return false;
		}

		public PropertyDescription Property
		{
			get { return m_propDesc; }
		}

		public int ID
		{
			get;
			set;
		}

		public IEnumerable<PropertyDescription> GetSubProperties()
		{
			return new PropertyDescription[0];
		}

		public void ShowEditForm()
		{
			DataLib.TypeEditors.SpriteEditor form = new TypeEditors.SpriteEditor(m_data);
			DialogResult dr = form.ShowDialog();
			if (dr == DialogResult.OK)
				m_data = form.Sprite;
		}

		public void WriteStream(Stream s)
		{
			m_data.WriteToStream(s);
		}

		public void ReadStream(Stream s)
		{
			m_data = SpriteData.ReadFromStream(s);
		}
		//--

		public SpriteData Value
		{
			get { return m_data; }
			set { m_data = value; }
		}

		private DataLib.SpriteData m_data;
		private PropertyDescription m_propDesc;
		private EditablePropertyDescription m_internalPropDesc;
	}
	//--
}
