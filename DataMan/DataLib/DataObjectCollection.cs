﻿//=============================================================//
//                                                             //
//              DataLib::DataObjectCollection.cs               //
//   -------------------------------------------------------   //
//  Copyright © 2011 Anthony Mushrow, SKGenius Development     //
//  All rights reserved                                        //
//                                                             //
//  This code is licensed under The Code Project Open License  //
//  (CPOL) See http://www.codeproject.com/info/cpol10.aspx or  //
//  LICENSE.TXT for more information                           //
//                                                             //
//=============================================================//
using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Text;
using DataLibInterfaces;

namespace DataLib
{
	public class DataObjectCollection : IEnumerable
	{
		private class ObjectGroup
		{
			public ObjectGroup(Guid typeGuid)
			{
				m_typeGuid = typeGuid;
				m_dataObjects = new List<IDataType>();
			}

			public Guid TypeGUID
			{
				get { return m_typeGuid; }
			}

			public List<IDataType> Objects
			{
				get { return m_dataObjects; }
			}

			private Guid m_typeGuid;
			private List<IDataType> m_dataObjects;
		}

		public DataObjectCollection()
		{
			m_dataGroups = new List<ObjectGroup>();
			m_fullObjectList = new List<IDataType>();
		}

		public void Add(IDataType dataObject)
		{
			//--Find or create group
			bool groupExists = false;
			ObjectGroup targetGroup = null;
			foreach (ObjectGroup group in m_dataGroups)
			{
				if (group.TypeGUID == dataObject.TypeGUID)
				{
					groupExists = true;
					targetGroup = group;
					break;
				}
			}

			if (!groupExists)
			{
				targetGroup = new ObjectGroup(dataObject.TypeGUID);
				m_dataGroups.Add(targetGroup);
			}
			//--

			//Add item to group
			targetGroup.Objects.Add(dataObject);
			m_fullObjectList.Add(dataObject);
		}

		public void Clear()
		{
			m_fullObjectList.Clear();
			m_dataGroups.Clear();
		}

		public void Remove(IDataType dataObject)
		{
			//--Find group
			bool groupExists = false;
			ObjectGroup targetGroup = null;
			foreach (ObjectGroup group in m_dataGroups)
			{
				if (group.TypeGUID == dataObject.TypeGUID)
				{
					groupExists = true;
					targetGroup = group;
				}
			}
			//--

			if (groupExists)
			{
				targetGroup.Objects.Remove(dataObject);
				m_fullObjectList.Remove(dataObject);

				if (targetGroup.Objects.Count == 0)
					m_dataGroups.Remove(targetGroup);
			}
		}

		public ReadOnlyCollection<IDataType> this[ITypeContainer typeContainer]
		{
			get
			{
				List<IDataType> collection = null;

				bool groupExists = false;
				foreach (ObjectGroup group in m_dataGroups)
				{
					if (group.TypeGUID == typeContainer.GUID)
					{
						groupExists = true;
						collection = group.Objects;
					}
				}

				if (!groupExists)
				{
					collection = new List<IDataType>();
				}

				return new ReadOnlyCollection<IDataType>(collection);
			}
		}

		public ReadOnlyCollection<IDataType> this[Guid typeGuid]
		{
			get
			{
				List<IDataType> collection = null;

				bool groupExists = false;
				foreach (ObjectGroup group in m_dataGroups)
				{
					if (group.TypeGUID == typeGuid)
					{
						groupExists = true;
						collection = group.Objects;
					}
				}

				if (!groupExists)
				{
					collection = new List<IDataType>();
				}

				return new ReadOnlyCollection<IDataType>(collection);
			}
		}

		public IDataType this[int index]
		{
			get
			{
				return m_fullObjectList[index];
			}
		}

		public ReadOnlyCollection<Guid> Groups
		{
			get
			{
				List<Guid> groups = new List<Guid>();
				foreach (ObjectGroup group in m_dataGroups)
				{
					groups.Add(group.TypeGUID);
				}

				return groups.AsReadOnly();
			}
		}

		public int Count
		{
			get { return m_fullObjectList.Count; }
		}

		public IEnumerator GetEnumerator()
		{
			return m_fullObjectList.GetEnumerator();
		}

		private List<ObjectGroup> m_dataGroups;
		private List<IDataType> m_fullObjectList;
	}
}
