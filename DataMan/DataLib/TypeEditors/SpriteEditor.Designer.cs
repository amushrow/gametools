﻿namespace DataLib.TypeEditors
{
	partial class SpriteEditor
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.components = new System.ComponentModel.Container();
			this.textBox1 = new System.Windows.Forms.TextBox();
			this.browseButton = new System.Windows.Forms.Button();
			this.label1 = new System.Windows.Forms.Label();
			this.numFramesUpDown = new System.Windows.Forms.NumericUpDown();
			this.label2 = new System.Windows.Forms.Label();
			this.label3 = new System.Windows.Forms.Label();
			this.dimensionsXUpDown = new System.Windows.Forms.NumericUpDown();
			this.dimensionsYUpDown = new System.Windows.Forms.NumericUpDown();
			this.label4 = new System.Windows.Forms.Label();
			this.frameIntervalUpDown = new System.Windows.Forms.NumericUpDown();
			this.label5 = new System.Windows.Forms.Label();
			this.prevFramButton = new System.Windows.Forms.Button();
			this.nextFrameButton = new System.Windows.Forms.Button();
			this.groupBox1 = new System.Windows.Forms.GroupBox();
			this.cancelButton = new System.Windows.Forms.Button();
			this.okButton = new System.Windows.Forms.Button();
			this.label9 = new System.Windows.Forms.Label();
			this.nameTextBox = new System.Windows.Forms.TextBox();
			this.groupBox2 = new System.Windows.Forms.GroupBox();
			this.stopButton = new System.Windows.Forms.Button();
			this.playButton = new System.Windows.Forms.Button();
			this.previewFrameLabel = new System.Windows.Forms.Label();
			this.groupBox3 = new System.Windows.Forms.GroupBox();
			this.texturePicBox = new System.Windows.Forms.PictureBox();
			this.label13 = new System.Windows.Forms.Label();
			this.label11 = new System.Windows.Forms.Label();
			this.label8 = new System.Windows.Forms.Label();
			this.label12 = new System.Windows.Forms.Label();
			this.cYUpDown = new System.Windows.Forms.NumericUpDown();
			this.label10 = new System.Windows.Forms.Label();
			this.heighOffsetUpDown = new System.Windows.Forms.NumericUpDown();
			this.cXUpDown = new System.Windows.Forms.NumericUpDown();
			this.label7 = new System.Windows.Forms.Label();
			this.widthOffsetUpDown = new System.Windows.Forms.NumericUpDown();
			this.posYUpDown = new System.Windows.Forms.NumericUpDown();
			this.posXUpDown = new System.Windows.Forms.NumericUpDown();
			this.editIndexUpDown = new System.Windows.Forms.NumericUpDown();
			this.label6 = new System.Windows.Forms.Label();
			this.frameTimer = new System.Windows.Forms.Timer(this.components);
			this.panel1 = new System.Windows.Forms.Panel();
			this.previewPanel = new DataLib.TypeEditors.RenderPanel();
			((System.ComponentModel.ISupportInitialize)(this.numFramesUpDown)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.dimensionsXUpDown)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.dimensionsYUpDown)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.frameIntervalUpDown)).BeginInit();
			this.groupBox1.SuspendLayout();
			this.groupBox2.SuspendLayout();
			this.groupBox3.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.texturePicBox)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.cYUpDown)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.heighOffsetUpDown)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.cXUpDown)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.widthOffsetUpDown)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.posYUpDown)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.posXUpDown)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.editIndexUpDown)).BeginInit();
			this.panel1.SuspendLayout();
			this.SuspendLayout();
			// 
			// textBox1
			// 
			this.textBox1.Location = new System.Drawing.Point(9, 32);
			this.textBox1.Name = "textBox1";
			this.textBox1.ReadOnly = true;
			this.textBox1.Size = new System.Drawing.Size(179, 20);
			this.textBox1.TabIndex = 1;
			// 
			// browseButton
			// 
			this.browseButton.Location = new System.Drawing.Point(194, 30);
			this.browseButton.Name = "browseButton";
			this.browseButton.Size = new System.Drawing.Size(67, 23);
			this.browseButton.TabIndex = 2;
			this.browseButton.Text = "Browse";
			this.browseButton.UseVisualStyleBackColor = true;
			this.browseButton.Click += new System.EventHandler(this.browseButton_Click);
			// 
			// label1
			// 
			this.label1.AutoSize = true;
			this.label1.Location = new System.Drawing.Point(6, 16);
			this.label1.Name = "label1";
			this.label1.Size = new System.Drawing.Size(46, 13);
			this.label1.TabIndex = 3;
			this.label1.Text = "Texture:";
			// 
			// numFramesUpDown
			// 
			this.numFramesUpDown.Location = new System.Drawing.Point(99, 85);
			this.numFramesUpDown.Name = "numFramesUpDown";
			this.numFramesUpDown.Size = new System.Drawing.Size(69, 20);
			this.numFramesUpDown.TabIndex = 4;
			this.numFramesUpDown.ValueChanged += new System.EventHandler(this.numFramesUpDown_ValueChanged);
			// 
			// label2
			// 
			this.label2.AutoSize = true;
			this.label2.Location = new System.Drawing.Point(6, 87);
			this.label2.Name = "label2";
			this.label2.Size = new System.Drawing.Size(72, 13);
			this.label2.TabIndex = 5;
			this.label2.Text = "Num. Frames:";
			// 
			// label3
			// 
			this.label3.AutoSize = true;
			this.label3.Location = new System.Drawing.Point(6, 113);
			this.label3.Name = "label3";
			this.label3.Size = new System.Drawing.Size(64, 13);
			this.label3.TabIndex = 6;
			this.label3.Text = "Dimesnions:";
			// 
			// dimensionsXUpDown
			// 
			this.dimensionsXUpDown.Location = new System.Drawing.Point(99, 111);
			this.dimensionsXUpDown.Maximum = new decimal(new int[] {
			5000,
			0,
			0,
			0});
			this.dimensionsXUpDown.Name = "dimensionsXUpDown";
			this.dimensionsXUpDown.Size = new System.Drawing.Size(69, 20);
			this.dimensionsXUpDown.TabIndex = 7;
			this.dimensionsXUpDown.ValueChanged += new System.EventHandler(this.dimensionsXUpDown_ValueChanged);
			// 
			// dimensionsYUpDown
			// 
			this.dimensionsYUpDown.Location = new System.Drawing.Point(192, 111);
			this.dimensionsYUpDown.Maximum = new decimal(new int[] {
			5000,
			0,
			0,
			0});
			this.dimensionsYUpDown.Name = "dimensionsYUpDown";
			this.dimensionsYUpDown.Size = new System.Drawing.Size(69, 20);
			this.dimensionsYUpDown.TabIndex = 7;
			this.dimensionsYUpDown.ValueChanged += new System.EventHandler(this.dimensionsYUpDown_ValueChanged);
			// 
			// label4
			// 
			this.label4.AutoSize = true;
			this.label4.Location = new System.Drawing.Point(174, 113);
			this.label4.Name = "label4";
			this.label4.Size = new System.Drawing.Size(12, 13);
			this.label4.TabIndex = 8;
			this.label4.Text = "x";
			// 
			// frameIntervalUpDown
			// 
			this.frameIntervalUpDown.DecimalPlaces = 6;
			this.frameIntervalUpDown.Increment = new decimal(new int[] {
			1,
			0,
			0,
			131072});
			this.frameIntervalUpDown.Location = new System.Drawing.Point(99, 137);
			this.frameIntervalUpDown.Maximum = new decimal(new int[] {
			2,
			0,
			0,
			0});
			this.frameIntervalUpDown.Minimum = new decimal(new int[] {
			1,
			0,
			0,
			196608});
			this.frameIntervalUpDown.Name = "frameIntervalUpDown";
			this.frameIntervalUpDown.Size = new System.Drawing.Size(69, 20);
			this.frameIntervalUpDown.TabIndex = 9;
			this.frameIntervalUpDown.Value = new decimal(new int[] {
			16667,
			0,
			0,
			393216});
			this.frameIntervalUpDown.ValueChanged += new System.EventHandler(this.frameIntervalUpDown_ValueChanged);
			// 
			// label5
			// 
			this.label5.AutoSize = true;
			this.label5.Location = new System.Drawing.Point(6, 139);
			this.label5.Name = "label5";
			this.label5.Size = new System.Drawing.Size(77, 13);
			this.label5.TabIndex = 10;
			this.label5.Text = "Frame Interval:";
			// 
			// prevFramButton
			// 
			this.prevFramButton.Location = new System.Drawing.Point(6, 163);
			this.prevFramButton.Name = "prevFramButton";
			this.prevFramButton.Size = new System.Drawing.Size(36, 23);
			this.prevFramButton.TabIndex = 11;
			this.prevFramButton.Text = "<<";
			this.prevFramButton.UseVisualStyleBackColor = true;
			this.prevFramButton.Click += new System.EventHandler(this.prevFrameButton_Click);
			// 
			// nextFrameButton
			// 
			this.nextFrameButton.Location = new System.Drawing.Point(217, 161);
			this.nextFrameButton.Name = "nextFrameButton";
			this.nextFrameButton.Size = new System.Drawing.Size(36, 23);
			this.nextFrameButton.TabIndex = 12;
			this.nextFrameButton.Text = ">>";
			this.nextFrameButton.UseVisualStyleBackColor = true;
			this.nextFrameButton.Click += new System.EventHandler(this.nextFrameButton_Click);
			// 
			// groupBox1
			// 
			this.groupBox1.Controls.Add(this.cancelButton);
			this.groupBox1.Controls.Add(this.okButton);
			this.groupBox1.Controls.Add(this.label9);
			this.groupBox1.Controls.Add(this.nameTextBox);
			this.groupBox1.Controls.Add(this.label1);
			this.groupBox1.Controls.Add(this.textBox1);
			this.groupBox1.Controls.Add(this.browseButton);
			this.groupBox1.Controls.Add(this.label5);
			this.groupBox1.Controls.Add(this.numFramesUpDown);
			this.groupBox1.Controls.Add(this.frameIntervalUpDown);
			this.groupBox1.Controls.Add(this.label2);
			this.groupBox1.Controls.Add(this.label4);
			this.groupBox1.Controls.Add(this.label3);
			this.groupBox1.Controls.Add(this.dimensionsYUpDown);
			this.groupBox1.Controls.Add(this.dimensionsXUpDown);
			this.groupBox1.Location = new System.Drawing.Point(12, 12);
			this.groupBox1.Name = "groupBox1";
			this.groupBox1.Size = new System.Drawing.Size(269, 192);
			this.groupBox1.TabIndex = 13;
			this.groupBox1.TabStop = false;
			this.groupBox1.Text = "Settings";
			// 
			// cancelButton
			// 
			this.cancelButton.DialogResult = System.Windows.Forms.DialogResult.Cancel;
			this.cancelButton.Location = new System.Drawing.Point(84, 163);
			this.cancelButton.Name = "cancelButton";
			this.cancelButton.Size = new System.Drawing.Size(69, 23);
			this.cancelButton.TabIndex = 14;
			this.cancelButton.Text = "Cancel";
			this.cancelButton.UseVisualStyleBackColor = true;
			// 
			// okButton
			// 
			this.okButton.DialogResult = System.Windows.Forms.DialogResult.OK;
			this.okButton.Location = new System.Drawing.Point(9, 163);
			this.okButton.Name = "okButton";
			this.okButton.Size = new System.Drawing.Size(69, 23);
			this.okButton.TabIndex = 13;
			this.okButton.Text = "Sumbit";
			this.okButton.UseVisualStyleBackColor = true;
			// 
			// label9
			// 
			this.label9.AutoSize = true;
			this.label9.Location = new System.Drawing.Point(6, 62);
			this.label9.Name = "label9";
			this.label9.Size = new System.Drawing.Size(38, 13);
			this.label9.TabIndex = 12;
			this.label9.Text = "Name:";
			// 
			// nameTextBox
			// 
			this.nameTextBox.Location = new System.Drawing.Point(99, 59);
			this.nameTextBox.Name = "nameTextBox";
			this.nameTextBox.Size = new System.Drawing.Size(162, 20);
			this.nameTextBox.TabIndex = 11;
			this.nameTextBox.Text = "DefaultName";
			this.nameTextBox.TextChanged += new System.EventHandler(this.nameTextBox_TextChanged);
			// 
			// groupBox2
			// 
			this.groupBox2.Controls.Add(this.stopButton);
			this.groupBox2.Controls.Add(this.playButton);
			this.groupBox2.Controls.Add(this.previewFrameLabel);
			this.groupBox2.Controls.Add(this.previewPanel);
			this.groupBox2.Controls.Add(this.prevFramButton);
			this.groupBox2.Controls.Add(this.nextFrameButton);
			this.groupBox2.Location = new System.Drawing.Point(287, 12);
			this.groupBox2.Name = "groupBox2";
			this.groupBox2.Size = new System.Drawing.Size(259, 192);
			this.groupBox2.TabIndex = 14;
			this.groupBox2.TabStop = false;
			this.groupBox2.Text = "Preview";
			// 
			// stopButton
			// 
			this.stopButton.Font = new System.Drawing.Font("Arial", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.stopButton.Location = new System.Drawing.Point(48, 163);
			this.stopButton.Name = "stopButton";
			this.stopButton.Size = new System.Drawing.Size(27, 23);
			this.stopButton.TabIndex = 14;
			this.stopButton.Text = "■";
			this.stopButton.UseVisualStyleBackColor = true;
			this.stopButton.Click += new System.EventHandler(this.stopButton_Click);
			// 
			// playButton
			// 
			this.playButton.Font = new System.Drawing.Font("Arial", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.playButton.Location = new System.Drawing.Point(184, 160);
			this.playButton.Name = "playButton";
			this.playButton.Size = new System.Drawing.Size(27, 23);
			this.playButton.TabIndex = 14;
			this.playButton.Text = "►";
			this.playButton.UseVisualStyleBackColor = true;
			this.playButton.Click += new System.EventHandler(this.playButton_Click);
			// 
			// previewFrameLabel
			// 
			this.previewFrameLabel.Location = new System.Drawing.Point(81, 161);
			this.previewFrameLabel.Name = "previewFrameLabel";
			this.previewFrameLabel.Size = new System.Drawing.Size(97, 23);
			this.previewFrameLabel.TabIndex = 13;
			this.previewFrameLabel.Text = "0";
			this.previewFrameLabel.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
			// 
			// groupBox3
			// 
			this.groupBox3.Controls.Add(this.panel1);
			this.groupBox3.Controls.Add(this.label13);
			this.groupBox3.Controls.Add(this.label11);
			this.groupBox3.Controls.Add(this.label8);
			this.groupBox3.Controls.Add(this.label12);
			this.groupBox3.Controls.Add(this.cYUpDown);
			this.groupBox3.Controls.Add(this.label10);
			this.groupBox3.Controls.Add(this.heighOffsetUpDown);
			this.groupBox3.Controls.Add(this.cXUpDown);
			this.groupBox3.Controls.Add(this.label7);
			this.groupBox3.Controls.Add(this.widthOffsetUpDown);
			this.groupBox3.Controls.Add(this.posYUpDown);
			this.groupBox3.Controls.Add(this.posXUpDown);
			this.groupBox3.Controls.Add(this.editIndexUpDown);
			this.groupBox3.Controls.Add(this.label6);
			this.groupBox3.Dock = System.Windows.Forms.DockStyle.Fill;
			this.groupBox3.Location = new System.Drawing.Point(12, 210);
			this.groupBox3.Name = "groupBox3";
			this.groupBox3.Padding = new System.Windows.Forms.Padding(110, 3, 3, 3);
			this.groupBox3.Size = new System.Drawing.Size(774, 416);
			this.groupBox3.TabIndex = 15;
			this.groupBox3.TabStop = false;
			this.groupBox3.Text = "Edit";
			// 
			// texturePicBox
			// 
			this.texturePicBox.Location = new System.Drawing.Point(0, 0);
			this.texturePicBox.Name = "texturePicBox";
			this.texturePicBox.Size = new System.Drawing.Size(422, 169);
			this.texturePicBox.TabIndex = 4;
			this.texturePicBox.TabStop = false;
			this.texturePicBox.PreviewKeyDown += new System.Windows.Forms.PreviewKeyDownEventHandler(this.texturePicBox_PreviewKeyDown);
			this.texturePicBox.MouseMove += new System.Windows.Forms.MouseEventHandler(this.texturePicBox_MouseMove);
			this.texturePicBox.MouseDown += new System.Windows.Forms.MouseEventHandler(this.texturePicBox_MouseDown);
			this.texturePicBox.Paint += new System.Windows.Forms.PaintEventHandler(this.texturePicBox_Paint);
			// 
			// label13
			// 
			this.label13.AutoSize = true;
			this.label13.Location = new System.Drawing.Point(19, 167);
			this.label13.Name = "label13";
			this.label13.Size = new System.Drawing.Size(23, 13);
			this.label13.TabIndex = 3;
			this.label13.Text = "cY:";
			// 
			// label11
			// 
			this.label11.AutoSize = true;
			this.label11.Location = new System.Drawing.Point(24, 122);
			this.label11.Name = "label11";
			this.label11.Size = new System.Drawing.Size(18, 13);
			this.label11.TabIndex = 3;
			this.label11.Text = "H:";
			// 
			// label8
			// 
			this.label8.AutoSize = true;
			this.label8.Location = new System.Drawing.Point(25, 75);
			this.label8.Name = "label8";
			this.label8.Size = new System.Drawing.Size(17, 13);
			this.label8.TabIndex = 3;
			this.label8.Text = "Y:";
			// 
			// label12
			// 
			this.label12.AutoSize = true;
			this.label12.Location = new System.Drawing.Point(19, 148);
			this.label12.Name = "label12";
			this.label12.Size = new System.Drawing.Size(23, 13);
			this.label12.TabIndex = 3;
			this.label12.Text = "cX:";
			// 
			// cYUpDown
			// 
			this.cYUpDown.Location = new System.Drawing.Point(48, 167);
			this.cYUpDown.Maximum = new decimal(new int[] {
			10000,
			0,
			0,
			0});
			this.cYUpDown.Minimum = new decimal(new int[] {
			1000,
			0,
			0,
			-2147483648});
			this.cYUpDown.Name = "cYUpDown";
			this.cYUpDown.Size = new System.Drawing.Size(56, 20);
			this.cYUpDown.TabIndex = 1;
			this.cYUpDown.ValueChanged += new System.EventHandler(this.cYUpDown_ValueChanged);
			// 
			// label10
			// 
			this.label10.AutoSize = true;
			this.label10.Location = new System.Drawing.Point(21, 101);
			this.label10.Name = "label10";
			this.label10.Size = new System.Drawing.Size(21, 13);
			this.label10.TabIndex = 3;
			this.label10.Text = "W:";
			// 
			// heighOffsetUpDown
			// 
			this.heighOffsetUpDown.Location = new System.Drawing.Point(48, 120);
			this.heighOffsetUpDown.Maximum = new decimal(new int[] {
			10000,
			0,
			0,
			0});
			this.heighOffsetUpDown.Minimum = new decimal(new int[] {
			1000,
			0,
			0,
			-2147483648});
			this.heighOffsetUpDown.Name = "heighOffsetUpDown";
			this.heighOffsetUpDown.Size = new System.Drawing.Size(56, 20);
			this.heighOffsetUpDown.TabIndex = 1;
			this.heighOffsetUpDown.ValueChanged += new System.EventHandler(this.heightOffsetUpDown_ValueChanged);
			// 
			// cXUpDown
			// 
			this.cXUpDown.Location = new System.Drawing.Point(48, 146);
			this.cXUpDown.Maximum = new decimal(new int[] {
			10000,
			0,
			0,
			0});
			this.cXUpDown.Minimum = new decimal(new int[] {
			1000,
			0,
			0,
			-2147483648});
			this.cXUpDown.Name = "cXUpDown";
			this.cXUpDown.Size = new System.Drawing.Size(56, 20);
			this.cXUpDown.TabIndex = 1;
			this.cXUpDown.ValueChanged += new System.EventHandler(this.cXUpDown_ValueChanged);
			// 
			// label7
			// 
			this.label7.AutoSize = true;
			this.label7.Location = new System.Drawing.Point(25, 54);
			this.label7.Name = "label7";
			this.label7.Size = new System.Drawing.Size(17, 13);
			this.label7.TabIndex = 3;
			this.label7.Text = "X:";
			// 
			// widthOffsetUpDown
			// 
			this.widthOffsetUpDown.Location = new System.Drawing.Point(48, 99);
			this.widthOffsetUpDown.Maximum = new decimal(new int[] {
			10000,
			0,
			0,
			0});
			this.widthOffsetUpDown.Minimum = new decimal(new int[] {
			1000,
			0,
			0,
			-2147483648});
			this.widthOffsetUpDown.Name = "widthOffsetUpDown";
			this.widthOffsetUpDown.Size = new System.Drawing.Size(56, 20);
			this.widthOffsetUpDown.TabIndex = 1;
			this.widthOffsetUpDown.ValueChanged += new System.EventHandler(this.widthOffsetUpDown_ValueChanged);
			// 
			// posYUpDown
			// 
			this.posYUpDown.Location = new System.Drawing.Point(48, 73);
			this.posYUpDown.Maximum = new decimal(new int[] {
			10000,
			0,
			0,
			0});
			this.posYUpDown.Name = "posYUpDown";
			this.posYUpDown.Size = new System.Drawing.Size(56, 20);
			this.posYUpDown.TabIndex = 1;
			this.posYUpDown.ValueChanged += new System.EventHandler(this.posYUpDown_ValueChanged);
			// 
			// posXUpDown
			// 
			this.posXUpDown.Location = new System.Drawing.Point(48, 52);
			this.posXUpDown.Maximum = new decimal(new int[] {
			10000,
			0,
			0,
			0});
			this.posXUpDown.Name = "posXUpDown";
			this.posXUpDown.Size = new System.Drawing.Size(56, 20);
			this.posXUpDown.TabIndex = 1;
			this.posXUpDown.ValueChanged += new System.EventHandler(this.posXUpDown_ValueChanged);
			// 
			// editIndexUpDown
			// 
			this.editIndexUpDown.Location = new System.Drawing.Point(48, 16);
			this.editIndexUpDown.Maximum = new decimal(new int[] {
			1,
			0,
			0,
			-2147483648});
			this.editIndexUpDown.Minimum = new decimal(new int[] {
			1,
			0,
			0,
			-2147483648});
			this.editIndexUpDown.Name = "editIndexUpDown";
			this.editIndexUpDown.Size = new System.Drawing.Size(56, 20);
			this.editIndexUpDown.TabIndex = 1;
			this.editIndexUpDown.Value = new decimal(new int[] {
			1,
			0,
			0,
			-2147483648});
			this.editIndexUpDown.ValueChanged += new System.EventHandler(this.editIndexUpDown_ValueChanged);
			// 
			// label6
			// 
			this.label6.AutoSize = true;
			this.label6.Location = new System.Drawing.Point(6, 18);
			this.label6.Name = "label6";
			this.label6.Size = new System.Drawing.Size(36, 13);
			this.label6.TabIndex = 0;
			this.label6.Text = "Index:";
			// 
			// frameTimer
			// 
			this.frameTimer.Tick += new System.EventHandler(this.frameTimer_Tick);
			// 
			// panel1
			// 
			this.panel1.AutoScroll = true;
			this.panel1.Controls.Add(this.texturePicBox);
			this.panel1.Dock = System.Windows.Forms.DockStyle.Fill;
			this.panel1.Location = new System.Drawing.Point(110, 16);
			this.panel1.Name = "panel1";
			this.panel1.Size = new System.Drawing.Size(661, 397);
			this.panel1.TabIndex = 5;
			// 
			// previewPanel
			// 
			this.previewPanel.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
			this.previewPanel.Location = new System.Drawing.Point(6, 16);
			this.previewPanel.Name = "previewPanel";
			this.previewPanel.Size = new System.Drawing.Size(247, 142);
			this.previewPanel.TabIndex = 0;
			// 
			// SpriteEditor
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(798, 638);
			this.Controls.Add(this.groupBox3);
			this.Controls.Add(this.groupBox2);
			this.Controls.Add(this.groupBox1);
			this.Name = "SpriteEditor";
			this.Padding = new System.Windows.Forms.Padding(12, 210, 12, 12);
			this.Text = "Sprite Editor";
			((System.ComponentModel.ISupportInitialize)(this.numFramesUpDown)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.dimensionsXUpDown)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.dimensionsYUpDown)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.frameIntervalUpDown)).EndInit();
			this.groupBox1.ResumeLayout(false);
			this.groupBox1.PerformLayout();
			this.groupBox2.ResumeLayout(false);
			this.groupBox3.ResumeLayout(false);
			this.groupBox3.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.texturePicBox)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.cYUpDown)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.heighOffsetUpDown)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.cXUpDown)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.widthOffsetUpDown)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.posYUpDown)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.posXUpDown)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.editIndexUpDown)).EndInit();
			this.panel1.ResumeLayout(false);
			this.ResumeLayout(false);

		}

		#endregion

		private DataLib.TypeEditors.RenderPanel previewPanel;
		private System.Windows.Forms.TextBox textBox1;
		private System.Windows.Forms.Button browseButton;
		private System.Windows.Forms.Label label1;
		private System.Windows.Forms.NumericUpDown numFramesUpDown;
		private System.Windows.Forms.Label label2;
		private System.Windows.Forms.Label label3;
		private System.Windows.Forms.NumericUpDown dimensionsXUpDown;
		private System.Windows.Forms.NumericUpDown dimensionsYUpDown;
		private System.Windows.Forms.Label label4;
		private System.Windows.Forms.NumericUpDown frameIntervalUpDown;
		private System.Windows.Forms.Label label5;
		private System.Windows.Forms.Button prevFramButton;
		private System.Windows.Forms.Button nextFrameButton;
		private System.Windows.Forms.GroupBox groupBox1;
		private System.Windows.Forms.GroupBox groupBox2;
		private System.Windows.Forms.GroupBox groupBox3;
		private System.Windows.Forms.Label label8;
		private System.Windows.Forms.Label label7;
		private System.Windows.Forms.NumericUpDown editIndexUpDown;
		private System.Windows.Forms.Label label6;
		private System.Windows.Forms.PictureBox texturePicBox;
		private System.Windows.Forms.NumericUpDown posYUpDown;
		private System.Windows.Forms.NumericUpDown posXUpDown;
		private System.Windows.Forms.Label previewFrameLabel;
		private System.Windows.Forms.Label label9;
		private System.Windows.Forms.TextBox nameTextBox;
		private System.Windows.Forms.Label label11;
		private System.Windows.Forms.Label label10;
		private System.Windows.Forms.NumericUpDown heighOffsetUpDown;
		private System.Windows.Forms.NumericUpDown widthOffsetUpDown;
		private System.Windows.Forms.Button stopButton;
		private System.Windows.Forms.Button playButton;
		private System.Windows.Forms.Timer frameTimer;
		private System.Windows.Forms.Label label13;
		private System.Windows.Forms.Label label12;
		private System.Windows.Forms.NumericUpDown cYUpDown;
		private System.Windows.Forms.NumericUpDown cXUpDown;
		private System.Windows.Forms.Button cancelButton;
		private System.Windows.Forms.Button okButton;
		private System.Windows.Forms.Panel panel1;
	}
}

