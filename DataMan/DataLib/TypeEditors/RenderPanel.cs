﻿//=============================================================//
//                                                             //
//                   DataLib::RenderPanel.cs                   //
//   -------------------------------------------------------   //
//  Copyright © 2011 Anthony Mushrow, SKGenius Development     //
//  All rights reserved                                        //
//                                                             //
//  This code is licensed under The Code Project Open License  //
//  (CPOL) See http://www.codeproject.com/info/cpol10.aspx or  //
//  LICENSE.TXT for more information                           //
//                                                             //
//=============================================================//
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Windows.Forms;

namespace DataLib.TypeEditors
{
	/// <summary>
	/// Basically just an empty control, in which you can use an
	/// external function to do any rendering
	/// </summary>
	public partial class RenderPanel : UserControl, IComponent
	{
		public delegate void Renderer(PaintEventArgs e);

		private Renderer m_renderMethod;

		public RenderPanel()
		{
			InitializeComponent();
		}

		#region Public Functions
		public void SetRenderMethod(Renderer renderer)
		{
			m_renderMethod = renderer;
		}
		#endregion

		#region Events
		protected override void OnPaintBackground(PaintEventArgs e)
		{
			//Nothing
			if (m_renderMethod == null)
				base.OnPaintBackground(e);

		}

		protected override void OnPaint(PaintEventArgs e)
		{
			//Call our custom renderer
			if (m_renderMethod != null)
				m_renderMethod.Invoke(e);
			else
				base.OnPaint(e);
		}
		#endregion
	}
}
