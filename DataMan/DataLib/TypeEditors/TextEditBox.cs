﻿//=============================================================//
//                                                             //
//                   DataLib::TextEditBox.cs                   //
//   -------------------------------------------------------   //
//  Copyright © 2011 Anthony Mushrow, SKGenius Development     //
//  All rights reserved                                        //
//                                                             //
//  This code is licensed under The Code Project Open License  //
//  (CPOL) See http://www.codeproject.com/info/cpol10.aspx or  //
//  LICENSE.TXT for more information                           //
//                                                             //
//=============================================================//
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace DataLib.TypeEditors
{
	public partial class TextEditBox : Form
	{
		public TextEditBox()
		{
			InitializeComponent();
		}

		public string TextData
		{
			get { return this.textBox1.Text; }
			set
			{
				this.textBox1.Text = value;
				this.textBox1.Select(0, 0);
			}
		}
	}
}
