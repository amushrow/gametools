﻿//=============================================================//
//                                                             //
//                  DataLib::SpriteEditor.cs                   //
//   -------------------------------------------------------   //
//  Copyright © 2011 Anthony Mushrow, SKGenius Development     //
//  All rights reserved                                        //
//                                                             //
//  This code is licensed under The Code Project Open License  //
//  (CPOL) See http://www.codeproject.com/info/cpol10.aspx or  //
//  LICENSE.TXT for more information                           //
//                                                             //
//=============================================================//
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.IO;

namespace DataLib.TypeEditors
{
	public partial class SpriteEditor : Form
	{

		private int m_curFrame = -1;		//Frame that we are editing
		private int m_previewFrame = -1;	//Frame that we are previewing
		private SpriteData m_spriteData;

		public SpriteEditor(SpriteData data)
		{
			InitializeComponent();
		
			m_spriteData = data.Clone();

			//Setup fields with data from sprite
			this.textBox1.Text = data.TexName;
			this.nameTextBox.Text = data.Name;
			this.numFramesUpDown.Value = data.NumFrames;
			this.dimensionsXUpDown.Value = (decimal)data.Width;
			this.dimensionsYUpDown.Value = (decimal)data.Height;
			this.frameIntervalUpDown.Value = (decimal)data.FrameInterval;
			if (File.Exists(data.TexPath))
				this.texturePicBox.Image = new Bitmap(data.TexPath);

			this.previewPanel.SetRenderMethod(this.RenderPreview);
		}

		public SpriteEditor()
		{
			InitializeComponent();
			m_spriteData = new SpriteData();
			this.previewPanel.SetRenderMethod(this.RenderPreview);
		}

		public SpriteData Sprite
		{
			get { return m_spriteData;  }
		}

		private void RenderPreview(PaintEventArgs e)
		{
			e.Graphics.Clear(this.BackColor);
			if (m_previewFrame >= 0 && this.texturePicBox.Image != null)
			{
				float spriteWidth = m_spriteData.Width + m_spriteData.FrameCoords[m_previewFrame].Width;
				float spriteHeight = m_spriteData.Height + m_spriteData.FrameCoords[m_previewFrame].Height;

				float x = (float)Math.Floor(this.previewPanel.Width / 2 - m_spriteData.FrameCoords[m_previewFrame].cX);
				float y = (float)Math.Floor(this.previewPanel.Height / 2 - m_spriteData.FrameCoords[m_previewFrame].cY);

				RectangleF srcRect = new RectangleF(m_spriteData.FrameCoords[m_previewFrame].X, m_spriteData.FrameCoords[m_previewFrame].Y, spriteWidth, spriteHeight);
				
				RectangleF dstRect = new RectangleF(x, y, spriteWidth, spriteHeight);
				e.Graphics.DrawImage(this.texturePicBox.Image, dstRect, srcRect, GraphicsUnit.Pixel);

				x = (float)Math.Floor(this.previewPanel.Width / 2 - m_spriteData.Width / 2);
				y = (float)Math.Floor(this.previewPanel.Height / 2 - m_spriteData.Height / 2);
				Pen halfBlack = new Pen(Color.FromArgb(96, 128, 128, 128));
				e.Graphics.DrawRectangle(halfBlack, x, y, m_spriteData.Width, m_spriteData.Height);

				x = (float)Math.Floor(this.previewPanel.Width / 2d);
				y = (float)Math.Floor(this.previewPanel.Height / 2d);
				e.Graphics.DrawLine(halfBlack, x, 0, x, this.previewPanel.Height);
				e.Graphics.DrawLine(halfBlack, 0, y, this.previewPanel.Width, y);
			}
		}

		private void texturePicBox_Paint(object sender, PaintEventArgs e)
		{
			Pen redPen = Pens.Red;

			if (m_curFrame >= 0)
			{
				float spriteWidth = m_spriteData.Width + m_spriteData.FrameCoords[m_curFrame].Width;
				float spriteHeight = m_spriteData.Height + m_spriteData.FrameCoords[m_curFrame].Height;
				e.Graphics.DrawRectangle(redPen, m_spriteData.FrameCoords[m_curFrame].X, m_spriteData.FrameCoords[m_curFrame].Y, spriteWidth, spriteHeight);
			}
		}

		#region Events
		private void browseButton_Click(object sender, EventArgs e)
		{
			OpenFileDialog filediag = new OpenFileDialog();
			if(filediag.ShowDialog() == DialogResult.OK)
			{
				FileInfo fi = new FileInfo(filediag.FileName);
				this.textBox1.Text = fi.Name;
				this.texturePicBox.Image = new Bitmap(fi.FullName);
				this.texturePicBox.Size = this.texturePicBox.Image.Size;

				m_spriteData.TexPath = fi.FullName;
				m_spriteData.TexName = fi.Name;
			}
		}

		private void nextFrameButton_Click(object sender, EventArgs e)
		{
			m_previewFrame++;
			if (m_previewFrame >= m_spriteData.NumFrames)
			{
				if (m_spriteData.NumFrames > 0)
					m_previewFrame = 0;
				else
					m_previewFrame = -1;
			}
			this.previewFrameLabel.Text = m_previewFrame.ToString();
			this.previewPanel.Invalidate();
		}

		private void prevFrameButton_Click(object sender, EventArgs e)
		{
			m_previewFrame--;
			if (m_previewFrame < 0)
			{
				m_previewFrame = m_spriteData.NumFrames - 1;
			}
			this.previewFrameLabel.Text = m_previewFrame.ToString();
			this.previewPanel.Invalidate();
		}

		private void playButton_Click(object sender, EventArgs e)
		{
			this.frameTimer.Enabled = true;
			this.frameTimer.Interval = (int)(m_spriteData.FrameInterval * 1000);
			this.frameTimer.Start();
		}

		private void stopButton_Click(object sender, EventArgs e)
		{
			this.frameTimer.Stop();
		}

		private void numFramesUpDown_ValueChanged(object sender, EventArgs e)
		{
			int numFrames = (int)this.numFramesUpDown.Value;
			m_spriteData.SetNumFrameCoords(numFrames);

			if (numFrames <= m_previewFrame)
			{
				m_previewFrame = numFrames - 1;
				this.previewFrameLabel.Text = m_previewFrame.ToString();
				this.previewPanel.Invalidate();
			}


			this.editIndexUpDown.Maximum = this.numFramesUpDown.Value-1;

			if (numFrames > 0)
				this.editIndexUpDown.Minimum = 0;
			else
				this.editIndexUpDown.Minimum = -1;
		}

		private void dimensionsXUpDown_ValueChanged(object sender, EventArgs e)
		{
			m_spriteData.Width = (float)this.dimensionsXUpDown.Value;
			this.texturePicBox.Invalidate();
			this.previewPanel.Invalidate();
		}

		private void dimensionsYUpDown_ValueChanged(object sender, EventArgs e)
		{
			m_spriteData.Height = (float)this.dimensionsYUpDown.Value;
			this.texturePicBox.Invalidate();
			this.previewPanel.Invalidate();
		}

		private void frameIntervalUpDown_ValueChanged(object sender, EventArgs e)
		{
			m_spriteData.FrameInterval = (float)this.frameIntervalUpDown.Value;
		}

		private void nameTextBox_TextChanged(object sender, EventArgs e)
		{
			m_spriteData.Name = this.nameTextBox.Text;
		}


		private void posXUpDown_ValueChanged(object sender, EventArgs e)
		{
			if (m_curFrame >= 0)
			{
				m_spriteData.FrameCoords[m_curFrame].X = (float)this.posXUpDown.Value;
				this.texturePicBox.Invalidate();
				this.previewPanel.Invalidate();
			}
		}

		private void posYUpDown_ValueChanged(object sender, EventArgs e)
		{
			if (m_curFrame >= 0)
			{
				m_spriteData.FrameCoords[m_curFrame].Y = (float)this.posYUpDown.Value;
				this.texturePicBox.Invalidate();
				this.previewPanel.Invalidate();
			}
		}

		private void editIndexUpDown_ValueChanged(object sender, EventArgs e)
		{
			m_curFrame = (int)this.editIndexUpDown.Value;
			m_previewFrame = m_curFrame;
			if(m_curFrame >= 0)
			{
				this.posXUpDown.Value = (decimal)m_spriteData.FrameCoords[m_curFrame].X;
				this.posYUpDown.Value = (decimal)m_spriteData.FrameCoords[m_curFrame].Y;

				this.widthOffsetUpDown.Value = (decimal)m_spriteData.FrameCoords[m_curFrame].Width;
				this.heighOffsetUpDown.Value = (decimal)m_spriteData.FrameCoords[m_curFrame].Height;

				this.cXUpDown.Value = (decimal)m_spriteData.FrameCoords[m_curFrame].cX;
				this.cYUpDown.Value = (decimal)m_spriteData.FrameCoords[m_curFrame].cY;
			}
		}

		private void widthOffsetUpDown_ValueChanged(object sender, EventArgs e)
		{
			if (m_curFrame >= 0)
			{
				m_spriteData.FrameCoords[m_curFrame].Width = (float)this.widthOffsetUpDown.Value;
				this.texturePicBox.Invalidate();
				this.previewPanel.Invalidate();
			}
		}

		private void heightOffsetUpDown_ValueChanged(object sender, EventArgs e)
		{
			if (m_curFrame >= 0)
			{
				m_spriteData.FrameCoords[m_curFrame].Height = (float)this.heighOffsetUpDown.Value;
				this.texturePicBox.Invalidate();
				this.previewPanel.Invalidate();
			}
		}

		private void cXUpDown_ValueChanged(object sender, EventArgs e)
		{
			if (m_curFrame >= 0)
			{
				m_spriteData.FrameCoords[m_curFrame].cX = (float)this.cXUpDown.Value;
				this.texturePicBox.Invalidate();
				this.previewPanel.Invalidate();
			}
		}

		private void cYUpDown_ValueChanged(object sender, EventArgs e)
		{
			if (m_curFrame >= 0)
			{
				m_spriteData.FrameCoords[m_curFrame].cY = (float)this.cYUpDown.Value;
				this.texturePicBox.Invalidate();
				this.previewPanel.Invalidate();
			}
		}

		private void frameTimer_Tick(object sender, EventArgs e)
		{
			m_previewFrame++;
			if (m_previewFrame >= m_spriteData.NumFrames)
			{
				if (m_spriteData.NumFrames > 0)
					m_previewFrame = 0;
				else
				{
					m_previewFrame = -1;
					this.frameTimer.Stop();
				}
			}
			this.previewFrameLabel.Text = m_previewFrame.ToString();
			this.previewPanel.Invalidate();
		}

		private void texturePicBox_PreviewKeyDown(object sender, PreviewKeyDownEventArgs e)
		{
			if (m_curFrame >= 0 && e.KeyCode == Keys.Left)
			{
				if(this.posXUpDown.Value > this.posXUpDown.Minimum)
					this.posXUpDown.Value--;
				m_spriteData.FrameCoords[m_curFrame].X = (float)this.posXUpDown.Value;
				e.IsInputKey = true;
			}

			else if (m_curFrame >= 0 && e.KeyCode == Keys.Right)
			{
				if (this.posXUpDown.Value < this.posXUpDown.Maximum)
					this.posXUpDown.Value++;
				m_spriteData.FrameCoords[m_curFrame].X = (float)this.posXUpDown.Value;
				e.IsInputKey = true;
			}
			
			else if (m_curFrame >= 0 && e.KeyCode == Keys.Up)
			{
				if (this.posYUpDown.Value > this.posYUpDown.Minimum)
					this.posYUpDown.Value--;
				m_spriteData.FrameCoords[m_curFrame].Y = (float)this.posYUpDown.Value;
				e.IsInputKey = true;
			}

			else if (m_curFrame >= 0 && e.KeyCode == Keys.Down)
			{
				if (this.posYUpDown.Value < this.posYUpDown.Maximum)
					this.posYUpDown.Value++;
				m_spriteData.FrameCoords[m_curFrame].Y = (float)this.posYUpDown.Value;
				e.IsInputKey = true;
			}

			this.texturePicBox.Invalidate();
			this.previewPanel.Invalidate();
		}

		private void texturePicBox_MouseDown(object sender, MouseEventArgs e)
		{
			this.texturePicBox.Focus();
			if (m_curFrame >= 0)
			{
				if (e.Button == MouseButtons.Left)
				{
					this.posXUpDown.Value = e.X;
					this.posYUpDown.Value = e.Y;
				}
				else if (e.Button == MouseButtons.Right)
				{
					this.cXUpDown.Value = e.X - (decimal)m_spriteData.FrameCoords[m_curFrame].X;
					this.cYUpDown.Value = e.Y - (decimal)m_spriteData.FrameCoords[m_curFrame].Y;
				}

			}
		}

		private void texturePicBox_MouseMove(object sender, MouseEventArgs e)
		{
			if (e.Button == MouseButtons.Left && m_curFrame >= 0)
			{
				if (e.X >= this.posXUpDown.Minimum)
					this.posXUpDown.Value = e.X;
				else
					this.posXUpDown.Value = this.posXUpDown.Minimum;

				if (e.Y >= this.posYUpDown.Minimum)
					this.posYUpDown.Value = e.Y;
				else
					this.posYUpDown.Value = this.posYUpDown.Minimum;
			}
		}
		#endregion
	}
}
