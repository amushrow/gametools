﻿//=============================================================//
//                                                             //
//                DataLib::EmbeddedFileStream.cs               //
//   -------------------------------------------------------   //
//  Copyright © 2011 Anthony Mushrow, SKGenius Development     //
//  All rights reserved                                        //
//                                                             //
//  This code is licensed under The Code Project Open License  //
//  (CPOL) See http://www.codeproject.com/info/cpol10.aspx or  //
//  LICENSE.TXT for more information                           //
//                                                             //
//=============================================================//
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;

namespace DataLib
{
	public class EmbeddedFileStream : System.IO.Stream, System.Runtime.InteropServices.ComTypes.IStream
	{
		private System.IO.Stream m_content;
		private long m_pos;
		private long m_length;

		public EmbeddedFileStream(System.IO.Stream content, long length)
		{
			m_content = content;
			m_pos = 0;
			m_length = length;
		}

		public EmbeddedFileStream(System.IO.Stream content, long length, long internalStartPos)
		{
			m_content = content;
			m_pos = 0;
			m_length = length;
		}

		~EmbeddedFileStream()
		{
			m_content.Close();
		}

		public override void Close()
		{
			m_content.Close();
			base.Close();
		}

		public override bool CanRead
		{
			get { return true; }
		}

		public override bool CanSeek
		{
			get { return false; }
		}

		public override bool CanWrite
		{
			get { return false; }
		}

		public override long Length
		{
			get { return m_length; }
		}

		public override long Position
		{
			get
			{
				return m_pos;
			}
			set
			{
				throw new NotImplementedException();
			}
		}

		public override void Flush()
		{
			m_content.Flush();
		}

		public override long Seek(long offset, System.IO.SeekOrigin origin)
		{
			throw new NotImplementedException();
		}

		public override int Read(byte[] buffer, int offset, int count)
		{
			//const int CHUNK = 4096;
			long remain = m_length - m_pos;
			if (count > remain) {
				count = (int)remain;
			}

			/*int totalRead = 0;
			while(count > CHUNK)
			{
				int read = m_content.Read(buffer, offset + totalRead, CHUNK);
				count -= read;
				totalRead += read;
			}
			totalRead += m_content.Read(buffer, offset + totalRead, count);
			m_pos += totalRead;*/

			int read = m_content.Read(buffer, offset, count);
			m_pos += read;

			return read;
		}

		public override void SetLength(long value)
		{
			throw new NotImplementedException();
		}

		public override void Write(byte[] buffer, int offset, int count)
		{
			throw new NotImplementedException();
		}

		public void Clone(out System.Runtime.InteropServices.ComTypes.IStream ppstm)
		{
			throw new NotImplementedException();
		}

		public void Commit(int grfCommitFlags)
		{

		}

		public void CopyTo(System.Runtime.InteropServices.ComTypes.IStream pstm, long cb, IntPtr pcbRead, IntPtr pcbWritten)
		{
			throw new NotImplementedException();
		}

		public void LockRegion(long libOffset, long cb, int dwLockType)
		{
			throw new NotImplementedException();
		}

		public void Read(byte[] pv, int cb, IntPtr pcbRead)
		{
			//const int CHUNK = 4096;
			long remain = m_length - m_pos;
			if (cb > remain) {
				cb = (int)remain;
			}

			/*int totalRead = 0;
			while (cb > CHUNK)
			{
				int read = m_content.Read(pv, totalRead, CHUNK);
				cb -= read;
				totalRead += read;
			}
			totalRead += m_content.Read(pv, totalRead, cb);
			m_pos += totalRead;*/
			int read = m_content.Read(pv, 0, cb);
			m_pos += read;

			UInt32 bytesRead = (UInt32)read;
			System.Runtime.InteropServices.Marshal.StructureToPtr(bytesRead, pcbRead, false);
		}

		public void Revert()
		{
			throw new NotImplementedException();
		}

		public void Seek(long dlibMove, int dwOrigin, IntPtr plibNewPosition)
		{
			
		}

		public void SetSize(long libNewSize)
		{
			throw new NotImplementedException();
		}

		public void Stat(out System.Runtime.InteropServices.ComTypes.STATSTG pstatstg, int grfStatFlag)
		{
			pstatstg = new System.Runtime.InteropServices.ComTypes.STATSTG();
			pstatstg.atime = new System.Runtime.InteropServices.ComTypes.FILETIME();
			pstatstg.cbSize = m_length;
			pstatstg.clsid = Guid.Empty;
			pstatstg.ctime = new System.Runtime.InteropServices.ComTypes.FILETIME();
			pstatstg.grfLocksSupported = 0;
			pstatstg.mtime = new System.Runtime.InteropServices.ComTypes.FILETIME();
			pstatstg.pwcsName = "";
			pstatstg.type = 2;
		}

		public void UnlockRegion(long libOffset, long cb, int dwLockType)
		{
			throw new NotImplementedException();
		}

		public void Write(byte[] pv, int cb, IntPtr pcbWritten)
		{
			throw new NotImplementedException();
		}
	}
}
