﻿//=============================================================//
//                                                             //
//                  DataLib::TypeContainer.cs                  //
//   -------------------------------------------------------   //
//  Copyright © 2011 Anthony Mushrow, SKGenius Development     //
//  All rights reserved                                        //
//                                                             //
//  This code is licensed under The Code Project Open License  //
//  (CPOL) See http://www.codeproject.com/info/cpol10.aspx or  //
//  LICENSE.TXT for more information                           //
//                                                             //
//=============================================================//
using System;
using System.Collections.Generic;
using System.Text;
using DataLibInterfaces;

namespace DataLib
{
	public abstract class ITypeContainer : IComparable
	{
		public abstract string Name { get; }
		public abstract string Description { get; }
		public abstract Guid GUID { get; }
		public abstract IDataType CreateInstance();

		public int CompareTo(object other)
		{
			return Name.CompareTo(other.ToString());
		}
	}

	public class StandardTypeContainer : ITypeContainer
	{
		public StandardTypeContainer(string name, string desc, Type t)
		{
			m_name = name;
			m_desc = desc;
			m_type = t;
		}

		public override string Name
		{
			get { return m_name; }
		}

		public override string Description
		{
			get { return m_desc; }
		}

		public override Guid GUID
		{
			get { return m_type.GUID; }
		}

		public override IDataType CreateInstance()
		{
			return (IDataType)Activator.CreateInstance(m_type);
		}

		public override string ToString()
		{
			return m_name;
		}

		private string m_name;
		private string m_desc;
		private Type m_type;
	}

	public class ExtendedType : ITypeContainer
	{
		public ExtendedType(string name, string desc, Type t)
		{
			m_name = name;
			m_desc = desc;
			m_type = t;
		}

		public override string Name
		{
			get { return m_name; }
		}

		public override string Description
		{
			get { return m_desc; }
		}

		public override Guid GUID
		{
			get { return m_type.GUID; }
		}

		public override IDataType CreateInstance()
		{
			return (IDataType)Activator.CreateInstance(m_type);
		}

		public override string ToString()
		{
			return m_name;
		}

		private string m_name;
		private string m_desc;
		private Type m_type;
	}

	public class CustomTypeContainer : ITypeContainer
	{
		public CustomTypeContainer(CustomDataTypeDefinition Def, TypeCollection AvailableTypes)
		{
			m_def = Def;
			m_availableTypes = AvailableTypes;
		}

		public override string Name
		{
			get { return m_def.TypeName; }
		}

		public override string Description
		{
			get { return m_def.Description; }
		}

		public override Guid GUID
		{
			get { return m_def.GUID; }
		}

		public override IDataType CreateInstance()
		{
			return (IDataType)new CustomDataType(m_def, m_availableTypes);
		}

		public override string ToString()
		{
			return m_def.TypeName;
		}

		public CustomDataTypeDefinition Definition
		{
			get { return m_def; }
		}

		public TypeCollection AvailableTypes
		{
			get { return m_availableTypes;  }
		}

		CustomDataTypeDefinition m_def;
		TypeCollection m_availableTypes;
	}

	public class UnknownTypeContainer : ITypeContainer
	{
		public UnknownTypeContainer(Guid GUID)
		{
			m_guid = GUID;
		}

		public override IDataType CreateInstance()
		{
			return new UnknownDataType(m_guid);
		}

		public override string Description
		{
			get { return "Unknown Data Type"; }
		}

		public override Guid GUID
		{
			get { return m_guid; }
		}

		public override string Name
		{
			get { return "UnknownDataType"; }
		}

		private Guid m_guid = Guid.Empty;
	}
}
