﻿//=============================================================//
//                                                             //
//                   DataLib::DataManager.cs                   //
//   -------------------------------------------------------   //
//  Copyright © 2011 Anthony Mushrow, SKGenius Development     //
//  All rights reserved                                        //
//                                                             //
//  This code is licensed under The Code Project Open License  //
//  (CPOL) See http://www.codeproject.com/info/cpol10.aspx or  //
//  LICENSE.TXT for more information                           //
//                                                             //
//=============================================================//
using System;
using System.Collections.ObjectModel;
using System.Collections.Generic;
using System.Collections;
using System.Text;
using System.IO;
using System.IO.Compression;
using DataLibInterfaces;

namespace DataLib
{
	public class SaveFileException : Exception
	{
		public List<string> Files;
		public SaveFileException(string msg) : base(msg) 
		{
			Files = new List<string>();
		}
	}

	public class DataManager
	{
		public static readonly int DEFINITION_FILE = 149941919;
		public static readonly int DATA_FILE = 30982696;

		public DataManager()
		{
			m_types = new TypeCollection();
			m_objects = new DataObjectCollection();
			m_files = new InternalFileCollection();
			m_filesWrapper = new FileCollection(m_files);
			m_sourceFile = String.Empty;
		}

		public TypeCollection Types
		{
			get { return m_types; }
		}

		public DataObjectCollection DataObjects
		{
			get { return m_objects; }
		}

		public FileCollection Files
		{
			get { return m_filesWrapper; }
		}

		public void LoadDefinitions(string FilePath)
		{
			FileStream fs = new FileStream(FilePath, FileMode.Open);
			StreamWrapper SW = new StreamWrapper(fs);
			int magic = SW.ReadInt();

			if (magic != DATA_FILE && magic != DEFINITION_FILE)
			{
				fs.Close();
				System.Windows.Forms.MessageBox.Show("Invalid data file", "Error", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Error);
				return;
			}

			//--Skip ahead past other file data
			if (magic == DATA_FILE)
			{
				long dataEnd = SW.ReadLong();
				fs.Position = dataEnd;
				long tableEnd = SW.ReadLong();
				fs.Position = tableEnd;
			}
			//--

			//--Load Custom Definitions
			int numTypeDefs = SW.ReadInt();
			for (int i = 0; i < numTypeDefs; i++)
			{
				CustomDataTypeDefinition typeDef = CustomDataTypeDefinition.ReadStream(fs);
				CustomTypeContainer newType = new CustomTypeContainer(typeDef, m_types);
				m_types.Add(newType);
			}
			//--

			fs.Close();
		}

		public void SaveDefinitions(string FilePath)
		{
			FileStream fs = new FileStream(FilePath, FileMode.Create);
			StreamWrapper SW = new StreamWrapper(fs);
			SW.WriteInt(DEFINITION_FILE);
			SW.WriteInt(m_types.Custom.Count);
			foreach (CustomTypeContainer customType in m_types.Custom)
			{
				customType.Definition.WriteStream(fs);
			}

			fs.Close();
		}

		public void Clear()
		{
			m_files.Clear();
			m_objects.Clear();
			m_types.Clear();
			m_sourceFile = String.Empty;
		}

		public void SaveToFile(string filePath)
		{
			//--Write embedded files first
			byte[] buffer = new byte[4096];
			string ghostFilePath = String.Empty;
			FileStream out_file = null;
			FileStream source_file = null;
			StreamWrapper SW;
			bool copyEmbeddedFiles;
			bool usingGhostPath;

			InternalFileCollection erroredFiles = new InternalFileCollection();


			//--Get the ouput stream
			if (filePath.ToUpper() == m_sourceFile.ToUpper() && !m_files.RebuildRequired)
			{
				out_file = new FileStream(filePath, FileMode.Open);
				copyEmbeddedFiles = false;
				usingGhostPath = false;
			}
			else
			{
				ghostFilePath = getGhostFile(filePath);
				out_file = new FileStream(ghostFilePath, FileMode.Open);
				usingGhostPath = true;

				if (m_sourceFile != String.Empty)
					copyEmbeddedFiles = true;
				else
					copyEmbeddedFiles = false;
			}
			//--

			SW = new StreamWrapper(out_file);

			//--Check file format and whatnot
			if (!usingGhostPath)
			{
				int magic = SW.ReadInt();

				if (magic == DATA_FILE)
				{
					//Seek to the end of the file block
					long endData = SW.ReadLong();
					out_file.Position = endData;
				}
				else
				{
					//This is not actually our file format, we've been cheated!
					out_file.Position = 0;
					SW.WriteInt(DATA_FILE);
					SW.WriteLong(0);
				}
			}
			else
			{
				//Reserve some bytes
				SW.WriteInt(DATA_FILE);
				SW.WriteLong(0);
			}
			//--

			//--Try to open the source file
			if (copyEmbeddedFiles)
			{
				try
				{
					source_file = new FileStream(m_sourceFile, FileMode.Open);
				}
				catch(IOException e)
				{
					string msg = String.Format("Unable to copy embedded files.\n{0}", e.Message);
					System.Windows.Forms.MessageBox.Show(msg, "Error", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Error);
					copyEmbeddedFiles = false;
				}
			}
			//--

			//--Write out solid file data
			foreach (EmbeddedFile file in m_files)
			{
				if (file.Embedded && copyEmbeddedFiles)
				{
					source_file.Position = file.DataStart;
					file.DataStart = out_file.Position;
					long bytesToRead = file.CompressedLength;

					while (bytesToRead > 0)
					{
						int bytesRead = source_file.Read(buffer, 0, 4096);

						int bytesToWrite = bytesRead;
						if (bytesToRead < bytesRead)
						{
							bytesToWrite = (int)bytesToRead;
							bytesToRead = 0;
						}
						else
						{
							bytesToRead -= bytesRead;
						}

						out_file.Write(buffer, 0, bytesToWrite);
					}
				}
				else if(!file.Embedded)
				{
					//--Copress and insert the new addition
					FileStream externalFile = null;
					try
					{
						externalFile = new FileStream(file.FileName, FileMode.Open, FileAccess.Read, FileShare.Read);
						file.DataStart = out_file.Position;
						file.OriginalLength = externalFile.Length;

						Ionic.Zlib.ZlibStream compressed = new Ionic.Zlib.ZlibStream(out_file, Ionic.Zlib.CompressionMode.Compress, true);
						externalFile.CopyTo(compressed);
						compressed.Close();

						file.CompressedLength = out_file.Position - file.DataStart;
						file.Embedded = true;
					}
					catch (IOException)
					{
						erroredFiles.Add(file);
					}
					finally
					{
						if(externalFile != null)
							externalFile.Close();
					}
					//--
				}
			}

			//Go back and set the length of this block
			long dataLength = out_file.Position;
			out_file.Position = 4;
			SW.WriteLong(dataLength);
			out_file.Position = dataLength;
			//--

			foreach (EmbeddedFile errFile in erroredFiles)
			{
				m_files.Remove(errFile.UID);
			}

			//--Write out file table
			long tableStart = dataLength;
			SW.WriteLong(0);
			SW.WriteInt(m_files.FileCount);
			foreach (EmbeddedFile file in m_files)
			{
				SW.WriteInt(file.UID);
				SW.WriteString(file.InternalName);
				SW.WriteLong(file.DataStart);
				SW.WriteLong(file.CompressedLength);
				SW.WriteLong(file.OriginalLength);
				SW.WriteInt(file.FileAttributes);
				SW.WriteLong(file.CreationTimeUTC);
				SW.WriteLong(file.ModifiedTimeUTC);
				SW.WriteLong(file.AccessTimeUTC);
			}

			long tableEnd = out_file.Position;
			out_file.Position = tableStart;
			SW.WriteLong(tableEnd);
			out_file.Position = tableEnd;
			//--


			//--Then append the rest of the data
			MemoryStream tempStream = new MemoryStream();

			//--Save Custom Definitions
			SW.WriteInt(m_types.Custom.Count);
			foreach (CustomTypeContainer customType in m_types.Custom)
			{
				customType.Definition.WriteStream(out_file);
			}
			//--

			//--Save Data
			SW.WriteInt(m_objects.Groups.Count);
			foreach (Guid typeGuid in m_objects.Groups)
			{
				byte[] guid = typeGuid.ToByteArray();
				out_file.Write(guid, 0, 16);

				SW.WriteInt(m_objects[typeGuid].Count);
				foreach (IDataType dataObject in m_objects[typeGuid])
				{
					SW.WriteString(dataObject.Name);
					SW.WriteInt(dataObject.ID);

					//Get object data
					tempStream.SetLength(0);
					dataObject.WriteStream(tempStream);

					//Write data block to file
					SW.WriteInt((int)tempStream.Length);
					tempStream.WriteTo(out_file);
				}
			}
			//--

			out_file.Close();
			if (copyEmbeddedFiles)
				source_file.Close();

			if (usingGhostPath)
			{
				//Set the source path to the ghost incase anything goes wrong
				m_sourceFile = ghostFilePath;

				try
				{
					FileInfo dest = new FileInfo(ghostFilePath);
					dest.Attributes = dest.Attributes & ~FileAttributes.Hidden;
					if (File.Exists(filePath))
					{
						FileInfo source = new FileInfo(filePath);
						
						dest.Attributes = source.Attributes;

						File.Delete(filePath);
					}
					File.Move(ghostFilePath, filePath);

					//Set the source file
					m_sourceFile = filePath;
				}
				catch(IOException e)
				{
					string msg = String.Format("Unable to save file.\n{0}", e.Message);
					System.Windows.Forms.MessageBox.Show(msg, "Error", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Error);
				}
			}

			if (erroredFiles.FileCount > 0)
			{
				SaveFileException ecx = new SaveFileException("Could not embed files");
				foreach(EmbeddedFile errFile in erroredFiles)
				{
					ecx.Files.Add( errFile.FileName );
				}

				throw ecx;
			}
		}

		public bool LoadFromFile(string filePath)
		{
			FileStream s = new FileStream(filePath, FileMode.Open, FileAccess.Read, FileShare.Read);

			m_types.Clear(false);
			m_objects.Clear();
			m_files.Clear();

			StreamWrapper SW = new StreamWrapper(s);
			MemoryStream tempStream;

			int magic = SW.ReadInt();
			if (magic != DATA_FILE)
			{
				s.Close();
				System.Windows.Forms.MessageBox.Show("Invalid data file", "Error", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Error);
				return false;
			}
			//--Load file table
			long dataEnd = SW.ReadLong();
			s.Position = dataEnd;

			long tableEnd = SW.ReadLong();
			int numFiles = SW.ReadInt();
			for (int i = 0; i < numFiles; i++)
			{
				int uid = SW.ReadInt();
				EmbeddedFile newFile = new EmbeddedFile(uid);
				newFile.InternalName = SW.ReadString();
				newFile.DataStart = SW.ReadLong();
				newFile.CompressedLength = SW.ReadLong();
				newFile.OriginalLength = SW.ReadLong();
				newFile.FileAttributes = SW.ReadInt();
				newFile.CreationTimeUTC = SW.ReadLong();
				newFile.ModifiedTimeUTC = SW.ReadLong();
				newFile.AccessTimeUTC = SW.ReadLong();

				newFile.Embedded = true;
				m_files.Add(newFile);
			}
			//--

			//--Load Custom Definitions
			int numTypeDefs = SW.ReadInt();
			for (int i = 0; i < numTypeDefs; i++)
			{
				CustomDataTypeDefinition typeDef = CustomDataTypeDefinition.ReadStream(s);
				CustomTypeContainer newType = new CustomTypeContainer(typeDef, m_types);
				m_types.Add(newType);
			}
			//--

			//--Load Data
			int numTypes = SW.ReadInt();
			for (int r = 0; r < numTypes; r++)
			{
				byte[] guid = new byte[16];
				s.Read(guid, 0, 16);

				Guid typeId = new Guid(guid);
				ITypeContainer typeContainer;
				if (!m_types.FindType(typeId, out typeContainer))
				{
					typeContainer = new UnknownTypeContainer(typeId);
				}

				int numDataObjects = SW.ReadInt();
				for (int i = 0; i < numDataObjects; i++)
				{
					IDataType newData = typeContainer.CreateInstance();
					newData.Name = SW.ReadString();
					newData.ID = SW.ReadInt();

					//Read the data block
					int length = SW.ReadInt();
					byte[] data = new byte[length];
					s.Read(data, 0, length);

					if (newData is UnknownDataType)
					{
						//Store the data block in the unknown type
						(newData as UnknownDataType).SetData(data);
					}
					else
					{
						//Load using the tempStream toa void overruning into other
						//objects data
						tempStream = new MemoryStream(data);
						newData.ReadStream(tempStream);
					}

					m_objects.Add(newData);
				}
			}
			//--

			m_sourceFile = filePath;
			s.Close();

			return true;
		}

		public Stream GetFileStream(int UID)
		{
			EmbeddedFileStream ResultStream = null;

			EmbeddedFile target = null;
			foreach (EmbeddedFile file in m_files)
			{
				if (file.UID == UID)
				{
					target = file;
					break;
				}
			}

			if (target != null)
			{
				if (target.Embedded)
				{
					if (m_sourceFile != String.Empty)
					{
						FileStream fs = new FileStream(m_sourceFile, FileMode.Open, FileAccess.Read, FileShare.Read);
						fs.Position = target.DataStart;
						Ionic.Zlib.ZlibStream deflateStream = new Ionic.Zlib.ZlibStream(fs, Ionic.Zlib.CompressionMode.Decompress, false);
						ResultStream = new EmbeddedFileStream(deflateStream, target.OriginalLength);
					}
				}
				else
				{
					FileStream fs = new FileStream(target.FileName, FileMode.Open, FileAccess.Read, FileShare.Read);
					ResultStream = new EmbeddedFileStream(fs, fs.Length);
				}
			}

			return ResultStream;
		}

		public void ExtractFile(int UID, string Destination)
		{
			EmbeddedFile target = null;
			foreach (EmbeddedFile file in m_files)
			{
				if (file.UID == UID)
				{
					target = file;
					break;
				}
			}

			if (target != null)
			{
				if (target.Embedded)
				{
					if (m_sourceFile != String.Empty)
					{
						FileStream fs = null;
						FileStream source = null;
						try
						{
							fs = new FileStream(Destination, FileMode.Create);
							source = new FileStream(m_sourceFile, FileMode.Open, FileAccess.Read, FileShare.Read);
							source.Position = target.DataStart;
							Ionic.Zlib.ZlibStream decompress = new Ionic.Zlib.ZlibStream(source, Ionic.Zlib.CompressionMode.Decompress, true);
							byte[] buffer = new byte[4086];
							long bytesToRead = target.OriginalLength;
							while (bytesToRead > 0)
							{
								int bytesRead = decompress.Read(buffer, 0, 4086);

								int bytesToWrite = bytesRead;
								if (bytesToRead < bytesRead)
								{
									bytesToWrite = (int)bytesToRead;
									bytesToRead = 0;
								}
								else
								{
									bytesToRead -= bytesRead;
								}

								fs.Write(buffer, 0, bytesToWrite);
							}

							decompress.Close();
						}
						catch (IOException e)
						{
							string msg = String.Format("Unable to extract file \"{0}\"\n\n{1}", target.InternalName, e.Message);
							System.Windows.Forms.MessageBox.Show(msg, "Error", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Error);
						}
						finally
						{
							if (fs != null)
								fs.Close();
							if (source != null)
								source.Close();
						}
					}
				}
				else
				{
					try
					{
						File.Copy(target.FileName, Destination, true);
					}
					catch (IOException e)
					{
						string msg = String.Format("Unable to copy file \"{0}\"\n\n{1}", target.FileName, e.Message);
						System.Windows.Forms.MessageBox.Show(msg, "Error", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Error);
					}
				}
			}
		}
		
		private string getGhostFile(string FileName)
		{
			string targetDir = Path.GetDirectoryName(FileName);
			string name = Path.GetFileName(FileName);
			int i = 0;
			string ghostFilePath = String.Format("{0}\\{1}_{2}", targetDir, name, i);
			while (File.Exists(ghostFilePath))
			{
				ghostFilePath = String.Format("{0}\\{1}_{2}", targetDir, name, ++i);
			}

			FileStream fs = File.Create(ghostFilePath);
			fs.Close();
			FileInfo fi = new FileInfo(ghostFilePath);
			fi.Attributes = FileAttributes.Hidden;

			return ghostFilePath;
		}

		private InternalFileCollection m_files;
		private TypeCollection m_types;
		private DataObjectCollection m_objects;
		private FileCollection m_filesWrapper;
		private string m_sourceFile;
	}
}
