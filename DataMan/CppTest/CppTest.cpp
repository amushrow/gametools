// CppTest.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include "DataLib.h"

int _tmain(int argc, _TCHAR* argv[])
{
	HMODULE dll = LoadLibrary(L"CppDataLib.dll");
	CREATEDATAMANAGER createDataManager = (CREATEDATAMANAGER)GetProcAddress(dll, "CreateDataManager");
	DataLib::IDataManager* dm = createDataManager();
	dm->LoadFromFile("D:\\Documents\\SAVETEST.skd");


	dm->SaveToFile("D:\\Documents\\CPP_SAVETEST.skd");

	delete dm;

	FreeLibrary(dll);
	return 0;
}

