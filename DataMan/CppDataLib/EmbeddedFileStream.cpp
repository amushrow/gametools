//=============================================================//
//                                                             //
//              CppDataLib::EmbeddedFileStream.cpp             //
//   -------------------------------------------------------   //
//  Copyright � 2011 Anthony Mushrow, SKGenius Development     //
//  All rights reserved                                        //
//                                                             //
//  This code is licensed under The Code Project Open License  //
//  (CPOL) See http://www.codeproject.com/info/cpol10.aspx or  //
//  LICENSE.TXT for more information                           //
//                                                             //
//=============================================================//
#include "stdafx.h"
using namespace DataLib;

EmbeddedFileStream::EmbeddedFileStream(IStream* content, Int64 length)
{
	m_content = content;
	m_length = length;
	m_pos = 0;
}

EmbeddedFileStream::~EmbeddedFileStream()
{
	m_content->Close();
	delete m_content;
}

void EmbeddedFileStream::Close()
{
	m_content->Close();
}

void EmbeddedFileStream::Write(BYTE* buffer, int offset, int count)
{

}

void EmbeddedFileStream::WriteByte(BYTE val)
{

}

int EmbeddedFileStream::Read(BYTE* buffer, int offset, int count)
{
	Int64 remain = m_length - m_pos;
	if(count > remain)
		count = (int)remain;
	int read = m_content->Read(buffer, offset, count);
	m_pos += read;
	return read;
}

BYTE EmbeddedFileStream::ReadByte()
{
	if(m_pos < m_length)
	{
		BYTE b;
		Read(&b, 0, 1);
		return b;
	}
	else
		return 0x00;
}

Int64 EmbeddedFileStream::GetLength()
{
	return m_length;
}

Int64 EmbeddedFileStream::GetPosition()
{
	return m_pos;
}

void EmbeddedFileStream::SetPosition(Int64 pos)
{

}

bool EmbeddedFileStream::IsOpen()
{
	return m_content->IsOpen();
}