//=============================================================//
//                                                             //
//                 CppDataLib::DeflateStream.h                 //
//   -------------------------------------------------------   //
//  Copyright � 2011 Anthony Mushrow, SKGenius Development     //
//  All rights reserved                                        //
//                                                             //
//  This code is licensed under The Code Project Open License  //
//  (CPOL) See http://www.codeproject.com/info/cpol10.aspx or  //
//  LICENSE.TXT for more information                           //
//                                                             //
//=============================================================//
#pragma once
#include "stdafx.h"

struct z_stream_s;
namespace DataLib
{
	enum CompressionMode
	{
		Compress,
		Decompress
	};

	
	class DeflateStream : public BaseStream
	{
	public:
		DeflateStream(IStream* source, CompressionMode type, bool leaveOpen);
		~DeflateStream();

		virtual void	Write(BYTE* buffer, int offset, int length);
		virtual void	WriteByte(BYTE b);
		virtual int		Read(BYTE* buffer, int offset, int length);
		virtual BYTE	ReadByte();
		virtual Int64	GetLength();
		virtual Int64	GetPosition();
		virtual void	SetPosition(Int64 pos);
		virtual void	Close();
		virtual bool	IsOpen();

	private:
		IStream* m_source;
		BYTE m_buffer[4096];
		z_stream_s* m_zlibStream;
		bool m_initialised;
		bool m_leaveOpen;
		CompressionMode m_type;
	};
};