//=============================================================//
//                                                             //
//              CppDataLib::DataObjectCollection.h             //
//   -------------------------------------------------------   //
//  Copyright � 2011 Anthony Mushrow, SKGenius Development     //
//  All rights reserved                                        //
//                                                             //
//  This code is licensed under The Code Project Open License  //
//  (CPOL) See http://www.codeproject.com/info/cpol10.aspx or  //
//  LICENSE.TXT for more information                           //
//                                                             //
//=============================================================//
#pragma once
#include "stdafx.h"

namespace DataLib
{
	class DataObjectCollection : public IDataObjectCollection
	{
	private:
		class ObjectGroup
		{
		public:
			ObjectGroup(GUID typeGuid);
			~ObjectGroup();

			GUID						GetTypeGUID();
			std::vector<IDataType*>*	GetObjects();

		private:
			GUID m_typeGuid;
			std::vector<IDataType*>* m_dataObjects;
		};

	public:
		DataObjectCollection();
		~DataObjectCollection();

		void	Add(IDataType* object);
		void	Clear();
		void	Remove(IDataType* object);
		int		Count();

		ReadOnlyCollection<GUID> GetGroups();
		ReadOnlyCollection<IDataType*> operator[] (ITypeContainer* typeContainer);
		ReadOnlyCollection<IDataType*> operator[] (GUID typeGUID);
		IDataType* operator[] (int index);

	private:
		std::vector<ObjectGroup*> m_dataGroups;
		std::vector<IDataType*> m_fullObjectList;
	};
};