//=============================================================//
//                                                             //
//                CppDataLib::StreamWrapper.cpp                //
//   -------------------------------------------------------   //
//  Copyright � 2011 Anthony Mushrow, SKGenius Development     //
//  All rights reserved                                        //
//                                                             //
//  This code is licensed under The Code Project Open License  //
//  (CPOL) See http://www.codeproject.com/info/cpol10.aspx or  //
//  LICENSE.TXT for more information                           //
//                                                             //
//=============================================================//
#include "stdafx.h"

using namespace DataLib;

void BaseStream::WriteShort(short value)
{
	Write((BYTE*)&value, 0, 2);
}

void BaseStream::WriteInt(int value)
{
	Write((BYTE*)&value, 0, 4);
}

void BaseStream::WriteLong(Int64 value)
{
	Write((BYTE*)&value, 0, 8);
}

void BaseStream::WriteFloat(float value)
{
	Write((BYTE*)&value, 0, 4);
}

void BaseStream::WriteBool(bool value)
{
	if(value)
		WriteByte(0x01);
	else
		WriteByte(0x00);
}

void BaseStream::WriteString(std::string value)
{
	//This saves the string in unicode, without the null terminator
	wchar_t* buffer;
	int string_len = (int)value.size();
	int buff_len = string_len * sizeof(wchar_t);
	buffer = new wchar_t[string_len];
	MultiByteToWideChar(CP_ACP, 0, value.c_str(), string_len, buffer, string_len);

	//Write the length of the string data, the the actual data
	WriteInt(buff_len);
	Write((BYTE*)buffer, 0, buff_len);

	delete[] buffer;
}

short BaseStream::ReadShort()
{
	short value = 0;
	Read((BYTE*)&value, 0, 2);
	return value;
}

int BaseStream::ReadInt()
{
	int value = 0;
	Read((BYTE*)&value, 0, 4);
	return value;
}

Int64 BaseStream::ReadLong()
{
	Int64 value = 0;
	Read((BYTE*)&value, 0, 8);
	return value;
}

float BaseStream::ReadFloat()
{
	float value = 0;
	Read((BYTE*)&value, 0, 4);
	return value;
}

bool BaseStream::ReadBool()
{
	BYTE value = ReadByte();
	return value == 0 ? false : true;
}

std::string BaseStream::ReadString()
{
	int buff_len = ReadInt();
	int string_len = buff_len / sizeof(wchar_t);
	wchar_t* buffer = new wchar_t[string_len+1];
	buffer[string_len] = 0;
	Read((BYTE*)buffer, 0, buff_len);

	char* ansi_buffer = new char[string_len+1];

	WideCharToMultiByte(CP_ACP, 0, buffer, buff_len, ansi_buffer, string_len, 0, 0);
	ansi_buffer[string_len] = 0x00;

	std::string value(ansi_buffer);
	delete[] ansi_buffer;
	delete[] buffer;

	return value;
}