//=============================================================//
//                                                             //
//                  CppDataLib::FileStream.cpp                 //
//   -------------------------------------------------------   //
//  Copyright � 2011 Anthony Mushrow, SKGenius Development     //
//  All rights reserved                                        //
//                                                             //
//  This code is licensed under The Code Project Open License  //
//  (CPOL) See http://www.codeproject.com/info/cpol10.aspx or  //
//  LICENSE.TXT for more information                           //
//                                                             //
//=============================================================//
#include "stdafx.h"
using namespace DataLib;

FileStream::FileStream(std::string filename, DWORD fileMode, DWORD access, DWORD shareMode)
{
	m_file = CreateFileA(filename.c_str(), access, shareMode, NULL, fileMode, FILE_ATTRIBUTE_NORMAL, NULL);
	if(m_file == INVALID_HANDLE_VALUE)
		m_file = NULL;
}

FileStream::~FileStream()
{
	Close();
}

void FileStream::Close()
{
	if(m_file != NULL)
	{
		CloseHandle(m_file);
		m_file = NULL;
	}
}

bool FileStream::IsOpen()
{
	return (m_file != NULL);
}

void FileStream::Write(BYTE* buffer, int offset, int length)
{
	while(length > 0)
	{
		DWORD tmp = 0;
		if(WriteFile(m_file, (buffer+offset), length, &tmp, NULL))
		{
			offset += tmp;
			length -= tmp;
		}
		else
		{
			DWORD error = GetLastError();
			break;
		}
	}
}

void FileStream::WriteByte(BYTE val)
{
	Write(&val, 0, 1);
}

int FileStream::Read(BYTE* buffer, int offset, int length)
{
	DWORD tmp = 0;
	int bytesRead = 0;

	while(bytesRead < length)
	{
		if(ReadFile(m_file, (buffer+offset), length-bytesRead, &tmp, NULL) && tmp != 0)
		{
			bytesRead += tmp;
		}
		else
			break;
	}

	return bytesRead;
}

BYTE FileStream::ReadByte()
{
	BYTE b = 0x00;
	Read(&b, 0, 1);
	return b;
}

Int64 FileStream::GetLength()
{
	LARGE_INTEGER curPos;
	LARGE_INTEGER fileLen;
	LARGE_INTEGER zero; zero.QuadPart = 0;
	SetFilePointerEx(m_file, zero, &curPos, FILE_CURRENT);
	SetFilePointerEx(m_file, zero, &fileLen, FILE_END);
	SetFilePointerEx(m_file, curPos, NULL, FILE_BEGIN);

	return fileLen.QuadPart;
}

Int64 FileStream::GetPosition()
{
	LARGE_INTEGER curPos;
	LARGE_INTEGER zero; zero.QuadPart = 0;
	SetFilePointerEx(m_file, zero, &curPos, FILE_CURRENT);

	return curPos.QuadPart;
}

void FileStream::SetPosition(Int64 pos)
{
	LARGE_INTEGER newPos; newPos.QuadPart = pos;
	SetFilePointerEx(m_file, newPos, NULL, FILE_BEGIN);
}