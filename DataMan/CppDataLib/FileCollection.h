//=============================================================//
//                                                             //
//                CppDataLib::FileCollection.h                 //
//   -------------------------------------------------------   //
//  Copyright � 2011 Anthony Mushrow, SKGenius Development     //
//  All rights reserved                                        //
//                                                             //
//  This code is licensed under The Code Project Open License  //
//  (CPOL) See http://www.codeproject.com/info/cpol10.aspx or  //
//  LICENSE.TXT for more information                           //
//                                                             //
//=============================================================//
#pragma once
#include "stdafx.h"

namespace DataLib
{
	class EmbeddedFile : public IFile
	{
	public:
		EmbeddedFile(int ID) { m_ID = ID; }

		void	SetNewUID(int UID) { m_ID = UID; }
		int		GetUID() { return m_ID; }

		Int64	DataStart;
		Int64	CompressedLength;
		Int64	OriginalLength;

		Int64	CreationTimeUTC;
		Int64	ModifiedTimeUTC;
		Int64	AccessTimeUTC;
		int		FileAttributes;
		
		bool	Embedded;
		
		std::string FileName;
		std::string InternalName;
		
		std::string	GetName				() { return InternalName; }
		bool		IsEmbedded			() { return Embedded; }
		Int64		GetCompressedLength	() { return CompressedLength; }
		Int64		GetOriginalLength	() { return OriginalLength; }
		Int64		GetCreationTimeUTC	() { return CreationTimeUTC; }
		Int64		GetModifiedTimeUTC	() { return ModifiedTimeUTC; }
		Int64		GetAccessTimeUTC	() { return AccessTimeUTC; }
		int			GetFileAttribs		() { return FileAttributes; }


	private:
		int m_ID;
	};

	class InternalFileCollection
	{
	public:
		InternalFileCollection();
		~InternalFileCollection();

		bool	IsRebuildRequired();
		int		GetFileCount();
		void	Clear();
		void	Add(EmbeddedFile* newFile);
		int		Add(std::string fileName, std::string internalName);
		void	Remove(int UID);
		bool	Rename(int UID, std::string newName);
		EmbeddedFile* operator[] (int index);

	private:
		std::vector<EmbeddedFile*> m_files;
		int m_topId;
		bool m_rebuildFile;
	};

	class FileCollection : public IFileCollection
	{
	public:
		FileCollection() {}
		FileCollection(InternalFileCollection* source);

		void	Clear();
		int		Count();
		int		Add(std::string fileName, std::string internalName);
		void	Remove(int UID);
		bool	Rename(int UID, std::string newName);
		IFile*	Find(int UID);
		IFile*	Find(std::string name);

		void	SetSource(InternalFileCollection* source);

	private:
		InternalFileCollection* m_source;
	};

};