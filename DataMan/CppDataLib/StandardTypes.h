//=============================================================//
//                                                             //
//                 CppDataLib::StandardTypes.h                 //
//   -------------------------------------------------------   //
//  Copyright � 2011 Anthony Mushrow, SKGenius Development     //
//  All rights reserved                                        //
//                                                             //
//  This code is licensed under The Code Project Open License  //
//  (CPOL) See http://www.codeproject.com/info/cpol10.aspx or  //
//  LICENSE.TXT for more information                           //
//                                                             //
//=============================================================//
#pragma once
#include "stdafx.h"

namespace DataLib
{
	class StandardTypes
	{
	private:
		class _typeCollection
		{
		public:
			_typeCollection();
			~_typeCollection();

			int Count();
			ITypeContainer* operator[] (int index);
			

		private:
			ITypeContainer** m_types;
			int m_count;
		};

	public:
		static _typeCollection TypeCollection;
	};
};