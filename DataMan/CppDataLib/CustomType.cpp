//=============================================================//
//                                                             //
//                  CppDataLib::CustomType.cpp                 //
//   -------------------------------------------------------   //
//  Copyright � 2011 Anthony Mushrow, SKGenius Development     //
//  All rights reserved                                        //
//                                                             //
//  This code is licensed under The Code Project Open License  //
//  (CPOL) See http://www.codeproject.com/info/cpol10.aspx or  //
//  LICENSE.TXT for more information                           //
//                                                             //
//=============================================================//
#include "stdafx.h"
#include <sstream>

using namespace DataLib;

CustomDataTypeDefinition::CustomDataTypeDefinition()
{
	Properties = NULL;
	NumProperties = 0;
}

CustomDataTypeDefinition::~CustomDataTypeDefinition()
{
	if(Properties != NULL)
	{
		delete[] Properties;
	}
}

void CustomDataTypeDefinition::WriteStream(IStream* stream)
{
	stream->Write((BYTE*)&Guid,  0, 16);
	stream->WriteString(TypeName);
	stream->WriteString(Description);
	stream->WriteInt(NumProperties);
	
	for(int i=0; i<NumProperties; i++)
	{
		stream->WriteString(Properties[i].Name);
		stream->Write((BYTE*)&Properties[i].Guid, 0, 16);
		stream->WriteShort(Properties[i].ID);
	}
}

CustomDataTypeDefinition* CustomDataTypeDefinition::ReadStream(IStream* stream)
{
	CustomDataTypeDefinition* customType = new CustomDataTypeDefinition();
	stream->Read((BYTE*)&customType->Guid,  0, 16);
	customType->TypeName = stream->ReadString();
	customType->Description = stream->ReadString();
	customType->NumProperties = stream->ReadInt();
	customType->Properties = new PropID[customType->NumProperties];
	
	for(int i=0; i<customType->NumProperties; i++)
	{
		customType->Properties[i].Name = stream->ReadString();
		stream->Read((BYTE*)&customType->Properties[i].Guid, 0, 16);
		customType->Properties[i].ID = stream->ReadShort();
	}

	return customType;
}

CustomDataType::CustomDataType(CustomDataTypeDefinition* Def, TypeCollection* AvailableTypes)
{
	m_definition = Def;
	m_typeName = Def->TypeName;
	m_numProperties = Def->NumProperties;
	m_internalPropDesc = new EditablePropertyDescriptor(this);
	m_internalPropDesc->Name = m_typeName;
	m_internalPropDesc->EditDialog = false;
	m_internalPropDesc->TextEdit = false;

	m_properties = new LPPROPDESC[m_numProperties];
	for(int i=0; i<m_numProperties; i++)
	{
		IDataType* instance = NULL;
		ITypeContainer* container = NULL;
		if(AvailableTypes->FindType(Def->Properties[i].Guid, &container))
		{
			instance = container->CreateInstance();
		}
		else
		{
			instance = new UnknownDataType(Def->Properties[i].Guid);
		}

		instance->SetID(Def->Properties[i].ID);
		instance->SetName(Def->Properties[i].Name);
		m_properties[i] = instance->GetProperty();
	}
}

CustomDataType::~CustomDataType()
{
	delete m_internalPropDesc;

	for(int i=0; i<m_numProperties; i++)
	{
		IDataType* instance = m_properties[i]->GetIDataType();
		delete instance;
	}
	delete[] m_properties;
}

int CustomDataType::GetSubProperties(LPPROPDESC** Props)
{
	*Props = new LPPROPDESC[m_numProperties];
	for(int i=0; i<m_numProperties; i++)
	{
		(*Props)[i] = m_properties[i]->GetIDataType()->GetProperty();
	}

	return m_numProperties;
}

void CustomDataType::WriteStream(IStream* stream)
{
	for(int i=0; i<m_numProperties; i++)
	{
		Int64 streamStartPos = stream->GetPosition();
		//Reserve some bytes for the length of the data block
		stream->WriteInt(0);
		m_properties[i]->GetIDataType()->WriteStream(stream);
		Int64 streamEndPos = stream->GetPosition();
		int length = (int)(streamEndPos - streamStartPos - 4); //-8 bytes to discount the int

		//Jump to the start, write the length, then jump back
		stream->SetPosition(streamStartPos);
		stream->WriteInt(length);
		stream->SetPosition(streamEndPos);
	}
}

void CustomDataType::ReadStream(IStream* stream)
{
	for(int i=0; i<m_numProperties; i++)
	{
		int length = stream->ReadInt();
		if(length > 0)
		{
			UnknownDataType* test = dynamic_cast<UnknownDataType*>(m_properties[i]->GetIDataType());
			if(test != NULL)
			{
				BYTE* buffer = new BYTE[length];
				stream->Read(buffer, 0, length);
				test->SetData(buffer, length);
			}
			else
				m_properties[i]->GetIDataType()->ReadStream(stream);
		}
	}
}

std::string CustomDataType::GetTypeName()
{
	return m_typeName;
}

GUID CustomDataType::GetTypeGUID()
{
	return m_definition->Guid;
}

std::string CustomDataType::GetName()
{
	return m_internalPropDesc->Name;
}

void CustomDataType::SetName(std::string name)
{
	m_internalPropDesc->Name = name;
}

std::string CustomDataType::GetStringValue()
{
	std::ostringstream str;
	str << '[' << m_typeName << ']';
	return str.str();
}

const void* CustomDataType::GetValuePtr()
{
	return NULL;
}

bool CustomDataType::SetValue(std::string value)
{
	return false;
}

LPPROPDESC CustomDataType::GetProperty()
{
	return m_internalPropDesc;
}

int CustomDataType::GetID()
{
	return m_id;
}

void CustomDataType::SetID(int id)
{
	m_id = id;
}