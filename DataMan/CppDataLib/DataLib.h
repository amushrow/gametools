//=============================================================//
//                                                             //
//                    CppDataLib::DataLib.h                    //
//   -------------------------------------------------------   //
//  Copyright � 2011 Anthony Mushrow, SKGenius Development     //
//  All rights reserved                                        //
//                                                             //
//  This code is licensed under The Code Project Open License  //
//  (CPOL) See http://www.codeproject.com/info/cpol10.aspx or  //
//  LICENSE.TXT for more information                           //
//                                                             //
//=============================================================//
#pragma once
#define WIN32_LEAN_AND_MEAN             // Exclude rarely-used stuff from Windows headers
#include <Windows.h>
#include <string>
#include <vector>

typedef long long Int64;
typedef unsigned long long UInt64;
struct z_stream_s;

namespace DataLib
{
	class IDataType;

	template <class T>
	class ReadOnlyCollection
	{
	public:
		ReadOnlyCollection(std::vector<T>* baseList, bool owner)
		{
			m_baseList = baseList;
			m_owner = owner;
		}

		~ReadOnlyCollection()
		{
			if(m_owner)
				delete m_baseList;
		}

		typename std::vector<T>::iterator begin()
		{
			return m_baseList->begin();
		}

		typename std::vector<T>::iterator end()
		{
			return m_baseList->end();
		}

		int Count()
		{
			return (int)m_baseList->size();
		}

		T& operator[] (int index)
		{
			return m_baseList->at(index);
		}


	private:
		std::vector<T>* m_baseList;
		bool m_owner;
	};

	class ITypeContainer
	{
	public:
		virtual ~ITypeContainer() {}

		virtual std::string GetName()=0;
		virtual std::string GetDescription()=0;
		virtual GUID		GetGUID()=0;
		virtual IDataType*	CreateInstance()=0;

		int CompareTo(ITypeContainer* other)
		{
			return GetName().compare(other->GetName());
		}
	};

	class IFile
	{
	public:
		virtual ~IFile() {}

		virtual std::string	GetName()=0;
		virtual int			GetUID()=0;
		virtual bool		IsEmbedded()=0;
		virtual Int64		GetCompressedLength()=0;
		virtual Int64		GetOriginalLength()=0;
		virtual Int64		GetCreationTimeUTC()=0;
		virtual Int64		GetModifiedTimeUTC()=0;
		virtual Int64		GetAccessTimeUTC()=0;
		virtual int			GetFileAttribs()=0;
	};

	class IFileCollection
	{
	public:
		virtual ~IFileCollection() {}

		virtual void	Clear()=0;
		virtual int		Count()=0;
		virtual int		Add(std::string fileName, std::string internalName)=0;
		virtual void	Remove(int UID)=0;
		virtual bool	Rename(int UID, std::string newName)=0;
		virtual IFile*	Find(int UID)=0;
		virtual IFile*	Find(std::string name)=0;

	private:
		//Just to generate a compile-time warning that this type of thing is
		//futile
		IFileCollection& operator= (const IFileCollection& other)	{}
	};

	class IDataObjectCollection
	{
	public:
		virtual ~IDataObjectCollection() {}

		virtual void	Add(IDataType* object)=0;
		virtual void	Clear()=0;
		virtual void	Remove(IDataType* object)=0;
		virtual int		Count()=0;
		
		virtual ReadOnlyCollection<GUID> GetGroups()=0;
		virtual ReadOnlyCollection<IDataType*> operator[] (ITypeContainer* typeContainer)=0;
		virtual ReadOnlyCollection<IDataType*> operator[] (GUID typeGUID)=0;
		virtual IDataType* operator[] (int index)=0;

	private:
		//Just to generate a compile-time warning that this type of thing is
		//futile
		IDataObjectCollection& operator= (const IDataObjectCollection& other)	{}
	};

	class ITypeCollection
	{
	public:
		virtual ~ITypeCollection() {}
		
		virtual ReadOnlyCollection<ITypeContainer*> GetStandardTypes()=0;
		virtual ReadOnlyCollection<ITypeContainer*> GetCustomTypes()=0;
		virtual ReadOnlyCollection<ITypeContainer*> GetExtendedTypes()=0;
		virtual ReadOnlyCollection<ITypeContainer*> GetTypes()=0;
		virtual int Count()=0;
		virtual ITypeContainer* operator[] (int index)=0;
		virtual void Add(ITypeContainer* newType)=0;
		virtual void Remove(ITypeContainer* container)=0;
		virtual void Clear(bool extended = true)=0;
		virtual bool FindType(const GUID& guid, ITypeContainer** container)=0;

	private:
		//Just to generate a compile-time warning that this type of thing is
		//futile
		ITypeCollection& operator= (const ITypeCollection& other)	{}
	};

	class IStream
	{
	public:
		virtual ~IStream() {}

		//--Basic stream functions
		virtual void	Write(BYTE* buffer, int offset, int length)=0;
		virtual void	WriteByte(BYTE b)=0;
		virtual int		Read(BYTE* buffer, int offset, int length)=0;
		virtual BYTE	ReadByte()=0;
		virtual Int64	GetLength()=0;
		virtual Int64	GetPosition()=0;
		virtual void	SetPosition(Int64 pos)=0;
		virtual void	Close()=0;
		virtual bool	IsOpen()=0;
		virtual void	Release()=0;
		//--

		//--StreamWrapper functions
		virtual void		WriteShort(short value)=0;
		virtual short		ReadShort()=0;
		virtual void		WriteInt(int value)=0;
		virtual int			ReadInt()=0;
		virtual void		WriteFloat(float value)=0;
		virtual float		ReadFloat()=0;
		virtual void		WriteLong(Int64 value)=0;
		virtual Int64		ReadLong()=0;
		virtual void		WriteBool(bool value)=0;
		virtual bool		ReadBool()=0;
		virtual void		WriteString(std::string value)=0;
		virtual std::string	ReadString()=0;
		//--
	};

	class IPropertyDescriptor
	{
	public:
		virtual ~IPropertyDescriptor() {}

		virtual bool		CanTextEdit()=0;
		virtual bool		HasEditDialog()=0;
		virtual IDataType*	GetIDataType()=0;
		virtual std::string	GetName()=0;
		virtual std::string	GetTypeName()=0;
	};
	typedef IPropertyDescriptor* LPPROPDESC;

	class IDataType
	{
	public:
		virtual ~IDataType() {}

		virtual std::string	GetTypeName()=0;
		virtual GUID		GetTypeGUID()=0;
		virtual std::string	GetName()=0;
		virtual void		SetName(std::string name)=0;
		virtual std::string	GetStringValue()=0;
		virtual const void*	GetValuePtr()=0;
		virtual bool		SetValue(std::string value)=0;
		virtual int			GetID()=0;
		virtual void		SetID(int id)=0;
		virtual LPPROPDESC	GetProperty()=0;
		virtual int			GetSubProperties(LPPROPDESC** Props)=0;
		virtual void		ShowEditForm()=0;
		virtual void		WriteStream(IStream* stream)=0;
		virtual void		ReadStream(IStream* stream)=0;
	};

	class IDataManager
	{
	public:
		virtual ~IDataManager() {}

		virtual ITypeCollection&		GetTypes()=0;
		virtual IDataObjectCollection&	GetDataObjects()=0;
		virtual IFileCollection&		GetFiles()=0;

		virtual void					LoadDefinitions(std::string filePath)=0;
		virtual void					SaveDefinitions(std::string filePath)=0;
		virtual void					SaveToFile(std::string filePath)=0;
		virtual bool					LoadFromFile(std::string filePath)=0;
		virtual void					Clear()=0;
		virtual IStream*				GetFileStream(int UID)=0;
		virtual void					ExtractFile(int UID, std::string destination)=0;
	
	private:
		//Just to generate a compile-time warning that this type of thing is
		//futile
		IDataManager& operator= (const IDataManager& other)	{}
	};

	class EditablePropertyDescriptor;
	class Boolean : public IDataType
	{
	public:
		Boolean();
		~Boolean();

		virtual std::string	GetTypeName();
		virtual GUID		GetTypeGUID();
		virtual std::string	GetName();
		virtual void		SetName(std::string name);
		virtual std::string	GetStringValue();
		virtual const void*	GetValuePtr();
		virtual bool		SetValue(std::string value);
		virtual int			GetID();
		virtual void		SetID(int id);
		virtual LPPROPDESC	GetProperty();
		virtual int			GetSubProperties(LPPROPDESC** Props);
		virtual void		ShowEditForm();
		virtual void		WriteStream(IStream* stream);
		virtual void		ReadStream(IStream* stream);

		bool 	GetValue();
		void	SetValue(bool val);

	private:
		int m_id;
		bool m_val;
		EditablePropertyDescriptor* m_internalPropDesc;
	};

	class Unsigned8 : public IDataType
	{
	public:
		Unsigned8();
		~Unsigned8();

		virtual std::string	GetTypeName();
		virtual GUID		GetTypeGUID();
		virtual std::string	GetName();
		virtual void		SetName(std::string name);
		virtual std::string	GetStringValue();
		virtual const void*	GetValuePtr();
		virtual bool		SetValue(std::string value);
		virtual int			GetID();
		virtual void		SetID(int id);
		virtual LPPROPDESC	GetProperty();
		virtual int			GetSubProperties(LPPROPDESC** Props);
		virtual void		ShowEditForm();
		virtual void		WriteStream(IStream* stream);
		virtual void		ReadStream(IStream* stream);

		unsigned char	GetValue();
		void			SetValue(unsigned char val);

	private:
		int m_id;
		unsigned char m_val;
		EditablePropertyDescriptor* m_internalPropDesc;
	};

	class Signed8 : public IDataType
	{
	public:
		Signed8();
		~Signed8();

		virtual std::string	GetTypeName();
		virtual GUID		GetTypeGUID();
		virtual std::string	GetName();
		virtual void		SetName(std::string name);
		virtual std::string	GetStringValue();
		virtual const void*	GetValuePtr();
		virtual bool		SetValue(std::string value);
		virtual int			GetID();
		virtual void		SetID(int id);
		virtual LPPROPDESC	GetProperty();
		virtual int			GetSubProperties(LPPROPDESC** Props);
		virtual void		ShowEditForm();
		virtual void		WriteStream(IStream* stream);
		virtual void		ReadStream(IStream* stream);

		char 	GetValue();
		void	SetValue(char val);

	private:
		int m_id;
		char m_val;
		EditablePropertyDescriptor* m_internalPropDesc;
	};

	class Unsigned16 : public IDataType
	{
	public:
		Unsigned16();
		~Unsigned16();

		virtual std::string	GetTypeName();
		virtual GUID		GetTypeGUID();
		virtual std::string	GetName();
		virtual void		SetName(std::string name);
		virtual std::string	GetStringValue();
		virtual const void*	GetValuePtr();
		virtual bool		SetValue(std::string value);
		virtual int			GetID();
		virtual void		SetID(int id);
		virtual LPPROPDESC	GetProperty();
		virtual int			GetSubProperties(LPPROPDESC** Props);
		virtual void		ShowEditForm();
		virtual void		WriteStream(IStream* stream);
		virtual void		ReadStream(IStream* stream);

		unsigned short 	GetValue();
		void			SetValue(unsigned short val);

	private:
		int m_id;
		unsigned short m_val;
		EditablePropertyDescriptor* m_internalPropDesc;
	};

	class Signed16 : public IDataType
	{
	public:
		Signed16();
		~Signed16();

		virtual std::string	GetTypeName();
		virtual GUID		GetTypeGUID();
		virtual std::string	GetName();
		virtual void		SetName(std::string name);
		virtual std::string	GetStringValue();
		virtual const void*	GetValuePtr();
		virtual bool		SetValue(std::string value);
		virtual int			GetID();
		virtual void		SetID(int id);
		virtual LPPROPDESC	GetProperty();
		virtual int			GetSubProperties(LPPROPDESC** Props);
		virtual void		ShowEditForm();
		virtual void		WriteStream(IStream* stream);
		virtual void		ReadStream(IStream* stream);

		short 	GetValue();
		void	SetValue(short val);

	private:
		int m_id;
		short m_val;
		EditablePropertyDescriptor* m_internalPropDesc;
	};

	class Unsigned32 : public IDataType
	{
	public:
		Unsigned32();
		~Unsigned32();

		virtual std::string	GetTypeName();
		virtual GUID		GetTypeGUID();
		virtual std::string	GetName();
		virtual void		SetName(std::string name);
		virtual std::string	GetStringValue();
		virtual const void*	GetValuePtr();
		virtual bool		SetValue(std::string value);
		virtual int			GetID();
		virtual void		SetID(int id);
		virtual LPPROPDESC	GetProperty();
		virtual int			GetSubProperties(LPPROPDESC** Props);
		virtual void		ShowEditForm();
		virtual void		WriteStream(IStream* stream);
		virtual void		ReadStream(IStream* stream);

		unsigned 	GetValue();
		void		SetValue(unsigned val);

	private:
		int m_id;
		unsigned m_val;
		EditablePropertyDescriptor* m_internalPropDesc;
	};

	class Signed32 : public IDataType
	{
	public:
		Signed32();
		~Signed32();

		virtual std::string	GetTypeName();
		virtual GUID		GetTypeGUID();
		virtual std::string	GetName();
		virtual void		SetName(std::string name);
		virtual std::string	GetStringValue();
		virtual const void*	GetValuePtr();
		virtual bool		SetValue(std::string value);
		virtual int			GetID();
		virtual void		SetID(int id);
		virtual LPPROPDESC	GetProperty();
		virtual int			GetSubProperties(LPPROPDESC** Props);
		virtual void		ShowEditForm();
		virtual void		WriteStream(IStream* stream);
		virtual void		ReadStream(IStream* stream);

		int		GetValue();
		void	SetValue(int val);

	private:
		int m_id;
		int m_val;
		EditablePropertyDescriptor* m_internalPropDesc;
	};

	class Unsigned64 : public IDataType
	{
	public:
		Unsigned64();
		~Unsigned64();

		virtual std::string	GetTypeName();
		virtual GUID		GetTypeGUID();
		virtual std::string	GetName();
		virtual void		SetName(std::string name);
		virtual std::string	GetStringValue();
		virtual const void*	GetValuePtr();
		virtual bool		SetValue(std::string value);
		virtual int			GetID();
		virtual void		SetID(int id);
		virtual LPPROPDESC	GetProperty();
		virtual int			GetSubProperties(LPPROPDESC** Props);
		virtual void		ShowEditForm();
		virtual void		WriteStream(IStream* stream);
		virtual void		ReadStream(IStream* stream);

		UInt64	GetValue();
		void	SetValue(UInt64 val);

	private:
		int m_id;
		UInt64 m_val;
		EditablePropertyDescriptor* m_internalPropDesc;
	};

	class Signed64 : public IDataType
	{
	public:
		Signed64();
		~Signed64();

		virtual std::string	GetTypeName();
		virtual GUID		GetTypeGUID();
		virtual std::string	GetName();
		virtual void		SetName(std::string name);
		virtual std::string	GetStringValue();
		virtual const void*	GetValuePtr();
		virtual bool		SetValue(std::string value);
		virtual int			GetID();
		virtual void		SetID(int id);
		virtual LPPROPDESC	GetProperty();
		virtual int			GetSubProperties(LPPROPDESC** Props);
		virtual void		ShowEditForm();
		virtual void		WriteStream(IStream* stream);
		virtual void		ReadStream(IStream* stream);

		Int64	GetValue();
		void	SetValue(Int64 val);

	private:
		int m_id;
		Int64 m_val;
		EditablePropertyDescriptor* m_internalPropDesc;
	};

	class SinglePrecision : public IDataType
	{
	public:
		SinglePrecision();
		~SinglePrecision();

		virtual std::string	GetTypeName();
		virtual GUID		GetTypeGUID();
		virtual std::string	GetName();
		virtual void		SetName(std::string name);
		virtual std::string	GetStringValue();
		virtual const void*	GetValuePtr();
		virtual bool		SetValue(std::string value);
		virtual int			GetID();
		virtual void		SetID(int id);
		virtual LPPROPDESC	GetProperty();
		virtual int			GetSubProperties(LPPROPDESC** Props);
		virtual void		ShowEditForm();
		virtual void		WriteStream(IStream* stream);
		virtual void		ReadStream(IStream* stream);

		float	GetValue();
		void	SetValue(float val);

	private:
		int m_id;
		float m_val;
		EditablePropertyDescriptor* m_internalPropDesc;
	};

	class DoublePrecision : public IDataType
	{
	public:
		DoublePrecision();
		~DoublePrecision();

		virtual std::string	GetTypeName();
		virtual GUID		GetTypeGUID();
		virtual std::string	GetName();
		virtual void		SetName(std::string name);
		virtual std::string	GetStringValue();
		virtual const void*	GetValuePtr();
		virtual bool		SetValue(std::string value);
		virtual int			GetID();
		virtual void		SetID(int id);
		virtual LPPROPDESC	GetProperty();
		virtual int			GetSubProperties(LPPROPDESC** Props);
		virtual void		ShowEditForm();
		virtual void		WriteStream(IStream* stream);
		virtual void		ReadStream(IStream* stream);

		double	GetValue();
		void	SetValue(double val);

	private:
		int m_id;
		double m_val;
		EditablePropertyDescriptor* m_internalPropDesc;
	};

	class Text : public IDataType
	{
	public:
		Text();
		~Text();

		virtual std::string	GetTypeName();
		virtual GUID		GetTypeGUID();
		virtual std::string	GetName();
		virtual void		SetName(std::string name);
		virtual std::string	GetStringValue();
		virtual const void*	GetValuePtr();
		virtual bool		SetValue(std::string value);
		virtual int			GetID();
		virtual void		SetID(int id);
		virtual LPPROPDESC	GetProperty();
		virtual int			GetSubProperties(LPPROPDESC** Props);
		virtual void		ShowEditForm();
		virtual void		WriteStream(IStream* stream);
		virtual void		ReadStream(IStream* stream);

	private:
		int m_id;
		std::string m_val;
		EditablePropertyDescriptor* m_internalPropDesc;
	};

	class SpriteData
	{
	public:
		SpriteData();
		~SpriteData();

		struct Shape
		{
			float	X;
			float	Y;
			float	Width;
			float	Height;
			float	cX;
			float	cY;
		};

		SpriteData*		Clone();
		std::string		GetName()			{ return m_name; }
		std::string		GetTexName()		{ return m_texName; }
		std::string		GetTexPath()		{ return m_texPath; }
		float			GetWidth()			{ return m_width; }
		float			GetHeight()			{ return m_height; }
		float			GetFrameInterval()	{ return m_frameInterval; }
		int				GetNumFrames()		{ return m_numFrames; }
		const Shape*	GetFrameCoords()	{ return m_frameCoords; }
		void			WriteToStream(IStream* stream);

		static SpriteData*	ReadFromStream(IStream* stream);

	private:
		Shape*	m_frameCoords;
		int		m_numFrames;
		float	m_frameInterval;
		float	m_width;
		float	m_height;
		std::string	m_name;
		std::string	m_texName;
		std::string	m_texPath;
	};

	class Sprite : public IDataType
	{
	public:
		Sprite();
		~Sprite();

		virtual std::string	GetTypeName();
		virtual GUID		GetTypeGUID();
		virtual std::string	GetName();
		virtual void		SetName(std::string name);
		virtual std::string	GetStringValue();
		virtual const void*	GetValuePtr();
		virtual bool		SetValue(std::string value);
		virtual int			GetID();
		virtual void		SetID(int id);
		virtual LPPROPDESC	GetProperty();
		virtual int			GetSubProperties(LPPROPDESC** Props);
		virtual void		ShowEditForm();
		virtual void		WriteStream(IStream* stream);
		virtual void		ReadStream(IStream* stream);

		void		SetValue(SpriteData* data);
		SpriteData*	GetValue();

	private:
		int m_id;
		SpriteData* m_val;
		EditablePropertyDescriptor* m_internalPropDesc;
	};
};

extern "C" 
{
	DataLib::IDataManager* CreateDataManager();
	void ReleaseDataManager(DataLib::IDataManager** dataMan);

	typedef DataLib::IDataManager* (*CREATEDATAMANAGER) (void);
	typedef void (*RELEASEDATAMANAGER) (DataLib::IDataManager**);
}