//=============================================================//
//                                                             //
//                  CppDataLib::CustomType.h                   //
//   -------------------------------------------------------   //
//  Copyright � 2011 Anthony Mushrow, SKGenius Development     //
//  All rights reserved                                        //
//                                                             //
//  This code is licensed under The Code Project Open License  //
//  (CPOL) See http://www.codeproject.com/info/cpol10.aspx or  //
//  LICENSE.TXT for more information                           //
//                                                             //
//=============================================================//
#pragma once
#include "stdafx.h"
#include <sstream>
namespace DataLib
{
	struct PropID
	{
		std::string	Name;
		GUID		Guid;
		short		ID;
	};

	class CustomDataTypeDefinition
	{
	public:
		CustomDataTypeDefinition();
		~CustomDataTypeDefinition();
		GUID		Guid;
		std::string	TypeName;
		std::string	Description;
		PropID*		Properties;
		int			NumProperties;

		void WriteStream(IStream* stream);
		static CustomDataTypeDefinition* ReadStream(IStream* stream);
	};

	class TypeCollection;
	class CustomDataType : public IDataType
	{
	public:
		CustomDataType(CustomDataTypeDefinition* Def, TypeCollection* AvailableTypes);
		~CustomDataType();

		virtual std::string	GetTypeName();
		virtual GUID		GetTypeGUID();
		virtual std::string	GetName();
		virtual void		SetName(std::string name);
		virtual std::string	GetStringValue();
		virtual const void* GetValuePtr();
		virtual bool		SetValue(std::string value);
		virtual LPPROPDESC	GetProperty();
		virtual int			GetID();
		virtual void		SetID(int id);
		virtual void		ShowEditForm() {}
		virtual int			GetSubProperties(LPPROPDESC** Props);
		virtual void		WriteStream(IStream* stream);
		virtual void		ReadStream(IStream* stream);

		void						Rebuild(TypeCollection* AvailableTypes);

	private:
		CustomDataTypeDefinition*	m_definition;
		EditablePropertyDescriptor*	m_internalPropDesc;
		std::string					m_typeName;
		LPPROPDESC*					m_properties;
		int							m_numProperties;
		int							m_id;
	};
};