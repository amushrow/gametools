//=============================================================//
//                                                             //
//                   CppDataLib::IDataType.h                   //
//   -------------------------------------------------------   //
//  Copyright � 2011 Anthony Mushrow, SKGenius Development     //
//  All rights reserved                                        //
//                                                             //
//  This code is licensed under The Code Project Open License  //
//  (CPOL) See http://www.codeproject.com/info/cpol10.aspx or  //
//  LICENSE.TXT for more information                           //
//                                                             //
//=============================================================//
#pragma once
#include "stdafx.h"

namespace DataLib
{
	class EditablePropertyDescriptor : public IPropertyDescriptor
	{
	public:
		EditablePropertyDescriptor(IDataType* dataType);
				
		virtual IDataType*	GetIDataType();
		virtual bool		CanTextEdit();
		virtual bool		HasEditDialog();
		virtual std::string	GetName();
		virtual std::string	GetTypeName();

		bool		TextEdit;
		bool		EditDialog;
		std::string	Name;
		std::string	TypeName;

	private:
		IDataType* m_dataType;
	};
};