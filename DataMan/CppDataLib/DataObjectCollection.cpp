//=============================================================//
//                                                             //
//            CppDataLib::DataObjectCollection.cpp             //
//   -------------------------------------------------------   //
//  Copyright � 2011 Anthony Mushrow, SKGenius Development     //
//  All rights reserved                                        //
//                                                             //
//  This code is licensed under The Code Project Open License  //
//  (CPOL) See http://www.codeproject.com/info/cpol10.aspx or  //
//  LICENSE.TXT for more information                           //
//                                                             //
//=============================================================//
#include "stdafx.h"
using namespace DataLib;

DataObjectCollection::DataObjectCollection()
{

}

DataObjectCollection::~DataObjectCollection()
{
	Clear();
}

void DataObjectCollection::Clear()
{
	int len = m_fullObjectList.size();
	for(int i=0; i<len; i++)
	{
		delete m_fullObjectList[i];
	}
	m_fullObjectList.clear();

	len = m_dataGroups.size();
	for(int i=0; i<len; i++)
	{
		delete m_dataGroups[i];
	}
	m_dataGroups.clear();
}

void DataObjectCollection::Add(IDataType* dataObject)
{
	//--Find or create group
	bool groupExists = false;
	int len = m_dataGroups.size();
	ObjectGroup* targetGroup = NULL;
	for(int i=0; i<len; i++)
	{
		if(m_dataGroups[i]->GetTypeGUID() == dataObject->GetTypeGUID())
		{
			groupExists = true;
			targetGroup = m_dataGroups[i];
			break;
		}
	}

	if(!groupExists)
	{
		targetGroup = new ObjectGroup(dataObject->GetTypeGUID());
		m_dataGroups.push_back(targetGroup);
	}
	//--

	targetGroup->GetObjects()->push_back(dataObject);
	m_fullObjectList.push_back(dataObject);
}

void DataObjectCollection::Remove(IDataType* dataObject)
{
	bool groupExists = false;
	int len = m_dataGroups.size();
	ObjectGroup* targetGroup = NULL;
	int groupId = 0;
	for(groupId=0; groupId<len; groupId++)
	{
		if(m_dataGroups[groupId]->GetTypeGUID() == dataObject->GetTypeGUID())
		{
			groupExists = true;
			targetGroup = m_dataGroups[groupId];
			break;
		}
	}

	if(groupExists)
	{
		std::vector<IDataType*>::iterator iter;
		for(iter = m_fullObjectList.begin(); iter != m_fullObjectList.end(); iter++)
		{
			if(*iter == dataObject)
			{
				delete dataObject;
				m_fullObjectList.erase(iter);
				break;
			}
		}

		std::vector<IDataType*>* groupObjects = targetGroup->GetObjects();
		for(iter = groupObjects->begin(); iter != groupObjects->end(); iter++)
		{
			if(*iter == dataObject)
			{
				groupObjects->erase(iter);
				break;
			}
		}
		if(groupObjects->size() == 0)
		{
			delete groupObjects;
			m_dataGroups.erase( m_dataGroups.begin() + groupId );
		}
	}
}

int DataObjectCollection::Count()
{
	return m_fullObjectList.size();
}

ReadOnlyCollection<GUID> DataObjectCollection::GetGroups()
{
	std::vector<GUID>* guids = new std::vector<GUID>();
	int len = m_dataGroups.size();
	for(int i=0; i<len; i++)
	{
		guids->push_back( m_dataGroups[i]->GetTypeGUID() );
	}

	return ReadOnlyCollection<GUID>(guids, true);
}

ReadOnlyCollection<IDataType*> DataObjectCollection::operator[] (ITypeContainer* typeContainer)
{
	return operator[](typeContainer->GetGUID());
}

ReadOnlyCollection<IDataType*> DataObjectCollection::operator[] (GUID typeGUID)
{
	bool groupExists = false;
	int len = m_dataGroups.size();
	for(int i=0; i<len; i++)
	{
		if(m_dataGroups[i]->GetTypeGUID() == typeGUID)
		{
			return ReadOnlyCollection<IDataType*>(m_dataGroups[i]->GetObjects(), false);
		}
	}
	
	std::vector<IDataType*>* empty = new std::vector<IDataType*>();
	return ReadOnlyCollection<IDataType*>(empty, true);
}

IDataType* DataObjectCollection::operator[] (int index)
{
	return m_fullObjectList[index];
}


DataObjectCollection::ObjectGroup::ObjectGroup(GUID typeGuid)
{
	m_typeGuid = typeGuid;
	m_dataObjects = new std::vector<IDataType*>();
}

DataObjectCollection::ObjectGroup::~ObjectGroup()
{
	delete m_dataObjects;
}

GUID DataObjectCollection::ObjectGroup::GetTypeGUID()
{
	return m_typeGuid;
}

std::vector<IDataType*>* DataObjectCollection::ObjectGroup::GetObjects()
{
	return m_dataObjects;
}