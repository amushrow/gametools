//=============================================================//
//                                                             //
//                CppDataLib::TypeCollection.h                 //
//   -------------------------------------------------------   //
//  Copyright � 2011 Anthony Mushrow, SKGenius Development     //
//  All rights reserved                                        //
//                                                             //
//  This code is licensed under The Code Project Open License  //
//  (CPOL) See http://www.codeproject.com/info/cpol10.aspx or  //
//  LICENSE.TXT for more information                           //
//                                                             //
//=============================================================//
#pragma once
#include "stdafx.h"

namespace DataLib
{
	class TypeCollection : public ITypeCollection
	{
	public:
		TypeCollection();

		ReadOnlyCollection<ITypeContainer*> GetStandardTypes()
		{
			return ReadOnlyCollection<ITypeContainer*>(&m_stdTypes, false);
		}

		ReadOnlyCollection<ITypeContainer*> GetCustomTypes()
		{
			return ReadOnlyCollection<ITypeContainer*>(&m_custTypes, false);
		}

		ReadOnlyCollection<ITypeContainer*> GetExtendedTypes()
		{
			return ReadOnlyCollection<ITypeContainer*>(&m_extTypes, false);
		}

		ReadOnlyCollection<ITypeContainer*> GetTypes()
		{
			return ReadOnlyCollection<ITypeContainer*>(&m_allTypes, false);
		}

		int Count()
		{
			return (int)m_allTypes.size();
		}

		ITypeContainer* operator[] (int index)
		{
			return m_allTypes[index];
		}

		void Add(ITypeContainer* newType);
		void Remove(ITypeContainer* container);
		void Clear(bool extended = true);
		bool FindType(const GUID& guid, ITypeContainer** container);
		
	private:
		std::vector<ITypeContainer*> m_stdTypes;
		std::vector<ITypeContainer*> m_extTypes;
		std::vector<ITypeContainer*> m_custTypes;
		std::vector<ITypeContainer*> m_allTypes;
	};
};