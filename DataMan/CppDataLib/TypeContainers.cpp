//=============================================================//
//                                                             //
//                CppDataLib::TypeContainers.cpp               //
//   -------------------------------------------------------   //
//  Copyright � 2011 Anthony Mushrow, SKGenius Development     //
//  All rights reserved                                        //
//                                                             //
//  This code is licensed under The Code Project Open License  //
//  (CPOL) See http://www.codeproject.com/info/cpol10.aspx or  //
//  LICENSE.TXT for more information                           //
//                                                             //
//=============================================================//
#include "stdafx.h"
using namespace DataLib;

CustomTypeContainer::~CustomTypeContainer()
{
	delete m_def;
}

std::string CustomTypeContainer::GetName()
{
	return m_def->TypeName;
}

std::string CustomTypeContainer::GetDescription()
{
	return m_def->Description;
}

GUID CustomTypeContainer::GetGUID()
{
	return m_def->Guid;
}

IDataType* CustomTypeContainer::CreateInstance()
{
	return new CustomDataType(m_def, m_availableTypes);
}

UnknownTypeContainer::UnknownTypeContainer(GUID guid)
{
	m_guid = guid;
}

IDataType* UnknownTypeContainer::CreateInstance()
{
	return new UnknownDataType(m_guid);
}

std::string UnknownTypeContainer::GetDescription()
{
	return "Unknown Data Type";
}

std::string UnknownTypeContainer::GetName()
{
	return "UnknownDataType";
}

GUID UnknownTypeContainer::GetGUID()
{
	return m_guid;
}