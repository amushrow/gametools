//=============================================================//
//                                                             //
//                CppDataLib::FileCollection.cpp               //
//   -------------------------------------------------------   //
//  Copyright � 2011 Anthony Mushrow, SKGenius Development     //
//  All rights reserved                                        //
//                                                             //
//  This code is licensed under The Code Project Open License  //
//  (CPOL) See http://www.codeproject.com/info/cpol10.aspx or  //
//  LICENSE.TXT for more information                           //
//                                                             //
//=============================================================//
#include "stdafx.h"
#include "sys\stat.h"
using namespace DataLib;

InternalFileCollection::InternalFileCollection()
{
	m_topId = 1;
	m_rebuildFile = false;
}

InternalFileCollection::~InternalFileCollection()
{
	Clear();
}

bool InternalFileCollection::IsRebuildRequired()
{
	return m_rebuildFile;
}

int InternalFileCollection::GetFileCount()
{
	return m_files.size();
}

void InternalFileCollection::Clear()
{
	m_rebuildFile = false;
	int len = m_files.size();
	for(int i=0; i<len; i++)
	{
		if(m_files[i]->Embedded)
			m_rebuildFile = true;

		delete m_files[i];
	}

	m_files.clear();
}

void InternalFileCollection::Add(EmbeddedFile* newFile)
{
	int len = m_files.size();
	for(int i=0; i<len; i++)
	{
		if(newFile->GetUID() == m_files[i]->GetUID())
		{
			newFile->SetNewUID(m_topId++);
		}
	}

	if(newFile->GetUID() >= m_topId)
		m_topId = newFile->GetUID() + 1;

	m_files.push_back(newFile);
}

int InternalFileCollection::Add(std::string fileName, std::string internalName)
{
	WIN32_FILE_ATTRIBUTE_DATA fileInfo;
	GetFileAttributesExA(fileName.c_str(), GetFileExInfoStandard, &fileInfo);

	EmbeddedFile* newFile = new EmbeddedFile(m_topId);
	newFile->FileName = fileName;
	newFile->InternalName = internalName;
	newFile->Embedded = false;
	newFile->FileAttributes = fileInfo.dwFileAttributes;

	BYTE* dst = (BYTE*)&newFile->OriginalLength;
	memcpy(dst, &fileInfo.nFileSizeLow, 4);
	memcpy(dst+4, &fileInfo.nFileSizeHigh, 4);

	memcpy(&newFile->AccessTimeUTC, &fileInfo.ftLastAccessTime, 8);
	memcpy(&newFile->ModifiedTimeUTC, &fileInfo.ftLastWriteTime, 8);
	memcpy(&newFile->CreationTimeUTC, &fileInfo.ftCreationTime, 8);

	m_files.push_back(newFile);

	return m_topId++;
}

void InternalFileCollection::Remove(int UID)
{
	std::vector<EmbeddedFile*>::iterator iter;
	for(iter = m_files.begin(); iter != m_files.end(); iter++)
	{
		if((*iter)->GetUID() == UID)
		{
			if((*iter)->Embedded)
				m_rebuildFile = true;

			delete (*iter);
			m_files.erase(iter);
			break;
		}
	}
}

bool InternalFileCollection::Rename(int UID, std::string newName)
{
	bool success = false;
	int len = m_files.size();
	for(int i=0; i<len; i++)
	{
		if(m_files[i]->GetUID() == UID)
		{
			m_files[i]->InternalName = newName;
			success = true;
			break;
		}
	}

	return success;
}

EmbeddedFile* InternalFileCollection::operator[] (int index)
{
	return m_files[index];
}

FileCollection::FileCollection(InternalFileCollection* source)
{
	m_source = source;
}

void FileCollection::SetSource(InternalFileCollection* source)
{
	m_source = source;
}

void FileCollection::Clear()
{
	m_source->Clear();
}

int FileCollection::Count()
{
	return m_source->GetFileCount();
}

int FileCollection::Add(std::string fileName, std::string internalName)
{
	return m_source->Add(fileName, internalName);
}

void FileCollection::Remove(int UID)
{
	m_source->Remove(UID);
}

bool FileCollection::Rename(int UID, std::string newName)
{
	return m_source->Rename(UID, newName);
}

IFile* FileCollection::Find(int UID)
{
	int len = m_source->GetFileCount();
	for(int i=0; i<len; i++)
	{
		if(m_source->operator[](i)->GetUID() == UID)
		{
			return m_source->operator[](i);
		}
	}

	return NULL;
}

IFile* FileCollection::Find(std::string name)
{
	int len = m_source->GetFileCount();
	const char* cName = name.c_str();
	for(int i=0; i<len; i++)
	{
		if(_stricmp(cName, m_source->operator[](i)->InternalName.c_str()) == 0)
		{
			return m_source->operator[](i);
		}
	}

	return NULL;
}