//=============================================================//
//                                                             //
//               CppDataLib::UnknownDataType.cpp               //
//   -------------------------------------------------------   //
//  Copyright � 2011 Anthony Mushrow, SKGenius Development     //
//  All rights reserved                                        //
//                                                             //
//  This code is licensed under The Code Project Open License  //
//  (CPOL) See http://www.codeproject.com/info/cpol10.aspx or  //
//  LICENSE.TXT for more information                           //
//                                                             //
//=============================================================//
#include "stdafx.h"
using namespace DataLib;

UnknownDataType::UnknownDataType(GUID guid)
{
	m_guid = guid;
	m_name = "Unknown";
	m_data = NULL;
	m_dataLen = 0;

	m_internalPropDesc = new EditablePropertyDescriptor(this);
	m_internalPropDesc->Name = "UnknownDataType";
	m_internalPropDesc->TextEdit = false;
	m_internalPropDesc->EditDialog = false;
}

UnknownDataType::~UnknownDataType()
{
	delete m_internalPropDesc;
	if(m_data != NULL)
		delete[] m_data;
}

std::string UnknownDataType::GetTypeName()
{
	return "UnkwnownDataType";
}

GUID UnknownDataType::GetTypeGUID()
{
	return m_guid;
}
std::string UnknownDataType::GetName()
{
	return m_name;
}

void UnknownDataType::SetName(std::string name)
{
	m_name = name;
}

std::string UnknownDataType::GetStringValue()
{
	return "[Unknown]";
}

const void* UnknownDataType::GetValuePtr()
{
	return NULL;
}

bool UnknownDataType::SetValue(std::string value)
{
	return false;
}

void UnknownDataType::ShowEditForm()
{

}

LPPROPDESC UnknownDataType::GetProperty()
{
	return m_internalPropDesc;
}

int UnknownDataType::GetID()
{
	return m_id;
}

void UnknownDataType::SetID(int id)
{
	m_id = id;
}

int UnknownDataType::GetSubProperties(LPPROPDESC** Props)
{
	*Props = NULL;
	return 0;
}

void UnknownDataType::WriteStream(IStream* stream)
{
	stream->Write(m_data, 0, m_dataLen);
}

void UnknownDataType::ReadStream(IStream* stream)
{

}

void UnknownDataType::SetData(BYTE* data, int datalen)
{
	m_data = data;
	m_dataLen = datalen;
}