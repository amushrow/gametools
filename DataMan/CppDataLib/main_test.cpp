#include "stdafx.h"

int main(int argc, TCHAR* argv[])
{
	//Read test
	/*DataLib::FileStream fs("D:\\Documents\\SAVETEST.skd", OPEN_ALWAYS, GENERIC_READ, 0);
	fs.ReadInt();
	fs.ReadLong();
	DataLib::DeflateStream ds(&fs, DataLib::Decompress, false);
	BYTE* buffer = new BYTE[4444444];
	if(ds.IsOpen())
	{
		int bytesRead = ds.Read(buffer, 0, 4444444);
		DataLib::FileStream out("D:\\Documents\\Disc_copy.iso", OPEN_ALWAYS, GENERIC_WRITE, 0);
		out.Write(buffer, 0, bytesRead);
	}*/

	//Write Test
	DataLib::FileStream in("D:\\Documents\\Disc.iso", OPEN_ALWAYS, GENERIC_READ, 0);
	DataLib::FileStream out("D:\\Documents\\Disc.iso.zlib", OPEN_ALWAYS, GENERIC_WRITE, 0);
	DataLib::DeflateStream ds(&out, DataLib::Compress, false);
	int len = (int)in.GetLength();
	BYTE* buffer = new BYTE[len];
	in.Read(buffer, 0, len);
	if(ds.IsOpen())
	{
		ds.Write(buffer, 0, len);
	}
	ds.Close();
	in.Close();

	return 0;
}