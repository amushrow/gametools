//=============================================================//
//                                                             //
//                CppDataLib::TypeCollection.cpp               //
//   -------------------------------------------------------   //
//  Copyright � 2011 Anthony Mushrow, SKGenius Development     //
//  All rights reserved                                        //
//                                                             //
//  This code is licensed under The Code Project Open License  //
//  (CPOL) See http://www.codeproject.com/info/cpol10.aspx or  //
//  LICENSE.TXT for more information                           //
//                                                             //
//=============================================================//
#include "stdafx.h"
using namespace DataLib;

TypeCollection::TypeCollection()
{
	for(int i=0; i<StandardTypes::TypeCollection.Count(); i++)
	{
		m_stdTypes.push_back( StandardTypes::TypeCollection[i] );
		m_allTypes.push_back( StandardTypes::TypeCollection[i] );
	}
}

void TypeCollection::Add(ITypeContainer* newType)
{
	bool exists = false;
	int len = Count();
	for(int i=0; i<len; i++)
	{
		if(newType->GetGUID() == m_allTypes[i]->GetGUID())
		{
			exists = true;
			break;
		}
	}

	if(!exists)
	{
		CustomTypeContainer* test = dynamic_cast<CustomTypeContainer*>(newType);
		if(test != NULL)
			m_custTypes.push_back(newType);
		else
			m_extTypes.push_back(newType);

		m_allTypes.push_back(newType);
	}
}

void TypeCollection::Remove(ITypeContainer* type)
{
	std::vector<ITypeContainer*>::iterator iter;
	bool haveType = false;
	//--Remove from all types
	for(iter = m_allTypes.begin(); iter != m_allTypes.end(); iter++)
	{
		if(*iter == type)
		{
			m_allTypes.erase(iter);
			haveType = true;
			delete type;
			break;
		}
	}
	//--

	//--We don't know which group this type belongs to, so we have to
	// check all of them
	if(haveType)
	{
		for(iter = m_stdTypes.begin(); iter != m_stdTypes.end(); iter++)
		{
			if(*iter == type)
			{
				m_stdTypes.erase(iter);
				return;
			}
		}
		for(iter = m_custTypes.begin(); iter != m_custTypes.end(); iter++)
		{
			if(*iter == type)
			{
				m_custTypes.erase(iter);
				return;
			}
		}
		for(iter = m_extTypes.begin(); iter != m_extTypes.end(); iter++)
		{
			if(*iter == type)
			{
				m_extTypes.erase(iter);
				return;
			}
		}
	}
	//--		
}

void TypeCollection::Clear(bool extended)
{
	//--Remove only, ext and cust types
	unsigned len = m_custTypes.size();
	for(unsigned i=0; i<len; i++)
	{
		//Remove from all types
		std::vector<ITypeContainer*>::iterator iter;
		for(iter = m_allTypes.begin(); iter != m_allTypes.end(); iter++)
		{
			if(*iter == m_custTypes[i])
			{
				m_allTypes.erase(iter);
				delete m_custTypes[i];
				break;
			}
		}

	}

	if(extended)
	{
		len = m_extTypes.size();
		for(unsigned i=0; i<len; i++)
		{
			//Remove from all types
			std::vector<ITypeContainer*>::iterator iter;
			for(iter = m_allTypes.begin(); iter != m_allTypes.end(); iter++)
			{
				if(*iter == m_extTypes[i])
				{
					m_allTypes.erase(iter);
					delete m_extTypes[i];
					break;
				}
			}

		}
	}
	//--

	//--Clear non standard types
	m_custTypes.clear();
	if(extended) m_extTypes.clear();
	//--
}

bool TypeCollection::FindType(const GUID& guid, ITypeContainer** container)
{
	*container = NULL;
	unsigned len = m_allTypes.size();
	for(unsigned i=0; i<len; i++)
	{
		if(m_allTypes[i]->GetGUID() == guid)
		{
			*container = m_allTypes[i];
			break;
		}
	}

	return (*container != NULL);
}