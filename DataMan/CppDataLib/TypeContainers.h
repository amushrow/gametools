//=============================================================//
//                                                             //
//                CppDataLib::TypeContainers.h                 //
//   -------------------------------------------------------   //
//  Copyright � 2011 Anthony Mushrow, SKGenius Development     //
//  All rights reserved                                        //
//                                                             //
//  This code is licensed under The Code Project Open License  //
//  (CPOL) See http://www.codeproject.com/info/cpol10.aspx or  //
//  LICENSE.TXT for more information                           //
//                                                             //
//=============================================================//
#pragma once
#include "stdafx.h"

namespace DataLib
{
	template <class T>
	class StandardTypeContainer : public ITypeContainer
	{
	public:
		StandardTypeContainer(std::string name, std::string description, GUID guid)
		{
			m_name = name;
			m_desc = description;
			m_guid = guid;
		}

		virtual std::string GetName()
		{
			return m_name;
		}

		virtual std::string GetDescription()
		{
			return m_desc;
		}

		virtual GUID GetGUID()
		{
			return m_guid;
		}

		virtual IDataType* CreateInstance()
		{
			return (IDataType*)new T();
		}

	private:
		std::string m_name;
		std::string m_desc;
		GUID		m_guid;
	};

	class CustomDataTypeDefinition;
	class TypeCollection;
	class CustomTypeContainer : public ITypeContainer
	{
	public:
		CustomTypeContainer(CustomDataTypeDefinition* Def, TypeCollection* AvailableTypes)
		{
			m_def = Def;
			m_availableTypes = AvailableTypes;
		}
		~CustomTypeContainer();

		virtual std::string GetName();
		virtual std::string GetDescription();
		virtual GUID		GetGUID();
		virtual IDataType*	CreateInstance();

		CustomDataTypeDefinition* GetDefinition()
		{
			return m_def;
		}

		TypeCollection* GetAvailableTypes()
		{
			return m_availableTypes;
		}

	private:
		CustomDataTypeDefinition* m_def;
		TypeCollection* m_availableTypes;
	};

	class UnknownTypeContainer : public ITypeContainer
	{
	public:
		UnknownTypeContainer(GUID guid);
		IDataType* CreateInstance();
		std::string GetDescription();
		std::string GetName();
		GUID		GetGUID();

	private:
		GUID m_guid;
	};
};