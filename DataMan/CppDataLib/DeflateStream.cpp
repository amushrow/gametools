//=============================================================//
//                                                             //
//                CppDataLib::DeflateStream.cpp                //
//   -------------------------------------------------------   //
//  Copyright � 2011 Anthony Mushrow, SKGenius Development     //
//  All rights reserved                                        //
//                                                             //
//  This code is licensed under The Code Project Open License  //
//  (CPOL) See http://www.codeproject.com/info/cpol10.aspx or  //
//  LICENSE.TXT for more information                           //
//                                                             //
//=============================================================//
#include "stdafx.h"
#define ZLIB_WINAPI 
#include "zlib.h"

using namespace DataLib;

DeflateStream::DeflateStream(IStream* source, CompressionMode type, bool leaveOpen)
{
	m_zlibStream = new z_stream_s();
	m_zlibStream->zalloc = Z_NULL;
	m_zlibStream->zfree = Z_NULL;
	m_zlibStream->opaque = Z_NULL;

	m_zlibStream->total_in = m_zlibStream->avail_in = 0;
	m_zlibStream->total_out = m_zlibStream->avail_out = 0;
	m_zlibStream->next_in = NULL;
	m_zlibStream->next_out = NULL;
	
	m_initialised = false;
	m_type = type;
	m_leaveOpen = leaveOpen;
	m_source = source;

	if(m_type == Decompress)
	{
		int err = inflateInit(m_zlibStream);
		if(err == Z_OK)
			m_initialised = true;
	}
	else if(m_type == Compress)
	{
		int err = deflateInit(m_zlibStream, Z_DEFAULT_COMPRESSION);
		if(err == Z_OK)
			m_initialised = true;
	}
}

DeflateStream::~DeflateStream()
{
	Close();
	if(!m_leaveOpen)
		delete m_source;

	delete m_zlibStream;
}

int DeflateStream::Read(BYTE* buffer, int offset, int count)
{
	int ret = Z_OK;
	int bytesRead = 0;
	int bytesRemain = count;
	int curOffset = offset;
	
	if(m_type == Decompress && m_initialised)
	{
		
		while(true)
		{
			while(m_zlibStream->avail_in > 0)
			{
				m_zlibStream->avail_out = bytesRemain;
				m_zlibStream->next_out = buffer+curOffset;

				ret = inflate(m_zlibStream, Z_NO_FLUSH);
				bytesRead = bytesRemain - m_zlibStream->avail_out;
				curOffset += bytesRead;
				bytesRemain -= bytesRead;

				//On error, or the end of the stream, escape
				switch (ret)
				{
					case Z_NEED_DICT:
					case Z_DATA_ERROR:
					case Z_MEM_ERROR:
					case Z_STREAM_END:
						inflateEnd(m_zlibStream);
						return count - bytesRemain;
				}

				//If we've filled our output buffer, escape
				if(bytesRemain == 0)
					return count;
			}

			m_zlibStream->avail_in = m_source->Read(m_buffer, 0, 4096);
			if(m_zlibStream->avail_in == 0)
				break;
			m_zlibStream->next_in = m_buffer;
		}
	}

	return count - bytesRemain;
}

BYTE DeflateStream::ReadByte()
{
	BYTE val;
	Read(&val, 0, 1);
	return val;
}

void DeflateStream::Write(BYTE* buffer, int offset, int count)
{
	int ret = Z_OK;
	int bytesOut = 0;
	
	if(m_type == Compress && m_initialised && count > 0)
	{	
		//--Write data
		m_zlibStream->avail_in = count;
		m_zlibStream->next_in = buffer+offset;

		while(m_zlibStream->avail_in > 0)
		{
			m_zlibStream->avail_out = 4096;
			m_zlibStream->next_out = m_buffer;
			ret = deflate(m_zlibStream, Z_NO_FLUSH);

			bytesOut = 4096 - m_zlibStream->avail_out;
			m_source->Write(m_buffer, 0, bytesOut);
		}
		//--
	}
}

void DeflateStream::WriteByte(BYTE val)
{
	Write(&val, 0, 1);
}

Int64 DeflateStream::GetLength()
{
	return -1;
}

Int64 DeflateStream::GetPosition()
{
	return -1;
}

void DeflateStream::SetPosition(Int64 pos)
{

}

bool DeflateStream::IsOpen()
{
	return m_initialised && m_source->IsOpen();
}

void DeflateStream::Close()
{
	if(m_type == Compress && m_initialised)
	{
		int err = 0;
		do
		{
			m_zlibStream->avail_out = 4096;
			m_zlibStream->next_out = m_buffer;

			err = deflate(m_zlibStream, Z_FINISH);

			int bytesOut = 4096 - m_zlibStream->avail_out;
			m_source->Write(m_buffer, 0, bytesOut);
		} while(err != Z_STREAM_END);

		deflateEnd(m_zlibStream);
	}

	if(!m_leaveOpen)
		m_source->Close();

	m_initialised = false;
}