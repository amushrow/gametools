//=============================================================//
//                                                             //
//                  CppDataLib::DataManager.h                  //
//   -------------------------------------------------------   //
//  Copyright � 2011 Anthony Mushrow, SKGenius Development     //
//  All rights reserved                                        //
//                                                             //
//  This code is licensed under The Code Project Open License  //
//  (CPOL) See http://www.codeproject.com/info/cpol10.aspx or  //
//  LICENSE.TXT for more information                           //
//                                                             //
//=============================================================//
#pragma once
#include "stdafx.h"

namespace DataLib
{
	#define DEFINITION_FILE 149941919
	#define DATA_FILE 30982696

	class DataManager : public IDataManager
	{
	public:
		DataManager();
		~DataManager();

		ITypeCollection&		GetTypes();
		IDataObjectCollection&	GetDataObjects();
		IFileCollection&		GetFiles();

		void					LoadDefinitions(std::string filePath);
		void					SaveDefinitions(std::string filePath);
		void					SaveToFile(std::string filePath);
		bool					LoadFromFile(std::string filePath);
		void					Clear();
		IStream*				GetFileStream(int UID);
		void					ExtractFile(int UID, std::string destination);

	private:
		std::string				getGhostFile(std::string baseName);
		TypeCollection			m_types;
		InternalFileCollection	m_files;
		DataObjectCollection	m_objects;
		FileCollection			m_filesWrapper;
		std::string				m_sourceFile;
	};
};