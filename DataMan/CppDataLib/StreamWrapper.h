//=============================================================//
//                                                             //
//                 CppDataLib::StreamWrapper.h                 //
//   -------------------------------------------------------   //
//  Copyright � 2011 Anthony Mushrow, SKGenius Development     //
//  All rights reserved                                        //
//                                                             //
//  This code is licensed under The Code Project Open License  //
//  (CPOL) See http://www.codeproject.com/info/cpol10.aspx or  //
//  LICENSE.TXT for more information                           //
//                                                             //
//=============================================================//
#pragma once
#include "stdafx.h"

namespace DataLib
{
	class BaseStream : public IStream
	{
	public:
		//--StreamWrapper functions
		virtual void		WriteShort(short value);
		virtual short		ReadShort();
		virtual void		WriteInt(int value);
		virtual int			ReadInt();
		virtual void		WriteFloat(float value);
		virtual float		ReadFloat();
		virtual void		WriteLong(Int64 value);
		virtual Int64		ReadLong();
		virtual void		WriteBool(bool value);
		virtual bool		ReadBool();
		virtual void		WriteString(std::string value);
		virtual std::string	ReadString();
		//--

		virtual void		Release()
		{
			//Kaboom
			delete this;
		}
	};
};