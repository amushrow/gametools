//=============================================================//
//                                                             //
//                  CppDataLib::SpriteData.h                   //
//   -------------------------------------------------------   //
//  Copyright � 2011 Anthony Mushrow, SKGenius Development     //
//  All rights reserved                                        //
//                                                             //
//  This code is licensed under The Code Project Open License  //
//  (CPOL) See http://www.codeproject.com/info/cpol10.aspx or  //
//  LICENSE.TXT for more information                           //
//                                                             //
//=============================================================//
#pragma once
#include "stdafx.h"

namespace DataLib
{
	class SpriteData
	{
	public:
		SpriteData();
		~SpriteData();

		struct Shape
		{
			float	X;
			float	Y;
			float	Width;
			float	Height;
			float	cX;
			float	cY;
		};

		SpriteData*		Clone();
		std::string		GetName()			{ return m_name; }
		std::string		GetTexName()		{ return m_texName; }
		std::string		GetTexPath()		{ return m_texPath; }
		float			GetWidth()			{ return m_width; }
		float			GetHeight()			{ return m_height; }
		float			GetFrameInterval()	{ return m_frameInterval; }
		int				GetNumFrames()		{ return m_numFrames; }
		const Shape*	GetFrameCoords()	{ return m_frameCoords; }
		void			WriteToStream(IStream* stream);

		static SpriteData*	ReadFromStream(IStream* stream);

	private:
		Shape*	m_frameCoords;
		int		m_numFrames;
		float	m_frameInterval;
		float	m_width;
		float	m_height;
		std::string	m_name;
		std::string	m_texName;
		std::string	m_texPath;
	};
}
