// stdafx.h : include file for standard system include files,
// or project specific include files that are used frequently, but
// are changed infrequently
//

#pragma once

#include "targetver.h"

#define WIN32_LEAN_AND_MEAN             // Exclude rarely-used stuff from Windows headers

#include <Windows.h>
#include <string>
#include <vector>

typedef long long Int64;
typedef unsigned long long UInt64;

#include "DataLib.h"
#include "StreamWrapper.h"
#include "FileStream.h"
#include "DeflateStream.h"
#include "EmbeddedFileStream.h"
#include "Property.h"
#include "CustomType.h"
#include "UnknownDataType.h"
#include "TypeContainers.h"
#include "TypeCollection.h"
#include "StandardTypes.h"
#include "FileCollection.h"
#include "DataObjectCollection.h"
#include "DataManager.h"


