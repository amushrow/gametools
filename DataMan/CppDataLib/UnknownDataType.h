//=============================================================//
//                                                             //
//                CppDataLib::UnknownDataType.h                //
//   -------------------------------------------------------   //
//  Copyright � 2011 Anthony Mushrow, SKGenius Development     //
//  All rights reserved                                        //
//                                                             //
//  This code is licensed under The Code Project Open License  //
//  (CPOL) See http://www.codeproject.com/info/cpol10.aspx or  //
//  LICENSE.TXT for more information                           //
//                                                             //
//=============================================================//
#pragma once
#include "stdafx.h"

namespace DataLib
{
	class UnknownDataType : public IDataType
	{
	public:
		UnknownDataType(GUID guid);
		~UnknownDataType();

		virtual std::string	GetTypeName();
		virtual GUID		GetTypeGUID();
		virtual std::string	GetName();
		virtual void		SetName(std::string name);
		virtual std::string	GetStringValue();
		virtual const void*	GetValuePtr();
		virtual bool		SetValue(std::string value);
		virtual int			GetID();
		virtual void		SetID(int id);
		virtual LPPROPDESC	GetProperty();
		virtual int			GetSubProperties(LPPROPDESC** Props);
		virtual void		ShowEditForm();
		virtual void		WriteStream(IStream* stream);
		virtual void		ReadStream(IStream* stream);

		void	SetData(BYTE* data, int length);

	private:
		GUID m_guid;
		std::string m_name;
		BYTE* m_data;
		int m_dataLen;
		int m_id;
		EditablePropertyDescriptor* m_internalPropDesc;
	};
};