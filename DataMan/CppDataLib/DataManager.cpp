//=============================================================//
//                                                             //
//                 CppDataLib::DataManager.cpp                 //
//   -------------------------------------------------------   //
//  Copyright � 2011 Anthony Mushrow, SKGenius Development     //
//  All rights reserved                                        //
//                                                             //
//  This code is licensed under The Code Project Open License  //
//  (CPOL) See http://www.codeproject.com/info/cpol10.aspx or  //
//  LICENSE.TXT for more information                           //
//                                                             //
//=============================================================//
#include "stdafx.h"
#include <algorithm>
#include <cctype>
#include <sys/stat.h> 

using namespace DataLib;

DataManager::DataManager()
{
	m_filesWrapper.SetSource(&m_files);
}

DataManager::~DataManager()
{
	m_types.Clear(true);
	m_objects.Clear();
	m_files.Clear();
}

ITypeCollection& DataManager::GetTypes()
{
	return m_types;
}

IDataObjectCollection& DataManager::GetDataObjects()
{
	return m_objects;
}

IFileCollection& DataManager::GetFiles()
{
	return m_filesWrapper;
}

void DataManager::LoadDefinitions(std::string filePath)
{
	FileStream fs(filePath, OPEN_EXISTING, GENERIC_READ, 0);
	int magic = fs.ReadInt();

	if(magic != DATA_FILE && magic != DEFINITION_FILE)
	{
		//Unable to load definitions
		fs.Close();
		return;
	}

	//--Skip ahead to the type definitions
	if(magic == DATA_FILE)
	{
		Int64 dataEnd = fs.ReadLong();
		fs.SetPosition(dataEnd);
		Int64 tableEnd = fs.ReadLong();
		fs.SetPosition(tableEnd);
	}
	//--

	int numTypeDefs = fs.ReadInt();
	for(int i=0; i<numTypeDefs; i++)
	{
		CustomDataTypeDefinition* typeDef = CustomDataTypeDefinition::ReadStream(&fs);
		CustomTypeContainer* newType = new CustomTypeContainer(typeDef, &m_types);
		m_types.Add(newType);
	}

	fs.Close();
}

void DataManager::SaveDefinitions(std::string filePath)
{
	FileStream fs(filePath, CREATE_ALWAYS, GENERIC_WRITE, 0);
	fs.WriteInt(DEFINITION_FILE);

	ReadOnlyCollection<ITypeContainer*> customTypes = m_types.GetCustomTypes();
	int numCustomTypes = customTypes.Count();
	fs.WriteInt(numCustomTypes);
	for(int i=0; i<numCustomTypes; i++)
	{
		CustomTypeContainer* container = (CustomTypeContainer*)customTypes[i];
		container->GetDefinition()->WriteStream(&fs);
	}

	fs.Close();
}

void DataManager::SaveToFile(std::string filePath)
{
	BYTE* buffer = new BYTE[4096];
	std::string ghostFilePath;
	FileStream* out_file = NULL;
	FileStream* source_file = NULL;
	bool copyEmbeddedFiles = false;
	bool usingGhostPath = false;

	//--Paths.ToUpper
	std::transform(m_sourceFile.begin(), m_sourceFile.end(), m_sourceFile.begin(), ::toupper);
	std::string upperPath = filePath;
	std::transform(upperPath.begin(), upperPath.end(), upperPath.begin(), ::toupper);
	//--

	//--Setup streams
	if(upperPath.compare(m_sourceFile) == 0 && !m_files.IsRebuildRequired())
	{
		out_file = new FileStream(filePath, OPEN_EXISTING, GENERIC_ALL, 0);
		copyEmbeddedFiles = false;
		usingGhostPath = false;
	}
	else
	{
		ghostFilePath = getGhostFile(filePath);
		out_file = new FileStream(ghostFilePath, CREATE_ALWAYS, GENERIC_WRITE, 0);
		usingGhostPath = true;

		if(m_sourceFile.length() > 0)
			copyEmbeddedFiles = true;
		else
			copyEmbeddedFiles = false;
	}
	//--

	//--Check file format etc.
	if(!usingGhostPath)
	{
		int magic = out_file->ReadInt();
		if(magic == DATA_FILE)
		{
			Int64 dataEnd = out_file->ReadLong();
			out_file->SetPosition(dataEnd);
		}
		else
		{
			//Invalid file format, so we're just going to overwrite the whole
			//file
			out_file->SetPosition(0);
			out_file->WriteInt(DATA_FILE);
			out_file->WriteLong(0);
		}
	}
	else
	{
		//Reserve data
		out_file->WriteInt(DATA_FILE);
		out_file->WriteLong(0);
	}
	//--

	if(copyEmbeddedFiles)
	{
		source_file = new FileStream(m_sourceFile, OPEN_EXISTING, GENERIC_READ, 0);
		if(!source_file->IsOpen())
		{
			copyEmbeddedFiles = false;
		}
	}

	int fileCount = m_files.GetFileCount();
	for(int i=0; i<fileCount; i++)
	{
		if(m_files[i]->Embedded && copyEmbeddedFiles)
		{
			source_file->SetPosition(m_files[i]->DataStart);
			Int64 bytesToRead = m_files[i]->CompressedLength;

			while(bytesToRead > 0)
			{
				int bytesRead = source_file->Read(buffer, 0, 4096);
				int bytesToWrite = bytesRead;
				if (bytesToRead < bytesRead) {
					bytesToWrite = (int)bytesToRead;
					bytesToRead = 0;
				}
				else {
					bytesToRead -= bytesRead;
				}

				out_file->Write(buffer, 0, bytesToWrite);
			}
		}
		else if(!m_files[i]->Embedded)
		{
			//--Compress and embed file
			FileStream externalFile(m_files[i]->FileName, OPEN_EXISTING, GENERIC_READ, 0);
			DeflateStream compress(out_file, Compress, true);

			m_files[i]->DataStart = out_file->GetPosition();
			m_files[i]->OriginalLength = externalFile.GetLength();

			Int64 bytesWritten = 0;
			while(bytesWritten < m_files[i]->OriginalLength)
			{
				int bytesRead = externalFile.Read(buffer, 0, 4096);
				compress.Write(buffer, 0, bytesRead);
				bytesWritten += bytesRead;
			}
			compress.Close();
			externalFile.Close();

			m_files[i]->CompressedLength = out_file->GetPosition() - m_files[i]->DataStart;
			m_files[i]->Embedded = true;
			//--
		}
	}

	Int64 dataLength = out_file->GetPosition();
	out_file->SetPosition(4);
	out_file->WriteLong(dataLength);
	out_file->SetPosition(dataLength);

	//--Write out file table
	Int64 tableStart = dataLength;
	out_file->WriteLong(0);
	out_file->WriteInt( fileCount );
	for(int i=0; i<fileCount; i++)
	{
		out_file->WriteInt(m_files[i]->GetUID());
		out_file->WriteString(m_files[i]->InternalName);
		out_file->WriteLong(m_files[i]->DataStart);
		out_file->WriteLong(m_files[i]->CompressedLength);
		out_file->WriteLong(m_files[i]->OriginalLength);
		out_file->WriteInt(m_files[i]->FileAttributes);
		out_file->WriteLong(m_files[i]->CreationTimeUTC);
		out_file->WriteLong(m_files[i]->ModifiedTimeUTC);
		out_file->WriteLong(m_files[i]->AccessTimeUTC);
	}

	Int64 tableEnd = out_file->GetPosition();
	out_file->SetPosition(tableStart);
	out_file->WriteLong(tableEnd);
	out_file->SetPosition(tableEnd);
	//--

	//--Save defs
	ReadOnlyCollection<ITypeContainer*> customTypes = m_types.GetCustomTypes();
	int numDefs = customTypes.Count();
	out_file->WriteInt(numDefs);
	for(int i=0; i<numDefs; i++)
	{
		CustomTypeContainer* container = (CustomTypeContainer*)customTypes[i];
		container->GetDefinition()->WriteStream(out_file);
	}
	//--

	ReadOnlyCollection<GUID> groups = m_objects.GetGroups();
	int numGroups = groups.Count();
	out_file->WriteInt(numGroups);
	for(int i=0; i<numGroups; i++)
	{
		out_file->Write((BYTE*)&groups[i], 0, 16);
		ReadOnlyCollection<IDataType*> objects = m_objects[groups[i]];
		out_file->WriteInt(objects.Count());

		for(int j=0; j<objects.Count(); j++)
		{
			out_file->WriteString(objects[j]->GetName());
			out_file->WriteInt(objects[j]->GetID());

			Int64 startpos = out_file->GetPosition();
			out_file->WriteInt(0);
			objects[j]->WriteStream(out_file);
			Int64 endpos = out_file->GetPosition();
			out_file->SetPosition(startpos);
			out_file->WriteInt( (int)(endpos - startpos) );
			out_file->SetPosition(endpos);
		}
	}

	out_file->Close();
	if(copyEmbeddedFiles)
	{
		source_file->Close();
		delete source_file;
	}

	if(usingGhostPath)
	{
		m_sourceFile = ghostFilePath;
		int attribs = GetFileAttributesA(ghostFilePath.c_str());
		attribs = attribs & ~FILE_ATTRIBUTE_HIDDEN;
		SetFileAttributesA(ghostFilePath.c_str(), attribs);

		struct stat fileInfo;
		int err = stat(filePath.c_str(), &fileInfo);
		if(err == 0)
		{
			attribs = GetFileAttributesA(filePath.c_str());
			SetFileAttributesA(ghostFilePath.c_str(), attribs);

			remove(filePath.c_str());
		}
		rename(ghostFilePath.c_str(), filePath.c_str());
	}

	delete[] buffer;
	delete out_file;
}

bool DataManager::LoadFromFile(std::string filePath)
{
	FileStream in(filePath.c_str(), OPEN_EXISTING, GENERIC_READ, 0);

	m_types.Clear(false);
	m_objects.Clear();
	m_files.Clear();

	int magic = in.ReadInt();
	if(magic != DATA_FILE)
	{
		in.Close();
		return false;
	}

	//--Load file table
	Int64 dataEnd = in.ReadLong();
	in.SetPosition(dataEnd);

	Int64 tableEnd = in.ReadLong();
	int numFiles = in.ReadInt();
	for(int i=0; i<numFiles; i++)
	{
		int uid = in.ReadInt();

		EmbeddedFile* newFile = new EmbeddedFile(uid);
		newFile->InternalName = in.ReadString();
		newFile->DataStart = in.ReadLong();
		newFile->CompressedLength = in.ReadLong();
		newFile->OriginalLength = in.ReadLong();
		newFile->FileAttributes = in.ReadInt();
		newFile->CreationTimeUTC = in.ReadLong();
		newFile->ModifiedTimeUTC = in.ReadLong();
		newFile->AccessTimeUTC = in.ReadLong();

		newFile->Embedded = true;
		m_files.Add(newFile);
	}
	//--

	//--Load Definitions
	int numDefs = in.ReadInt();
	for(int i=0; i<numDefs; i++)
	{
		CustomDataTypeDefinition* typeDef = CustomDataTypeDefinition::ReadStream(&in);
		CustomTypeContainer* newType = new CustomTypeContainer(typeDef, &m_types);
		m_types.Add(newType);
	}
	//--

	//--Load Data
	int numTypes = in.ReadInt();
	for(int i=0; i<numTypes; i++)
	{
		GUID typeGuid;
		in.Read((BYTE*)&typeGuid, 0, 16);

		bool unknown = false;
		ITypeContainer* container;
		if(!m_types.FindType(typeGuid, &container))
		{
			container = new UnknownTypeContainer(typeGuid);
			unknown = true;
		}

		int numDataObjects = in.ReadInt();
		for(int j=0; j<numDataObjects; j++)
		{
			IDataType* newData = container->CreateInstance();
			newData->SetName( in.ReadString() );
			newData->SetID( in.ReadInt() );

			int length = in.ReadInt();
			if(unknown)
			{
				BYTE* data = new BYTE[length];
				in.Read(data, 0, length);
				((UnknownDataType*)newData)->SetData(data, length);
			}
			else
			{
				newData->ReadStream(&in);
			}

			m_objects.Add(newData);
		}
	}
	//--

	m_sourceFile = filePath;
	in.Close();

	return true;
}

IStream* DataManager::GetFileStream(int UID)
{
	EmbeddedFileStream* ResultStream = NULL;
	EmbeddedFile* target = NULL;

	int numFiles = m_files.GetFileCount();
	for(int i=0; i<numFiles; i++)
	{
		if(m_files[i]->GetUID() == UID)
		{
			target = m_files[i];
			break;
		}
	}

	if(target != NULL)
	{
		if(target->Embedded)
		{
			if(m_sourceFile.length() > 0)
			{
				FileStream* fs = new FileStream(m_sourceFile, OPEN_EXISTING, GENERIC_READ, FILE_SHARE_READ);
				fs->SetPosition(target->DataStart);
				DeflateStream* ds = new DeflateStream(fs, Decompress, false);
				ResultStream = new EmbeddedFileStream(ds, target->OriginalLength);
			}
		}
		else
		{
			FileStream* fs = new FileStream(target->FileName, OPEN_EXISTING, GENERIC_READ, 0);
			ResultStream = new EmbeddedFileStream(fs, fs->GetLength());
		}
	}

	return ResultStream;
}

void DataManager::ExtractFile(int UID, std::string destination)
{
	IStream* targetStream = GetFileStream(UID);
	if(targetStream != NULL)
	{
		FileStream fs(destination, CREATE_ALWAYS, GENERIC_WRITE, 0);
		BYTE* buffer = new BYTE[4096];

		int bytesRead = 0;
		do
		{
			bytesRead = targetStream->Read(buffer, 0, 4096);
			fs.Write(buffer, 0, bytesRead);

		}while(bytesRead > 0);

		fs.Close();
		targetStream->Close();
		delete targetStream;
		delete[] buffer;
	}
}

void DataManager::Clear()
{
	m_types.Clear();
	m_files.Clear();
	m_objects.Clear();
	m_sourceFile = "";
}

std::string DataManager::getGhostFile(std::string baseName)
{
	std::string ghostFilePath;
	int slash = baseName.find_last_of('\\');
	std::string targetDir = baseName.substr(0, slash);
	std::string name = baseName.substr(slash+1);

	int i=0;
	char* ghostFile = new char[MAX_PATH];

	sprintf_s(ghostFile, MAX_PATH, "%s\\%s_%i", targetDir.c_str(), name.c_str(), i++);
	struct stat fileInfo;
	while(stat(ghostFile, &fileInfo) == 0)
	{
		sprintf_s(ghostFile, MAX_PATH, "%s\\%s_%i", targetDir.c_str(), name.c_str(), i++);
	}

	ghostFilePath = ghostFile;
	delete[] ghostFile;

	return ghostFilePath;
}