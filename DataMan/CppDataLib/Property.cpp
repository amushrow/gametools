//=============================================================//
//                                                             //
//                  CppDataLib::Property.cpp                   //
//   -------------------------------------------------------   //
//  Copyright � 2011 Anthony Mushrow, SKGenius Development     //
//  All rights reserved                                        //
//                                                             //
//  This code is licensed under The Code Project Open License  //
//  (CPOL) See http://www.codeproject.com/info/cpol10.aspx or  //
//  LICENSE.TXT for more information                           //
//                                                             //
//=============================================================//
#include "stdafx.h"
using namespace DataLib;

EditablePropertyDescriptor::EditablePropertyDescriptor(IDataType* dataType)
{
	m_dataType = dataType;
}
		
IDataType*	EditablePropertyDescriptor::GetIDataType()
{
	return m_dataType;
}


bool EditablePropertyDescriptor::CanTextEdit()
{ 
	return TextEdit;
}

bool EditablePropertyDescriptor::HasEditDialog()
{
	return EditDialog;
}

std::string EditablePropertyDescriptor::GetName()
{
	return Name;
}

std::string EditablePropertyDescriptor::GetTypeName()
{
	return TypeName;
}