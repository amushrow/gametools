//=============================================================//
//                                                             //
//                CppDataLib::StandardTypes.cpp                //
//   -------------------------------------------------------   //
//  Copyright � 2011 Anthony Mushrow, SKGenius Development     //
//  All rights reserved                                        //
//                                                             //
//  This code is licensed under The Code Project Open License  //
//  (CPOL) See http://www.codeproject.com/info/cpol10.aspx or  //
//  LICENSE.TXT for more information                           //
//                                                             //
//=============================================================//
#include "stdafx.h"
#include <ObjBase.h>
#include <algorithm>
#include <cctype>
#include <sstream>
using namespace DataLib;

StandardTypes::_typeCollection StandardTypes::TypeCollection;

template <class T>
bool StrToSigned(const char* str, T* res)
{
	*res = 0;
	int i=0;
	bool neg = false;
	bool err = false;
	char cur = str[i++];

	if(cur == 0x00) {
		//zero length string
		return false;
	}
	
	if(cur == '-') {
		neg = true;
		cur = str[i++];
	}
	else if(cur == '+') {
		neg = false;
		cur = str[i++];
	}

	while(cur != 0x00)
	{
		if(cur >= '0' && cur <= '9')
		{
			*res *= 10;
			*res += (cur - '0');
		}
		else
		{
			*res = 0;
			err = true;
			break;
		}
		cur = str[i++];	
	}

	if(neg)
		*res *= -1;

	return !err;
}

template <class T>
bool StrToUnsigned(const char* str, T* res)
{
	*res = 0;
	int i=0;
	bool err = false;
	char cur = str[i++];

	if(cur == 0x00) {
		//zero length string
		return false;
	}

	while(cur != 0x00)
	{
		if(cur >= '0' && cur <= '9')
		{
			*res *= 10;
			*res += (cur - '0');
		}
		else
		{
			*res = 0;
			err = true;
			break;
		}
		cur = str[i++];	
	}

	return !err;
}


StandardTypes::_typeCollection::_typeCollection()
{
	m_count = 13;
	m_types = new ITypeContainer*[13];
	GUID tmpGuid;

#define SetTypeContainer(index, type, name, description, guid) CLSIDFromString(guid, &tmpGuid); \
	m_types[index] = new StandardTypeContainer<type>(name, description, tmpGuid); \

	SetTypeContainer(0,		Boolean,			"Boolean",	"Boolean value",							L"{E9B9875C-FA7D-4224-A30F-7F4DB77E9A59}");
	SetTypeContainer(1,		Unsigned8,			"Byte",		"8-bit unsigned integer",					L"{5FBF54F9-2960-4B04-91D5-008432151111}");
	SetTypeContainer(2,		Signed8,			"SByte",	"8-bit signed integer",						L"{3CAD0EB0-86E8-4484-B20A-1444A4B6A978}");
	SetTypeContainer(3,		Unsigned16,			"UInt16",	"16-bit unsigned integer",					L"{5293574C-8AA6-46E0-952A-30DDAE8AC168}");
	SetTypeContainer(4,		Signed16,			"Int16",	"16-bit signed integer",					L"{0874170E-6BF6-4125-8F04-A1587261FDD1}");
	SetTypeContainer(5,		Unsigned32,			"UInt32",	"32-bit unsigned integer",					L"{242DA566-B7BA-4624-8CC2-9D019E94796F}");
	SetTypeContainer(6,		Signed32,			"Int32",	"32-bit signed integer",					L"{DDDA40EE-6554-45BD-9DDF-CD191CC10EAE}");
	SetTypeContainer(7,		Unsigned64,			"UInt64",	"64-bit unsigned integer",					L"{65042023-B366-4AC6-A959-FE03F0AD0C9E}");
	SetTypeContainer(8,		Signed64,			"Int64",	"64-bit signed integer",					L"{47B8BA1C-D424-4D53-9786-99FF886BFAA0}");
	SetTypeContainer(9,		SinglePrecision,	"Float",	"Single-precision floating-point number",	L"{F4D8AE27-C914-4EC3-8D25-F341AC24D5B0}");
	SetTypeContainer(10,	DoublePrecision,	"Double",	"Double-precision floating-point number",	L"{4CB9D9B9-DC49-40F4-8CF8-0983BA895204}");
	SetTypeContainer(11,	Text,				"String",	"Array of Unicode characters",				L"{DBEEB451-E406-47F9-8BDE-72BA94038AB7}");
	SetTypeContainer(12,	Sprite,				"Sprite",	"2D animated sprite",						L"{CB9EC996-4A46-4EDE-8ACE-6E287D5C3368}");

#undef SetTypeContainer
}

StandardTypes::_typeCollection::~_typeCollection()
{
	for(int i=0; i<m_count; i++)
		delete m_types[i];

	delete[] m_types;
}

int StandardTypes::_typeCollection::Count()
{
	return m_count;
}

ITypeContainer* StandardTypes::_typeCollection::operator[] (int index)
{
	if(index < m_count)
		return m_types[index];
		
	return NULL;
}

//--Boolean
Boolean::Boolean()
{
	m_internalPropDesc = new EditablePropertyDescriptor(this);
	m_internalPropDesc->Name = "Boolean";
	m_internalPropDesc->TextEdit = true;
	m_internalPropDesc->EditDialog = false;
}

Boolean::~Boolean()
{
	delete m_internalPropDesc;
}

std::string Boolean::GetTypeName()
{
	return "Boolean";
}

GUID Boolean::GetTypeGUID()
{
	return StandardTypes::TypeCollection[0]->GetGUID();
}

std::string Boolean::GetName()
{
	return m_internalPropDesc->Name;
}

void Boolean::SetName(std::string name)
{
	m_internalPropDesc->Name = name;
}

std::string Boolean::GetStringValue()
{
	if(m_val)
		return "True";
	else
		return "False";
}

const void* Boolean::GetValuePtr()
{
	return &m_val;
}

bool Boolean::SetValue(std::string value)
{
	//ToUpper
	std::transform(value.begin(), value.end(), value.begin(), ::toupper);

	bool result = true;
	if(value.compare("TRUE") == 0)
		m_val = true;
	else if(value.compare("FALSE") == 0)
		m_val = false;
	else
		result = false;

	return result;
}

int Boolean::GetID()
{
	return m_id;
}

void Boolean::SetID(int id)
{
	m_id = id;
}

LPPROPDESC Boolean::GetProperty()
{
	return m_internalPropDesc;
}

int Boolean::GetSubProperties(LPPROPDESC** Props)
{
	*Props = NULL;
	return 0;
}

void Boolean::ShowEditForm()
{

}

void Boolean::WriteStream(DataLib::IStream* stream)
{
	BYTE val = m_val ? 0x01 : 0x00;
	stream->WriteByte(val);
}

void Boolean::ReadStream(DataLib::IStream* stream)
{
	BYTE val = stream->ReadByte();
	m_val = (val != 0x00);
}

bool Boolean::GetValue()
{
	return m_val;
}

void Boolean::SetValue(bool val)
{
	m_val = val;
}
//--

//--Unsigned8
Unsigned8::Unsigned8()
{
	m_internalPropDesc = new EditablePropertyDescriptor(this);
	m_internalPropDesc->Name = "Byte";
	m_internalPropDesc->TextEdit = true;
	m_internalPropDesc->EditDialog = false;
}

Unsigned8::~Unsigned8()
{
	delete m_internalPropDesc;
}

std::string Unsigned8::GetTypeName()
{
	return "Byte";
}

GUID Unsigned8::GetTypeGUID()
{
	return StandardTypes::TypeCollection[1]->GetGUID();
}

std::string Unsigned8::GetName()
{
	return m_internalPropDesc->Name;
}

void Unsigned8::SetName(std::string name)
{
	m_internalPropDesc->Name = name;
}

std::string Unsigned8::GetStringValue()
{
	std::ostringstream ss;
	ss << m_val;
	
	return ss.str();
}

const void* Unsigned8::GetValuePtr()
{
	return &m_val;
}

bool Unsigned8::SetValue(std::string value)
{
	BYTE val = 0;
	if(StrToUnsigned<BYTE>(value.c_str(), &val)) {
		m_val = val;
		return true;
	}
	else
		return false;
}

int Unsigned8::GetID()
{
	return m_id;
}

void Unsigned8::SetID(int id)
{
	m_id = id;
}

LPPROPDESC Unsigned8::GetProperty()
{
	return m_internalPropDesc;
}

int Unsigned8::GetSubProperties(LPPROPDESC** Props)
{
	*Props = NULL;
	return 0;
}

void Unsigned8::ShowEditForm()
{

}

void Unsigned8::WriteStream(DataLib::IStream* stream)
{
	stream->WriteByte(m_val);
}

void Unsigned8::ReadStream(DataLib::IStream* stream)
{
	m_val = stream->ReadByte();
	BYTE representHex = stream->ReadByte();
}

BYTE Unsigned8::GetValue()
{
	return m_val;
}

void Unsigned8::SetValue(BYTE val)
{
	m_val = val;
}
//--

//--Signed8
Signed8::Signed8()
{
	m_internalPropDesc = new EditablePropertyDescriptor(this);
	m_internalPropDesc->Name = "SByte";
	m_internalPropDesc->TextEdit = true;
	m_internalPropDesc->EditDialog = false;
}

Signed8::~Signed8()
{
	delete m_internalPropDesc;
}

std::string Signed8::GetTypeName()
{
	return "SByte";
}

GUID Signed8::GetTypeGUID()
{
	return StandardTypes::TypeCollection[2]->GetGUID();
}

std::string Signed8::GetName()
{
	return m_internalPropDesc->Name;
}

void Signed8::SetName(std::string name)
{
	m_internalPropDesc->Name = name;
}

std::string Signed8::GetStringValue()
{
	std::ostringstream ss;
	ss << m_val;
	
	return ss.str();
}

const void* Signed8::GetValuePtr()
{
	return &m_val;
}

bool Signed8::SetValue(std::string value)
{
	char val = 0;
	if(StrToSigned<char>(value.c_str(), &val)) {
		m_val = val;
		return true;
	}
	else
		return false;
}

int Signed8::GetID()
{
	return m_id;
}

void Signed8::SetID(int id)
{
	m_id = id;
}

LPPROPDESC Signed8::GetProperty()
{
	return m_internalPropDesc;
}

int Signed8::GetSubProperties(LPPROPDESC** Props)
{
	*Props = NULL;
	return 0;
}

void Signed8::ShowEditForm()
{

}

void Signed8::WriteStream(DataLib::IStream* stream)
{
	stream->WriteByte(m_val);
}

void Signed8::ReadStream(DataLib::IStream* stream)
{
	m_val = stream->ReadByte();
	BYTE representHex = stream->ReadByte();
}

char Signed8::GetValue()
{
	return m_val;
}

void Signed8::SetValue(char val)
{
	m_val = val;
}
//--

//--Unsigned16
Unsigned16::Unsigned16()
{
	m_internalPropDesc = new EditablePropertyDescriptor(this);
	m_internalPropDesc->Name = "UInt16";
	m_internalPropDesc->TextEdit = true;
	m_internalPropDesc->EditDialog = false;
}

Unsigned16::~Unsigned16()
{
	delete m_internalPropDesc;
}

std::string Unsigned16::GetTypeName()
{
	return "UInt16";
}

GUID Unsigned16::GetTypeGUID()
{
	return StandardTypes::TypeCollection[3]->GetGUID();
}

std::string Unsigned16::GetName()
{
	return m_internalPropDesc->Name;
}

void Unsigned16::SetName(std::string name)
{
	m_internalPropDesc->Name = name;
}

std::string Unsigned16::GetStringValue()
{
	std::ostringstream ss;
	ss << m_val;
	
	return ss.str();
}

const void* Unsigned16::GetValuePtr()
{
	return &m_val;
}

bool Unsigned16::SetValue(std::string value)
{
	unsigned short val = 0;
	if(StrToUnsigned<unsigned short>(value.c_str(), &val)) {
		m_val = val;
		return true;
	}
	else
		return false;
}

int Unsigned16::GetID()
{
	return m_id;
}

void Unsigned16::SetID(int id)
{
	m_id = id;
}

LPPROPDESC Unsigned16::GetProperty()
{
	return m_internalPropDesc;
}

int Unsigned16::GetSubProperties(LPPROPDESC** Props)
{
	*Props = NULL;
	return 0;
}

void Unsigned16::ShowEditForm()
{

}

void Unsigned16::WriteStream(DataLib::IStream* stream)
{
	stream->Write((BYTE*)&m_val, 0, 2);
}

void Unsigned16::ReadStream(DataLib::IStream* stream)
{
	stream->Read((BYTE*)&m_val, 0, 2);
	BYTE representHex = stream->ReadByte();
}

unsigned short Unsigned16::GetValue()
{
	return m_val;
}

void Unsigned16::SetValue(unsigned short val)
{
	m_val = val;
}
//--

//--Signed16
Signed16::Signed16()
{
	m_internalPropDesc = new EditablePropertyDescriptor(this);
	m_internalPropDesc->Name = "Int16";
	m_internalPropDesc->TextEdit = true;
	m_internalPropDesc->EditDialog = false;
}

Signed16::~Signed16()
{
	delete m_internalPropDesc;
}

std::string Signed16::GetTypeName()
{
	return "Int16";
}

GUID Signed16::GetTypeGUID()
{
	return StandardTypes::TypeCollection[4]->GetGUID();
}

std::string Signed16::GetName()
{
	return m_internalPropDesc->Name;
}

void Signed16::SetName(std::string name)
{
	m_internalPropDesc->Name = name;
}

std::string Signed16::GetStringValue()
{
	std::ostringstream ss;
	ss << m_val;
	
	return ss.str();
}

const void* Signed16::GetValuePtr()
{
	return &m_val;
}

bool Signed16::SetValue(std::string value)
{
	short val = 0;
	if(StrToSigned<short>(value.c_str(), &val)) {
		m_val = val;
		return true;
	}
	else
		return false;
}

int Signed16::GetID()
{
	return m_id;
}

void Signed16::SetID(int id)
{
	m_id = id;
}

LPPROPDESC Signed16::GetProperty()
{
	return m_internalPropDesc;
}

int Signed16::GetSubProperties(LPPROPDESC** Props)
{
	*Props = NULL;
	return 0;
}

void Signed16::ShowEditForm()
{

}

void Signed16::WriteStream(DataLib::IStream* stream)
{
	stream->Write((BYTE*)&m_val, 0, 2);
}

void Signed16::ReadStream(DataLib::IStream* stream)
{
	stream->Read((BYTE*)&m_val, 0, 2);
	BYTE representHex = stream->ReadByte();
}

short Signed16::GetValue()
{
	return m_val;
}

void Signed16::SetValue(short val)
{
	m_val = val;
}
//--

//--Unsigned32
Unsigned32::Unsigned32()
{
	m_internalPropDesc = new EditablePropertyDescriptor(this);
	m_internalPropDesc->Name = "UInt32";
	m_internalPropDesc->TextEdit = true;
	m_internalPropDesc->EditDialog = false;
}

Unsigned32::~Unsigned32()
{
	delete m_internalPropDesc;
}

std::string Unsigned32::GetTypeName()
{
	return "UInt32";
}

GUID Unsigned32::GetTypeGUID()
{
	return StandardTypes::TypeCollection[5]->GetGUID();
}

std::string Unsigned32::GetName()
{
	return m_internalPropDesc->Name;
}

void Unsigned32::SetName(std::string name)
{
	m_internalPropDesc->Name = name;
}

std::string Unsigned32::GetStringValue()
{
	std::ostringstream ss;
	ss << m_val;
	
	return ss.str();
}

const void* Unsigned32::GetValuePtr()
{
	return &m_val;
}

bool Unsigned32::SetValue(std::string value)
{
	unsigned val = 0;
	if(StrToUnsigned<unsigned>(value.c_str(), &val)) {
		m_val = val;
		return true;
	}
	else
		return false;
}

int Unsigned32::GetID()
{
	return m_id;
}

void Unsigned32::SetID(int id)
{
	m_id = id;
}

LPPROPDESC Unsigned32::GetProperty()
{
	return m_internalPropDesc;
}

int Unsigned32::GetSubProperties(LPPROPDESC** Props)
{
	*Props = NULL;
	return 0;
}

void Unsigned32::ShowEditForm()
{

}

void Unsigned32::WriteStream(DataLib::IStream* stream)
{
	stream->Write((BYTE*)&m_val, 0, 4);
}

void Unsigned32::ReadStream(DataLib::IStream* stream)
{
	stream->Read((BYTE*)&m_val, 0, 4);
	BYTE representHex = stream->ReadByte();
}

unsigned Unsigned32::GetValue()
{
	return m_val;
}

void Unsigned32::SetValue(unsigned val)
{
	m_val = val;
}
//--

//--Signed32
Signed32::Signed32()
{
	m_internalPropDesc = new EditablePropertyDescriptor(this);
	m_internalPropDesc->Name = "Int32";
	m_internalPropDesc->TextEdit = true;
	m_internalPropDesc->EditDialog = false;
}

Signed32::~Signed32()
{
	delete m_internalPropDesc;
}

std::string Signed32::GetTypeName()
{
	return "Int32";
}

GUID Signed32::GetTypeGUID()
{
	return StandardTypes::TypeCollection[6]->GetGUID();
}

std::string Signed32::GetName()
{
	return m_internalPropDesc->Name;
}

void Signed32::SetName(std::string name)
{
	m_internalPropDesc->Name = name;
}

std::string Signed32::GetStringValue()
{
	std::ostringstream ss;
	ss << m_val;
	
	return ss.str();
}

const void* Signed32::GetValuePtr()
{
	return &m_val;
}

bool Signed32::SetValue(std::string value)
{
	unsigned val = 0;
	if(StrToSigned<unsigned>(value.c_str(), &val)) {
		m_val = val;
		return true;
	}
	else
		return false;
}

int Signed32::GetID()
{
	return m_id;
}

void Signed32::SetID(int id)
{
	m_id = id;
}

LPPROPDESC Signed32::GetProperty()
{
	return m_internalPropDesc;
}

int Signed32::GetSubProperties(LPPROPDESC** Props)
{
	*Props = NULL;
	return 0;
}

void Signed32::ShowEditForm()
{

}

void Signed32::WriteStream(DataLib::IStream* stream)
{
	stream->Write((BYTE*)&m_val, 0, 4);
}

void Signed32::ReadStream(DataLib::IStream* stream)
{
	stream->Read((BYTE*)&m_val, 0, 4);
	BYTE representHex = stream->ReadByte();
}

int Signed32::GetValue()
{
	return m_val;
}

void Signed32::SetValue(int val)
{
	m_val = val;
}
//--

//--Unsigned64
Unsigned64::Unsigned64()
{
	m_internalPropDesc = new EditablePropertyDescriptor(this);
	m_internalPropDesc->Name = "UInt64";
	m_internalPropDesc->TextEdit = true;
	m_internalPropDesc->EditDialog = false;
}

Unsigned64::~Unsigned64()
{
	delete m_internalPropDesc;
}

std::string Unsigned64::GetTypeName()
{
	return "UInt64";
}

GUID Unsigned64::GetTypeGUID()
{
	return StandardTypes::TypeCollection[7]->GetGUID();
}

std::string Unsigned64::GetName()
{
	return m_internalPropDesc->Name;
}

void Unsigned64::SetName(std::string name)
{
	m_internalPropDesc->Name = name;
}

std::string Unsigned64::GetStringValue()
{
	std::ostringstream ss;
	ss << m_val;
	
	return ss.str();
}

const void* Unsigned64::GetValuePtr()
{
	return &m_val;

}
bool Unsigned64::SetValue(std::string value)
{
	UInt64 val = 0;
	if(StrToUnsigned<UInt64>(value.c_str(), &val)) {
		m_val = val;
		return true;
	}
	else
		return false;
}

int Unsigned64::GetID()
{
	return m_id;
}

void Unsigned64::SetID(int id)
{
	m_id = id;
}

LPPROPDESC Unsigned64::GetProperty()
{
	return m_internalPropDesc;
}

int Unsigned64::GetSubProperties(LPPROPDESC** Props)
{
	*Props = NULL;
	return 0;
}

void Unsigned64::ShowEditForm()
{

}

void Unsigned64::WriteStream(DataLib::IStream* stream)
{
	stream->Write((BYTE*)&m_val, 0, 8);
}

void Unsigned64::ReadStream(DataLib::IStream* stream)
{
	stream->Read((BYTE*)&m_val, 0, 8);
	BYTE representHex = stream->ReadByte();
}

UInt64 Unsigned64::GetValue()
{
	return m_val;
}

void Unsigned64::SetValue(UInt64 val)
{
	m_val = val;
}
//--

//--Signed64
Signed64::Signed64()
{
	m_internalPropDesc = new EditablePropertyDescriptor(this);
	m_internalPropDesc->Name = "Int64";
	m_internalPropDesc->TextEdit = true;
	m_internalPropDesc->EditDialog = false;
}

Signed64::~Signed64()
{
	delete m_internalPropDesc;
}

std::string Signed64::GetTypeName()
{
	return "Int64";
}

GUID Signed64::GetTypeGUID()
{
	return StandardTypes::TypeCollection[8]->GetGUID();
}

std::string Signed64::GetName()
{
	return m_internalPropDesc->Name;
}

void Signed64::SetName(std::string name)
{
	m_internalPropDesc->Name = name;
}

std::string Signed64::GetStringValue()
{
	std::ostringstream ss;
	ss << m_val;
	
	return ss.str();
}

const void* Signed64::GetValuePtr()
{
	return &m_val;
}

bool Signed64::SetValue(std::string value)
{
	UInt64 val = 0;
	if(StrToSigned<UInt64>(value.c_str(), &val)) {
		m_val = val;
		return true;
	}
	else
		return false;
}

int Signed64::GetID()
{
	return m_id;
}

void Signed64::SetID(int id)
{
	m_id = id;
}

LPPROPDESC Signed64::GetProperty()
{
	return m_internalPropDesc;
}

int Signed64::GetSubProperties(LPPROPDESC** Props)
{
	*Props = NULL;
	return 0;
}

void Signed64::ShowEditForm()
{

}

void Signed64::WriteStream(DataLib::IStream* stream)
{
	stream->Write((BYTE*)&m_val, 0, 8);
}

void Signed64::ReadStream(DataLib::IStream* stream)
{
	stream->Read((BYTE*)&m_val, 0, 8);
	BYTE representHex = stream->ReadByte();
}

Int64 Signed64::GetValue()
{
	return m_val;
}

void Signed64::SetValue(Int64 val)
{
	m_val = val;
}
//--

//--SinglePrecision
SinglePrecision::SinglePrecision()
{
	m_internalPropDesc = new EditablePropertyDescriptor(this);
	m_internalPropDesc->Name = "Float";
	m_internalPropDesc->TextEdit = true;
	m_internalPropDesc->EditDialog = false;
}

SinglePrecision::~SinglePrecision()
{
	delete m_internalPropDesc;
}

std::string SinglePrecision::GetTypeName()
{
	return "Float";
}

GUID SinglePrecision::GetTypeGUID()
{
	return StandardTypes::TypeCollection[9]->GetGUID();
}

std::string SinglePrecision::GetName()
{
	return m_internalPropDesc->Name;
}

void SinglePrecision::SetName(std::string name)
{
	m_internalPropDesc->Name = name;
}

std::string SinglePrecision::GetStringValue()
{
	std::ostringstream ss;
	ss << m_val;
	
	return ss.str();
}

const void* SinglePrecision::GetValuePtr()
{
	return &m_val;
}

bool SinglePrecision::SetValue(std::string value)
{
	double val = atof(value.c_str());
	bool success = true;

	//check output
	if(val == 0)
	{
		for(unsigned i=0; i<value.size(); i++)
		{
			if(value[i] != '0' && value[i] != '.')
			{
				success = false;
				break;
			}
		}
	}
	if(success)
		m_val = (float)val;

	return success;
}

int SinglePrecision::GetID()
{
	return m_id;
}

void SinglePrecision::SetID(int id)
{
	m_id = id;
}

LPPROPDESC SinglePrecision::GetProperty()
{
	return m_internalPropDesc;
}

int SinglePrecision::GetSubProperties(LPPROPDESC** Props)
{
	*Props = NULL;
	return 0;
}

void SinglePrecision::ShowEditForm()
{

}

void SinglePrecision::WriteStream(DataLib::IStream* stream)
{
	stream->Write((BYTE*)&m_val, 0, 4);
}

void SinglePrecision::ReadStream(DataLib::IStream* stream)
{
	stream->Read((BYTE*)&m_val, 0, 4);
}

float SinglePrecision::GetValue()
{
	return m_val;
}

void SinglePrecision::SetValue(float val)
{
	m_val = val;
}
//--

//--DoublePrecision
DoublePrecision::DoublePrecision()
{
	m_internalPropDesc = new EditablePropertyDescriptor(this);
	m_internalPropDesc->Name = "Double";
	m_internalPropDesc->TextEdit = true;
	m_internalPropDesc->EditDialog = false;
}

DoublePrecision::~DoublePrecision()
{
	delete m_internalPropDesc;
}

std::string DoublePrecision::GetTypeName()
{
	return "Double";
}

GUID DoublePrecision::GetTypeGUID()
{
	return StandardTypes::TypeCollection[10]->GetGUID();
}

std::string DoublePrecision::GetName()
{
	return m_internalPropDesc->Name;
}

void DoublePrecision::SetName(std::string name)
{
	m_internalPropDesc->Name = name;
}

std::string DoublePrecision::GetStringValue()
{
	std::ostringstream ss;
	ss << m_val;
	
	return ss.str();
}

const void* DoublePrecision::GetValuePtr()
{
	return &m_val;
}

bool DoublePrecision::SetValue(std::string value)
{
	double val = atof(value.c_str());
	bool success = true;

	//check output
	if(val == 0)
	{
		for(unsigned i=0; i<value.size(); i++)
		{
			if(value[i] != '0' && value[i] != '.')
			{
				success = false;
				break;
			}
		}
	}
	if(success)
		m_val = val;

	return success;
}

int DoublePrecision::GetID()
{
	return m_id;
}

void DoublePrecision::SetID(int id)
{
	m_id = id;
}

LPPROPDESC DoublePrecision::GetProperty()
{
	return m_internalPropDesc;
}

int DoublePrecision::GetSubProperties(LPPROPDESC** Props)
{
	*Props = NULL;
	return 0;
}

void DoublePrecision::ShowEditForm()
{

}

void DoublePrecision::WriteStream(DataLib::IStream* stream)
{
	stream->Write((BYTE*)&m_val, 0, 8);
}

void DoublePrecision::ReadStream(DataLib::IStream* stream)
{
	stream->Read((BYTE*)&m_val, 0, 8);
}

double DoublePrecision::GetValue()
{
	return m_val;
}

void DoublePrecision::SetValue(double val)
{
	m_val = val;
}
//--

//--Text
Text::Text()
{
	m_internalPropDesc = new EditablePropertyDescriptor(this);
	m_internalPropDesc->Name = "String";
	m_internalPropDesc->TextEdit = true;
	m_internalPropDesc->EditDialog = false;
}

Text::~Text()
{
	delete m_internalPropDesc;
}

std::string Text::GetTypeName()
{
	return "String";
}

GUID Text::GetTypeGUID()
{
	return StandardTypes::TypeCollection[11]->GetGUID();
}

std::string Text::GetName()
{
	return m_internalPropDesc->Name;
}

void Text::SetName(std::string name)
{
	m_internalPropDesc->Name = name;
}

std::string Text::GetStringValue()
{
	return m_val;
}

const void* Text::GetValuePtr()
{
	return &m_val;
}

bool Text::SetValue(std::string value)
{
	m_val = value;
	return true;
}

int Text::GetID()
{
	return m_id;
}

void Text::SetID(int id)
{
	m_id = id;
}

LPPROPDESC Text::GetProperty()
{
	return m_internalPropDesc;
}

int Text::GetSubProperties(LPPROPDESC** Props)
{
	*Props = NULL;
	return 0;
}

void Text::ShowEditForm()
{

}

void Text::WriteStream(DataLib::IStream* stream)
{
	stream->WriteString(m_val);
}

void Text::ReadStream(DataLib::IStream* stream)
{
	m_val = stream->ReadString();
}
//--

//--Sprite
Sprite::Sprite()
{
	m_internalPropDesc = new EditablePropertyDescriptor(this);
	m_internalPropDesc->Name = "Sprite";
	m_internalPropDesc->TextEdit = false;
	m_internalPropDesc->EditDialog = false;

	m_val = new SpriteData();
}

Sprite::~Sprite()
{
	delete m_internalPropDesc;
	delete m_val;
}

std::string Sprite::GetTypeName()
{
	return "Sprite";
}

GUID Sprite::GetTypeGUID()
{
	return StandardTypes::TypeCollection[12]->GetGUID();
}

std::string Sprite::GetName()
{
	return m_internalPropDesc->Name;
}

void Sprite::SetName(std::string name)
{
	m_internalPropDesc->Name = name;
}

std::string Sprite::GetStringValue()
{
	return "[Sprite]";
}

const void* Sprite::GetValuePtr()
{
	return m_val;
}

bool Sprite::SetValue(std::string value)
{
	return false;
}

int Sprite::GetID()
{
	return m_id;
}

void Sprite::SetID(int id)
{
	m_id = id;
}

LPPROPDESC Sprite::GetProperty()
{
	return m_internalPropDesc;
}

int Sprite::GetSubProperties(LPPROPDESC** Props)
{
	*Props = NULL;
	return 0;
}

void Sprite::ShowEditForm()
{

}

void Sprite::WriteStream(DataLib::IStream* stream)
{
	m_val->WriteToStream(stream);
}

void Sprite::ReadStream(DataLib::IStream* stream)
{
	delete m_val;
	m_val = SpriteData::ReadFromStream(stream);
}

void Sprite::SetValue(SpriteData* data)
{
	delete m_val;
	m_val = data;
}

SpriteData* Sprite::GetValue()
{
	return m_val;
}
//--