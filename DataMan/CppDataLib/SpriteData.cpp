//=============================================================//
//                                                             //
//                  CppDataLib::SpriteData.cpp                 //
//   -------------------------------------------------------   //
//  Copyright � 2011 Anthony Mushrow, SKGenius Development     //
//  All rights reserved                                        //
//                                                             //
//  This code is licensed under The Code Project Open License  //
//  (CPOL) See http://www.codeproject.com/info/cpol10.aspx or  //
//  LICENSE.TXT for more information                           //
//                                                             //
//=============================================================//
#include "stdafx.h"
using namespace DataLib;

SpriteData::SpriteData()
{
	m_frameCoords = NULL;
	m_numFrames = 0;

	m_width = 0;
	m_height = 0;
	m_frameInterval = 0.0166667f;
	m_name = "DefaultName";
	m_texName = "";
	m_texPath = "";
}

SpriteData::~SpriteData()
{
	if(m_frameCoords != NULL)
		delete[] m_frameCoords;
}

SpriteData* SpriteData::Clone()
{
	SpriteData* newData = new SpriteData();

	newData->m_name = std::string(m_name);
	newData->m_texName = std::string(m_texName);
	newData->m_texPath = std::string(m_texPath);
	newData->m_frameInterval = m_frameInterval;
	newData->m_width = m_width;
	newData->m_height = m_height;
	newData->m_numFrames = m_numFrames;
	newData->m_frameCoords = new Shape[m_numFrames];
	for(int i=0; i<m_numFrames; i++)
	{
		newData->m_frameCoords[i] = m_frameCoords[i];
	}

	return newData;
}

SpriteData* SpriteData::ReadFromStream(IStream* stream)
{
	SpriteData* newData = new SpriteData();

	newData->m_name = stream->ReadString();
	newData->m_texName = stream->ReadString();
	newData->m_texPath = stream->ReadString();
	newData->m_width = stream->ReadFloat();
	newData->m_height = stream->ReadFloat();
	newData->m_frameInterval = stream->ReadFloat();
	newData->m_numFrames = stream->ReadInt();
	newData->m_frameCoords = new Shape[newData->m_numFrames];
	
	for(int i=0; i<newData->m_numFrames; i++)
	{
		newData->m_frameCoords[i].cX = stream->ReadFloat();
		newData->m_frameCoords[i].cY = stream->ReadFloat();
		newData->m_frameCoords[i].X = stream->ReadFloat();
		newData->m_frameCoords[i].Y = stream->ReadFloat();
		newData->m_frameCoords[i].Width = stream->ReadFloat();
		newData->m_frameCoords[i].Height = stream->ReadFloat();
	}

	return newData;
}

void SpriteData::WriteToStream(IStream* stream)
{
	stream->WriteString(m_name);
	stream->WriteString(m_texName);
	stream->WriteString(m_texPath);
	stream->WriteFloat(m_width);
	stream->WriteFloat(m_height);
	stream->WriteFloat(m_frameInterval);
	stream->WriteInt(m_numFrames);
	
	for(int i=0; i<m_numFrames; i++)
	{
		stream->WriteFloat(m_frameCoords[i].cX);
		stream->WriteFloat(m_frameCoords[i].cY);
		stream->WriteFloat(m_frameCoords[i].X);
		stream->WriteFloat(m_frameCoords[i].Y);
		stream->WriteFloat(m_frameCoords[i].Width);
		stream->WriteFloat(m_frameCoords[i].Height);
	}
}