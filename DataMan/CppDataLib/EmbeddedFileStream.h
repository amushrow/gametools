//=============================================================//
//                                                             //
//              CppDataLib::EmbeddedFileStream.h               //
//   -------------------------------------------------------   //
//  Copyright � 2011 Anthony Mushrow, SKGenius Development     //
//  All rights reserved                                        //
//                                                             //
//  This code is licensed under The Code Project Open License  //
//  (CPOL) See http://www.codeproject.com/info/cpol10.aspx or  //
//  LICENSE.TXT for more information                           //
//                                                             //
//=============================================================//
#pragma once
#include "stdafx.h"

namespace DataLib
{
	class EmbeddedFileStream : public BaseStream
	{
	public:
		EmbeddedFileStream(IStream* content, Int64 length);
		~EmbeddedFileStream();

		virtual void	Write(BYTE* buffer, int offset, int length);
		virtual void	WriteByte(BYTE b);
		virtual int		Read(BYTE* buffer, int offset, int length);
		virtual BYTE	ReadByte();
		virtual Int64	GetLength();
		virtual Int64	GetPosition();
		virtual void	SetPosition(Int64 pos);
		virtual void	Close();
		virtual bool	IsOpen();

	private:
		Int64 m_length;
		Int64 m_pos;
		IStream* m_content;
	};
};