﻿//=============================================================//
//                                                             //
//                  Controls::NicerTreeView.cs                 //
//   -------------------------------------------------------   //
//  Copyright © 2011 Anthony Mushrow, SKGenius Development     //
//  All rights reserved                                        //
//                                                             //
//  This code is licensed under The Code Project Open License  //
//  (CPOL) See http://www.codeproject.com/info/cpol10.aspx or  //
//  LICENSE.TXT for more information                           //
//                                                             //
//=============================================================//
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Windows.Forms;
using System.Runtime.InteropServices;

namespace Controls
{
	public partial class NicerTreeView : TreeView
	{
		[DllImport("user32.dll")]
		public static extern IntPtr SendMessage(IntPtr hWnd, uint Msg, IntPtr wParam, IntPtr lParam);

		[Category("Behavior")]
		[DefaultValue(255)]
		public int MaxLabelLength
		{
			get { return m_maxLabelLenth; }
			set { m_maxLabelLenth = value; }
		}

		protected override void OnBeforeLabelEdit(NodeLabelEditEventArgs e)
		{
			//Get the edit control handle
			IntPtr editHandle = SendMessage(this.Handle, 0x110F, IntPtr.Zero, IntPtr.Zero);
			//Set the text limit
			SendMessage(editHandle, 0xC5, (IntPtr)this.MaxLabelLength, IntPtr.Zero);

			base.OnBeforeLabelEdit(e);
		}

		private int m_maxLabelLenth = 255;
	}
}
