﻿//=============================================================//
//                                                             //
//                  Controls::LabelButton.cs                   //
//   -------------------------------------------------------   //
//  Copyright © 2011 Anthony Mushrow, SKGenius Development     //
//  All rights reserved                                        //
//                                                             //
//  This code is licensed under The Code Project Open License  //
//  (CPOL) See http://www.codeproject.com/info/cpol10.aspx or  //
//  LICENSE.TXT for more information                           //
//                                                             //
//=============================================================//
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Text;
using System.Windows.Forms;
using System.Drawing;
using System.Drawing.Drawing2D;

namespace Controls
{
	public partial class LabelButton : Label
	{
		public LabelButton()
		{
			m_hoverState = new DisplayState();
			m_clickState = new DisplayState();
			m_standardState = new DisplayState();
			m_curState = m_standardState;
		}

		[Category("Appearance")]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Content)]
		public DisplayState StandardState
		{
			get { return m_standardState; }
			set { m_standardState = value; }
		}

		[Category("Appearance")]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Content)]
		public DisplayState HoverState
		{
			get { return m_hoverState; }
			set { m_hoverState = value; }
		}

		[Category("Appearance")]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Content)]
		public DisplayState ClickState
		{
			get { return m_clickState; }
			set { m_clickState = value; }
		}

		protected override void OnMouseEnter(EventArgs e)
		{
			m_curState = m_hoverState;
			this.Invalidate();
			base.OnMouseEnter(e);
		}

		protected override void OnMouseLeave(EventArgs e)
		{
			m_curState = m_standardState;
			this.Invalidate();
			base.OnMouseLeave(e);
		}

		protected override void OnMouseDown(MouseEventArgs e)
		{
			m_curState = m_clickState;
			this.Invalidate();
			base.OnMouseDown(e);
		}

		protected override void OnMouseUp(MouseEventArgs e)
		{
			m_curState = m_hoverState;
			this.Invalidate();
			base.OnMouseUp(e);
		}

		protected override void OnPaintBackground(PaintEventArgs pevent)
		{
			SolidBrush backBrush = new SolidBrush(m_curState.BackColour);
			Pen borderPen = new Pen(m_curState.BorderColour);
			pevent.Graphics.FillRectangle(backBrush, pevent.ClipRectangle);
			
			//Draw Border
			for (int i = 0; i < m_curState.BorderWidth; i++)
			{
				Rectangle borderRect = pevent.ClipRectangle;
				borderRect.X += i; borderRect.Width -= (i * 2)+1;
				borderRect.Y += i; borderRect.Height -= (i * 2)+1;
				pevent.Graphics.DrawRectangle(borderPen, borderRect);
			}

			this.ForeColor = m_curState.ForeColour;
		}

		private DisplayState m_hoverState;
		private DisplayState m_clickState;
		private DisplayState m_standardState;
		private DisplayState m_curState;
	}


	[TypeConverter(typeof(DisplayStateConverter))]
	public class DisplayState
	{
		public DisplayState()
		{
			m_borderWidth = 0;
			m_borderCol = System.Drawing.SystemColors.Control;
			m_backCol = System.Drawing.SystemColors.Control;
			m_foreCol = System.Drawing.SystemColors.ControlText;
		}

		[DefaultValue(0)]
		public int BorderWidth
		{
			get { return m_borderWidth; }
			set { m_borderWidth = value; }
		}

		[DefaultValue(typeof(System.Drawing.Color), "Control")]
		public Color BorderColour
		{
			get { return m_borderCol; }
			set { m_borderCol = value; }
		}

		[DefaultValue(typeof(System.Drawing.Color), "Control")]
		public Color BackColour
		{
			get { return m_backCol; }
			set { m_backCol = value; }
		}

		[DefaultValue(typeof(System.Drawing.Color), "ControlText")]
		public Color ForeColour
		{
			get { return m_foreCol; }
			set { m_foreCol = value; }
		}

		public override string ToString()
		{
			return "[DisplayState]";
		}

		private int m_borderWidth;
		private Color m_borderCol;
		private Color m_backCol;
		private Color m_foreCol;
	}

	public class DisplayStateConverter : TypeConverter
	{
		public override PropertyDescriptorCollection GetProperties(ITypeDescriptorContext context, object value, Attribute[] attributes)
		{
			return TypeDescriptor.GetProperties(typeof(DisplayState));
		}

		public override bool GetPropertiesSupported(ITypeDescriptorContext context)
		{
			return true;
		}
	}
}
