﻿//=============================================================//
//                                                             //
//                  Controls::NicerListBox.cs                  //
//   -------------------------------------------------------   //
//  Copyright © 2011 Anthony Mushrow, SKGenius Development     //
//  All rights reserved                                        //
//                                                             //
//  This code is licensed under The Code Project Open License  //
//  (CPOL) See http://www.codeproject.com/info/cpol10.aspx or  //
//  LICENSE.TXT for more information                           //
//                                                             //
//=============================================================//
using System;
using System.Collections.Generic;
using System.Text;
using System.Windows.Forms;
using System.Drawing;
using System.Drawing.Drawing2D;

namespace Controls
{
	public class NicerListBox : ListBox
	{
		private int m_hotlightItem = -1;

		public NicerListBox()
		{
			this.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.ItemHeight = 19;
			this.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawFixed;
		}

		public void SelectHotItem()
		{
			this.SelectedIndex = m_hotlightItem;
		}

		protected override void OnDrawItem(DrawItemEventArgs e)
		{
			if (e.Index >= this.Items.Count)
				return;

			//Clear background
			Rectangle bounds = e.Bounds;
			bounds.Y += 2;
			Rectangle textArea = bounds; textArea.Height -= 2; textArea.X += 7; textArea.Width -= 7;
			Rectangle border = bounds; border.X += 5; border.Width -= 10; border.Height = 18;
			Rectangle in_border = border; in_border.X++; in_border.Y++; in_border.Width -= 2; in_border.Height -= 2;
			Rectangle hlArea = in_border; hlArea.X++; hlArea.Y++; hlArea.Width -= 2; hlArea.Height -= 2;
			SolidBrush clearBrush = new SolidBrush(this.BackColor);
			LinearGradientBrush gradBrush = null;
			Color borderCol = Color.Empty;
			Color gradTop = Color.Empty;
			Color gradBottom = Color.Empty;
			Color borderGradTop = Color.Empty;
			Color borderGradBottom = Color.Empty;

			bool drawHighlight = false;
			if ((e.State & DrawItemState.Selected) == DrawItemState.Selected)
			{
				drawHighlight = true;
				borderCol = Color.FromArgb(154, 223, 254);
				gradTop = Color.FromArgb(242, 249, 254);
				gradBottom = Color.FromArgb(214, 240, 253);
				borderGradTop = Color.FromArgb(247, 252, 254);
				borderGradBottom = Color.FromArgb(232, 246, 254);

				if (!this.Focused)
				{
					borderCol = ToGrayscale(borderCol);
					gradTop = ToGrayscale(gradTop);
					gradBottom = ToGrayscale(gradBottom);
					borderGradTop = ToGrayscale(borderGradTop);
					borderGradBottom = ToGrayscale(borderGradBottom);
				}
			}
			else if (m_hotlightItem == e.Index)
			{
				drawHighlight = true;
				borderCol = Color.FromArgb(218, 242, 252);
				gradTop = Color.FromArgb(247, 252, 255);
				gradBottom = Color.FromArgb(234, 247, 255);
				borderGradTop = Color.FromArgb(249, 253, 255);
				borderGradBottom = Color.FromArgb(240, 250, 255);
			}

			//Clear area
			e.Graphics.FillRectangle(clearBrush, bounds);

			if (drawHighlight)
			{
				Point p1 = new Point(0, in_border.Y);
				Point p2 = new Point(0, in_border.Bottom - 1);
				gradBrush = new LinearGradientBrush(p1, p2, borderGradTop, borderGradBottom);
				gradBrush.WrapMode = WrapMode.TileFlipX;
				e.Graphics.FillRectangle(gradBrush, in_border);

				p1 = new Point(0, hlArea.Y);
				p2 = new Point(0, hlArea.Bottom - 1);
				gradBrush = new LinearGradientBrush(p1, p2, gradTop, gradBottom);
				gradBrush.WrapMode = WrapMode.TileFlipX;
				e.Graphics.FillRectangle(gradBrush, hlArea);

				drawRoundRect(e.Graphics, borderCol, border, 2);
			}

			//Draw text
			SolidBrush textBrush = new SolidBrush(this.ForeColor);
			string text = this.GetItemText(this.Items[e.Index]);
			StringFormat f = StringFormat.GenericDefault;
			f.LineAlignment = StringAlignment.Center;
			e.Graphics.DrawString(text, e.Font, textBrush, textArea, f);

			clearBrush.Dispose();
			textBrush.Dispose();
			if (gradBrush != null)
				gradBrush.Dispose();
		}

		protected override void OnMouseMove(MouseEventArgs e)
		{
			int hotlightIndex = -1;
			for (int i = 0; i < this.Items.Count; i++)
			{
				bool hotlight = this.GetItemRectangle(i).Contains(e.Location);
				if (hotlight)
				{
					hotlightIndex = i;
					break;
				}
			}

			if (hotlightIndex != m_hotlightItem)
			{
				m_hotlightItem = hotlightIndex;
				this.Invalidate();
			}

			base.OnMouseMove(e);
		}

		protected override void OnMouseLeave(EventArgs e)
		{
			if (m_hotlightItem != -1)
			{
				m_hotlightItem = -1;
				this.Invalidate();
			}

			base.OnMouseLeave(e);
		}

		private Color ToGrayscale(Color col)
		{
			int luma = (int)(col.R * 0.3 + col.G * 0.59 + col.B * 0.11);
			return Color.FromArgb(luma, luma, luma);
		}

		private void drawRoundRect(Graphics g, Color col, Rectangle rect, float radius)
		{
			GraphicsPath gp = new GraphicsPath();
			g.SmoothingMode = SmoothingMode.AntiAlias;
			int x = rect.X;
			int y = rect.Y;
			int bottom = rect.Bottom - 1;
			int right = rect.Right - 1;

			gp.AddLine(x + radius, y, right - (radius * 2), y);
			gp.AddArc(right - (radius * 2), y, radius * 2, radius * 2, 270, 90);
			gp.AddLine(right, y + radius, right, bottom - (radius * 2));
			gp.AddArc(right - (radius * 2), bottom - (radius * 2), radius * 2, radius * 2, 0, 90);
			gp.AddLine(right - (radius * 2), bottom, x + radius, bottom);
			gp.AddArc(x, bottom - (radius * 2), radius * 2, radius * 2, 90, 90);
			gp.AddLine(x, bottom - (radius * 2), x, y + radius);
			gp.AddArc(x, y, radius * 2, radius * 2, 180, 90);

			gp.CloseFigure();

			Pen p = new Pen(col);
			g.DrawPath(p, gp);

			p.Dispose();
			gp.Dispose();

			g.SmoothingMode = SmoothingMode.HighSpeed;
		}

		private void InitializeComponent()
		{
			this.SuspendLayout();
			// 
			// NicerListBox
			// 
			this.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawFixed;
			this.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.ItemHeight = 19;
			this.ResumeLayout(false);

		}
	}
}
