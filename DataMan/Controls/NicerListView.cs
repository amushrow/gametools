﻿//=============================================================//
//                                                             //
//                  Controls::NicerListView.cs                 //
//   -------------------------------------------------------   //
//  Copyright © 2011 Anthony Mushrow, SKGenius Development     //
//  All rights reserved                                        //
//                                                             //
//  This code is licensed under The Code Project Open License  //
//  (CPOL) See http://www.codeproject.com/info/cpol10.aspx or  //
//  LICENSE.TXT for more information                           //
//                                                             //
//=============================================================//
using System;
using System.Collections.Generic;
using System.Collections;
using System.ComponentModel;
using System.Text;
using System.Windows.Forms;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Runtime.InteropServices;

namespace Controls
{
	public class NicerListView : ListView
	{
		[DllImport("user32.dll")]
		public static extern IntPtr SendMessage(IntPtr hWnd, uint Msg, IntPtr wParam, IntPtr lParam);

		public NicerListView()
		{
			this.SetStyle(ControlStyles.OptimizedDoubleBuffer | ControlStyles.AllPaintingInWmPaint, true);
		}

		public ListViewItem HotItem
		{
			get { return m_hotItem; }
		}

		[Category("Behavior")]
		[DefaultValue(false)]
		public bool AllowReorder
		{
			get { return m_allowReorder; }
			set
			{
				m_allowReorder = value;
				if (value)
					this.AllowDrop = true;
			}
		}

		[Category("Behavior")]
		[DefaultValue(255)]
		public int MaxLabelLength
		{
			get { return m_maxLabelLen; }
			set
			{
				if (value < 0)
					m_maxLabelLen = 0;
				else
					m_maxLabelLen = value;
			}
		}

		protected override void OnBeforeLabelEdit(LabelEditEventArgs e)
		{
			//Get the edit control handle
			IntPtr editHandle = SendMessage(this.Handle, 0x1018, IntPtr.Zero, IntPtr.Zero);
			//Set the text limit
			SendMessage(editHandle, 0xC5, (IntPtr)this.MaxLabelLength, IntPtr.Zero);

			base.OnBeforeLabelEdit(e);
		}

		protected override void OnDrawItem(DrawListViewItemEventArgs e)
		{
			if (this.View == System.Windows.Forms.View.Tile)
			{
				e.DrawBackground();

				SolidBrush clearBrush = new SolidBrush(this.BackColor);
				LinearGradientBrush gradBrush = null;
				Color borderCol = Color.Empty;
				Color gradTop = Color.Empty;
				Color gradBottom = Color.Empty;
				Color borderGradTop = Color.Empty;
				Color borderGradBottom = Color.Empty;

				bool drawHighlight = false;
				borderCol = System.Drawing.SystemColors.ActiveBorder;//Color.FromArgb(205, 205, 205);
				if (e.Item.Selected)
				{
					drawHighlight = true;
					borderCol = Color.FromArgb(154, 223, 254);
					gradTop = Color.FromArgb(242, 249, 254);
					gradBottom = Color.FromArgb(214, 240, 253);
					borderGradTop = Color.FromArgb(247, 252, 254);
					borderGradBottom = Color.FromArgb(232, 246, 254);

					if (!this.Focused)
					{
						borderCol = ToGrayscale(borderCol);
						gradTop = ToGrayscale(gradTop);
						gradBottom = ToGrayscale(gradBottom);
						borderGradTop = ToGrayscale(borderGradTop);
						borderGradBottom = ToGrayscale(borderGradBottom);
					}
				}
				else if (e.Item == m_hotItem)
				{
					drawHighlight = true;
					gradTop = Color.FromArgb(247, 252, 255);
					gradBottom = Color.FromArgb(234, 247, 255);
					borderGradTop = Color.FromArgb(249, 253, 255);
					borderGradBottom = Color.FromArgb(240, 250, 255);
				}

				if (drawHighlight)
				{
					Rectangle inner_border = e.Bounds;
					inner_border.X += 1; inner_border.Width -= 2;
					inner_border.Y += 1; inner_border.Height -= 2;

					Rectangle hl_area = inner_border;
					hl_area.X += 1; hl_area.Width -= 2;
					hl_area.Y += 1; hl_area.Height -= 2;

					Point p1 = new Point(0, inner_border.Y);
					Point p2 = new Point(0, inner_border.Bottom - 1);
					gradBrush = new LinearGradientBrush(p1, p2, borderGradTop, borderGradBottom);
					gradBrush.WrapMode = WrapMode.TileFlipX;
					e.Graphics.FillRectangle(gradBrush, inner_border);

					p1 = new Point(0, hl_area.Y);
					p2 = new Point(0, hl_area.Bottom - 1);
					gradBrush = new LinearGradientBrush(p1, p2, gradTop, gradBottom);
					gradBrush.WrapMode = WrapMode.TileFlipX;
					e.Graphics.FillRectangle(gradBrush, hl_area);
				}

				drawRoundRect(e.Graphics, borderCol, e.Bounds, 2);
				e.DrawText(TextFormatFlags.HorizontalCenter | TextFormatFlags.VerticalCenter);
			}
			else
				e.DrawDefault = true;
		}

		protected override void OnMouseDoubleClick(MouseEventArgs e)
		{
			base.OnMouseDoubleClick(e);
			if (e.Button == System.Windows.Forms.MouseButtons.Left && this.LabelEdit && m_hotItem != null)
			{
				m_hotItem.BeginEdit();
			}
		}

		protected override void OnMouseMove(MouseEventArgs e)
		{
			ListViewItem hot = GetItemAt(e.Location.X, e.Location.Y);	

			if (hot != m_hotItem)
			{
				if (m_hotItem != null)
					this.Invalidate(m_hotItem.Bounds);

				m_hotItem = hot;
				if (m_hotItem != null)
					this.Invalidate(m_hotItem.Bounds);

			}

			base.OnMouseMove(e);
		}

		protected override void OnMouseLeave(EventArgs e)
		{
			if (m_hotItem != null)
				this.Invalidate(m_hotItem.Bounds);
			m_hotItem = null;
			base.OnMouseLeave(e);
		}

		protected override void OnItemDrag(ItemDragEventArgs e)
		{
			base.OnItemDrag(e);
			if (AllowReorder)
				DoDragDrop(m_reorderFlag, DragDropEffects.Move);
		}

		protected override void OnDragEnter(DragEventArgs drgevent)
		{
			if (m_allowReorder && drgevent.Data.GetDataPresent(typeof(ReorderFlag)))
			{
				drgevent.Effect = DragDropEffects.Move;
			}
			else
			{
				base.OnDragEnter(drgevent);
			}
		}

		protected override void OnDragOver(DragEventArgs drgevent)
		{
			Point me = new Point(drgevent.X, drgevent.Y);
			me = this.PointToClient(me);
			m_hotItem = GetItemAt(me.X, me.Y);
			if (!m_allowReorder || !drgevent.Data.GetDataPresent(typeof(ReorderFlag)) || (m_hotItem == null))
			{
				base.OnDragOver(drgevent);
			}
			else
			{
				bool same = false;
				foreach (ListViewItem moveItem in this.SelectedItems)
				{
					if (moveItem == m_hotItem)
					{
						same = true;
						break;
					}
				}

				drgevent.Effect = same ? DragDropEffects.None : DragDropEffects.Move;
				m_hotItem.EnsureVisible();
			}
		}

		protected override void OnDragDrop(DragEventArgs drgevent)
		{
			Point me = new Point(drgevent.X, drgevent.Y);
			me = this.PointToClient(me);
			m_hotItem = GetItemAt(me.X, me.Y);
			if (!m_allowReorder || this.SelectedItems.Count == 0 || m_hotItem == null)
			{
				base.OnDragDrop(drgevent);
			}
			else
			{
				List<ListViewItem> moveItems = new List<ListViewItem>();
				foreach (ListViewItem item in this.SelectedItems)
				{
					moveItems.Add(item);
				}
				foreach (ListViewItem item in base.SelectedItems)
				{
					this.Items.Remove(item);
				}

				foreach (ListViewItem item in moveItems)
				{
					this.Items.Insert(m_hotItem.Index, item);
				}
			}
		}

		private Color ToGrayscale(Color col)
		{
			int luma = (int)(col.R * 0.3 + col.G * 0.59 + col.B * 0.11);
			return Color.FromArgb(luma, luma, luma);
		}

		private void drawRoundRect(Graphics g, Color col, Rectangle rect, float radius)
		{
			GraphicsPath gp = new GraphicsPath();
			g.SmoothingMode = SmoothingMode.AntiAlias;
			int x = rect.X;
			int y = rect.Y;
			int bottom = rect.Bottom - 1;
			int right = rect.Right - 1;

			gp.AddLine(x + radius, y, right - (radius * 2), y);
			gp.AddArc(right - (radius * 2), y, radius * 2, radius * 2, 270, 90);
			gp.AddLine(right, y + radius, right, bottom - (radius * 2));
			gp.AddArc(right - (radius * 2), bottom - (radius * 2), radius * 2, radius * 2, 0, 90);
			gp.AddLine(right - (radius * 2), bottom, x + radius, bottom);
			gp.AddArc(x, bottom - (radius * 2), radius * 2, radius * 2, 90, 90);
			gp.AddLine(x, bottom - (radius * 2), x, y + radius);
			gp.AddArc(x, y, radius * 2, radius * 2, 180, 90);

			gp.CloseFigure();

			Pen p = new Pen(col);
			g.DrawPath(p, gp);

			p.Dispose();
			gp.Dispose();

			g.SmoothingMode = SmoothingMode.HighSpeed;
		}

		private ListViewItem m_hotItem = null;
		private bool m_allowReorder = false;
		private int m_maxLabelLen = 255;

		private class ReorderFlag {}
		private ReorderFlag m_reorderFlag = new ReorderFlag();
	}

	public class ListViewAlphaSorter : IComparer
	{
		public SortOrder Order
		{
			get { return m_order; }
			set { m_order = value; }
		}

		int IComparer.Compare(object A, object B)
		{
			if (m_order == SortOrder.None)
				return 0;

			int value = 0;
			if (A is ListViewItem && B is ListViewItem)
			{
				ListViewItem itemA = A as ListViewItem;
				ListViewItem itemB = B as ListViewItem;

				value = itemA.Text.CompareTo(itemB.Text);
			}

			if (m_order == SortOrder.Descending)
				value *= -1;

			return value;
		}

		private SortOrder m_order = SortOrder.None;
	}
}