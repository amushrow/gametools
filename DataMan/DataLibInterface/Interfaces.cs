﻿//=============================================================//
//                                                             //
//              DataLibInterfaces::Interfaces.cs               //
//   -------------------------------------------------------   //
//  Copyright © 2011 Anthony Mushrow, SKGenius Development     //
//  All rights reserved                                        //
//                                                             //
//  This code is licensed under The Code Project Open License  //
//  (CPOL) See http://www.codeproject.com/info/cpol10.aspx or  //
//  LICENSE.TXT for more information                           //
//                                                             //
//=============================================================//
using System;
using System.Collections.Generic;
using System.Text;
using System.IO;

namespace DataLibInterfaces
{
	public interface IDataType
	{
		string TypeName
		{
			get;
		}

		Guid TypeGUID
		{
			get;
		}

		string Name
		{
			get;
			set;
		}

		string StringValue
		{
			get;
		}

		int ID
		{
			get;
			set;
		}

		PropertyDescription Property
		{
			get;
		}

		IEnumerable<PropertyDescription> GetSubProperties();
		void ShowEditForm();
		bool SetValue(string val);
		void WriteStream(Stream s);
		void ReadStream(Stream s);
	}

	public class PropertyDescription
	{
		public PropertyDescription(EditablePropertyDescription desc)
		{
			m_desc = desc;
		}

		public bool TextEdit
		{
			get { return m_desc.TextEdit; }
		}

		public bool EditDialog
		{
			get { return m_desc.EditDialog; }
		}

		public IDataType DataType
		{
			get { return m_desc.DataType; }
		}

		public string Name
		{
			get { return m_desc.Name; }
		}

		public string TypeName
		{
			get { return m_desc.TypeName; }
		}

		private EditablePropertyDescription m_desc;
	}

	public class EditablePropertyDescription
	{
		public EditablePropertyDescription(IDataType dataType)
		{
			m_dataType = dataType;
		}

		public bool TextEdit
		{
			get; set;
		}

		public bool EditDialog
		{
			get; set;
		}

		public IDataType DataType
		{
			get { return m_dataType; }
		}

		public string Name
		{
			get; set;
		}

		public string TypeName
		{
			get { return DataType.TypeName; }
		}

		private IDataType m_dataType;
	}

	[AttributeUsage(AttributeTargets.Class, Inherited = false)]
	public class DataTypeAttribute : Attribute
	{
		public string Name { get; set; }
		public string Description { get; set; }
		public DataTypeAttribute(string Name, string Desc)
		{
			this.Name = Name;
			this.Description = Desc;
		}
	}
}
