﻿using System.Reflection;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

// General Information about an assembly is controlled through the following 
// set of attributes. Change these attribute values to modify the information
// associated with an assembly.
[assembly: AssemblyTitle("DataMan")]
[assembly: AssemblyDescription(
@"This software is licensed under The Code Project Open License (CPOL)

You can view a copy of this license at http://www.codeproject.com/info/cpol10.aspx or in the LICENSE.TXT file.")]
[assembly: AssemblyConfiguration("")]
[assembly: AssemblyCompany("SKGenius Development")]
[assembly: AssemblyProduct("DataMan")]
[assembly: AssemblyCopyright(
@"Copyright © 2011 Anthony Mushrow, SKGenius Development
All rights reserved.")]

[assembly: AssemblyTrademark("")]
[assembly: AssemblyCulture("")]

// Setting ComVisible to false makes the types in this assembly not visible 
// to COM components.  If you need to access a type in this assembly from 
// COM, set the ComVisible attribute to true on that type.
[assembly: ComVisible(false)]

// The following GUID is for the ID of the typelib if this project is exposed to COM
[assembly: Guid("5f6300f0-1768-4dac-a156-3025b838ddbd")]

// Version information for an assembly consists of the following four values:
//
//      Major Version
//      Minor Version 
//      Build Number
//      Revision
//
// You can specify all the values or you can default the Build and Revision Numbers 
// by using the '*' as shown below:
// [assembly: AssemblyVersion("1.0.*")]
[assembly: AssemblyVersion("2.0.19.34")]
[assembly: AssemblyFileVersion("2.0.19.34")]
