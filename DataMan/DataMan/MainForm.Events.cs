﻿//=============================================================//
//                                                             //
//                 DataMan::MainForm.Events.cs                 //
//   -------------------------------------------------------   //
//  Copyright © 2011 Anthony Mushrow, SKGenius Development     //
//  All rights reserved                                        //
//                                                             //
//  This code is licensed under The Code Project Open License  //
//  (CPOL) See http://www.codeproject.com/info/cpol10.aspx or  //
//  LICENSE.TXT for more information                           //
//                                                             //
//=============================================================//
using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Text;
using System.Windows.Forms;
using System.IO;
using System.Drawing;
using DataMan.EditorControls;
using DataLib;
using DataLibInterfaces;

namespace DataMan
{
	public partial class MainForm : Form
	{
		#region lsvDataTypes
		private void lsvDataTypes_ItemDrag(object sender, ItemDragEventArgs e)
		{
			DataObject dropData = new DataObject();
			ListViewItem item = e.Item as ListViewItem;
			dropData.SetData(typeof(ITypeContainer), item.Tag);
			lsvDataTypes.DoDragDrop(dropData, DragDropEffects.Move);
		}

		private void lsvDataTypes_MouseClick(object sender, MouseEventArgs e)
		{
			if (e.Button == System.Windows.Forms.MouseButtons.Right)
			{

				if (this.lsvDataTypes.SelectedItems.Count > 0)
				{
					ListViewItem hot = this.lsvDataTypes.SelectedItems[0];
					if (hot.Group == this.lsvDataTypes.Groups["lsvGrpCustom"])
					{
						this.ctxMenuDataItem.Enabled = true;
					}
					else
					{
						this.ctxMenuDataItem.Enabled = false;
					}

					this.ctxMenuDataItem.Tag = hot;
					this.ctxMenuDataItem.Show(this.lsvDataTypes, e.Location);
				}
			}
		}
		#endregion

		#region trvEmbeddedFiles
		private void trvEmbeddedFiles_DragEnter(object sender, DragEventArgs e)
		{
			if (e.Data.GetDataPresent(DataFormats.FileDrop) || e.Data.GetDataPresent(typeof(FileNode)))
			{
				e.Effect = DragDropEffects.Move;
			}
		}

		private void trvEmbeddedFiles_DragOver(object sender, DragEventArgs e)
		{
			if (e.Data.GetDataPresent(DataFormats.FileDrop) || e.Data.GetDataPresent(typeof(FileNode)))
			{
				e.Effect = DragDropEffects.Move;
			}
		}

		private void trvEmbeddedFiles_DragDrop(object sender, DragEventArgs e)
		{
			//--Get HotNode and the relevane node collection
			Point mousePos = new Point(e.X, e.Y);
			mousePos = this.trvEmbeddedFiles.PointToClient(mousePos);
			FileNode hot = this.trvEmbeddedFiles.GetNodeAt(mousePos) as FileNode;
			TreeNodeCollection collection = this.trvEmbeddedFiles.Nodes;
			if (hot != null)
			{
				if (hot.IsFolder)
					collection = hot.Nodes;
				else if (hot.Parent != null)
					collection = hot.Parent.Nodes;
			}
			//--

			if (e.Data.GetDataPresent(typeof(FileNode)))
			{
				FileNode node = e.Data.GetData(typeof(FileNode)) as FileNode;
				if (node != null)
				{
					sortNode(collection, node);
				}
			}
			else if (e.Data.GetDataPresent(DataFormats.FileDrop))
			{
				string[] files = (string[])e.Data.GetData(DataFormats.FileDrop);
				foreach (string file in files)
				{
					embedFiles(file, collection);
				}
			}
		}

		private void trvEmbeddedFiles_MouseDown(object sender, MouseEventArgs e)
		{
			if (e.Button == System.Windows.Forms.MouseButtons.Right)
			{
				Point mousePos = new Point(e.X, e.Y);
				FileNode hot = this.trvEmbeddedFiles.GetNodeAt(mousePos) as FileNode;
				if (hot == null)
				{
					this.renameToolStripMenuItem.Visible = false;
					this.deleteToolStripMenuItem1.Visible = false;
				}
				else
				{
					this.renameToolStripMenuItem.Visible = true;
					this.deleteToolStripMenuItem1.Visible = true;
				}

				this.trvEmbeddedFiles.SelectedNode = hot;
				this.ctxMenuFileTree.Tag = hot;
				this.ctxMenuFileTree.Show(this.trvEmbeddedFiles, mousePos);
			}
		}

		private void trvEmbeddedFiles_AfterLabelEdit(object sender, NodeLabelEditEventArgs e)
		{
			if (e.Label != null)
			{
				TreeNodeCollection collection;
				if (e.Node.Parent != null)
					collection = e.Node.Parent.Nodes;
				else
					collection = this.trvEmbeddedFiles.Nodes;

				if (e.Label.Length == 0)
				{
					e.CancelEdit = true;
				}
				else if (nodeExists(collection, e.Label))
				{
					MessageBox.Show("An item with that name already exists", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
					e.CancelEdit = true;
				}
				else if (e.Label.IndexOf('\\') != -1)
				{
					MessageBox.Show("The Backslash character is not allowed in file names", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
					e.CancelEdit = true;
				}
				else
				{
					e.Node.Name = e.Label;
					e.Node.Text = e.Label;
					foreach (FileNode child in e.Node.Nodes)
					{
						m_dataManager.Files.Rename(child.FileID, child.FullPath);
					}
					trvEmbeddedFiles.BeginInvoke(m_sortDelegate, new object[] { collection, e.Node });
				}
			}
		}

		private void trvEmbeddedFiles_ItemDrag(object sender, ItemDragEventArgs e)
		{
			FileNode fnode = e.Item as FileNode;
			DataManFileExtractorObj dataObject = new DataManFileExtractorObj(m_dataManager);

			bool emptyFolder = (fnode.IsFolder && fnode.Nodes.Count == 0);
			if (!emptyFolder)
			{
				addFilesToExtract(fnode, dataObject, "");
			}

			dataObject.SetDescriptors();
			dataObject.SetData(e.Item);
			this.trvEmbeddedFiles.SelectedNode = e.Item as TreeNode;
			this.trvEmbeddedFiles.DoDragDrop(dataObject, DragDropEffects.Move);
		}

		private void trvEmbeddedFiles_KeyDown(object sender, KeyEventArgs e)
		{
			if (e.KeyCode == Keys.Delete)
			{
				FileNode selected = this.trvEmbeddedFiles.SelectedNode as FileNode;
				if (selected != null)
				{
					deleteFileNode(selected);
				}
			}
		}
		#endregion

		#region trvDataObjects
		private void trvDataObjects_DragEnter(object sender, DragEventArgs e)
		{
			if (e.Data.GetDataPresent(typeof(ITypeContainer)))
			{
				e.Effect = DragDropEffects.Move;
			}
		}

		private void trvDataObjects_DragOver(object sender, DragEventArgs e)
		{
			if (e.Data.GetDataPresent(typeof(ITypeContainer)))
			{
				e.Effect = DragDropEffects.Move;
			}
		}

		private void trvDataObjects_DragDrop(object sender, DragEventArgs e)
		{
			if (e.Data.GetDataPresent(typeof(ITypeContainer)))
			{
				object dragData = e.Data.GetData(typeof(ITypeContainer));
				if (dragData is ITypeContainer)
				{
					ITypeContainer type = dragData as ITypeContainer;
					bool haveType = false;
					TreeNode root = null;
					foreach (TreeNode node in trvDataObjects.Nodes)
					{
						if (node.Tag == type)
						{
							root = node;
							haveType = true;
							break;
						}
					}

					if (!haveType)
					{
						root = new TreeNode(type.Name);
						root.Tag = type;
						this.trvDataObjects.Nodes.Add(root);
					}

					//--Create unique name
					int nameExt = 1;
					string name = String.Format("{0}_{1}", type.Name, nameExt);
					while (nodeExists(root.Nodes, name))
					{
						nameExt++;
						name = String.Format("{0}_{1}", type.Name, nameExt);
					}
					//--

					TreeNode newNode = new TreeNode(name);
					IDataType data = type.CreateInstance();
					data.Name = name;
					newNode.Tag = data;
					m_dataManager.DataObjects.Add(data);
					data.ID = m_topId++;
					root.Nodes.Add(newNode);
					trvDataObjects.SelectedNode = newNode;
					newNode.BeginEdit();

				}
			}
		}

		private void trvDataObjects_MouseDoubleClick(object sender, MouseEventArgs e)
		{
			if (e.Button == System.Windows.Forms.MouseButtons.Left && this.trvDataObjects.LabelEdit)
			{
				if (this.trvDataObjects.SelectedNode != null)
					this.trvDataObjects.SelectedNode.BeginEdit();
			}
		}

		private void trvDataObjects_BeforeLabelEdit(object sender, NodeLabelEditEventArgs e)
		{
			//Can't allow you to rename root nodes
			if (e.Node.Parent == null)
				e.CancelEdit = true;
		}

		private void trvDataObjects_AfterLabelEdit(object sender, NodeLabelEditEventArgs e)
		{
			if (e.Label != null)
			{
				if (e.Label.Length == 0)
				{
					e.CancelEdit = true;
				}
				else if (nodeExists(e.Node.Parent.Nodes, e.Label))
				{
					MessageBox.Show("An item with that name already exists", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
					e.CancelEdit = true;
				}
				else
				{
					IDataType data = e.Node.Tag as IDataType;
					data.Name = e.Label;

					m_propertyViewer.Clear();
					EditorControls.PropertyViewerItem rootProperty = new EditorControls.PropertyViewerItem(data.Property);
					if (rootProperty.CanExpand)
						rootProperty.Expanded = true;
					m_propertyViewer.AddProperty(rootProperty);
				}
			}
		}

		private void trvDataObjects_AfterSelect(object sender, TreeViewEventArgs e)
		{
			if (e.Node.Parent != null)
			{
				m_propertyViewer.Clear();
				IDataType editType = e.Node.Tag as IDataType;
				EditorControls.PropertyViewerItem rootProperty = new EditorControls.PropertyViewerItem(editType.Property);
				if (rootProperty.CanExpand)
					rootProperty.Expanded = true;
				m_propertyViewer.AddProperty(rootProperty);
				this.tabCtrlMain.SelectedTab = this.tabEditor;
			}

			setStatusBar(e.Node);
		}

		private void trvDataObjects_KeyDown(object sender, KeyEventArgs e)
		{
			if (e.KeyData == Keys.Delete)
			{
				TreeNode node = this.trvDataObjects.SelectedNode;
				if (node != null && node.Parent != null)
				{
					DialogResult dr = MessageBox.Show("Are yous sure you want to delete this item?", "Confirm", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
					if (dr == DialogResult.Yes)
					{
						m_dataManager.DataObjects.Remove(node.Tag as IDataType);
						m_propertyViewer.Clear();
						
						//--Update treeview
						TreeNode parent = node.Parent;
						node.Remove();
						if (parent.Nodes.Count == 0)
						{
							parent.Remove();
							resetStatusBar();
						}
						//--
					}
				}
			}
		}
		#endregion

		#region Menu
		private void ascendingToolStripMenuItem_Click(object sender, EventArgs e)
		{
			m_listViewSorter.Order = SortOrder.Ascending;
			this.lsvDataTypes.Sort();
		}

		private void descendingToolStripMenuItem_Click(object sender, EventArgs e)
		{
			m_listViewSorter.Order = SortOrder.Descending;
			this.lsvDataTypes.Sort();
		}

		private void loadTypesToolStripMenuItem1_Click(object sender, EventArgs e)
		{
			m_plugMan.ShowDialog();
		}

		private void createNewTypeToolStripMenuItem1_Click(object sender, EventArgs e)
		{
			EditorControls.CustomTypeCreator typeCreator = new EditorControls.CustomTypeCreator(m_dataManager);
			typeCreator.Callback += new EditorControls.CustomTypeCreator.TypeCreatorEventHandler(typeCreator_Callback);
			this.tabCtrlMain.TabPages.Add(typeCreator);
			this.tabCtrlMain.SelectedTab = typeCreator;
		}

		private void editToolStripMenuItem_Click(object sender, EventArgs e)
		{
			ListViewItem clicked = this.ctxMenuDataItem.Tag as ListViewItem;
			CustomTypeContainer customType = clicked.Tag as CustomTypeContainer;

			bool tabExists = false;
			TabPage targetTab = null;
			foreach (TypeTabPage editTab in m_typeEditList)
			{
				if (editTab.TypeContainer == customType)
				{
					targetTab = editTab.EditorTab;
					tabExists = true;
					break;
				}
			}

			if (tabExists)
			{
				this.tabCtrlMain.SelectedTab = targetTab;
			}
			else
			{
				EditorControls.CustomTypeCreator typeCreator = new EditorControls.CustomTypeCreator(m_dataManager, customType);
				typeCreator.Callback += new EditorControls.CustomTypeCreator.TypeCreatorEventHandler(typeCreator_Callback);
				this.tabCtrlMain.TabPages.Add(typeCreator);
				this.tabCtrlMain.SelectedTab = typeCreator;

				TypeTabPage newEdit = new TypeTabPage();
				newEdit.EditorTab = typeCreator;
				newEdit.TypeContainer = customType;
				m_typeEditList.Add(newEdit);
			}
		}

		private void menuItem4_Click(object sender, EventArgs e)
		{
			save(false);
		}

		private void menuItem5_Click(object sender, EventArgs e)
		{
			save(true);
		}

		private void menuItem2_Click(object sender, EventArgs e)
		{
			if (checkSaveState())
			{
				m_filePath = String.Empty;
				reset();
			}
		}

		private void menuItem3_Click(object sender, EventArgs e)
		{
			load();
		}

		private void deleteToolStripMenuItem_Click(object sender, EventArgs e)
		{
			DialogResult dr = MessageBox.Show("Any instances of this type will also be deleted, are you sure you want to continue?", "", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
			if (dr == System.Windows.Forms.DialogResult.Yes)
			{
				ListViewItem clicked = this.ctxMenuDataItem.Tag as ListViewItem;
				CustomTypeContainer customType = clicked.Tag as CustomTypeContainer;

				this.lsvDataTypes.Items.Remove(clicked);
				m_dataManager.Types.Remove(customType);

				IDataType[] objectsToRemove = new IDataType[m_dataManager.DataObjects[customType].Count];
				m_dataManager.DataObjects[customType].CopyTo(objectsToRemove, 0);
				foreach (IDataType thing in objectsToRemove)
					m_dataManager.DataObjects.Remove(thing);

				populateDataLists();
			}
		}

		private void menuItem11_Click(object sender, EventArgs e)
		{
			OpenFileDialog fileDiag = new OpenFileDialog();
			fileDiag.Filter = "DataMan Type Definitions|*.skdef;*.skd";
			fileDiag.Multiselect = false;
			DialogResult dr = fileDiag.ShowDialog();
			if (dr == System.Windows.Forms.DialogResult.OK)
			{
				m_dataManager.LoadDefinitions(fileDiag.FileName);
				populateDataLists();
			}
		}

		private void menuItem12_Click(object sender, EventArgs e)
		{
			SaveFileDialog fileDiag = new SaveFileDialog();
			fileDiag.Filter = "DataMan Type Definitions|*.skdef";
			DialogResult dr = fileDiag.ShowDialog();
			if (dr == System.Windows.Forms.DialogResult.OK)
			{
				m_dataManager.SaveDefinitions(fileDiag.FileName);
			}
		}

		private void deleteToolStripMenuItem1_Click(object sender, EventArgs e)
		{
			FileNode selected = this.ctxMenuFileTree.Tag as FileNode;
			if (selected != null)
			{
				deleteFileNode(selected);
			}
		}

		private void createNewFolderToolStripMenuItem_Click(object sender, EventArgs e)
		{
			FileNode node = this.ctxMenuFileTree.Tag as FileNode;
			TreeNodeCollection collection = this.trvEmbeddedFiles.Nodes; ;
			if (node != null)
			{
				if (node.IsFolder)
					collection = node.Nodes;
				else if (node.Parent != null)
					collection = node.Parent.Nodes;
			}

			string name = "New Folder";
			int num = 1;
			while (nodeExists(collection, name))
			{
				name = String.Format("New Folder_{0}", num++);
			}

			FileNode newFolder = new FileNode(FileNode.Type.Folder);
			newFolder.Text = name;
			newFolder.ImageIndex = 1;
			newFolder.SelectedImageIndex = 1;
			int insertIndex = findInsertIndex(name, collection, FileNode.Type.Folder);
			collection.Insert(insertIndex, newFolder);
			this.trvEmbeddedFiles.SelectedNode = newFolder;
			newFolder.BeginEdit();
		}

		private void renameToolStripMenuItem_Click(object sender, EventArgs e)
		{
			TreeNode node = this.ctxMenuFileTree.Tag as TreeNode;
			if (node != null)
				node.BeginEdit();
		}

		private void menuItem7_Click(object sender, EventArgs e)
		{
			this.Close();
		}

		private void menuItem14_Click(object sender, EventArgs e)
		{
			AboutBox about = new AboutBox();
			about.ShowDialog();
		}
		#endregion

		#region Random
		private void MainForm_Shown(object sender, EventArgs e)
		{
			populateDataLists();
		}

		//--Tab Control redraw bug workaround
		private void MainForm_Resize(object sender, EventArgs e)
		{
			this.tabCtrlMain.SelectedTab.Invalidate();
		}
		//--

		private void splitContainer2_Panel1_Resize(object sender, EventArgs e)
		{
			this.tabCtrlMain.Width = this.splitContainer2.Panel1.Width + 2;
			this.tabCtrlMain.Height = this.splitContainer2.Panel1.Height + 1;
		}

		void pluginsUpdated(object sender, FormClosedEventArgs e)
		{
			//Unload all extended types
			foreach (PluginManager.AssemblyGroup gr in m_plugMan.LoadedAssemblies)
			{
				foreach (Plugin plugin in gr.Plugins)
				{
					m_dataManager.Types.Remove(plugin);
				}
			}

			//Only load types that are enabled
			foreach (PluginManager.AssemblyGroup gr in m_plugMan.LoadedAssemblies)
			{
				if (gr.Enabled)
				{
					foreach (Plugin plugin in gr.Plugins)
					{
						m_dataManager.Types.Add(plugin);
					}
				}
			}
			populateDataLists();
		}

		private void MainForm_FormClosed(object sender, FormClosedEventArgs e)
		{
			//Make sure we havn't left any bits behind
			if (Directory.Exists(m_tempDir))
			{
				Directory.Delete(m_tempDir, true);
			}
		}
		#endregion
	}
}
