﻿//=============================================================//
//                                                             //
//                  DataMan::PluginManager.cs                  //
//   -------------------------------------------------------   //
//  Copyright © 2011 Anthony Mushrow, SKGenius Development     //
//  All rights reserved                                        //
//                                                             //
//  This code is licensed under The Code Project Open License  //
//  (CPOL) See http://www.codeproject.com/info/cpol10.aspx or  //
//  LICENSE.TXT for more information                           //
//                                                             //
//=============================================================//
using System;
using System.Collections.Specialized;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.Reflection;
using System.Runtime.InteropServices;
using System.IO;
using DataLib;

namespace DataMan
{
	public partial class PluginManager : Form
	{
		public class AssemblyGroup
		{
			public AssemblyGroup(List<Plugin> plugins, AssemblyName nameInfo, Guid GUID)
			{
				m_guid = GUID;
				m_name = nameInfo;
				m_plugins = new List<Plugin>(plugins);
			}

			public ReadOnlyCollection<Plugin> Plugins
			{
				get { return m_plugins.AsReadOnly(); }
			}

			public AssemblyName NameInfo
			{
				get { return m_name; }
			}
			
			public Guid AssemblyGuid
			{
				get { return m_guid; }
			}

			public bool Enabled
			{
				get;
				set;
			}

			private AssemblyName m_name;
			private Guid m_guid;
			private List<Plugin> m_plugins;
		}

		public PluginManager()
		{
			InitializeComponent();
			m_assemblyGroups = new List<AssemblyGroup>();
		}

		public ReadOnlyCollection<AssemblyGroup> LoadedAssemblies
		{
			get { return m_assemblyGroups.AsReadOnly(); }
		}

		public void ScanPlugins()
		{
			string pluginDir = Path.GetDirectoryName(Application.ExecutablePath);
			pluginDir = Path.Combine(pluginDir, "Plugins");
			if (Directory.Exists(pluginDir))
			{
				DirectoryInfo di = new DirectoryInfo(pluginDir);
				FileInfo[] files = di.GetFiles();
				foreach (FileInfo fi in files)
				{
					loadPlugin(fi.FullName);
				}
			}

            populateList();
		}

		private void populateList()
		{
			this.listView1.SuspendLayout();
			this.listView1.Items.Clear();
			foreach (AssemblyGroup gr in m_assemblyGroups)
			{
				ListViewItem item = new ListViewItem();
				item.Text = gr.NameInfo.Name;
				item.SubItems.Add(gr.NameInfo.Version.ToString());
				item.SubItems.Add(gr.Plugins[0].AssemblyFile);
				item.Checked = gr.Enabled;
				item.Tag = gr;
				this.listView1.Items.Add(item);
			}
			this.listView1.ResumeLayout();
		}

		private Guid getAssemblyGuid(Assembly assembly)
		{
			object[] objects = assembly.GetCustomAttributes(typeof(GuidAttribute), false);
			Guid result = Guid.Empty;
			if (objects.Length > 0)
			{
				result = new Guid((objects[0] as GuidAttribute).Value);
			}

			return result;
		}

		private void loadPlugins(IEnumerable<string> Files)
		{
			foreach (string filename in Files)
			{
				loadPlugin(filename);
			}
		}

		private void loadPlugin(string filename)
		{
			try
			{
				Assembly asm = Assembly.LoadFile(filename);
				List<Plugin> plugins = PluginLoader.FindDataPlugins(asm);
				if (plugins.Count > 0)
				{
					AssemblyGroup newGroup = new AssemblyGroup(plugins, asm.GetName(), getAssemblyGuid(asm));
					newGroup.Enabled = true;
					bool alreadyLoaded = false;
					foreach (AssemblyGroup gr in m_assemblyGroups)
					{
						if (gr.AssemblyGuid == newGroup.AssemblyGuid)
						{
							alreadyLoaded = true;
							break;
						}
					}

					if (!alreadyLoaded)
					{
						//--Attempt to copy file to plugins folder
						FileInfo fi = new FileInfo(filename);
						string dir = Path.GetDirectoryName(Application.ExecutablePath);
						string TargetPath = String.Format("{0}\\{1}\\{2}", dir, "Plugins", fi.Name);
						if (!File.Exists(TargetPath))
						{
							Directory.CreateDirectory(Path.GetDirectoryName(TargetPath));
							File.Copy(filename, TargetPath);
						}
						//--

						//--Get the 'Checked' state of this assembly, if possible
						OrderedDictionary dict = Properties.Settings.Default.PluginPrefs;
						if (dict != null && dict.Contains(newGroup.AssemblyGuid))
						{
							newGroup.Enabled = (bool)dict[newGroup.AssemblyGuid];
						}
						//--

						m_assemblyGroups.Add(newGroup);
					}
				}
			}
			catch (BadImageFormatException)
			{

			}
		}

		private void button2_Click(object sender, EventArgs e)
		{
			OpenFileDialog fileDiag = new OpenFileDialog();
			fileDiag.Filter = "Excecutable Files|*.exe;*.dll";
			fileDiag.Multiselect = true;
			DialogResult dr = fileDiag.ShowDialog();
			if (dr == System.Windows.Forms.DialogResult.OK)
			{
				loadPlugins(fileDiag.FileNames);
				populateList();
			}
		}

		private void listView1_ItemChecked(object sender, ItemCheckedEventArgs e)
		{
			AssemblyGroup gr = e.Item.Tag as AssemblyGroup;
			gr.Enabled = e.Item.Checked;
		}

		private void PluginManager_FormClosing(object sender, FormClosingEventArgs e)
		{
			OrderedDictionary newDict = new OrderedDictionary(m_assemblyGroups.Count);
			foreach (AssemblyGroup gr in m_assemblyGroups)
			{
				newDict.Add(gr.AssemblyGuid, gr.Enabled);
			}
			Properties.Settings.Default.PluginPrefs = newDict;
			Properties.Settings.Default.Save();
		}

		private List<AssemblyGroup> m_assemblyGroups;
	}
}
