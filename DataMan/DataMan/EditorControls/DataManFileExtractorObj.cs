﻿//=============================================================//
//                                                             //
//             DataMan::DataManFileExtractorObj.cs             //
//   -------------------------------------------------------   //
//  Copyright © 2011 Anthony Mushrow, SKGenius Development     //
//  All rights reserved                                        //
//                                                             //
//  This code is licensed under The Code Project Open License  //
//  (CPOL) See http://www.codeproject.com/info/cpol10.aspx or  //
//  LICENSE.TXT for more information                           //
//                                                             //
//=============================================================//
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.InteropServices;
using System.Runtime.InteropServices.ComTypes;
using System.Drawing;
using DataLib;

namespace DataMan.EditorControls
{
	[StructLayout(LayoutKind.Sequential, CharSet=CharSet.Ansi)]
	public struct FILEDESCRIPTORA
	{
		public UInt32 dwFlags;
		public Guid clsid;
		public Size sizel;
		public Point pointl;
		public UInt32 dwFileAttributes;
		public System.Runtime.InteropServices.ComTypes.FILETIME ftCreationTime;
		public System.Runtime.InteropServices.ComTypes.FILETIME ftLastAccessTime;
		public System.Runtime.InteropServices.ComTypes.FILETIME ftLastWriteTime;
		public UInt32 nFileSizeHigh;
		public UInt32 nFileSizeLow;
		[MarshalAs(UnmanagedType.ByValTStr, SizeConst = 260)]
		public string cFileName;
	}

	[StructLayout(LayoutKind.Sequential, CharSet = CharSet.Unicode)]
	public struct FILEDESCRIPTORW
	{
		public UInt32 dwFlags;
		public Guid clsid;
		public Size sizel;
		public Point pointl;
		public UInt32 dwFileAttributes;
		public System.Runtime.InteropServices.ComTypes.FILETIME ftCreationTime;
		public System.Runtime.InteropServices.ComTypes.FILETIME ftLastAccessTime;
		public System.Runtime.InteropServices.ComTypes.FILETIME ftLastWriteTime;
		public UInt32 nFileSizeHigh;
		public UInt32 nFileSizeLow;
		[MarshalAs(UnmanagedType.ByValTStr, SizeConst = 260)]
		public string cFileName;
	}

	[StructLayout(LayoutKind.Sequential, CharSet=CharSet.Ansi)]
	public struct FILEGROUPDESCRIPTORA
	{
		public UInt32 cItems;
		public List<FILEDESCRIPTORA> fgd;
	}

	[StructLayout(LayoutKind.Sequential, CharSet=CharSet.Unicode)]
	public struct FILEGROUPDESCRIPTORW
	{
		public UInt32 cItems;
		public List<FILEDESCRIPTORW> fgd;
	}

	public enum FileDescriptorFlags : uint
	{
		FD_CLSID =  0x00000001,
		FD_SIZEPOINT = 0x00000002,
		FD_ATTRIBUTES = 0x00000004,
		FD_CREATETIME = 0x00000008,
		FD_ACCESSTIME = 0x00000010,
		FD_WRITESTIME = 0x00000020,
		FD_FILESIZE = 0x00000040,
		FD_PROGRESSUI = 0x00004000,
		FD_LINKUI = 0x00008000,
		FD_UNICODE = 0x80000000
	}

	class DataManFileExtractorObj : ComDataObject
	{
		private readonly string CFSTR_FILEDESCRIPTORA = "FileGroupDescriptor";
		private readonly string CFSTR_FILEDESCRIPTORW = "FileGroupDescriptorW";
		private readonly string CFSTR_FILECONTENTS = "FileContents";
		private readonly string CFSTR_PERFORMEDDROPEFFECT = "Performed DropEffect";

		class DataGroup
		{
			public FORMATETC Format;
			public STGMEDIUM Medium;
			public bool Owner;
		}
		struct FileID
		{
			public FileID(string Name, int UID)
			{
				this.Name = Name;
				this.UID = UID;
			}
			public string Name;
			public int UID;
		}
		private List<DataGroup> m_data;
		private DataManager m_dataManager;
		private List<FileID> m_files;
		private FILEGROUPDESCRIPTORA m_groupA;
		private FILEGROUPDESCRIPTORW m_groupW;

		public DataManFileExtractorObj(DataManager dataMan)
		{
			m_dataManager = dataMan;
			m_data = new List<DataGroup>();
			m_files = new List<FileID>();
			m_groupA = new FILEGROUPDESCRIPTORA();
			m_groupW = new FILEGROUPDESCRIPTORW();

			m_groupA.cItems = 0;
			m_groupW.cItems = 0;
			m_groupA.fgd = new List<FILEDESCRIPTORA>();
			m_groupW.fgd = new List<FILEDESCRIPTORW>();
		}

		public List<FORMATETC> GetFORMATETCList()
		{
			List<FORMATETC> myFormats = new List<FORMATETC>();
			foreach (DataGroup dg in m_data)
			{
				myFormats.Add(dg.Format);
			}
			return myFormats;
		}

		public void AddFile(string Name, int UID)
		{
			FILEDESCRIPTORA descA = new FILEDESCRIPTORA();
			FileCollection.File fileInfo = m_dataManager.Files.Find(UID);
			descA.cFileName = Name;
			descA.dwFlags =  0x04 | 0x08 | 0x10 | 0x20 | 0x40 | 0x4000;
			descA.dwFileAttributes = (uint)fileInfo.FileAttributes;
			descA.ftCreationTime.dwHighDateTime = (int)(fileInfo.CreationTimeUTC >> 32);
			descA.ftCreationTime.dwLowDateTime = (int)(fileInfo.CreationTimeUTC & 0xFFFFFFFF);
			descA.ftLastWriteTime.dwHighDateTime = (int)(fileInfo.ModifiedTimeUTC >> 32);
			descA.ftLastWriteTime.dwLowDateTime = (int)(fileInfo.ModifiedTimeUTC & 0xFFFFFFFF);
			descA.ftLastAccessTime.dwHighDateTime = (int)(fileInfo.AccessTimeUTC >> 32);
			descA.ftLastAccessTime.dwLowDateTime = (int)(fileInfo.AccessTimeUTC & 0xFFFFFFFF);
			descA.nFileSizeHigh = (uint)(fileInfo.OriginalLength >> 32);
			descA.nFileSizeLow = (uint)(fileInfo.OriginalLength & 0xFFFFFFFF);
			
			m_groupA.cItems++;
			m_groupA.fgd.Add(descA);

			FILEDESCRIPTORW descW = new FILEDESCRIPTORW();
			descW.cFileName = Name;	
			descW.dwFlags = 0x04 | 0x08 | 0x10 | 0x20 | 0x40 | 0x4000 | 0x80000000;
			descW.dwFileAttributes = (uint)fileInfo.FileAttributes;
			descW.ftCreationTime.dwHighDateTime = (int)(fileInfo.CreationTimeUTC >> 32);
			descW.ftCreationTime.dwLowDateTime = (int)(fileInfo.CreationTimeUTC & 0xFFFFFFFF);
			descW.ftLastWriteTime.dwHighDateTime = (int)(fileInfo.ModifiedTimeUTC >> 32);
			descW.ftLastWriteTime.dwLowDateTime = (int)(fileInfo.ModifiedTimeUTC & 0xFFFFFFFF);
			descW.ftLastAccessTime.dwHighDateTime = (int)(fileInfo.AccessTimeUTC >> 32);
			descW.ftLastAccessTime.dwLowDateTime = (int)(fileInfo.AccessTimeUTC & 0xFFFFFFFF);
			descW.nFileSizeHigh = (uint)(fileInfo.OriginalLength >> 32);
			descW.nFileSizeLow = (uint)(fileInfo.OriginalLength & 0xFFFFFFFF);

			m_groupW.cItems++;
			m_groupW.fgd.Add(descW);

			m_files.Add(new FileID(Name, UID));
		}

		public void SetDescriptors()
		{
			byte[] buffer;
			System.IO.MemoryStream ms = new System.IO.MemoryStream();

			//--Set ANSI structures
			buffer = BitConverter.GetBytes(m_groupA.cItems);
			ms.Write(buffer, 0, buffer.Length);

			foreach (FILEDESCRIPTORA desc in m_groupA.fgd)
			{
				int descSize = Marshal.SizeOf(desc);
				IntPtr tempMem = Marshal.AllocHGlobal(descSize);
				Marshal.StructureToPtr(desc, tempMem, true);

				buffer = new byte[descSize];
				Marshal.Copy(tempMem, buffer, 0, descSize);
				Marshal.FreeHGlobal(tempMem);
				ms.Write(buffer, 0, descSize);
			}

			FORMATETC formatEtc = new FORMATETC();
			formatEtc.cfFormat = (short)RegisterClipboardFormat(CFSTR_FILEDESCRIPTORA);
			formatEtc.dwAspect = DVASPECT.DVASPECT_CONTENT;
			formatEtc.lindex = -1;
			formatEtc.ptd = IntPtr.Zero;
			formatEtc.tymed = TYMED.TYMED_HGLOBAL;

			STGMEDIUM medium = new STGMEDIUM();
			medium.tymed = TYMED.TYMED_HGLOBAL;
			medium.pUnkForRelease = null;

			buffer = ms.GetBuffer();
			medium.unionmember = Marshal.AllocHGlobal(buffer.Length);
			Marshal.Copy(buffer, 0, medium.unionmember, buffer.Length);
			this.SetData(ref formatEtc, ref medium, true);
			//--


			//--Set UNICODE structures
			buffer = BitConverter.GetBytes(m_groupA.cItems);
			ms.Write(buffer, 0, buffer.Length);

			foreach (FILEDESCRIPTORW desc in m_groupW.fgd)
			{
				int descSize = Marshal.SizeOf(desc);
				IntPtr tempMem = Marshal.AllocHGlobal(descSize);
				Marshal.StructureToPtr(desc, tempMem, true);

				buffer = new byte[descSize];
				Marshal.Copy(tempMem, buffer, 0, descSize);
				Marshal.FreeHGlobal(tempMem);
				ms.Write(buffer, 0, descSize);
			}

			formatEtc = new FORMATETC();
			formatEtc.cfFormat = (short)RegisterClipboardFormat(CFSTR_FILEDESCRIPTORW);
			formatEtc.dwAspect = DVASPECT.DVASPECT_CONTENT;
			formatEtc.lindex = -1;
			formatEtc.ptd = IntPtr.Zero;
			formatEtc.tymed = TYMED.TYMED_HGLOBAL;

			medium = new STGMEDIUM();
			medium.tymed = TYMED.TYMED_HGLOBAL;
			medium.pUnkForRelease = null;

			buffer = ms.GetBuffer();
			medium.unionmember = Marshal.AllocHGlobal(buffer.Length);
			Marshal.Copy(buffer, 0, medium.unionmember, buffer.Length);
			//this.SetData(ref formatEtc, ref medium, true);
			//--
		}

		public override void SetData(ref System.Runtime.InteropServices.ComTypes.FORMATETC formatIn, ref System.Runtime.InteropServices.ComTypes.STGMEDIUM medium, bool release)
		{
			DataGroup existing = null;
			foreach (DataGroup dg in m_data)
			{
				if (dg.Format.cfFormat == formatIn.cfFormat)
				{
					existing = dg;
					break;
				}
			}

			short CF_PERFORMEDDROPEFFECT = (short)RegisterClipboardFormat(CFSTR_PERFORMEDDROPEFFECT);
			if (formatIn.cfFormat == CF_PERFORMEDDROPEFFECT)
			{
				//Force garbage collection to close any streams we may have had open
				GC.Collect();
			}
			
			short CF_FILEGROUPDESCRIPTORA = (short)RegisterClipboardFormat(CFSTR_FILEDESCRIPTORA);
			short CF_FILEGROUPDESCRIPTORW = (short)RegisterClipboardFormat(CFSTR_FILEDESCRIPTORW);
			if (formatIn.cfFormat == CF_FILEGROUPDESCRIPTORA || formatIn.cfFormat == CF_FILEGROUPDESCRIPTORW)
			{
				if (existing != null)
				{
					existing.Format.dwAspect = formatIn.dwAspect;
					existing.Format.lindex = formatIn.lindex;
					existing.Format.ptd = formatIn.ptd;
					existing.Format.tymed = formatIn.tymed;

					if (medium.unionmember != existing.Medium.unionmember && existing.Owner)
					{
						Marshal.FreeHGlobal(existing.Medium.unionmember);
					}
					existing.Medium.tymed = medium.tymed;
					existing.Medium.unionmember = medium.unionmember;

					existing.Owner = release;
				}
				else
				{
					DataGroup dg = new DataGroup();
					dg.Format.cfFormat = formatIn.cfFormat;
					dg.Format.dwAspect = formatIn.dwAspect;
					dg.Format.lindex = formatIn.lindex;
					dg.Format.ptd = formatIn.ptd;
					dg.Format.tymed = formatIn.tymed;

					dg.Medium.tymed = medium.tymed;
					dg.Medium.unionmember = medium.unionmember;

					dg.Owner = release;

					m_data.Add(dg);
				}
				return;
			}

			//base.SetData(ref formatIn, ref medium, release);
		}

		public override int QueryGetData(ref System.Runtime.InteropServices.ComTypes.FORMATETC format)
		{
			foreach (DataGroup dg in m_data)
			{
				if (dg.Format.cfFormat == format.cfFormat)
				{
					if ((dg.Format.tymed & format.tymed) != 0)
						return 0;
					else
					{
						unchecked { return (int)0x80004005; }
					}
				}
			}
			return base.QueryGetData(ref format);
		}

		public override void GetData(ref System.Runtime.InteropServices.ComTypes.FORMATETC format, out System.Runtime.InteropServices.ComTypes.STGMEDIUM medium)
		{
			short filecontents = (short)RegisterClipboardFormat(CFSTR_FILECONTENTS);
			if (format.cfFormat == filecontents)
			{
				medium = new STGMEDIUM();
				int UID = m_files[format.lindex].UID;
				System.IO.Stream dataStream = m_dataManager.GetFileStream(UID);
				medium.unionmember = Marshal.GetComInterfaceForObject(dataStream, typeof(IStream));
				medium.tymed = TYMED.TYMED_ISTREAM;
				medium.pUnkForRelease = null;
				return;
			}
			else
			{
				foreach (DataGroup dg in m_data)
				{
					if (dg.Format.cfFormat == format.cfFormat && (dg.Format.tymed&format.tymed) != 0)
					{
						medium = dg.Medium;
						return;
					}
				}
			}

			base.GetData(ref format, out medium);
		}

		public override IEnumFORMATETC EnumFormatEtc(DATADIR direction)
		{
			return new DataManFormatEnumerator(this);
		}
	}

	class DataManFormatEnumerator : IEnumFORMATETC
	{
		private DataManFileExtractorObj m_parent;
		private List<FORMATETC> m_formats;
		private int m_curPos;

		public DataManFormatEnumerator(DataManFileExtractorObj parent, List<FORMATETC> formats)
		{
			m_curPos = 0;
			m_parent = parent;
			m_formats = new List<FORMATETC>(formats);
		}

		public DataManFormatEnumerator(DataManFileExtractorObj parent)
		{
			m_curPos = 0;
			m_parent = parent;
			m_formats = new List<FORMATETC>();

			//--Get formats from the managed portion of our data object
			string[] managedFormats = parent.GetFormats();
			for (int i = 0; i < managedFormats.Length; i++)
			{
				string format = managedFormats[i];

				FORMATETC temp = new FORMATETC();
				temp.cfFormat = (short)System.Windows.Forms.DataFormats.GetFormat(format).Id;
				temp.dwAspect = DVASPECT.DVASPECT_CONTENT;
				temp.ptd = IntPtr.Zero;
				temp.lindex = -1;

				if (format.Equals(System.Windows.Forms.DataFormats.Bitmap))
				{
					temp.tymed = TYMED.TYMED_GDI;
				}
				else if (format.Equals(System.Windows.Forms.DataFormats.EnhancedMetafile))
				{
					temp.tymed = TYMED.TYMED_ENHMF;
				}
				else
				{
					temp.tymed = TYMED.TYMED_HGLOBAL;
				}

				m_formats.Add(temp);
			}
			//--

			//--Get the formats from the com half
			m_formats.AddRange(parent.GetFORMATETCList());
			//--
		}

		public int Next(int celt, FORMATETC[] rgelt, int[] pceltFetched)
		{
			if (m_curPos < m_formats.Count && celt > 0)
			{
				rgelt[0].cfFormat = m_formats[m_curPos].cfFormat;
				rgelt[0].tymed = m_formats[m_curPos].tymed;
				rgelt[0].dwAspect = m_formats[m_curPos].dwAspect;
				rgelt[0].ptd = m_formats[m_curPos].ptd;
				rgelt[0].lindex = m_formats[m_curPos].lindex;

				if (pceltFetched != null)
				{
					pceltFetched[0] = 1;
				}
				m_curPos++;
			}
			else
			{
				if (pceltFetched != null)
				{
					pceltFetched[0] = 0;
				}
				return 1;
			}
			return 0;
		}

		public int Skip(int celt)
		{
			if (m_curPos + celt >= m_formats.Count)
			{
				return 1;
			}
			m_curPos += celt;
			
			return 0;
		}

		public int Reset()
		{
			m_curPos = 0;
			return 0;
		}

		public void Clone(out IEnumFORMATETC ppenum)
		{
			ppenum = new DataManFormatEnumerator(m_parent, m_formats);
		}
	}
}
