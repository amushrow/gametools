﻿namespace DataMan.EditorControls
{
	partial class PropertyViewerItem
	{
		/// <summary> 
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary> 
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Component Designer generated code

		/// <summary> 
		/// Required method for Designer support - do not modify 
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(PropertyViewerItem));
            this.lblText = new System.Windows.Forms.Label();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.btnShowEdit = new System.Windows.Forms.Button();
            this.picBox = new System.Windows.Forms.PictureBox();
            this.arrowList = new System.Windows.Forms.ImageList(this.components);
            this.lblFocus = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.picBox)).BeginInit();
            this.SuspendLayout();
            // 
            // lblText
            // 
            this.lblText.AutoSize = true;
            this.lblText.Enabled = false;
            this.lblText.Location = new System.Drawing.Point(29, 5);
            this.lblText.Name = "lblText";
            this.lblText.Size = new System.Drawing.Size(84, 15);
            this.lblText.TabIndex = 0;
            this.lblText.Text = "PropertyName";
            // 
            // textBox1
            // 
            this.textBox1.Location = new System.Drawing.Point(193, 2);
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(190, 23);
            this.textBox1.TabIndex = 2;
            this.textBox1.KeyDown += new System.Windows.Forms.KeyEventHandler(this.textBox1_KeyDown);
            this.textBox1.MouseDown += new System.Windows.Forms.MouseEventHandler(this.PropertyViewerItem_ForwardClickEvent);
            this.textBox1.Validating += new System.ComponentModel.CancelEventHandler(this.textBox1_Validating);
            // 
            // btnShowEdit
            // 
            this.btnShowEdit.AutoSize = true;
            this.btnShowEdit.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.btnShowEdit.Location = new System.Drawing.Point(389, 1);
            this.btnShowEdit.Name = "btnShowEdit";
            this.btnShowEdit.Size = new System.Drawing.Size(26, 25);
            this.btnShowEdit.TabIndex = 3;
            this.btnShowEdit.Text = "...";
            this.btnShowEdit.UseVisualStyleBackColor = true;
            this.btnShowEdit.Visible = false;
            this.btnShowEdit.Click += new System.EventHandler(this.btnShowEdit_Click);
            this.btnShowEdit.MouseDown += new System.Windows.Forms.MouseEventHandler(this.PropertyViewerItem_ForwardClickEvent);
            // 
            // picBox
            // 
            this.picBox.Cursor = System.Windows.Forms.Cursors.Hand;
            this.picBox.Location = new System.Drawing.Point(7, 5);
            this.picBox.Name = "picBox";
            this.picBox.Size = new System.Drawing.Size(16, 16);
            this.picBox.TabIndex = 3;
            this.picBox.TabStop = false;
            this.picBox.Click += new System.EventHandler(this.PropertyViewerItem_Click);
            this.picBox.MouseDown += new System.Windows.Forms.MouseEventHandler(this.PropertyViewerItem_ForwardClickEvent);
            this.picBox.MouseMove += new System.Windows.Forms.MouseEventHandler(this.PropertyViewerItem_ForwardMoveEvent);
            // 
            // arrowList
            // 
            this.arrowList.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("arrowList.ImageStream")));
            this.arrowList.TransparentColor = System.Drawing.Color.Transparent;
            this.arrowList.Images.SetKeyName(0, "ArrowRight.png");
            this.arrowList.Images.SetKeyName(1, "ArrowDown.png");
            // 
            // lblFocus
            // 
            this.lblFocus.AutoSize = true;
            this.lblFocus.Location = new System.Drawing.Point(421, 6);
            this.lblFocus.Name = "lblFocus";
            this.lblFocus.Size = new System.Drawing.Size(0, 15);
            this.lblFocus.TabIndex = 1;
            // 
            // PropertyViewerItem
            // 
            this.Controls.Add(this.lblFocus);
            this.Controls.Add(this.picBox);
            this.Controls.Add(this.btnShowEdit);
            this.Controls.Add(this.textBox1);
            this.Controls.Add(this.lblText);
            this.DoubleBuffered = true;
            this.Font = new System.Drawing.Font("Segoe UI", 9F);
            this.Margin = new System.Windows.Forms.Padding(0);
            this.MaximumSize = new System.Drawing.Size(0, 29);
            this.MinimumSize = new System.Drawing.Size(418, 27);
            this.Name = "PropertyViewerItem";
            this.Size = new System.Drawing.Size(469, 27);
            this.DoubleClick += new System.EventHandler(this.PropertyViewerItem_Click);
            ((System.ComponentModel.ISupportInitialize)(this.picBox)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

		}

		#endregion

		private System.Windows.Forms.Label lblText;
		private System.Windows.Forms.TextBox textBox1;
		private System.Windows.Forms.Button btnShowEdit;
		private System.Windows.Forms.PictureBox picBox;
		private System.Windows.Forms.ImageList arrowList;
		private System.Windows.Forms.Label lblFocus;
	}
}
