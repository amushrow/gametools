﻿//=============================================================//
//                                                             //
//                  DataMan::PropertyViewer.cs                 //
//   -------------------------------------------------------   //
//  Copyright © 2011 Anthony Mushrow, SKGenius Development     //
//  All rights reserved                                        //
//                                                             //
//  This code is licensed under The Code Project Open License  //
//  (CPOL) See http://www.codeproject.com/info/cpol10.aspx or  //
//  LICENSE.TXT for more information                           //
//                                                             //
//=============================================================//
using System;
using System.Collections.Generic;
using System.Collections;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Windows.Forms;

namespace DataMan.EditorControls
{
	public partial class PropertyViewer : UserControl
	{
        private class PosInfo
        {
            public int VPos = 0;
            public int Indent = 0;
            public int TabIndex = 0;
        }

		public List<PropertyViewerItem> Items
		{
			get { return m_items; }
		}

		public PropertyViewer()
		{
			m_items = new List<PropertyViewerItem>();
			InitializeComponent();
			this.VerticalScroll.Enabled = false;
		}

		public void Clear()
		{
			m_items.Clear();
			m_selectedItem = null;
			m_contentHeight = 0;
			performLayout();
		}

		public override void Refresh()
		{
			base.Refresh();
			performLayout();
		}

		public void AddProperty(PropertyViewerItem item)
		{
			if (m_selectedItem == null)
				m_selectedItem = item;

			m_items.Add(item);
			performLayout();
		}

		protected override void OnControlAdded(ControlEventArgs e)
		{
			if (e.Control.Bottom > m_contentHeight)
				m_contentHeight = e.Control.Bottom;
			
			base.OnControlAdded(e);
		}

		protected override void OnResize(EventArgs e)
		{
			this.SuspendLayout();
			setWidths();
			this.ResumeLayout();

			base.OnResize(e);
		}

		private void stateChange(object sender, EventArgs e)
		{
			performLayout();
		}

		private void performLayout()
		{
			this.SuspendLayout();
            int scrollValue = this.VerticalScroll.Value;
            foreach(PropertyViewerItem item in this.Controls)
            {
                item.StateChanged -=new PropertyViewerItem.StateChangedHanlder(stateChange);
			    item.MouseMove -= new MouseEventHandler(PropertyViewer_MouseMove);
			    item.Enter -= new EventHandler(PropertyViewerItem_Enter);
            }
			this.Controls.Clear();

            PosInfo posInfo = new PosInfo();
			posInfo.Indent = this.Padding.Left;
			posInfo.VPos = this.Padding.Top;
            posInfo.TabIndex = 0;
			foreach (PropertyViewerItem item in m_items)
			{
				addItem(item, posInfo);
			}
			setWidths();
            //Scroll value resets when we do this, so we store the value ourselves
            this.VerticalScroll.Value = scrollValue;
			this.ResumeLayout();

            //--Make sure we can see the expanded item and all it's bits
            if(m_selectedItem != null && m_selectedItem.Expanded)
            {
                this.SuspendLayout();
                int scroll = 0;
                foreach(PropertyViewerItem item in m_selectedItem.SubItems)
                {
                    if(item.Bottom > this.ClientRectangle.Height)
                    {
                        int diff = item.Bottom - this.ClientRectangle.Height;
                        if(diff > scroll)
                            scroll = diff;
                    }
                }

                int top = m_selectedItem.Top;
                if(m_selectedItem == m_items[0])
                    top -= this.Padding.Top;
                if(top - scroll < 0)
                    scroll -= (scroll - top);

                this.VerticalScroll.Value += scroll;
                this.ResumeLayout();
                this.PerformLayout();
            }
            //--
		}

		private void setWidths()
		{
			int scrollWidth = 0;
			if (this.Height < m_contentHeight)
				scrollWidth = SystemInformation.VerticalScrollBarWidth;
			
			int maxWidth = 0;
			foreach (Control control in this.Controls)
			{
				control.Width = this.Width - scrollWidth;
				if (control.Width > maxWidth)
					maxWidth = control.Width;
			}

			if (maxWidth > this.Width - scrollWidth)
			{
				foreach (Control control in this.Controls)
				{
					control.Width = maxWidth;
				}
			}
		}

		private void addItem(PropertyViewerItem item, PosInfo posInfo)
		{
			//--Set BackColor
			if (item == m_selectedItem)
				item.BackColor = Color.FromArgb(214, 230, 243);
			else if (item == m_hotItem)
				item.BackColor = Color.FromArgb(242, 249, 254);
			else
				item.BackColor = this.BackColor;
			//--

			item.Location = new Point(0, posInfo.VPos-this.VerticalScroll.Value);
			item.Width = this.Width;
			item.Indent = posInfo.Indent;
            item.TabIndex = posInfo.TabIndex++;
            item.StateChanged +=new PropertyViewerItem.StateChangedHanlder(stateChange);
			item.MouseMove += new MouseEventHandler(PropertyViewer_MouseMove);
			item.Enter += new EventHandler(PropertyViewerItem_Enter);
			posInfo.VPos += item.Height;
			
			this.Controls.Add(item);
            if (item == m_selectedItem)
                item.Focus();

			if (item.Expanded)
			{
				posInfo.Indent += 15;
				foreach (PropertyViewerItem subItem in item.SubItems)
				{
					addItem(subItem, posInfo);
				}
				posInfo.Indent -= 15;
				posInfo.VPos += 15;
			}
		}

		private void PropertyViewer_MouseMove(object sender, MouseEventArgs e)
		{
            Control ctrlSender = sender as Control;
			Control prevHot = m_hotItem;
			m_hotItem = null;
            Point location = e.Location;
            location.Y += ctrlSender.Location.Y;
			foreach(PropertyViewerItem item in Controls)
			{
				if(item.Bounds.Contains(location))
				{
					m_hotItem = item;
					break;
				}
			}

			if(m_hotItem != prevHot)
			{
				if(m_hotItem != null && m_hotItem != m_selectedItem)
					m_hotItem.BackColor = Color.FromArgb(242, 249, 254);
				
				if(prevHot != null && prevHot != m_selectedItem)
					prevHot.BackColor = this.BackColor;
			}
		}

		private void PropertyViewerItem_Enter(object sender, EventArgs e)
		{
			PropertyViewerItem selected = sender as PropertyViewerItem;
			if (selected != m_selectedItem)
			{
				if (m_selectedItem != null)
					m_selectedItem.BackColor = this.BackColor;

				if (selected != null)
				{
					selected.BackColor = Color.FromArgb(214, 230, 243);
					m_selectedItem = selected;
				}			
			}
		}

		private List<PropertyViewerItem> m_items;
		private PropertyViewerItem m_hotItem = null;
		private PropertyViewerItem m_selectedItem = null;
		private int m_contentHeight = 0;
	}
}
