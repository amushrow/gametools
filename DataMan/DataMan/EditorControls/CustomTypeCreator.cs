﻿//=============================================================//
//                                                             //
//                DataMan::CustomTypeCreator.cs                //
//   -------------------------------------------------------   //
//  Copyright © 2011 Anthony Mushrow, SKGenius Development     //
//  All rights reserved                                        //
//                                                             //
//  This code is licensed under The Code Project Open License  //
//  (CPOL) See http://www.codeproject.com/info/cpol10.aspx or  //
//  LICENSE.TXT for more information                           //
//                                                             //
//=============================================================//
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Windows.Forms;
using DataLib;

namespace DataMan.EditorControls
{
	public partial class CustomTypeCreator : TabPage
	{
		static readonly string NO_NAME = "You must enter a name for this type";
		static readonly string CIRC_REF = "Adding this type causes a circular reference";
		static readonly string NO_PROP = "This type has no properties. Are you sure you want to continue?";
		static readonly string DEL_PROPS = "You have removed properties from this type.\nAny existing instances will lose this data, are you sure you want to continue?";

		public delegate void TypeCreatorEventHandler(object sender, TypeCreatorEventArgs e);
		public event TypeCreatorEventHandler Callback;

		private CustomDataTypeDefinition m_definition;
		private CustomTypeContainer m_container;
		private UInt16 m_curId;
		private bool m_reducedData;
		private DataManager m_dataMan;
		private delegate void InvokeErrorMessage(string msg);
		private InvokeErrorMessage ErrorMessage = new InvokeErrorMessage(invokeErrorMessage);

		public CustomTypeCreator(DataManager dataMan)
		{
			m_definition = new CustomDataTypeDefinition();
			m_definition.TypeName = String.Empty;
			m_definition.Description = String.Empty;
			m_definition.Properties = new List<PropID>();
			m_definition.GUID = Guid.NewGuid();

			m_container = null;
			m_curId = 1;
			m_reducedData = false;
			m_dataMan = dataMan;
			
			InitializeComponent();
			this.Text = "Type - [Untitled]";
		}

		public CustomTypeCreator(DataManager dataMan, CustomTypeContainer container)
		{
			m_definition = new CustomDataTypeDefinition();
			m_definition.TypeName = container.Definition.TypeName;
			m_definition.Description = container.Definition.Description;
			m_definition.Properties = new List<PropID>(container.Definition.Properties);
			m_definition.GUID = container.Definition.GUID;

			m_container = container;
			m_reducedData = false;
			m_dataMan = dataMan;

			InitializeComponent();
			this.Text = String.Format("Type - {0}", m_definition.TypeName);
			this.txtBoxTypeName.Text = m_definition.TypeName;
			this.txtBoxDesc.Text = m_definition.Description;

			foreach (PropID newProp in m_definition.Properties)
			{
				ListViewItem newItem = new ListViewItem();
				newItem.Text = newProp.Name;
				newItem.Tag = newProp;

				ITypeContainer type;
				if (container.AvailableTypes.FindType(newProp.GUID, out type))
					newItem.SubItems.Add(type.Name);
				else
					newItem.SubItems.Add("Unknown Type");

				newItem.SubItems.Add(newProp.ID.ToString());
				this.lsvProperties.Items.Add(newItem);
			}

			m_curId = 0;
			foreach (PropID prop in m_definition.Properties)
			{
				if (prop.ID > m_curId)
					m_curId = prop.ID;
			}
			m_curId++;
		}

		private void lsvProperties_DragEnter(object sender, DragEventArgs e)
		{
			if (e.Data.GetDataPresent(typeof(ITypeContainer)))
			{
				e.Effect = DragDropEffects.Move;
			}
		}

		private void lsvProperties_DragOver(object sender, DragEventArgs e)
		{
			if (e.Data.GetDataPresent(typeof(ITypeContainer)))
			{
				e.Effect = DragDropEffects.Move;
			}
		}

		private void lsvProperties_DragDrop(object sender, DragEventArgs e)
		{
			if (e.Data.GetDataPresent(typeof(ITypeContainer)))
			{
				object dragData = e.Data.GetData(typeof(ITypeContainer));
				if (dragData is ITypeContainer)
				{
					ITypeContainer type = dragData as ITypeContainer;
					if (checkCircularReference(type))
					{
						PropID newProp = new PropID();
						newProp.ID = m_curId++;
						newProp.GUID = type.GUID;
						
						//--Find a suitable name
						int startId = 1;
						string name = "";
						bool inUse = false;
						do
						{
							inUse = false;
							name = type.Name + "_" + startId;
							foreach(ListViewItem item in this.lsvProperties.Items)
							{
								if(item.Text == name)
								{
									inUse = true;
									break;
								}
							}
							startId++;

						}while(inUse);
						//--

						newProp.Name = name;

						ListViewItem newItem = new ListViewItem();
						newItem.Text = newProp.Name;
						newItem.Tag = newProp;
						newItem.SubItems.Add(type.Name);
						newItem.SubItems.Add(newProp.ID.ToString());
						this.lsvProperties.Items.Add(newItem);
						newItem.BeginEdit();
					}
					else
					{
						MessageBox.Show(CIRC_REF, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
					}
				}
			}
		}

		private void lsvProperties_AfterLabelEdit(object sender, LabelEditEventArgs e)
		{
			PropID property = (PropID)lsvProperties.Items[e.Item].Tag;

			if (e.Label != null)
			{
				if (e.Label.Length == 0)
				{
					e.CancelEdit = true;
				}
				else
				{
					//--Validate property name
					validate_error err = validateName(e.Label);
					if(err == validate_error.InvalidChars)
					{
						this.BeginInvoke(ErrorMessage, "Property name has some invalid characters.\nOnly a-Z, 0-9 and underscores are allowed");
						e.CancelEdit = true;
						return;
					}
					else if(err == validate_error.NumericStart)
					{
						this.BeginInvoke(ErrorMessage, "Property name must begin with a letter");
						e.CancelEdit = true;
						return;
					}
					else if(err == validate_error.Reserved)
					{
						this.BeginInvoke(ErrorMessage, "The property name '" + e.Label + "' is reserved");
						e.CancelEdit = true;
						return;
					}
					//--

					//--Check we are allowed to use this name
					foreach(ListViewItem item in this.lsvProperties.Items)
					{
						if(item.Index == e.Item)
							continue;

						if(e.Label == item.Text)
						{
							this.BeginInvoke(ErrorMessage,"Property name already exists");
							e.CancelEdit = true;
							return;
						}
					}
					//--

					property.Name = e.Label;
				}
			}
		}

		private void txtBoxTypeName_TextChanged(object sender, EventArgs e)
		{
			string name = "[Untitled]";
			if (txtBoxTypeName.Text.Length > 0)
				name = txtBoxTypeName.Text;

			this.Text = String.Format("Type - {0}", name);
		}

		private void bntOK_Click(object sender, EventArgs e)
		{
			bool exit = true;

			validate_error err = validateName(txtBoxTypeName.Text);
			bool nameTaken = false;
			foreach(ITypeContainer type in m_dataMan.Types)
			{
				if(type.GUID == m_definition.GUID)
					continue;

				if(type.Name == txtBoxTypeName.Text)
				{
					nameTaken = true;
					break;
				}
			}

			if (txtBoxTypeName.Text.Length == 0)
			{
				MessageBox.Show(NO_NAME, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
				exit = false;
			}
			else if(err == validate_error.InvalidChars)
			{
				MessageBox.Show("Type name has some invalid characters.\nOnly a-Z, 0-9 and underscores are allowed", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
				exit = false;
			}
			else if(err == validate_error.NumericStart)
			{
				MessageBox.Show("Type name must begin with a letter", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
				exit = false;
			}
			else if(err == validate_error.Reserved)
			{
				MessageBox.Show("The type name '" + txtBoxTypeName.Text + "' is reserved", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
				exit = false;
			}
			else if(nameTaken)
			{
				MessageBox.Show("Type name is already in use", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
				exit = false;
			}
			else if (lsvProperties.Items.Count == 0)
			{
				DialogResult dr = MessageBox.Show(NO_PROP, "Warning", MessageBoxButtons.YesNo, MessageBoxIcon.Warning);
				if (dr == DialogResult.No)
					exit = false;
			}
			else if (m_reducedData)
			{
				DialogResult dr = MessageBox.Show(DEL_PROPS, "Warning", MessageBoxButtons.YesNo, MessageBoxIcon.Warning);
				if (dr == DialogResult.No)
					exit = false;
			}

			if (exit)
			{
				//--Set Definition Properties
				m_definition.TypeName = txtBoxTypeName.Text;
				m_definition.Description = txtBoxDesc.Text;
				
                m_definition.Properties.Clear();
                foreach(ListViewItem item in this.lsvProperties.Items)
                    m_definition.Properties.Add( item.Tag as PropID );
                //--

				TypeCreatorEventArgs args = new TypeCreatorEventArgs(m_definition, m_container, false);
				OnCreatorCallback(args);
			}
		}

		private void btnCancel_Click(object sender, EventArgs e)
		{
			TypeCreatorEventArgs args = new TypeCreatorEventArgs(m_definition, m_container, true);
			OnCreatorCallback(args);
		}

		private void lsvProperties_KeyDown(object sender, KeyEventArgs e)
		{
			if (e.KeyData == Keys.Delete)
			{
				foreach (ListViewItem delItem in this.lsvProperties.SelectedItems)
				{
					PropID prop = delItem.Tag as PropID;
					m_definition.Properties.Remove(prop);
					this.lsvProperties.Items.Remove(delItem);

					if (m_container != null)
					{
						foreach (PropID originalProp in m_container.Definition.Properties)
						{
							if (originalProp.ID == prop.ID)
								m_reducedData = true;
						}
					}
				}
			}
		}

		protected virtual void OnCreatorCallback(TypeCreatorEventArgs e)
		{
			if (Callback != null)
				Callback(this, e);
		}

		private bool checkCircularReference(ITypeContainer type)
		{
			bool result = true;
			if (type.GUID == m_definition.GUID)
				result = false;
			else if (type is CustomTypeContainer)
			{
				CustomTypeContainer customType = (CustomTypeContainer)type;
				foreach (PropID prop in customType.Definition.Properties)
				{
					ITypeContainer next = null;
					if(customType.AvailableTypes.FindType(prop.GUID, out next))
					{
						result = checkCircularReference(next);
						if (!result)
							break;
					}
				}
			}

			return result;
		}

		enum validate_error
		{
			InvalidChars,
			NumericStart,
			Reserved,
			None
		};

		private validate_error validateName(string name)
		{
			foreach(char character in name)
			{
				bool invalid = true;
				if(character == '_')
					invalid = false;
				if(character >= 'a' && character <= 'z')
					invalid = false;
				if(character >= 'A' && character <= 'Z')
					invalid = false;
				if(character >= '0' && character <= '9')
					invalid = false;

				if(invalid)
				{
					return validate_error.InvalidChars;
				}
			}

			char first = name[0];
			if(first != '_' && !(first >= 'a' && first <= 'z') && !(first >= 'A' && first <= 'Z'))
			{
				return validate_error.NumericStart;
			}

			foreach(string reserved in CppExporter.ReservedNames)
			{
				if(name == reserved)
					return validate_error.Reserved;
			}

			return validate_error.None;
		}

		private static void invokeErrorMessage(string message)
		{
			MessageBox.Show(message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
		}
	}

	public class TypeCreatorEventArgs : EventArgs
	{
		public TypeCreatorEventArgs(CustomDataTypeDefinition Def, bool cancel)
		{
			m_def = Def;
			m_container = null;
			m_cancel = cancel;
		}

		public TypeCreatorEventArgs(CustomDataTypeDefinition Def, CustomTypeContainer container, bool cancel)
		{
			m_def = Def;
			m_container = container;
			m_cancel = cancel;
		}

		public CustomDataTypeDefinition TypeDef
		{
			get { return m_def; }
		}

		public CustomTypeContainer Container
		{
			get { return m_container; }
		}

		public bool Canceled
		{
			get { return m_cancel; }
		}

		private CustomDataTypeDefinition m_def;
		private CustomTypeContainer m_container;
		private bool m_cancel;
	}
}
