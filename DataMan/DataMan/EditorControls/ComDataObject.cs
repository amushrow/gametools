﻿//=============================================================//
//                                                             //
//                  DataMan::ComDataObject.cs                  //
//   -------------------------------------------------------   //
//  Copyright © 2011 Anthony Mushrow, SKGenius Development     //
//  All rights reserved                                        //
//                                                             //
//  This code is licensed under The Code Project Open License  //
//  (CPOL) See http://www.codeproject.com/info/cpol10.aspx or  //
//  LICENSE.TXT for more information                           //
//                                                             //
//=============================================================//
using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Text;
using System.Runtime.InteropServices;
using System.Runtime.InteropServices.ComTypes;
using System.Windows.Forms;

namespace DataMan.EditorControls
{
	public class ComDataObject : System.Runtime.InteropServices.ComTypes.IDataObject, System.Windows.Forms.IDataObject
	{
		[DllImport("user32.dll", SetLastError=true)]
		protected static extern int RegisterClipboardFormat(string lpszFormat);

		[DllImport("user32.dll")]
		protected static extern int GetClipboardFormatName(uint format, StringBuilder lpszFormatName, int cchMaxCount);

		public ComDataObject()
		{
			m_dataObject = new System.Windows.Forms.DataObject();
			m_comObject = m_dataObject as System.Runtime.InteropServices.ComTypes.IDataObject;
		}

		public virtual void GetData(ref FORMATETC format, out STGMEDIUM medium)
		{
			m_comObject.GetData(ref format, out medium);
		}

		public virtual void GetDataHere(ref FORMATETC format, ref STGMEDIUM medium)
		{
			m_comObject.GetDataHere(format, medium);
		}

		public virtual int QueryGetData(ref FORMATETC format)
		{
			return m_comObject.QueryGetData(format);
		}

		public virtual int GetCanonicalFormatEtc(ref FORMATETC formatIn, out FORMATETC formatOut)
		{
			return m_comObject.GetCanonicalFormatEtc(ref formatIn, out formatOut);
		}

		public virtual void SetData(ref FORMATETC formatIn, ref STGMEDIUM medium, bool release)
		{
			m_comObject.SetData(formatIn, medium, release);
		}

		public virtual IEnumFORMATETC EnumFormatEtc(DATADIR direction)
		{
			return m_comObject.EnumFormatEtc(direction);
		}

		public virtual int DAdvise(ref FORMATETC pFormatetc, ADVF advf, IAdviseSink adviseSink, out int connection)
		{
			return m_comObject.DAdvise(pFormatetc, advf, adviseSink, out connection);
		}

		public virtual void DUnadvise(int connection)
		{
			m_comObject.DUnadvise(connection);
		}

		public virtual int EnumDAdvise(out IEnumSTATDATA enumAdvise)
		{
			return m_comObject.EnumDAdvise(out enumAdvise);
		}

		//-- Forms Object
		public object GetData(string format)
		{
			return m_dataObject.GetData(format);
		}

		public object GetData(Type format)
		{
			return m_dataObject.GetData(format);
		}

		public object GetData(string format, bool autoConvert)
		{
			return m_dataObject.GetData(format, autoConvert);
		}

		public bool GetDataPresent(string format)
		{
			return m_dataObject.GetDataPresent(format);
		}

		public bool GetDataPresent(Type format)
		{
			return m_dataObject.GetDataPresent(format);
		}

		public bool GetDataPresent(string format, bool autoConvert)
		{
			return m_dataObject.GetDataPresent(format, autoConvert);
		}

		public string[] GetFormats()
		{
			return m_dataObject.GetFormats();
		}

		public string[] GetFormats(bool autoConvert)
		{
			return m_dataObject.GetFormats(autoConvert);
		}

		public void SetData(object data)
		{
			m_dataObject.SetData(data);
		}

		public void SetData(string format, object data)
		{
			m_dataObject.SetData(format, data);
		}

		public void SetData(Type format, object data)
		{
			m_dataObject.SetData(format, data);
		}

		public void SetData(string format, bool autoConvert, object data)
		{
			m_dataObject.SetData(format, autoConvert, data);
		}

		/*public override object GetData(string format)
		{
			
		}*/

		public bool ExtractedFiles
		{
			get { return m_extractedFiles; }
			set { m_extractedFiles = value; }
		}

		public List<int> FileIDs
		{
			get { return m_fileIDs; }
			set { m_fileIDs = value; }
		}

		public StringCollection FileDestinations
		{
			get { return m_extractList; }
			set { m_extractList = value; }
		}


		private System.Windows.Forms.DataObject m_dataObject;
		System.Runtime.InteropServices.ComTypes.IDataObject m_comObject;
		private bool m_extractedFiles = false;
		private List<int> m_fileIDs;
		private StringCollection m_extractList;
	}
}
