﻿//=============================================================//
//                                                             //
//                    DataMan::FileNode.cs                     //
//   -------------------------------------------------------   //
//  Copyright © 2011 Anthony Mushrow, SKGenius Development     //
//  All rights reserved                                        //
//                                                             //
//  This code is licensed under The Code Project Open License  //
//  (CPOL) See http://www.codeproject.com/info/cpol10.aspx or  //
//  LICENSE.TXT for more information                           //
//                                                             //
//=============================================================//
using System;
using System.Collections.Generic;
using System.Text;
using System.Windows.Forms;
using System.Drawing;

namespace DataMan.EditorControls
{
	public class FileNode : TreeNode
	{
		public enum Type
		{
			Folder,
			File
		}
		
		public FileNode(Type type)
		{
			m_type = type;
			m_defCol = this.ForeColor;
			m_filePath = String.Empty;
		}

		public bool IsFolder
		{
			get { return m_type == Type.Folder; }
		}

		public bool IsFile
		{
			get { return m_type == Type.File; }
		}

		public bool IsEmbedded
		{
			get { return m_embedded; }
			set
			{
				if (value)
					this.ForeColor = Color.ForestGreen;
				else
					this.ForeColor = m_defCol;

				m_embedded = value;
			}
		}

		public string FilePath
		{
			get { return m_filePath; }
			set { m_filePath = value; }
		}

		public FileNode.Type NodeType
		{
			get { return m_type; }
		}

		public int FileID
		{
			get;
			set;
		}

		private Type m_type;
		private bool m_embedded;
		private string m_filePath;
		private Color m_defCol;
	}
}
