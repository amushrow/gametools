﻿//=============================================================//
//                                                             //
//                  DataMan::InstanceEditor.cs                 //
//   -------------------------------------------------------   //
//  Copyright © 2011 Anthony Mushrow, SKGenius Development     //
//  All rights reserved                                        //
//                                                             //
//  This code is licensed under The Code Project Open License  //
//  (CPOL) See http://www.codeproject.com/info/cpol10.aspx or  //
//  LICENSE.TXT for more information                           //
//                                                             //
//=============================================================//
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Windows.Forms;

namespace DataMan.EditorControls
{
	public partial class InstanceEditor : UserControl
	{
		public InstanceEditor()
		{
			InitializeComponent();
		}
	}
}
