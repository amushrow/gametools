﻿namespace DataMan.EditorControls
{
	partial class CustomTypeCreator
	{
		/// <summary> 
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary> 
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Component Designer generated code

		/// <summary> 
		/// Required method for Designer support - do not modify 
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.txtBoxTypeName = new System.Windows.Forms.TextBox();
			this.label1 = new System.Windows.Forms.Label();
			this.label2 = new System.Windows.Forms.Label();
			this.txtBoxDesc = new System.Windows.Forms.TextBox();
			this.bntOK = new System.Windows.Forms.Button();
			this.btnCancel = new System.Windows.Forms.Button();
			this.lsvProperties = new Controls.NicerListView();
			this.hdrName = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
			this.hdrType = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
			this.hrdID = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
			this.SuspendLayout();
			// 
			// txtBoxTypeName
			// 
			this.txtBoxTypeName.Location = new System.Drawing.Point(82, 11);
			this.txtBoxTypeName.MaxLength = 20;
			this.txtBoxTypeName.Name = "txtBoxTypeName";
			this.txtBoxTypeName.Size = new System.Drawing.Size(160, 23);
			this.txtBoxTypeName.TabIndex = 0;
			this.txtBoxTypeName.TextChanged += new System.EventHandler(this.txtBoxTypeName_TextChanged);
			// 
			// label1
			// 
			this.label1.AutoSize = true;
			this.label1.Location = new System.Drawing.Point(8, 11);
			this.label1.Margin = new System.Windows.Forms.Padding(0);
			this.label1.Name = "label1";
			this.label1.Size = new System.Drawing.Size(71, 15);
			this.label1.TabIndex = 1;
			this.label1.Text = "Type Name:";
			this.label1.TextAlign = System.Drawing.ContentAlignment.TopRight;
			// 
			// label2
			// 
			this.label2.AutoSize = true;
			this.label2.Location = new System.Drawing.Point(9, 40);
			this.label2.Margin = new System.Windows.Forms.Padding(0);
			this.label2.Name = "label2";
			this.label2.Size = new System.Drawing.Size(70, 15);
			this.label2.TabIndex = 3;
			this.label2.Text = "Description:";
			this.label2.TextAlign = System.Drawing.ContentAlignment.TopRight;
			// 
			// txtBoxDesc
			// 
			this.txtBoxDesc.Location = new System.Drawing.Point(82, 40);
			this.txtBoxDesc.MaxLength = 128;
			this.txtBoxDesc.Multiline = true;
			this.txtBoxDesc.Name = "txtBoxDesc";
			this.txtBoxDesc.Size = new System.Drawing.Size(379, 39);
			this.txtBoxDesc.TabIndex = 4;
			// 
			// bntOK
			// 
			this.bntOK.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
			this.bntOK.Location = new System.Drawing.Point(386, 176);
			this.bntOK.Name = "bntOK";
			this.bntOK.Size = new System.Drawing.Size(75, 23);
			this.bntOK.TabIndex = 5;
			this.bntOK.Text = "Save";
			this.bntOK.UseVisualStyleBackColor = true;
			this.bntOK.Click += new System.EventHandler(this.bntOK_Click);
			// 
			// btnCancel
			// 
			this.btnCancel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
			this.btnCancel.Location = new System.Drawing.Point(305, 176);
			this.btnCancel.Name = "btnCancel";
			this.btnCancel.Size = new System.Drawing.Size(75, 23);
			this.btnCancel.TabIndex = 6;
			this.btnCancel.Text = "Cancel";
			this.btnCancel.UseVisualStyleBackColor = true;
			this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
			// 
			// lsvProperties
			// 
			this.lsvProperties.AllowDrop = true;
			this.lsvProperties.AllowReorder = true;
			this.lsvProperties.AutoArrange = false;
			this.lsvProperties.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.hdrName,
            this.hdrType,
            this.hrdID});
			this.lsvProperties.Dock = System.Windows.Forms.DockStyle.Fill;
			this.lsvProperties.HeaderStyle = System.Windows.Forms.ColumnHeaderStyle.Nonclickable;
			this.lsvProperties.LabelEdit = true;
			this.lsvProperties.LabelWrap = false;
			this.lsvProperties.Location = new System.Drawing.Point(8, 85);
			this.lsvProperties.MaxLabelLength = 20;
			this.lsvProperties.Name = "lsvProperties";
			this.lsvProperties.Size = new System.Drawing.Size(453, 85);
			this.lsvProperties.TabIndex = 0;
			this.lsvProperties.UseCompatibleStateImageBehavior = false;
			this.lsvProperties.View = System.Windows.Forms.View.Details;
			this.lsvProperties.AfterLabelEdit += new System.Windows.Forms.LabelEditEventHandler(this.lsvProperties_AfterLabelEdit);
			this.lsvProperties.DragDrop += new System.Windows.Forms.DragEventHandler(this.lsvProperties_DragDrop);
			this.lsvProperties.DragEnter += new System.Windows.Forms.DragEventHandler(this.lsvProperties_DragEnter);
			this.lsvProperties.DragOver += new System.Windows.Forms.DragEventHandler(this.lsvProperties_DragOver);
			this.lsvProperties.KeyDown += new System.Windows.Forms.KeyEventHandler(this.lsvProperties_KeyDown);
			// 
			// hdrName
			// 
			this.hdrName.Text = "Name";
			this.hdrName.Width = 170;
			// 
			// hdrType
			// 
			this.hdrType.Text = "Type";
			this.hdrType.Width = 170;
			// 
			// hrdID
			// 
			this.hrdID.Text = "ID";
			// 
			// CustomTypeCreator
			// 
			this.AllowDrop = true;
			this.Controls.Add(this.btnCancel);
			this.Controls.Add(this.bntOK);
			this.Controls.Add(this.lsvProperties);
			this.Controls.Add(this.txtBoxDesc);
			this.Controls.Add(this.label2);
			this.Controls.Add(this.label1);
			this.Controls.Add(this.txtBoxTypeName);
			this.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.MinimumSize = new System.Drawing.Size(469, 205);
			this.Padding = new System.Windows.Forms.Padding(8, 85, 8, 35);
			this.Size = new System.Drawing.Size(469, 205);
			this.UseVisualStyleBackColor = true;
			this.ResumeLayout(false);
			this.PerformLayout();

		}

		#endregion

		private System.Windows.Forms.TextBox txtBoxTypeName;
		private System.Windows.Forms.Label label1;
		private System.Windows.Forms.Label label2;
		private System.Windows.Forms.TextBox txtBoxDesc;
		private Controls.NicerListView lsvProperties;
		private System.Windows.Forms.ColumnHeader hdrName;
		private System.Windows.Forms.ColumnHeader hdrType;
		private System.Windows.Forms.ColumnHeader hrdID;
		private System.Windows.Forms.Button bntOK;
		private System.Windows.Forms.Button btnCancel;
	}
}
