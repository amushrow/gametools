﻿//=============================================================//
//                                                             //
//                DataMan::PropertyViewerItem.cs               //
//   -------------------------------------------------------   //
//  Copyright © 2011 Anthony Mushrow, SKGenius Development     //
//  All rights reserved                                        //
//                                                             //
//  This code is licensed under The Code Project Open License  //
//  (CPOL) See http://www.codeproject.com/info/cpol10.aspx or  //
//  LICENSE.TXT for more information                           //
//                                                             //
//=============================================================//
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Windows.Forms;
using DataLibInterfaces;

namespace DataMan.EditorControls
{
	public partial class PropertyViewerItem : UserControl
	{
		public delegate void StateChangedHanlder(object sender, EventArgs e);
		public event StateChangedHanlder StateChanged;

		public bool CanExpand
		{
			get { return m_subItems.Count > 0; }
		}

		public bool Expanded
		{
			get { return m_expanded; }
			set
			{
				if (CanExpand && m_expanded != value)
				{
					m_expanded = value;
					OnStateChange(new EventArgs());
				}
			}
		}

		[Category("Appearance")]
		[DefaultValue(0)]
		public int Indent
		{
			get { return m_indent; }
			set
			{
				int diff = value - m_indent;
				m_indent = value;
				Size minSize = new Size(this.MinimumSize.Width + diff, this.MinimumSize.Height);
				this.MinimumSize = minSize;
				if (diff != 0)
				{
					this.SuspendLayout();
					foreach (Control control in this.Controls)
					{
						control.Left += diff;
					}
					this.ResumeLayout();
				}
			}
		}

		[DefaultValue(true)]
		public bool TextEdit
		{
			get { return m_textEdit; }
			set
			{
				if (value != m_textEdit)
				{
					m_textEdit = value;
					if (m_textEdit)
						this.textBox1.Enabled = true;
					else
						this.textBox1.Enabled = false;
				}
			}
		}

		[DefaultValue(false)]
		public bool HasEditDialog
		{
			get { return m_editDiag; }
			set
			{
				if (value != m_editDiag)
				{
					m_editDiag = value;
					if (m_editDiag)
						this.btnShowEdit.Show();
					else
						this.btnShowEdit.Hide();
				}
			}
		}

		public List<PropertyViewerItem> SubItems
		{
			get { return m_subItems; }
		}

		public PropertyViewerItem(DataLibInterfaces.PropertyDescription data)
		{
			m_subItems = new List<PropertyViewerItem>();
			InitializeComponent();
			m_data = data;

			this.lblText.Text = data.DataType.Name;
			this.textBox1.Text = data.DataType.StringValue;
			this.TextEdit = data.TextEdit;
			this.HasEditDialog = data.EditDialog;

			IEnumerable<PropertyDescription> things = m_data.DataType.GetSubProperties();
			foreach (DataLibInterfaces.PropertyDescription description in things)
			{
				PropertyViewerItem subItem = new PropertyViewerItem(description);
				m_subItems.Add(subItem);
			}

			this.picBox.Image = m_expanded ? arrowList.Images[1] : arrowList.Images[0];
			if (CanExpand)
				this.picBox.Show();
			else
				this.picBox.Hide();
		}

		void subItem_StateChanged(object sender, EventArgs e)
		{
			OnStateChange(e);
		}

		private void OnStateChange(EventArgs e)
		{
			this.picBox.Image = m_expanded ? arrowList.Images[1] : arrowList.Images[0];

			if (CanExpand)
				this.picBox.Show();
			else
				this.picBox.Hide();

			if (StateChanged != null)
			{
				StateChanged(this, e);
			}

		}

		private void btnShowEdit_Click(object sender, EventArgs e)
		{
			if (m_data.EditDialog)
			{
				m_data.DataType.ShowEditForm();
				this.textBox1.Text = m_data.DataType.StringValue;
			}
		}

		private void textBox1_Validating(object sender, CancelEventArgs e)
		{
			if (m_data.TextEdit)
			{
				e.Cancel = !m_data.DataType.SetValue(this.textBox1.Text);
				if (e.Cancel)
				{
					this.textBox1.SelectAll();
					MessageBox.Show("Invalid property value", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
				}
				else
				{
					this.textBox1.Text = m_data.DataType.StringValue;
				}
			}
		}

		private void PropertyViewerItem_Click(object sender, EventArgs e)
		{
			this.Expanded = !this.Expanded;
		}

		private void textBox1_KeyDown(object sender, KeyEventArgs e)
		{
			if(e.KeyData == Keys.Return || e.KeyData == Keys.Enter)
			{
				e.Handled = true;
				e.SuppressKeyPress = true;
				if (m_data.TextEdit)
				{
					if (!m_data.DataType.SetValue(this.textBox1.Text))
					{
						this.textBox1.SelectAll();
						DialogResult dr = MessageBox.Show("Invalid property value", "Error", MessageBoxButtons.OKCancel, MessageBoxIcon.Error);
						if (dr == DialogResult.Cancel)
						{
							this.textBox1.Text = m_data.DataType.StringValue;
						}
					}
					else
					{
						//Set data successful, set the textbox to the new value of the datatype
						// (in most cases it will stay the same)
						this.textBox1.Text = m_data.DataType.StringValue;
						this.lblFocus.Focus();
					}
				}
			}
			else if (e.KeyData == Keys.Escape)
			{
				e.Handled = true;
				e.SuppressKeyPress = true;
				this.textBox1.Text = m_data.DataType.StringValue;
				this.lblFocus.Focus();
			}
		}

		private void PropertyViewerItem_ForwardMoveEvent(object sender, MouseEventArgs e)
		{
			Control control = sender as Control;
			MouseEventArgs newArgs = new MouseEventArgs(e.Button, e.Clicks, control.Location.X+e.X, control.Location.Y+e.Y, e.Delta);
			this.OnMouseMove(newArgs);
		}

        protected override void OnMouseDown(MouseEventArgs e)
        {
 	        base.OnMouseDown(e);
            if(m_stealFocus)
                this.lblFocus.Focus();
        }

		private void PropertyViewerItem_ForwardClickEvent(object sender, MouseEventArgs e)
		{
            if(sender == this.textBox1 || sender == this.btnShowEdit)
                m_stealFocus = false;
            else
                m_stealFocus = true;

            Control ctrl = sender as Control;
            MouseEventArgs args = new MouseEventArgs(e.Button, e.Clicks, e.X+ctrl.Location.X, e.Y+ctrl.Location.Y, e.Delta);
			this.OnMouseDown(args);
		}

		private List<PropertyViewerItem> m_subItems;
		DataLibInterfaces.PropertyDescription m_data;
		private bool m_expanded = false;
		private bool m_textEdit = true;
		private bool m_editDiag = false;
		private int m_indent = 0;
        private bool m_stealFocus = true;
	}
}
