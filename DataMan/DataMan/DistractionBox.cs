﻿//=============================================================//
//                                                             //
//                  DataMan::DistractionBox.cs                 //
//   -------------------------------------------------------   //
//  Copyright © 2011 Anthony Mushrow, SKGenius Development     //
//  All rights reserved                                        //
//                                                             //
//  This code is licensed under The Code Project Open License  //
//  (CPOL) See http://www.codeproject.com/info/cpol10.aspx or  //
//  LICENSE.TXT for more information                           //
//                                                             //
//=============================================================//
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace DataMan
{
	public partial class DistractionBox : Form
	{
		public DistractionBox()
		{
			InitializeComponent();
			m_canClose = true;
			m_targetWorker = null;
		}

		public BackgroundWorker Worker
		{
			get { return m_targetWorker; }
			set { m_targetWorker = value; }
		}

		public string DisplayText
		{
			get { return this.label1.Text; }
			set { this.label1.Text = value; }
		}

		public void EndDistraction()
		{
			m_canClose = true;
			this.Close();
		}

		public void StartDistraction()
		{
			if (m_targetWorker != null)
			{
				m_targetWorker.RunWorkerCompleted += new RunWorkerCompletedEventHandler(targetWorker_RunWorkerCompleted);
				m_targetWorker.RunWorkerAsync();
			}
			m_canClose = false;
			this.ShowDialog();
		}

		void targetWorker_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
		{
			m_canClose = true;
			this.Close();
		}

		private void DistractionBox_FormClosing(object sender, FormClosingEventArgs e)
		{
			if (!m_canClose)
				e.Cancel = true;
		}

		private bool m_canClose;
		private BackgroundWorker m_targetWorker;
	}
}
