﻿namespace DataMan
{
	partial class MainForm
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.components = new System.ComponentModel.Container();
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MainForm));
			System.Windows.Forms.ListViewGroup listViewGroup1 = new System.Windows.Forms.ListViewGroup("Standard", System.Windows.Forms.HorizontalAlignment.Center);
			System.Windows.Forms.ListViewGroup listViewGroup2 = new System.Windows.Forms.ListViewGroup("Custom", System.Windows.Forms.HorizontalAlignment.Center);
			System.Windows.Forms.ListViewGroup listViewGroup3 = new System.Windows.Forms.ListViewGroup("Extended", System.Windows.Forms.HorizontalAlignment.Center);
			this.notifyIcon1 = new System.Windows.Forms.NotifyIcon(this.components);
			this.windowStatusStrip = new System.Windows.Forms.StatusStrip();
			this.lblStatusName = new System.Windows.Forms.ToolStripStatusLabel();
			this.lblStatusID = new System.Windows.Forms.ToolStripStatusLabel();
			this.lblStatusTypeGUID = new System.Windows.Forms.ToolStripStatusLabel();
			this.notifyIcon2 = new System.Windows.Forms.NotifyIcon(this.components);
			this.splitContainer1 = new System.Windows.Forms.SplitContainer();
			this.splitContainer3 = new System.Windows.Forms.SplitContainer();
			this.label4 = new System.Windows.Forms.Label();
			this.trvIconList = new System.Windows.Forms.ImageList(this.components);
			this.label5 = new System.Windows.Forms.Label();
			this.splitContainer2 = new System.Windows.Forms.SplitContainer();
			this.tabCtrlMain = new System.Windows.Forms.TabControl();
			this.tabEditor = new System.Windows.Forms.TabPage();
			this.mnuDataTypes = new System.Windows.Forms.MenuStrip();
			this.toolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
			this.createNewTypeToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
			this.loadTypesToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
			this.toolStripMenuItem2 = new System.Windows.Forms.ToolStripSeparator();
			this.sortByToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			this.ascendingToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			this.descendingToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			this.label1 = new System.Windows.Forms.Label();
			this.ctxMenuDataItem = new System.Windows.Forms.ContextMenuStrip(this.components);
			this.editToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			this.deleteToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			this.windowMenu = new System.Windows.Forms.MainMenu(this.components);
			this.menuItem1 = new System.Windows.Forms.MenuItem();
			this.menuItem2 = new System.Windows.Forms.MenuItem();
			this.menuItem3 = new System.Windows.Forms.MenuItem();
			this.menuItem4 = new System.Windows.Forms.MenuItem();
			this.menuItem5 = new System.Windows.Forms.MenuItem();
			this.menuItem13 = new System.Windows.Forms.MenuItem();
			this.menuItem11 = new System.Windows.Forms.MenuItem();
			this.menuItem12 = new System.Windows.Forms.MenuItem();
			this.menuItem15 = new System.Windows.Forms.MenuItem();
			this.menuItem16 = new System.Windows.Forms.MenuItem();
			this.menuItem6 = new System.Windows.Forms.MenuItem();
			this.menuItem7 = new System.Windows.Forms.MenuItem();
			this.menuItem8 = new System.Windows.Forms.MenuItem();
			this.menuItem9 = new System.Windows.Forms.MenuItem();
			this.menuItem10 = new System.Windows.Forms.MenuItem();
			this.menuItem14 = new System.Windows.Forms.MenuItem();
			this.ctxMenuFileTree = new System.Windows.Forms.ContextMenuStrip(this.components);
			this.createNewFolderToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			this.renameToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			this.deleteToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
			this.trvDataObjects = new Controls.NicerTreeView();
			this.trvEmbeddedFiles = new Controls.NicerTreeView();
			this.lsvDataTypes = new Controls.NicerListView();
			this.windowStatusStrip.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).BeginInit();
			this.splitContainer1.Panel1.SuspendLayout();
			this.splitContainer1.Panel2.SuspendLayout();
			this.splitContainer1.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.splitContainer3)).BeginInit();
			this.splitContainer3.Panel1.SuspendLayout();
			this.splitContainer3.Panel2.SuspendLayout();
			this.splitContainer3.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.splitContainer2)).BeginInit();
			this.splitContainer2.Panel1.SuspendLayout();
			this.splitContainer2.Panel2.SuspendLayout();
			this.splitContainer2.SuspendLayout();
			this.tabCtrlMain.SuspendLayout();
			this.mnuDataTypes.SuspendLayout();
			this.ctxMenuDataItem.SuspendLayout();
			this.ctxMenuFileTree.SuspendLayout();
			this.SuspendLayout();
			// 
			// notifyIcon1
			// 
			this.notifyIcon1.Text = "notifyIcon1";
			this.notifyIcon1.Visible = true;
			// 
			// windowStatusStrip
			// 
			this.windowStatusStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.lblStatusName,
            this.lblStatusID,
            this.lblStatusTypeGUID});
			this.windowStatusStrip.Location = new System.Drawing.Point(0, 420);
			this.windowStatusStrip.Name = "windowStatusStrip";
			this.windowStatusStrip.Padding = new System.Windows.Forms.Padding(1, 0, 16, 0);
			this.windowStatusStrip.Size = new System.Drawing.Size(763, 24);
			this.windowStatusStrip.TabIndex = 1;
			// 
			// lblStatusName
			// 
			this.lblStatusName.BorderSides = System.Windows.Forms.ToolStripStatusLabelBorderSides.Right;
			this.lblStatusName.BorderStyle = System.Windows.Forms.Border3DStyle.Etched;
			this.lblStatusName.Margin = new System.Windows.Forms.Padding(0, 3, -3, 2);
			this.lblStatusName.Name = "lblStatusName";
			this.lblStatusName.Padding = new System.Windows.Forms.Padding(3, 0, 3, 0);
			this.lblStatusName.Size = new System.Drawing.Size(49, 19);
			this.lblStatusName.Text = "Ready";
			// 
			// lblStatusID
			// 
			this.lblStatusID.BorderStyle = System.Windows.Forms.Border3DStyle.Etched;
			this.lblStatusID.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
			this.lblStatusID.Margin = new System.Windows.Forms.Padding(0, 3, -3, 2);
			this.lblStatusID.Name = "lblStatusID";
			this.lblStatusID.Padding = new System.Windows.Forms.Padding(3, 0, 3, 0);
			this.lblStatusID.Size = new System.Drawing.Size(6, 19);
			// 
			// lblStatusTypeGUID
			// 
			this.lblStatusTypeGUID.BorderStyle = System.Windows.Forms.Border3DStyle.Etched;
			this.lblStatusTypeGUID.Name = "lblStatusTypeGUID";
			this.lblStatusTypeGUID.Padding = new System.Windows.Forms.Padding(3, 0, 3, 0);
			this.lblStatusTypeGUID.Size = new System.Drawing.Size(6, 19);
			// 
			// notifyIcon2
			// 
			this.notifyIcon2.Text = "notifyIcon2";
			this.notifyIcon2.Visible = true;
			// 
			// splitContainer1
			// 
			this.splitContainer1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
			this.splitContainer1.Dock = System.Windows.Forms.DockStyle.Fill;
			this.splitContainer1.Location = new System.Drawing.Point(0, 0);
			this.splitContainer1.Name = "splitContainer1";
			// 
			// splitContainer1.Panel1
			// 
			this.splitContainer1.Panel1.Controls.Add(this.splitContainer3);
			this.splitContainer1.Panel1.Padding = new System.Windows.Forms.Padding(2, 1, 0, 2);
			// 
			// splitContainer1.Panel2
			// 
			this.splitContainer1.Panel2.Controls.Add(this.splitContainer2);
			this.splitContainer1.Panel2.Padding = new System.Windows.Forms.Padding(0, 1, 2, 2);
			this.splitContainer1.Size = new System.Drawing.Size(763, 420);
			this.splitContainer1.SplitterDistance = 253;
			this.splitContainer1.SplitterWidth = 5;
			this.splitContainer1.TabIndex = 2;
			// 
			// splitContainer3
			// 
			this.splitContainer3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
			this.splitContainer3.Dock = System.Windows.Forms.DockStyle.Fill;
			this.splitContainer3.Location = new System.Drawing.Point(2, 1);
			this.splitContainer3.Name = "splitContainer3";
			this.splitContainer3.Orientation = System.Windows.Forms.Orientation.Horizontal;
			// 
			// splitContainer3.Panel1
			// 
			this.splitContainer3.Panel1.Controls.Add(this.trvDataObjects);
			this.splitContainer3.Panel1.Controls.Add(this.label4);
			// 
			// splitContainer3.Panel2
			// 
			this.splitContainer3.Panel2.Controls.Add(this.trvEmbeddedFiles);
			this.splitContainer3.Panel2.Controls.Add(this.label5);
			this.splitContainer3.Size = new System.Drawing.Size(251, 417);
			this.splitContainer3.SplitterDistance = 218;
			this.splitContainer3.SplitterWidth = 2;
			this.splitContainer3.TabIndex = 1;
			// 
			// label4
			// 
			this.label4.BackColor = System.Drawing.SystemColors.GradientInactiveCaption;
			this.label4.Dock = System.Windows.Forms.DockStyle.Top;
			this.label4.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.label4.Location = new System.Drawing.Point(0, 0);
			this.label4.Name = "label4";
			this.label4.Size = new System.Drawing.Size(249, 20);
			this.label4.TabIndex = 0;
			this.label4.Text = "[File] Data Objects";
			// 
			// trvIconList
			// 
			this.trvIconList.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("trvIconList.ImageStream")));
			this.trvIconList.TransparentColor = System.Drawing.Color.Transparent;
			this.trvIconList.Images.SetKeyName(0, "File.png");
			this.trvIconList.Images.SetKeyName(1, "folder.png");
			// 
			// label5
			// 
			this.label5.BackColor = System.Drawing.SystemColors.GradientInactiveCaption;
			this.label5.Dock = System.Windows.Forms.DockStyle.Top;
			this.label5.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.label5.Location = new System.Drawing.Point(0, 0);
			this.label5.Name = "label5";
			this.label5.Size = new System.Drawing.Size(249, 20);
			this.label5.TabIndex = 0;
			this.label5.Text = "[File] Embedded Files";
			// 
			// splitContainer2
			// 
			this.splitContainer2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
			this.splitContainer2.Dock = System.Windows.Forms.DockStyle.Fill;
			this.splitContainer2.Location = new System.Drawing.Point(0, 1);
			this.splitContainer2.Name = "splitContainer2";
			this.splitContainer2.Orientation = System.Windows.Forms.Orientation.Horizontal;
			// 
			// splitContainer2.Panel1
			// 
			this.splitContainer2.Panel1.BackColor = System.Drawing.SystemColors.InactiveCaption;
			this.splitContainer2.Panel1.Controls.Add(this.tabCtrlMain);
			this.splitContainer2.Panel1.Resize += new System.EventHandler(this.splitContainer2_Panel1_Resize);
			// 
			// splitContainer2.Panel2
			// 
			this.splitContainer2.Panel2.Controls.Add(this.mnuDataTypes);
			this.splitContainer2.Panel2.Controls.Add(this.lsvDataTypes);
			this.splitContainer2.Panel2.Controls.Add(this.label1);
			this.splitContainer2.Size = new System.Drawing.Size(503, 417);
			this.splitContainer2.SplitterDistance = 285;
			this.splitContainer2.SplitterWidth = 5;
			this.splitContainer2.TabIndex = 0;
			// 
			// tabCtrlMain
			// 
			this.tabCtrlMain.Controls.Add(this.tabEditor);
			this.tabCtrlMain.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.tabCtrlMain.ItemSize = new System.Drawing.Size(48, 17);
			this.tabCtrlMain.Location = new System.Drawing.Point(-1, -1);
			this.tabCtrlMain.Name = "tabCtrlMain";
			this.tabCtrlMain.SelectedIndex = 0;
			this.tabCtrlMain.Size = new System.Drawing.Size(505, 316);
			this.tabCtrlMain.TabIndex = 0;
			// 
			// tabEditor
			// 
			this.tabEditor.Location = new System.Drawing.Point(4, 21);
			this.tabEditor.Margin = new System.Windows.Forms.Padding(0);
			this.tabEditor.Name = "tabEditor";
			this.tabEditor.Padding = new System.Windows.Forms.Padding(0, 0, 2, 0);
			this.tabEditor.Size = new System.Drawing.Size(497, 291);
			this.tabEditor.TabIndex = 0;
			this.tabEditor.Text = "Editor";
			this.tabEditor.UseVisualStyleBackColor = true;
			// 
			// mnuDataTypes
			// 
			this.mnuDataTypes.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
			this.mnuDataTypes.AutoSize = false;
			this.mnuDataTypes.BackColor = System.Drawing.SystemColors.InactiveCaption;
			this.mnuDataTypes.Dock = System.Windows.Forms.DockStyle.None;
			this.mnuDataTypes.Font = new System.Drawing.Font("Segoe UI", 8F);
			this.mnuDataTypes.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripMenuItem1});
			this.mnuDataTypes.LayoutStyle = System.Windows.Forms.ToolStripLayoutStyle.HorizontalStackWithOverflow;
			this.mnuDataTypes.Location = new System.Drawing.Point(474, 0);
			this.mnuDataTypes.Name = "mnuDataTypes";
			this.mnuDataTypes.Padding = new System.Windows.Forms.Padding(0);
			this.mnuDataTypes.RenderMode = System.Windows.Forms.ToolStripRenderMode.Professional;
			this.mnuDataTypes.Size = new System.Drawing.Size(111, 22);
			this.mnuDataTypes.TabIndex = 2;
			this.mnuDataTypes.Text = "menuStrip1";
			// 
			// toolStripMenuItem1
			// 
			this.toolStripMenuItem1.AutoSize = false;
			this.toolStripMenuItem1.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.createNewTypeToolStripMenuItem1,
            this.loadTypesToolStripMenuItem1,
            this.toolStripMenuItem2,
            this.sortByToolStripMenuItem});
			this.toolStripMenuItem1.Font = new System.Drawing.Font("Segoe UI", 8F);
			this.toolStripMenuItem1.ForeColor = System.Drawing.SystemColors.GrayText;
			this.toolStripMenuItem1.Name = "toolStripMenuItem1";
			this.toolStripMenuItem1.Padding = new System.Windows.Forms.Padding(0);
			this.toolStripMenuItem1.Size = new System.Drawing.Size(22, 17);
			this.toolStripMenuItem1.Text = "▼";
			this.toolStripMenuItem1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// createNewTypeToolStripMenuItem1
			// 
			this.createNewTypeToolStripMenuItem1.Font = new System.Drawing.Font("Segoe UI", 9F);
			this.createNewTypeToolStripMenuItem1.Name = "createNewTypeToolStripMenuItem1";
			this.createNewTypeToolStripMenuItem1.Size = new System.Drawing.Size(164, 22);
			this.createNewTypeToolStripMenuItem1.Text = "Create New Type";
			this.createNewTypeToolStripMenuItem1.Click += new System.EventHandler(this.createNewTypeToolStripMenuItem1_Click);
			// 
			// loadTypesToolStripMenuItem1
			// 
			this.loadTypesToolStripMenuItem1.Font = new System.Drawing.Font("Segoe UI", 9F);
			this.loadTypesToolStripMenuItem1.Name = "loadTypesToolStripMenuItem1";
			this.loadTypesToolStripMenuItem1.Size = new System.Drawing.Size(164, 22);
			this.loadTypesToolStripMenuItem1.Text = "Load Types...";
			this.loadTypesToolStripMenuItem1.Click += new System.EventHandler(this.loadTypesToolStripMenuItem1_Click);
			// 
			// toolStripMenuItem2
			// 
			this.toolStripMenuItem2.Name = "toolStripMenuItem2";
			this.toolStripMenuItem2.Size = new System.Drawing.Size(161, 6);
			// 
			// sortByToolStripMenuItem
			// 
			this.sortByToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.ascendingToolStripMenuItem,
            this.descendingToolStripMenuItem});
			this.sortByToolStripMenuItem.Font = new System.Drawing.Font("Segoe UI", 9F);
			this.sortByToolStripMenuItem.Name = "sortByToolStripMenuItem";
			this.sortByToolStripMenuItem.Size = new System.Drawing.Size(164, 22);
			this.sortByToolStripMenuItem.Text = "Sort By Name";
			// 
			// ascendingToolStripMenuItem
			// 
			this.ascendingToolStripMenuItem.Name = "ascendingToolStripMenuItem";
			this.ascendingToolStripMenuItem.Size = new System.Drawing.Size(136, 22);
			this.ascendingToolStripMenuItem.Text = "Ascending";
			this.ascendingToolStripMenuItem.Click += new System.EventHandler(this.ascendingToolStripMenuItem_Click);
			// 
			// descendingToolStripMenuItem
			// 
			this.descendingToolStripMenuItem.Name = "descendingToolStripMenuItem";
			this.descendingToolStripMenuItem.Size = new System.Drawing.Size(136, 22);
			this.descendingToolStripMenuItem.Text = "Descending";
			this.descendingToolStripMenuItem.Click += new System.EventHandler(this.descendingToolStripMenuItem_Click);
			// 
			// label1
			// 
			this.label1.BackColor = System.Drawing.SystemColors.InactiveCaption;
			this.label1.Dock = System.Windows.Forms.DockStyle.Top;
			this.label1.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.label1.Location = new System.Drawing.Point(0, 0);
			this.label1.Name = "label1";
			this.label1.Size = new System.Drawing.Size(501, 22);
			this.label1.TabIndex = 0;
			this.label1.Text = "Data Types";
			this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// ctxMenuDataItem
			// 
			this.ctxMenuDataItem.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.editToolStripMenuItem,
            this.deleteToolStripMenuItem});
			this.ctxMenuDataItem.Name = "ctxMenuDataItem";
			this.ctxMenuDataItem.Size = new System.Drawing.Size(108, 48);
			// 
			// editToolStripMenuItem
			// 
			this.editToolStripMenuItem.Name = "editToolStripMenuItem";
			this.editToolStripMenuItem.Size = new System.Drawing.Size(107, 22);
			this.editToolStripMenuItem.Text = "Edit";
			this.editToolStripMenuItem.Click += new System.EventHandler(this.editToolStripMenuItem_Click);
			// 
			// deleteToolStripMenuItem
			// 
			this.deleteToolStripMenuItem.Name = "deleteToolStripMenuItem";
			this.deleteToolStripMenuItem.Size = new System.Drawing.Size(107, 22);
			this.deleteToolStripMenuItem.Text = "Delete";
			this.deleteToolStripMenuItem.Click += new System.EventHandler(this.deleteToolStripMenuItem_Click);
			// 
			// windowMenu
			// 
			this.windowMenu.MenuItems.AddRange(new System.Windows.Forms.MenuItem[] {
            this.menuItem1,
            this.menuItem8,
            this.menuItem14});
			// 
			// menuItem1
			// 
			this.menuItem1.Index = 0;
			this.menuItem1.MenuItems.AddRange(new System.Windows.Forms.MenuItem[] {
            this.menuItem2,
            this.menuItem3,
            this.menuItem4,
            this.menuItem5,
            this.menuItem13,
            this.menuItem11,
            this.menuItem12,
            this.menuItem6,
            this.menuItem7});
			this.menuItem1.Text = "File";
			// 
			// menuItem2
			// 
			this.menuItem2.Index = 0;
			this.menuItem2.Shortcut = System.Windows.Forms.Shortcut.CtrlN;
			this.menuItem2.Text = "New";
			this.menuItem2.Click += new System.EventHandler(this.menuItem2_Click);
			// 
			// menuItem3
			// 
			this.menuItem3.Index = 1;
			this.menuItem3.Shortcut = System.Windows.Forms.Shortcut.CtrlO;
			this.menuItem3.Text = "Open...";
			this.menuItem3.Click += new System.EventHandler(this.menuItem3_Click);
			// 
			// menuItem4
			// 
			this.menuItem4.Index = 2;
			this.menuItem4.Shortcut = System.Windows.Forms.Shortcut.CtrlS;
			this.menuItem4.Text = "Save";
			this.menuItem4.Click += new System.EventHandler(this.menuItem4_Click);
			// 
			// menuItem5
			// 
			this.menuItem5.Index = 3;
			this.menuItem5.Text = "Save As...";
			this.menuItem5.Click += new System.EventHandler(this.menuItem5_Click);
			// 
			// menuItem13
			// 
			this.menuItem13.Index = 4;
			this.menuItem13.Text = "-";
			// 
			// menuItem11
			// 
			this.menuItem11.Index = 5;
			this.menuItem11.Text = "Import Definitions";
			this.menuItem11.Click += new System.EventHandler(this.menuItem11_Click);
			// 
			// menuItem12
			// 
			this.menuItem12.Index = 6;
			this.menuItem12.MenuItems.AddRange(new System.Windows.Forms.MenuItem[] {
            this.menuItem15,
            this.menuItem16});
			this.menuItem12.Text = "Export";
			this.menuItem12.Click += new System.EventHandler(this.menuItem12_Click);
			// 
			// menuItem15
			// 
			this.menuItem15.Index = 0;
			this.menuItem15.Text = "Definitions";
			this.menuItem15.Click += new System.EventHandler(this.menuItem12_Click);
			// 
			// menuItem16
			// 
			this.menuItem16.Index = 1;
			this.menuItem16.Text = "C++ Header";
			this.menuItem16.Click += new System.EventHandler(this.menuItem16_Click);
			// 
			// menuItem6
			// 
			this.menuItem6.Index = 7;
			this.menuItem6.Text = "-";
			// 
			// menuItem7
			// 
			this.menuItem7.Index = 8;
			this.menuItem7.Text = "Exit";
			this.menuItem7.Click += new System.EventHandler(this.menuItem7_Click);
			// 
			// menuItem8
			// 
			this.menuItem8.Index = 1;
			this.menuItem8.MenuItems.AddRange(new System.Windows.Forms.MenuItem[] {
            this.menuItem9,
            this.menuItem10});
			this.menuItem8.Text = "Data";
			// 
			// menuItem9
			// 
			this.menuItem9.Index = 0;
			this.menuItem9.Text = "Create New Type";
			this.menuItem9.Click += new System.EventHandler(this.createNewTypeToolStripMenuItem1_Click);
			// 
			// menuItem10
			// 
			this.menuItem10.Index = 1;
			this.menuItem10.Text = "Load Types...";
			this.menuItem10.Click += new System.EventHandler(this.loadTypesToolStripMenuItem1_Click);
			// 
			// menuItem14
			// 
			this.menuItem14.Index = 2;
			this.menuItem14.Text = "About";
			this.menuItem14.Click += new System.EventHandler(this.menuItem14_Click);
			// 
			// ctxMenuFileTree
			// 
			this.ctxMenuFileTree.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.createNewFolderToolStripMenuItem,
            this.renameToolStripMenuItem,
            this.deleteToolStripMenuItem1});
			this.ctxMenuFileTree.Name = "ctxMenuFileTree";
			this.ctxMenuFileTree.Size = new System.Drawing.Size(172, 70);
			// 
			// createNewFolderToolStripMenuItem
			// 
			this.createNewFolderToolStripMenuItem.Name = "createNewFolderToolStripMenuItem";
			this.createNewFolderToolStripMenuItem.Size = new System.Drawing.Size(171, 22);
			this.createNewFolderToolStripMenuItem.Text = "Create New Folder";
			this.createNewFolderToolStripMenuItem.Click += new System.EventHandler(this.createNewFolderToolStripMenuItem_Click);
			// 
			// renameToolStripMenuItem
			// 
			this.renameToolStripMenuItem.Name = "renameToolStripMenuItem";
			this.renameToolStripMenuItem.Size = new System.Drawing.Size(171, 22);
			this.renameToolStripMenuItem.Text = "Rename";
			this.renameToolStripMenuItem.Click += new System.EventHandler(this.renameToolStripMenuItem_Click);
			// 
			// deleteToolStripMenuItem1
			// 
			this.deleteToolStripMenuItem1.Name = "deleteToolStripMenuItem1";
			this.deleteToolStripMenuItem1.Size = new System.Drawing.Size(171, 22);
			this.deleteToolStripMenuItem1.Text = "Delete";
			this.deleteToolStripMenuItem1.Click += new System.EventHandler(this.deleteToolStripMenuItem1_Click);
			// 
			// trvDataObjects
			// 
			this.trvDataObjects.AllowDrop = true;
			this.trvDataObjects.BorderStyle = System.Windows.Forms.BorderStyle.None;
			this.trvDataObjects.Dock = System.Windows.Forms.DockStyle.Fill;
			this.trvDataObjects.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.trvDataObjects.HideSelection = false;
			this.trvDataObjects.LabelEdit = true;
			this.trvDataObjects.Location = new System.Drawing.Point(0, 20);
			this.trvDataObjects.MaxLabelLength = 20;
			this.trvDataObjects.Name = "trvDataObjects";
			this.trvDataObjects.Size = new System.Drawing.Size(249, 196);
			this.trvDataObjects.TabIndex = 1;
			this.trvDataObjects.BeforeLabelEdit += new System.Windows.Forms.NodeLabelEditEventHandler(this.trvDataObjects_BeforeLabelEdit);
			this.trvDataObjects.AfterLabelEdit += new System.Windows.Forms.NodeLabelEditEventHandler(this.trvDataObjects_AfterLabelEdit);
			this.trvDataObjects.AfterSelect += new System.Windows.Forms.TreeViewEventHandler(this.trvDataObjects_AfterSelect);
			this.trvDataObjects.DragDrop += new System.Windows.Forms.DragEventHandler(this.trvDataObjects_DragDrop);
			this.trvDataObjects.DragEnter += new System.Windows.Forms.DragEventHandler(this.trvDataObjects_DragEnter);
			this.trvDataObjects.DragOver += new System.Windows.Forms.DragEventHandler(this.trvDataObjects_DragOver);
			this.trvDataObjects.KeyDown += new System.Windows.Forms.KeyEventHandler(this.trvDataObjects_KeyDown);
			this.trvDataObjects.MouseDoubleClick += new System.Windows.Forms.MouseEventHandler(this.trvDataObjects_MouseDoubleClick);
			// 
			// trvEmbeddedFiles
			// 
			this.trvEmbeddedFiles.AllowDrop = true;
			this.trvEmbeddedFiles.BorderStyle = System.Windows.Forms.BorderStyle.None;
			this.trvEmbeddedFiles.Dock = System.Windows.Forms.DockStyle.Fill;
			this.trvEmbeddedFiles.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.trvEmbeddedFiles.HideSelection = false;
			this.trvEmbeddedFiles.HotTracking = true;
			this.trvEmbeddedFiles.ImageIndex = 0;
			this.trvEmbeddedFiles.ImageList = this.trvIconList;
			this.trvEmbeddedFiles.LabelEdit = true;
			this.trvEmbeddedFiles.Location = new System.Drawing.Point(0, 20);
			this.trvEmbeddedFiles.Name = "trvEmbeddedFiles";
			this.trvEmbeddedFiles.SelectedImageIndex = 0;
			this.trvEmbeddedFiles.Size = new System.Drawing.Size(249, 175);
			this.trvEmbeddedFiles.TabIndex = 1;
			this.trvEmbeddedFiles.AfterLabelEdit += new System.Windows.Forms.NodeLabelEditEventHandler(this.trvEmbeddedFiles_AfterLabelEdit);
			this.trvEmbeddedFiles.ItemDrag += new System.Windows.Forms.ItemDragEventHandler(this.trvEmbeddedFiles_ItemDrag);
			this.trvEmbeddedFiles.DragDrop += new System.Windows.Forms.DragEventHandler(this.trvEmbeddedFiles_DragDrop);
			this.trvEmbeddedFiles.DragEnter += new System.Windows.Forms.DragEventHandler(this.trvEmbeddedFiles_DragEnter);
			this.trvEmbeddedFiles.DragOver += new System.Windows.Forms.DragEventHandler(this.trvEmbeddedFiles_DragOver);
			this.trvEmbeddedFiles.KeyDown += new System.Windows.Forms.KeyEventHandler(this.trvEmbeddedFiles_KeyDown);
			this.trvEmbeddedFiles.MouseDown += new System.Windows.Forms.MouseEventHandler(this.trvEmbeddedFiles_MouseDown);
			// 
			// lsvDataTypes
			// 
			this.lsvDataTypes.Activation = System.Windows.Forms.ItemActivation.OneClick;
			this.lsvDataTypes.AutoArrange = false;
			this.lsvDataTypes.BorderStyle = System.Windows.Forms.BorderStyle.None;
			this.lsvDataTypes.Dock = System.Windows.Forms.DockStyle.Fill;
			this.lsvDataTypes.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			listViewGroup1.Header = "Standard";
			listViewGroup1.HeaderAlignment = System.Windows.Forms.HorizontalAlignment.Center;
			listViewGroup1.Name = "lsvGrpStandard";
			listViewGroup2.Header = "Custom";
			listViewGroup2.HeaderAlignment = System.Windows.Forms.HorizontalAlignment.Center;
			listViewGroup2.Name = "lsvGrpCustom";
			listViewGroup3.Header = "Extended";
			listViewGroup3.HeaderAlignment = System.Windows.Forms.HorizontalAlignment.Center;
			listViewGroup3.Name = "lsvGrpExtended";
			this.lsvDataTypes.Groups.AddRange(new System.Windows.Forms.ListViewGroup[] {
            listViewGroup1,
            listViewGroup2,
            listViewGroup3});
			this.lsvDataTypes.Location = new System.Drawing.Point(0, 22);
			this.lsvDataTypes.MultiSelect = false;
			this.lsvDataTypes.Name = "lsvDataTypes";
			this.lsvDataTypes.OwnerDraw = true;
			this.lsvDataTypes.ShowItemToolTips = true;
			this.lsvDataTypes.Size = new System.Drawing.Size(501, 103);
			this.lsvDataTypes.TabIndex = 1;
			this.lsvDataTypes.TileSize = new System.Drawing.Size(120, 30);
			this.lsvDataTypes.UseCompatibleStateImageBehavior = false;
			this.lsvDataTypes.View = System.Windows.Forms.View.Tile;
			this.lsvDataTypes.ItemDrag += new System.Windows.Forms.ItemDragEventHandler(this.lsvDataTypes_ItemDrag);
			this.lsvDataTypes.MouseClick += new System.Windows.Forms.MouseEventHandler(this.lsvDataTypes_MouseClick);
			// 
			// MainForm
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 15F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(763, 444);
			this.Controls.Add(this.splitContainer1);
			this.Controls.Add(this.windowStatusStrip);
			this.DoubleBuffered = true;
			this.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.Menu = this.windowMenu;
			this.Name = "MainForm";
			this.Text = "DataMan";
			this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.MainForm_FormClosed);
			this.Shown += new System.EventHandler(this.MainForm_Shown);
			this.Resize += new System.EventHandler(this.MainForm_Resize);
			this.windowStatusStrip.ResumeLayout(false);
			this.windowStatusStrip.PerformLayout();
			this.splitContainer1.Panel1.ResumeLayout(false);
			this.splitContainer1.Panel2.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).EndInit();
			this.splitContainer1.ResumeLayout(false);
			this.splitContainer3.Panel1.ResumeLayout(false);
			this.splitContainer3.Panel2.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.splitContainer3)).EndInit();
			this.splitContainer3.ResumeLayout(false);
			this.splitContainer2.Panel1.ResumeLayout(false);
			this.splitContainer2.Panel2.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.splitContainer2)).EndInit();
			this.splitContainer2.ResumeLayout(false);
			this.tabCtrlMain.ResumeLayout(false);
			this.mnuDataTypes.ResumeLayout(false);
			this.mnuDataTypes.PerformLayout();
			this.ctxMenuDataItem.ResumeLayout(false);
			this.ctxMenuFileTree.ResumeLayout(false);
			this.ResumeLayout(false);
			this.PerformLayout();

		}

		#endregion

		private System.Windows.Forms.NotifyIcon notifyIcon1;
		private System.Windows.Forms.StatusStrip windowStatusStrip;
		private System.Windows.Forms.NotifyIcon notifyIcon2;
		private System.Windows.Forms.SplitContainer splitContainer1;
		private System.Windows.Forms.SplitContainer splitContainer2;
		private System.Windows.Forms.Label label1;
		private System.Windows.Forms.SplitContainer splitContainer3;
		private System.Windows.Forms.Label label4;
		private System.Windows.Forms.Label label5;
		private Controls.NicerTreeView trvDataObjects;
		private Controls.NicerTreeView trvEmbeddedFiles;
		private Controls.NicerListView lsvDataTypes;
		private System.Windows.Forms.ContextMenuStrip ctxMenuDataItem;
		private System.Windows.Forms.ToolStripMenuItem editToolStripMenuItem;
		private System.Windows.Forms.ToolStripMenuItem deleteToolStripMenuItem;
		private System.Windows.Forms.MenuStrip mnuDataTypes;
		private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem1;
		private System.Windows.Forms.ToolStripMenuItem createNewTypeToolStripMenuItem1;
		private System.Windows.Forms.ToolStripMenuItem loadTypesToolStripMenuItem1;
		private System.Windows.Forms.ToolStripSeparator toolStripMenuItem2;
		private System.Windows.Forms.ToolStripMenuItem sortByToolStripMenuItem;
		private System.Windows.Forms.ToolStripMenuItem ascendingToolStripMenuItem;
		private System.Windows.Forms.ToolStripMenuItem descendingToolStripMenuItem;
		private System.Windows.Forms.TabControl tabCtrlMain;
		private System.Windows.Forms.TabPage tabEditor;
		private System.Windows.Forms.ToolStripStatusLabel lblStatusName;
		private System.Windows.Forms.ToolStripStatusLabel lblStatusID;
		private System.Windows.Forms.ToolStripStatusLabel lblStatusTypeGUID;
		private System.Windows.Forms.MainMenu windowMenu;
		private System.Windows.Forms.MenuItem menuItem1;
		private System.Windows.Forms.MenuItem menuItem2;
		private System.Windows.Forms.MenuItem menuItem3;
		private System.Windows.Forms.MenuItem menuItem4;
		private System.Windows.Forms.MenuItem menuItem5;
		private System.Windows.Forms.MenuItem menuItem6;
		private System.Windows.Forms.MenuItem menuItem7;
		private System.Windows.Forms.MenuItem menuItem8;
		private System.Windows.Forms.MenuItem menuItem9;
		private System.Windows.Forms.MenuItem menuItem10;
		private System.Windows.Forms.MenuItem menuItem13;
		private System.Windows.Forms.MenuItem menuItem11;
		private System.Windows.Forms.MenuItem menuItem12;
		private System.Windows.Forms.ImageList trvIconList;
		private System.Windows.Forms.ContextMenuStrip ctxMenuFileTree;
		private System.Windows.Forms.ToolStripMenuItem createNewFolderToolStripMenuItem;
		private System.Windows.Forms.ToolStripMenuItem renameToolStripMenuItem;
		private System.Windows.Forms.ToolStripMenuItem deleteToolStripMenuItem1;
		private System.Windows.Forms.MenuItem menuItem14;
		private System.Windows.Forms.MenuItem menuItem15;
		private System.Windows.Forms.MenuItem menuItem16;

	}
}

