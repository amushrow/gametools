﻿//=============================================================//
//                                                             //
//                    DataMan::MainForm.cs                     //
//   -------------------------------------------------------   //
//  Copyright © 2011 Anthony Mushrow, SKGenius Development     //
//  All rights reserved                                        //
//                                                             //
//  This code is licensed under The Code Project Open License  //
//  (CPOL) See http://www.codeproject.com/info/cpol10.aspx or  //
//  LICENSE.TXT for more information                           //
//                                                             //
//=============================================================//
using System;
using System.Collections.Generic;
using System.Collections;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Text;
using System.Windows.Forms;
using DataLibInterfaces;
using DataLib;
using DataMan.EditorControls;
using System.IO;

namespace DataMan
{
	public partial class MainForm : Form
	{
		private struct TypeTabPage
		{
			public CustomTypeContainer TypeContainer;
			public TabPage EditorTab;
		}

		private delegate void SortNodeHandler(TreeNodeCollection collection, FileNode node);

		public MainForm()
		{
			InitializeComponent();

			m_dataManager = new DataManager();
			m_typeEditList = new List<TypeTabPage>();
			m_listViewSorter = new Controls.ListViewAlphaSorter();
			this.lsvDataTypes.ListViewItemSorter = m_listViewSorter;
			m_filePath = String.Empty;

			m_propertyViewer = new EditorControls.PropertyViewer();
			m_propertyViewer.Location = new Point(0, 0);
			m_propertyViewer.Dock = DockStyle.Fill;
			
			this.tabEditor.Controls.Add(m_propertyViewer);

			m_plugMan = new PluginManager();
			m_plugMan.ScanPlugins();
			m_plugMan.FormClosed += new FormClosedEventHandler(pluginsUpdated);
			
			foreach (PluginManager.AssemblyGroup gr in m_plugMan.LoadedAssemblies)
			{
				if (gr.Enabled)
				{
					foreach (Plugin plugin in gr.Plugins)
					{
						m_dataManager.Types.Add(plugin);
					}
				}
			}
			populateDataLists();

			m_topId = 1;
			m_sortDelegate = new SortNodeHandler(this.sortNode);

			string timeStamp = DateTime.Now.ToString("yyyyMMddHHmmssfff");
			string tempDir = Path.GetTempPath();
			m_tempDir = Path.Combine(tempDir, timeStamp);
			Directory.CreateDirectory(m_tempDir);
		}

		private void typeCreator_Callback(object sender, EditorControls.TypeCreatorEventArgs e)
		{
			if (!e.Canceled)
			{
				//Copy new definition over the old one
				if (e.Container != null)
				{
					e.Container.Definition.TypeName = e.TypeDef.TypeName;
					e.Container.Definition.Description = e.TypeDef.Description;
					e.Container.Definition.Properties.Clear();
					e.Container.Definition.Properties.AddRange(e.TypeDef.Properties);

					foreach (ListViewItem editItem in this.lsvDataTypes.Items)
					{
						if(editItem.Tag == e.Container)
						{
							editItem.Text = e.Container.Name;
							editItem.ToolTipText = e.Container.Description;
						}
					}

					//--Update any existing data objects of the same type
					foreach (CustomDataType data in m_dataManager.DataObjects)
					{
						data.Rebuild(m_dataManager.Types);
					}

					m_propertyViewer.Clear();
					populateDataLists();
				}
				else
				{
					//Create new type
					CustomTypeContainer newType = new CustomTypeContainer(e.TypeDef, m_dataManager.Types);
					ListViewItem item = new ListViewItem();
					item.Text = newType.Name;
					item.ToolTipText = newType.Description;
					item.Tag = newType;
					item.Group = this.lsvDataTypes.Groups["lsvGrpCustom"];
					this.lsvDataTypes.Items.Add(item);
					m_dataManager.Types.Add(newType);
				}
			}

			EditorControls.CustomTypeCreator typeCreator = sender as EditorControls.CustomTypeCreator;
			typeCreator.Callback -= new EditorControls.CustomTypeCreator.TypeCreatorEventHandler(typeCreator_Callback);
			this.tabCtrlMain.TabPages.Remove(sender as TabPage);

			foreach (TypeTabPage editTab in m_typeEditList)
			{
				if (editTab.EditorTab == typeCreator)
				{
					m_typeEditList.Remove(editTab);
					break;
				}
			}
		}

		private bool nodeExists(TreeNodeCollection collection, string Name)
		{
			bool exists = false;
			foreach (TreeNode node in collection)
			{
				if (String.Compare(node.Text, Name, true) == 0)
				{
					exists = true;
					break;
				}
			}

			return exists;
		}	

		private void setStatusBar(TreeNode item)
		{
			IDataType dataType;
			ITypeContainer typeContainer;

			if (item.Parent == null)
			{
				typeContainer = item.Tag as ITypeContainer;
				this.lblStatusName.Text = String.Format("{0}", typeContainer.Name);
				this.lblStatusID.BorderSides = ToolStripStatusLabelBorderSides.None;
				this.lblStatusID.Text = String.Format("Type GUID: {0}", typeContainer.GUID.ToString("B").ToUpper());
				this.lblStatusTypeGUID.Text = String.Empty;
			}
			else
			{
				dataType = item.Tag as IDataType;
				typeContainer = item.Parent.Tag as ITypeContainer;
				this.lblStatusName.Text = String.Format("[{0}] {1}", dataType.TypeName, dataType.Name);
				this.lblStatusID.BorderSides = ToolStripStatusLabelBorderSides.Right;
				this.lblStatusID.Text = String.Format("ID: {0}", dataType.ID);
				this.lblStatusTypeGUID.Text = String.Format("Type GUID: {0}", typeContainer.GUID.ToString("B").ToUpper());
			}
		}

		private void resetStatusBar()
		{
			this.lblStatusName.Text = String.Format("Ready");
			this.lblStatusID.BorderSides = ToolStripStatusLabelBorderSides.None;
			this.lblStatusID.Text = "";
			this.lblStatusTypeGUID.Text = String.Empty;
		}

		private void populateDataLists()
		{
			//-- Poulate Data Types
			this.lsvDataTypes.BeginUpdate();
			this.lsvDataTypes.Clear();
			foreach (ITypeContainer dataType in m_dataManager.Types)
			{
				ListViewItem item = new ListViewItem();
				item.Text = dataType.Name;
				item.ToolTipText = dataType.Description;
				item.Tag = dataType;

				if (dataType is StandardTypeContainer)
					item.Group = this.lsvDataTypes.Groups["lsvGrpStandard"];

				else if (dataType is CustomTypeContainer)
					item.Group = this.lsvDataTypes.Groups["lsvGrpCustom"];

				else
					item.Group = this.lsvDataTypes.Groups["lsvGrpExtended"];

				this.lsvDataTypes.Items.Add(item);
			}
			this.lsvDataTypes.EndUpdate();
			//--

			//--And Data Objects
			this.trvDataObjects.BeginUpdate();
			this.trvDataObjects.Nodes.Clear();
			foreach (Guid typeGuid in m_dataManager.DataObjects.Groups)
			{
				ITypeContainer container = null;
				if (!m_dataManager.Types.FindType(typeGuid, out container))
				{
					container = new UnknownTypeContainer(typeGuid);
				}

				TreeNode rootNode = new TreeNode(container.Name);
				rootNode.Tag = container;
				this.trvDataObjects.Nodes.Add(rootNode);

				foreach (IDataType dataObject in m_dataManager.DataObjects[typeGuid])
				{
					TreeNode subNode = new TreeNode(dataObject.Name);
					subNode.Tag = dataObject;
					rootNode.Nodes.Add(subNode);
				}
			}
			this.trvDataObjects.EndUpdate();
			//--

			//--Setup file treeview
			this.trvEmbeddedFiles.BeginUpdate();
			this.trvEmbeddedFiles.Nodes.Clear();
			foreach (DataLib.FileCollection.File storedFile in m_dataManager.Files)
			{
				string fullPath = storedFile.Name;
				string[] parts = fullPath.Split('\\');
				TreeNodeCollection collection = this.trvEmbeddedFiles.Nodes;
				int insertIndex;

				//--Setup folder nodes
				for (int i=0; i<parts.Length-1; i++)
				{
					bool found = false;
					foreach (FileNode treeNode in collection)
					{
						if (treeNode.Text == parts[i])
						{
							found = true;
							collection = treeNode.Nodes;
							break;
						}
					}

					if (!found)
					{
						FileNode newFolder = new FileNode(FileNode.Type.Folder);
						newFolder.ImageIndex = 1;
						newFolder.SelectedImageIndex = 1;
						newFolder.Text = parts[i];
						newFolder.Name = newFolder.Text.ToUpper();
						insertIndex = findInsertIndex(parts[i], collection, FileNode.Type.Folder);
						collection.Insert(insertIndex, newFolder);
						collection = newFolder.Nodes;
					}
				}
				//--

				FileNode newFile = new FileNode(FileNode.Type.File);
				newFile.Text = parts[parts.Length - 1];
				newFile.Name = newFile.Text.ToUpper();
				newFile.ImageIndex = 0;
				newFile.SelectedImageIndex = 0;
				newFile.IsEmbedded = storedFile.Embedded;
				newFile.FileID = storedFile.UID;
				insertIndex = findInsertIndex(newFile.Text, collection, FileNode.Type.File);
				collection.Insert(insertIndex, newFile);
			}
			this.trvEmbeddedFiles.EndUpdate();
			//--
		}

		private void reset()
		{
			for (int i = this.tabCtrlMain.TabPages.Count - 1; i > 0; i--)
			{
				this.tabCtrlMain.TabPages.RemoveAt(i);
			}
			m_typeEditList.Clear();
			m_dataManager.Clear();
			m_propertyViewer.Clear();
			this.trvDataObjects.Nodes.Clear();
			m_topId = 1;

			foreach (PluginManager.AssemblyGroup gr in m_plugMan.LoadedAssemblies) {
				if (gr.Enabled) {
					foreach (Plugin plugin in gr.Plugins) {
						m_dataManager.Types.Add(plugin);
					}
				}
			}

			populateDataLists();

			this.Text = "DataMan";
		}
		
		private bool checkSaveState()
		{
			return true;
		}

		private void save(bool saveAs)
		{
			//--Get the file path
			bool haveFile = true;
			if (saveAs || m_filePath == String.Empty)
			{
				SaveFileDialog fileDiag = new SaveFileDialog();
				fileDiag.Filter = "DataMan Files|*.skd";
				fileDiag.AddExtension = true;
				DialogResult dr = fileDiag.ShowDialog();
				if (dr == System.Windows.Forms.DialogResult.OK)
					m_filePath = fileDiag.FileName;
				else
					haveFile = false;
			}
			//--

			if (haveFile)
			{
				BackgroundWorker worker = new BackgroundWorker();
				worker.DoWork += new DoWorkEventHandler(worker_DoWork);

				DistractionBox distraction = new DistractionBox();
				distraction.DisplayText = "Packing...";
				distraction.Worker = worker;
				distraction.StartDistraction();

				populateDataLists();
				string name = Path.GetFileName(m_filePath);
				this.Text = String.Format("DataMan - {0}", name);
			}
		}

		void worker_DoWork(object sender, DoWorkEventArgs e)
		{
			try
			{
				m_dataManager.SaveToFile(m_filePath);
			}
			catch (SaveFileException ecx)
			{
				StringBuilder sb = new StringBuilder();
				sb.Append("Unable to embed following files:\n");
				foreach(string s in ecx.Files)
				{
					sb.Append("  ");
					sb.Append(s);
					sb.Append('\n');
				}
				MessageBox.Show(sb.ToString(), "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
			}
		}

		private void load()
		{
			//--Get the file path
			bool haveFile = false;
			if (checkSaveState())
			{
				OpenFileDialog fileDiag = new OpenFileDialog();
				fileDiag.Filter = "DataMan Files|*.skd";
				fileDiag.Multiselect = false;
				DialogResult dr = fileDiag.ShowDialog();
				if (dr == System.Windows.Forms.DialogResult.OK)
				{
					m_filePath = fileDiag.FileName;
					haveFile = true;
				}
			}
			//--

			if (haveFile)
			{
				reset();
				bool sucess = m_dataManager.LoadFromFile(m_filePath);
				populateDataLists();

				foreach (IDataType data in m_dataManager.DataObjects)
				{
					if (data.ID >= m_topId)
						m_topId = data.ID + 1;
				}

				if (sucess)
				{
					string name = Path.GetFileName(m_filePath);
					this.Text = String.Format("DataMan - {0}", name);
				}
				else
				{
					this.Text = "DataMan";
				}
			}
		}

		private void embedFiles(string file, TreeNodeCollection collection)
		{
			if (Directory.Exists(file))
			{
				DirectoryInfo di = new DirectoryInfo(file);

				FileNode existingFolder = collection[di.Name] as FileNode;
				if (existingFolder == null)
				{
					FileNode newFolder = new FileNode(FileNode.Type.Folder);
					newFolder.Text = di.Name;
					newFolder.Name = newFolder.Text.ToUpper();
					newFolder.ImageIndex = 1;
					newFolder.SelectedImageIndex = 1;
					int insertIndex = findInsertIndex(di.Name, collection, FileNode.Type.Folder);
					collection.Insert(insertIndex, newFolder);
					collection = newFolder.Nodes;
				}
				else
				{
					collection = existingFolder.Nodes;
				}

				foreach (DirectoryInfo subDir in di.GetDirectories())
				{
					embedFiles(subDir.FullName, collection);
				}
				foreach (FileInfo fi in di.GetFiles())
				{
					FileNode existing = collection[fi.Name.ToUpper()] as FileNode;
					if (existing != null)
					{
						string Message = String.Format("[{0}]\n\nA file with that name already exists, do you want to overwrite it?", existing.FullPath);
						DialogResult dr = MessageBox.Show(Message, "Confirm", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
						if (dr == System.Windows.Forms.DialogResult.Yes)
						{
							existing.Remove();
							m_dataManager.Files.Remove(existing.FileID);
						}
						else
						{
							continue;
						}
					}
					FileNode newFile = new FileNode(FileNode.Type.File);
					newFile.Text = fi.Name;
					newFile.Name = newFile.Text.ToUpper(); ;
					newFile.ImageIndex = 0;
					newFile.SelectedImageIndex = 0;
					newFile.FilePath = fi.FullName;

					int insertIndex = findInsertIndex(fi.Name, collection, FileNode.Type.File);
					collection.Insert(insertIndex, newFile);
					newFile.FileID = m_dataManager.Files.Add(fi.FullName, newFile.FullPath);

					this.trvEmbeddedFiles.SelectedNode = newFile;
				}
			}
			else
			{
				string name = Path.GetFileName(file);
				FileNode existing = collection[name.ToUpper()] as FileNode;
				if (existing != null)
				{
					string Message = String.Format("[{0}]\n\nA file with that name already exists, do you want to overwrite it?", existing.FullPath);
					DialogResult dr = MessageBox.Show(Message, "Confirm", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
					if (dr == System.Windows.Forms.DialogResult.Yes)
					{
						existing.Remove();
						m_dataManager.Files.Remove(existing.FileID);
					}
					else
					{
						return;
					}
				}
				FileNode newFile = new FileNode(FileNode.Type.File);
				newFile.Text = name;
				newFile.Name = newFile.Text.ToUpper();
				newFile.ImageIndex = 0;
				newFile.SelectedImageIndex = 0;
				newFile.FilePath = file;

				int insertIndex = findInsertIndex(newFile.Text, collection, FileNode.Type.File);
				collection.Insert(insertIndex, newFile);
				newFile.FileID = m_dataManager.Files.Add(file, newFile.FullPath);

				this.trvEmbeddedFiles.SelectedNode = newFile;
			}
		}

		private int findInsertIndex(string name, TreeNodeCollection collection, FileNode.Type type)
		{
			int insertIndex = 0;
			for (insertIndex = 0; insertIndex < collection.Count; insertIndex++)
			{
				FileNode curNode = collection[insertIndex] as FileNode;
				if (curNode.IsFile && type == FileNode.Type.Folder)
					break;
				else if (curNode.NodeType == type)
				{
					string A = name.ToLower();
					string B = collection[insertIndex].Text.ToLower();
					if (A.CompareTo(B) < 0)
						break;
				}
			}

			return insertIndex;
		}

		private void sortNode(TreeNodeCollection collection, FileNode node)
		{
			node.Remove();
			int insertIndex = findInsertIndex(node.Text, collection, node.NodeType);
			collection.Insert(insertIndex, node);
			m_dataManager.Files.Rename(node.FileID, node.FullPath);
			this.trvEmbeddedFiles.SelectedNode = node;
		}

		private void addFilesToExtract(FileNode root, DataManFileExtractorObj dataObj, string FullPath)
		{
			string relPath = String.Format("{0}{1}\\", FullPath, root.Text);
			foreach (FileNode node in root.Nodes)
			{
				addFilesToExtract(node, dataObj, relPath);
			}

			if (root.IsFile)
			{
				dataObj.AddFile(FullPath + root.Text, root.FileID);
			}
		}

		private void deleteFileNode(FileNode item)
		{
			string msg;
			if (item.IsFolder)
				msg = "Are you sure you want to delete this folder and all of it's contents?";
			else
				msg = "Are you sure you want to delete this file?";
			DialogResult dr = MessageBox.Show(msg, "Confirm", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
			if (dr == System.Windows.Forms.DialogResult.Yes)
			{
				item.Remove();
				clearFiles(item);
			}
		}

		private void clearFiles(FileNode files)
		{
			foreach (FileNode node in files.Nodes)
			{
				if (node.IsFolder)
				{
					clearFiles(node);
				}
				else
				{
					m_dataManager.Files.Remove(node.FileID);
				}
			}

			if (files.IsFile)
				m_dataManager.Files.Remove(files.FileID);
		}

		private int m_topId;
		private DataManager m_dataManager;
		private Controls.ListViewAlphaSorter m_listViewSorter;
		EditorControls.PropertyViewer m_propertyViewer;
		private List<TypeTabPage> m_typeEditList;
		private string m_filePath;
		private PluginManager m_plugMan;
		private SortNodeHandler m_sortDelegate;
		private string m_tempDir;

		private void menuItem16_Click(object sender, EventArgs e)
		{
			SaveFileDialog diag = new SaveFileDialog();
			diag.AddExtension = true;
			diag.DefaultExt = ".h";
			diag.Filter = "C++ Header Files|*.h;*.hh;*.hpp;*.hxx;*.inl;*.tlh;*.tli";
			DialogResult dr = diag.ShowDialog();
			if(dr == System.Windows.Forms.DialogResult.OK)
			{
				CppExporter.ExportDefs(m_dataManager, diag.FileName);
			}
		}
	}
}
