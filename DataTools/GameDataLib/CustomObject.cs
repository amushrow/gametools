﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.IO;

namespace GameDataLib
{
	public class CustomObject
	{
		private string m_name;
		private string m_type;
		private long m_id;
		private List<CustomObjectData> m_data;

		public CustomObject(string name, long id, string type)
		{
			m_name = name;
			m_type = type;
			m_id = id;
			m_data = new List<CustomObjectData>();
		}

		public CustomObject Clone(bool id)
		{
			CustomObject clone;
			if(id)
				clone = new CustomObject(m_name, m_id, m_type);
			else
				clone = new CustomObject(m_name, Globals.GetNextID(), m_type);
			
			foreach (CustomObjectData data in m_data)
			{
				clone.AddData(data.Clone());
			}

			return clone;
		}

		public void InitializeData()
		{
			foreach (CustomObjectData data in m_data)
			{
				switch (data.Type)
				{
					case DataType.INT:
						data.SetValue(0);
						break;

					case DataType.FLOAT:
						data.SetValue(0f);
						break;

					case DataType.STRING:
						data.SetValue("");
						break;

					case DataType.SPRITE:
						data.SetValue(new SpriteData());
						break;

					case DataType.BOOL:
						data.SetValue(false);
						break;

					case DataType.LONG:
						data.SetValue(0);
						break;

					case DataType.CUSTOM_OBJECT:
						data.SetValue((CustomObject)null);
						break;

					case DataType.VECTOR3:
						data.SetValue(new Vector3());
						break;
				}
			}
		}

		public long ID
		{
			get { return m_id; }
		}

		public string ParentType
		{
			get { return m_type; }
			set { m_type = value; }
		}

		public void WriteToStream(Stream output)
		{
			StreamWrapper sw = new StreamWrapper(output);
			
			sw.WriteString(m_name);
			sw.WriteLong(m_id);
			sw.WriteString(m_type);

			sw.WriteInt(m_data.Count);

			foreach (CustomObjectData data in m_data)
			{
				data.WriteToStream(output);
			}
		}

		public static CustomObject ReadFromStream(Stream input)
		{
			StreamWrapper sw = new StreamWrapper(input);
			CustomObject newObj = new CustomObject(sw.ReadString(), sw.ReadLong(), sw.ReadString());

			int count = sw.ReadInt();
			for(int i=0; i<count; i++)
			{
				CustomObjectData data = CustomObjectData.ReadFromStream(input);
				newObj.AddData(data);
			}

			return newObj;
		}

		public void AddData(CustomObjectData obj)
		{
			m_data.Add(obj);
		}

		public CustomObjectData AddData(string name, DataType type, long id)
		{
			CustomObjectData obj = new CustomObjectData(type, name, id);
			m_data.Add(obj);
			return obj;
		}

		public string Name
		{
			get { return m_name; }
			set { m_name = value; }
		}

		public ReadOnlyCollection<CustomObjectData> GetData()
		{
			return m_data.AsReadOnly();
		}

		public void Clear()
		{
			m_name = "";
			m_data.Clear();
		}

		public override string ToString()
		{
			return m_name;
		}
	}
}
