﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.IO;

namespace GameDataLib
{
	public static class Globals
	{
		private static long CurID = 0;

		public static long GetNextID()
		{
			return CurID++;
		}

		public static void IncID()
		{
			CurID++;
		}

		public static void ResetID()
		{
			CurID = 0;
		}

		public static void SetID(long id)
		{
			CurID = id;
		}
	}

	public struct Vector3
	{
		public float X, Y, Z;

		public Vector3(float X, float Y, float Z)
		{
			this.X = X;
			this.Y = Y;
			this.Z = Z;
		}
	}
}
