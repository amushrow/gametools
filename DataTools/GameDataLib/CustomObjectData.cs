﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;

namespace GameDataLib
{
	public enum DataType
	{
		BOOL,
		FLOAT,
		INT,
		SPRITE,
		STRING,
		LONG,
		CUSTOM_OBJECT,
		VECTOR3
	}

	public class CustomObjectData
	{
		private string m_name;
		private DataType m_type;
		private long m_id;

		//Vars
		private float m_fValue;
		private string m_sValue;
		private int m_iValue;
		private bool m_bValue;
		private long m_lValue;
		private CustomObject m_coValue;
		private SpriteData m_spValue;
		private Vector3 m_vValue;

		public CustomObjectData(DataType type, string name, long id)
		{
			m_name = name;
			m_type = type;
			m_id = id;

			m_fValue = 0;
			m_sValue = "";
			m_iValue = 0;
			m_lValue = 0;
			m_bValue = false;
			m_spValue = new SpriteData();
			m_coValue = null;
		}

		public CustomObjectData Clone()
		{
			CustomObjectData data = new CustomObjectData(m_type, m_name, m_id);

			switch (m_type)
			{
				case DataType.INT:
					data.SetValue(m_iValue);
					break;

				case DataType.FLOAT:
					data.SetValue(m_fValue);
					break;

				case DataType.STRING:
					data.SetValue(m_sValue);
					break;

				case DataType.SPRITE:
					if (m_spValue != null)
						data.SetValue(m_spValue.Clone());
					break;

				case DataType.BOOL:
					data.SetValue(m_bValue);
					break;

				case DataType.LONG:
					data.SetValue(m_lValue);
					break;

				case DataType.CUSTOM_OBJECT:
					data.SetValue(m_coValue);
					break;

				case DataType.VECTOR3:
					data.SetValue(m_vValue);
					break;
			}

			return data;
		}

		public DataType Type
		{
			get { return m_type; }
		}

		public string Name
		{
			get { return m_name; }
			set { m_name = value; }
		}

		public long ID
		{
			get { return m_id; }
		}

		public void SetValue(Vector3 val)
		{
			m_vValue = val;
		}

		public void SetValue(float val)
		{
			m_fValue = val;
		}

		public void SetValue(int val)
		{
			m_iValue = val;
		}

		public void SetValue(long val)
		{
			m_lValue = val;
		}

		public void SetValue(string val)
		{
			m_sValue = val;
		}

		public void SetValue(SpriteData val)
		{
			m_spValue = val;
		}

		public void SetValue(CustomObject val)
		{
			m_coValue = val;
		}

		public void SetValue(bool val)
		{
			m_bValue = val;
		}

		public string GetString()
		{
			return m_sValue;
		}

		public int GetInt()
		{
			return m_iValue;
		}

		public float GetFloat()
		{
			return m_fValue;
		}

		public bool GetBool()
		{
			return m_bValue;
		}

		public SpriteData GetSprite()
		{
			return m_spValue;
		}

		public long GetLong()
		{
			return m_lValue;
		}

		public CustomObject GetCustomObject()
		{
			return m_coValue;
		}

		public Vector3 GetVector()
		{
			return m_vValue;
		}

		public override string ToString()
		{
			string type = "";
			switch (m_type)
			{
				case DataType.INT:
					type = " : INT";
					break;

				case DataType.FLOAT:
					type = " : FLOAT";
					break;

				case DataType.STRING:
					type = " : STRING";
					break;

				case DataType.SPRITE:
					type = " : SPRITE";
					break;

				case DataType.BOOL:
					type = " : BOOL";
					break;

				case DataType.LONG:
					type = " : LONG";
					break;

				case DataType.CUSTOM_OBJECT:
					type = " : CUSTOM";
					break;

				case DataType.VECTOR3:
					type = " : VECTOR3";
					break;
			}

			return m_name + type;
		}

		public void WriteToStream(Stream output)
		{
			StreamWrapper sw = new StreamWrapper(output);

			sw.WriteString(m_name);
			sw.WriteInt((int)m_type);
			sw.WriteLong(m_id);

			switch (m_type)
			{
				case DataType.BOOL:
					sw.WriteBool(m_bValue);
					break;

				case DataType.FLOAT:
					sw.WriteFloat(m_fValue);
					break;

				case DataType.INT:
					sw.WriteInt(m_iValue);
					break;

				case DataType.SPRITE:
					m_spValue.WriteToStream(output);
					break;

				case DataType.STRING:
					sw.WriteString(m_sValue);
					break;

				case DataType.LONG:
					sw.WriteLong(m_lValue);
					break;

				case DataType.CUSTOM_OBJECT:
					m_coValue.WriteToStream(output);
					break;

				case DataType.VECTOR3:
					sw.WriteFloat(m_vValue.X);
					sw.WriteFloat(m_vValue.Y);
					sw.WriteFloat(m_vValue.Z);
					break;
			}
		}

		public static CustomObjectData ReadFromStream(Stream input)
		{
			StreamWrapper sw = new StreamWrapper(input);

			string name = sw.ReadString();
			DataType type = (DataType)sw.ReadInt();
			long id = sw.ReadLong();

			CustomObjectData newData = new CustomObjectData(type, name, id);
			switch (type)
			{
				case DataType.BOOL:
					newData.SetValue(sw.ReadBool());
					break;

				case DataType.FLOAT:
					newData.SetValue(sw.ReadFloat());
					break;

				case DataType.INT:
					newData.SetValue(sw.ReadInt());
					break;

				case DataType.SPRITE:
					SpriteData data = SpriteData.ReadFromStream(input);
					newData.SetValue(data);
					break;

				case DataType.STRING:
					newData.SetValue(sw.ReadString());
					break;

				case DataType.LONG:
					newData.SetValue(sw.ReadLong());
					break;

				case DataType.CUSTOM_OBJECT:
					CustomObject newObj = CustomObject.ReadFromStream(input);
					newData.SetValue(newObj);
					break;

				case DataType.VECTOR3:
					Vector3 vec;
					vec.X = sw.ReadFloat();
					vec.Y = sw.ReadFloat();
					vec.Z = sw.ReadFloat();
					newData.SetValue(vec);
					break;
			}

			return newData;
		}
	}
}
