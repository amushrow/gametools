﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.IO;

namespace DataTools
{
	public partial class MainForm : Form
	{
		public MainForm()
		{
			InitializeComponent();
		}

		private void addTypeToolStripMenuItem_Click(object sender, EventArgs e)
		{
			AddObjectTypeDialog diag = new AddObjectTypeDialog();
			if(diag.ShowDialog() == DialogResult.OK)
			{
				ObjectType type = diag.NewType;

				TreeNode newRoot = new TreeNode(type.Name.ToUpper(), 0, 0);
				newRoot.Tag = type;
				this.treeView1.Nodes.Add(newRoot);
			}
		}

		private void treeView1_AfterSelect(object sender, TreeViewEventArgs e)
		{
			ObjectType type = e.Node.Tag as ObjectType;
			if (type == null)
				return;

			if(e.Node.Parent == null)
			{
				setPanelForRoot(type);
			}
			else
			{
				setPanelForChild(type);
			}
		}

		private void setPanelForRoot(ObjectType type)
		{
			this.panel1.SuspendLayout();
			this.panel1.Controls.Clear();

			Button addNew_button = new Button();
			addNew_button.Name = "AddNew_Button";
			addNew_button.Text = "Add New " + type.Name;
			addNew_button.AutoSize = true;
			addNew_button.Click += new EventHandler(this.addNewChild_click);
			addNew_button.Location = new System.Drawing.Point(49, 46);
			addNew_button.UseVisualStyleBackColor = true;

			TextBox nameEdit = new TextBox();
			nameEdit.Name = "Name_EditBox";
			nameEdit.Location = new System.Drawing.Point(49, 20);
			nameEdit.Size = new System.Drawing.Size(156, 20);

			Label nameLabel = new Label();
			nameLabel.Name = "Name_Label";
			nameLabel.Text = "Name:";
			nameLabel.AutoSize = true;
			nameLabel.Location = new System.Drawing.Point(6, 20);
			nameLabel.Size = new System.Drawing.Size(35, 13);

			this.panel1.Controls.Add(addNew_button);
			this.panel1.Controls.Add(nameEdit);
			this.panel1.Controls.Add(nameLabel);

			this.panel1.ResumeLayout();
		}

		private void setPanelForChild(ObjectType type)
		{
			this.panel1.SuspendLayout();
			this.panel1.Controls.Clear();

			TextBox nameEdit = new TextBox();
			nameEdit.Text = type.Name;
			nameEdit.Name = "Name_EditBox";
			nameEdit.Location = new System.Drawing.Point(49, 41);
			nameEdit.Size = new System.Drawing.Size(160, 20);
			this.panel1.Controls.Add(nameEdit);

			Label nameLabel = new Label();
			nameLabel.Name = "Name_Label";
			nameLabel.Text = "Name:";
			nameLabel.AutoSize = true;
			nameLabel.Location = new System.Drawing.Point(6, 44);
			nameLabel.Size = new System.Drawing.Size(35, 13);
			this.panel1.Controls.Add(nameLabel);

            nameLabel = new Label();
            nameLabel.Name = "ID_Label";
            nameLabel.Text = "ID: " + type.ID;
            nameLabel.AutoSize = true;
            nameLabel.Location = new System.Drawing.Point(6, 23);
            nameLabel.Size = new System.Drawing.Size(35, 13);
            this.panel1.Controls.Add(nameLabel);

			int x = 6;
			int y = 67;
			int padding = 8;
			foreach(ObjectData data in type.GetData())
			{
				Label myLabel = new Label();
				myLabel.Size = new Size(15, 15);
				myLabel.AutoSize = true;
				myLabel.Location = new Point(x, y + 4);
				myLabel.Name = data.Name + "_label";
				myLabel.Text = data.Name;
				this.panel1.Controls.Add(myLabel);
				x += myLabel.Width + padding;

				switch(data.Type)
				{
					case DataType.STRING:
						TextBox stringEdit = new TextBox();
						stringEdit.Text = data.GetString();
						stringEdit.Name = data.Name + "_string";
						stringEdit.Location = new System.Drawing.Point(x, y);
						stringEdit.Width = 160;
						this.panel1.Controls.Add(stringEdit);

						x = 6;
						y += stringEdit.Height + padding;
						break;

					case DataType.INT:
						NumericUpDown number = new NumericUpDown();
						number.Text = data.GetString();
						number.Name = data.Name + "_int";
						number.Location = new System.Drawing.Point(x, y);
						number.Increment = 1;
						number.DecimalPlaces = 0;
                        number.Value = data.GetInt();
						number.Minimum = decimal.MinValue;
						number.Maximum = decimal.MaxValue;
						this.panel1.Controls.Add(number);

						x = 6;
						y += number.Height + padding;
						break;

					case DataType.FLOAT:
						number = new NumericUpDown();
						number.Value = (decimal)data.GetFloat();
						number.Name = data.Name + "_float";
						number.Location = new System.Drawing.Point(x, y);
						number.Increment = 0.1m;
						number.DecimalPlaces = 8;
						number.Value = (decimal)data.GetFloat();
						number.Minimum = decimal.MinValue;
						number.Maximum = decimal.MaxValue;
						this.panel1.Controls.Add(number);

						x = 6;
						y += number.Height + padding;
						break;

					case DataType.BOOL:
						CheckBox checkBox = new CheckBox();
						checkBox.Name = data.Name + "_bool";
						checkBox.Location = new System.Drawing.Point(x, y);
						checkBox.Checked = data.GetBool();
						this.panel1.Controls.Add(checkBox);

						x = 6;
						y += checkBox.Height + padding;
						break;

					case DataType.SPRITE:
						Button spriteButton = new Button();
						spriteButton.Name = data.Name + "_sprite";
						spriteButton.Text = "Edit Sprite";
						spriteButton.AutoSize = true;
						spriteButton.Click += new EventHandler(this.editSpriteClick);
						spriteButton.Location = new System.Drawing.Point(x, y);
						spriteButton.UseVisualStyleBackColor = true;
						spriteButton.Tag = data.GetSprite().Clone();
						this.panel1.Controls.Add(spriteButton);

						x = 6;
						y += spriteButton.Height + padding;
						break;
				}
			}


            Button apply = new Button();
            apply.Name = "apply_button";
            apply.Text = "Apply";
            apply.AutoSize = true;
            apply.Click += new EventHandler(this.applyClick);
            apply.Location = new System.Drawing.Point(x, y);
            apply.UseVisualStyleBackColor = true;
            apply.Tag = type;

            this.panel1.Controls.Add(apply);

		}

        private void applyClick(object sender, EventArgs e)
        {
            if (sender is Button)
            {
                Button button_sender = sender as Button;
                if (button_sender.Tag is ObjectType)
                {
                    ObjectType type = button_sender.Tag as ObjectType;

                    foreach (ObjectData data in type.GetData())
                    {
                        switch (data.Type)
                        {
                            case DataType.BOOL:
                                CheckBox cb = this.panel1.Controls[data.Name + "_bool"] as CheckBox;
                                if (cb != null)
                                {
                                    data.SetValue(cb.Checked);
                                }
                                break;

                            case DataType.FLOAT:
                                NumericUpDown nup = this.panel1.Controls[data.Name + "_float"] as NumericUpDown;
                                if (nup != null)
                                {
                                    data.SetValue((float)nup.Value);
                                }
                                break;

                            case DataType.INT:
                                nup = this.panel1.Controls[data.Name + "_int"] as NumericUpDown;
                                if (nup != null)
                                {
                                    data.SetValue((int)nup.Value);
                                }
                                break;

                            case DataType.SPRITE:
                                Button but = this.panel1.Controls[data.Name + "_sprite"] as Button;
                                if (but.Tag is SpriteData)
                                {
                                    data.SetValue(but.Tag as SpriteData);
                                }
                                break;

                            case DataType.STRING:
                                TextBox tb = this.panel1.Controls[data.Name + "_string"] as TextBox;
                                if (tb != null)
                                {
                                    data.SetValue(tb.Text);
                                }
                                break;
                        }
                    }
                }
            }
        }

		private void editSpriteClick(object sender, EventArgs e)
		{
			if (sender is Button)
			{
				Button button_sender = sender as Button;
				if(button_sender.Tag is SpriteData)
				{
					SpriteData data = button_sender.Tag as SpriteData;
					SpriteEditor diag = new SpriteEditor(data);
					diag.ShowDialog();
				}
			}
		}

		private void addNewChild_click(object sender, EventArgs e)
		{
			TextBox box = this.panel1.Controls["Name_EditBox"] as TextBox;
			if(box != null)
			{
				if(box.Text.Length > 0)
				{
					TreeNode newChild = new TreeNode(box.Text, 1, 1);
					ObjectType typeData = this.treeView1.SelectedNode.Tag as ObjectType;
					ObjectType data = typeData.Clone();
					data.InitializeData();
					data.Name = box.Text;
					newChild.Tag = data;

					this.treeView1.SelectedNode.Nodes.Add(newChild);
					this.treeView1.SelectedNode = newChild;
				}
			}
		}

        private void exitToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void saveToolStripMenuItem_Click(object sender, EventArgs e)
        {
            SaveFileDialog diag = new SaveFileDialog();
            diag.AddExtension = true;
            diag.DefaultExt = ".skd";
            diag.Filter = "Game Data File|*.skd";
            if (diag.ShowDialog() == DialogResult.OK)
            {
                FileStream fs = new FileStream(diag.FileName, FileMode.Create);
                fs.Close();
            }
        }
	}
}
