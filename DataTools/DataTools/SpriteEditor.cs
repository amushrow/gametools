﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.IO;

namespace DataTools
{
	public partial class SpriteEditor : Form
	{
		private float m_spriteWidth = 0;
		private float m_spriteHeight = 0;
		private float m_frameInterval = 0.016667f;
		private int m_numFrames = 0;
		private int m_curFrame = -1;
		private int m_previewFrame = -1;
		private string m_texpath;
		private Shape[] m_frameCoords;

		private SpriteData m_spriteData;

		public SpriteEditor(SpriteData data)
		{
			InitializeComponent();
			m_frameCoords = new Shape[30];
			for (int i = 0; i < data.NumFrames; i++)
			{
				m_frameCoords[i] = data.FrameCoords[i];
			}
		
			this.panel1.SetRenderMethod(this.RenderPreview);
			m_spriteData = data;

			this.textBox1.Text = data.TexName;
			this.textBox2.Text = data.Name;
			this.numFramesUpDown.Value = data.NumFrames;
			this.numericUpDown1.Value = (decimal)data.Width;
			this.numericUpDown2.Value = (decimal)data.Width;
			this.numericUpDown3.Value = (decimal)data.FrameInterval;
			m_texpath = data.TexPath;

			if (data.TexPath.Length > 0)
				this.pictureBox1.Image = new Bitmap(data.TexPath);
		}

		public SpriteEditor()
		{
			InitializeComponent();
			m_frameCoords = new Shape[30];
			m_spriteData = new SpriteData();
			m_texpath = "";
			
			this.panel1.SetRenderMethod(this.RenderPreview);
		}

		public SpriteData Sprite
		{
			get { return m_spriteData;  }
		}

		private void RenderPreview(PaintEventArgs e)
		{
			e.Graphics.Clear(this.BackColor);
			if (m_previewFrame >= 0 && this.pictureBox1.Image != null)
			{
				float spriteWidth = m_spriteWidth + m_frameCoords[m_previewFrame].Width;
				float spriteHeight = m_spriteHeight + m_frameCoords[m_previewFrame].Height;

				float x = (float)Math.Floor(this.panel1.Width / 2 - m_frameCoords[m_previewFrame].cX);
				float y = (float)Math.Floor(this.panel1.Height / 2 - m_frameCoords[m_previewFrame].cY);

				RectangleF srcRect = new RectangleF(m_frameCoords[m_previewFrame].X, m_frameCoords[m_previewFrame].Y, spriteWidth, spriteHeight);
				
				RectangleF dstRect = new RectangleF(x, y, spriteWidth, spriteHeight);
				e.Graphics.DrawImage(this.pictureBox1.Image, dstRect, srcRect, GraphicsUnit.Pixel);

				x = (float)Math.Floor(this.panel1.Width / 2 - m_spriteWidth / 2);
				y = (float)Math.Floor(this.panel1.Height / 2 - m_spriteHeight /2);
				Pen halfBlack = new Pen(Color.FromArgb(96, 128, 128, 128));
				e.Graphics.DrawRectangle(halfBlack, x, y, m_spriteWidth, m_spriteHeight);

				x = (float)Math.Floor(this.panel1.Width / 2d);
				y = (float)Math.Floor(this.panel1.Height / 2d);
				e.Graphics.DrawLine(halfBlack, x, 0, x, this.panel1.Height);
				e.Graphics.DrawLine(halfBlack, 0, y, this.panel1.Width, y);
			}
		}

		private void browseButton_Click(object sender, EventArgs e)
		{
			OpenFileDialog filediag = new OpenFileDialog();
			if(filediag.ShowDialog() == DialogResult.OK)
			{
				FileInfo fi = new FileInfo(filediag.FileName);
				this.textBox1.Text = fi.Name;
				this.pictureBox1.Image = new Bitmap(filediag.FileName);
				m_texpath = filediag.FileName;
			}
		}

		private void numFramesUpDown_ValueChanged(object sender, EventArgs e)
		{
			m_numFrames = (int)this.numFramesUpDown.Value;
			this.numericUpDown4.Maximum = this.numFramesUpDown.Value-1;

			if(m_numFrames > 0)
				this.numericUpDown4.Minimum = 0;
			else
				this.numericUpDown4.Minimum = -1;
		}

		private void numericUpDown1_ValueChanged(object sender, EventArgs e)
		{
			m_spriteWidth = (float)this.numericUpDown1.Value;
			this.pictureBox1.Invalidate();
			this.panel1.Invalidate();
		}

		private void numericUpDown2_ValueChanged(object sender, EventArgs e)
		{
			m_spriteHeight = (float)this.numericUpDown2.Value;
			this.pictureBox1.Invalidate();
			this.panel1.Invalidate();
		}

		private void pictureBox1_Paint(object sender, PaintEventArgs e)
		{
			Pen redPen = Pens.Red;

			if (m_curFrame >= 0)
			{
				float spriteWidth = m_spriteWidth + m_frameCoords[m_curFrame].Width;
				float spriteHeight = m_spriteHeight + m_frameCoords[m_curFrame].Height;
				e.Graphics.DrawRectangle(redPen, m_frameCoords[m_curFrame].X, m_frameCoords[m_curFrame].Y, spriteWidth, spriteHeight);
			}
		}

		private void numericUpDown5_ValueChanged(object sender, EventArgs e)
		{
			if (m_curFrame >= 0)
			{
				m_frameCoords[m_curFrame].X = (float)this.numericUpDown5.Value;
				this.pictureBox1.Invalidate();
				this.panel1.Invalidate();
			}
		}

		private void numericUpDown6_ValueChanged(object sender, EventArgs e)
		{
			if (m_curFrame >= 0)
			{
				m_frameCoords[m_curFrame].Y = (float)this.numericUpDown6.Value;
				this.pictureBox1.Invalidate();
				this.panel1.Invalidate();
			}
		}

		private void numericUpDown4_ValueChanged(object sender, EventArgs e)
		{
			m_curFrame = (int)this.numericUpDown4.Value;
			if(m_curFrame >= 0)
			{
				this.numericUpDown5.Value = (decimal)m_frameCoords[m_curFrame].X;
				this.numericUpDown6.Value = (decimal)m_frameCoords[m_curFrame].Y;

				this.numericUpDown7.Value = (decimal)m_frameCoords[m_curFrame].Width;
				this.numericUpDown8.Value = (decimal)m_frameCoords[m_curFrame].Height;

				this.numericUpDown9.Value = (decimal)m_frameCoords[m_curFrame].cX;
				this.numericUpDown10.Value = (decimal)m_frameCoords[m_curFrame].cY;
			}
		}

		private void button2_Click(object sender, EventArgs e)
		{
			m_previewFrame++;
			if(m_previewFrame >= m_numFrames)
			{
				if (m_numFrames > 0)
					m_previewFrame = 0;
				else
					m_previewFrame = -1;
			}
			this.previewFrameNum.Text = m_previewFrame.ToString();
			this.panel1.Invalidate();
		}

		private void button1_Click(object sender, EventArgs e)
		{
			m_previewFrame--;
			if(m_previewFrame < 0)
			{
				m_previewFrame = m_numFrames - 1;
			}
			this.previewFrameNum.Text = m_previewFrame.ToString();
			this.panel1.Invalidate();
		}

		private void button3_Click(object sender, EventArgs e)
		{
			m_spriteData.TexName = this.textBox1.Text;
			m_spriteData.Name = this.textBox2.Text;
			m_spriteData.Width = m_spriteWidth;
			m_spriteData.Height = m_spriteHeight;
			m_spriteData.FrameInterval = m_frameInterval;
			m_spriteData.SetFrameCoords(m_numFrames, m_frameCoords);
			m_spriteData.TexPath = m_texpath;
		}

		private void writeInt(int value, Stream outputData)
		{
			byte[] buffer = BitConverter.GetBytes(value);
			outputData.Write(buffer, 0, buffer.Length);
		}

		private void writeFloat(float value, Stream outputData)
		{
			byte[] buffer = BitConverter.GetBytes(value);
			outputData.Write(buffer, 0, buffer.Length);
		}

		private void writeBool(bool value, Stream outputData)
		{
			byte[] buffer = BitConverter.GetBytes(value);
			outputData.Write(buffer, 0, buffer.Length);
		}

		private void writeString(string value, Stream outputData)
		{
			byte[] buffer = System.Text.ASCIIEncoding.ASCII.GetBytes(value);
			writeInt(buffer.Length, outputData);
			outputData.Write(buffer, 0, buffer.Length);
		}

		private void numericUpDown3_ValueChanged(object sender, EventArgs e)
		{
			m_frameInterval = (float)this.numericUpDown3.Value;
		}

		private void pictureBox1_MouseDown(object sender, MouseEventArgs e)
		{
			this.pictureBox1.Focus();
			if (m_curFrame >= 0)
			{
				if(e.Button == MouseButtons.Left)
				{
					this.numericUpDown5.Value = e.X;
					this.numericUpDown6.Value = e.Y;
				}
				else if (e.Button == MouseButtons.Right)
				{
					this.numericUpDown9.Value = e.X - (decimal)m_frameCoords[m_curFrame].X;
					this.numericUpDown10.Value = e.Y - (decimal)m_frameCoords[m_curFrame].Y;
				}
				
			}
		}

		private void pictureBox1_MouseMove(object sender, MouseEventArgs e)
		{
			if (e.Button == MouseButtons.Left && m_curFrame >= 0)
			{
				if (e.X >= this.numericUpDown5.Minimum)
					this.numericUpDown5.Value = e.X;
				else
					this.numericUpDown5.Value = this.numericUpDown5.Minimum;

				if (e.Y >= this.numericUpDown6.Minimum)
					this.numericUpDown6.Value = e.Y;
				else
					this.numericUpDown6.Value = this.numericUpDown6.Minimum;
			}
		}

		private void numericUpDown7_ValueChanged(object sender, EventArgs e)
		{
			if (m_curFrame >= 0)
			{
				m_frameCoords[m_curFrame].Width = (float)this.numericUpDown7.Value;
				this.pictureBox1.Invalidate();
				this.panel1.Invalidate();
			}
		}

		private void numericUpDown8_ValueChanged(object sender, EventArgs e)
		{
			if (m_curFrame >= 0)
			{
				m_frameCoords[m_curFrame].Height = (float)this.numericUpDown8.Value;
				this.pictureBox1.Invalidate();
				this.panel1.Invalidate();
			}
		}

		private void button4_Click(object sender, EventArgs e)
		{
			this.timer1.Enabled = true;
			this.timer1.Interval = (int)(m_frameInterval * 1000);
			this.timer1.Start();
		}

		private void timer1_Tick(object sender, EventArgs e)
		{
			m_previewFrame++;
			if (m_previewFrame >= m_numFrames)
			{
				if (m_numFrames > 0)
					m_previewFrame = 0;
				else
				{
					m_previewFrame = -1;
					this.timer1.Stop();
				}
			}
			this.previewFrameNum.Text = m_previewFrame.ToString();
			this.panel1.Invalidate();
		}

		private void button5_Click(object sender, EventArgs e)
		{
			this.timer1.Stop();
		}

		private void pictureBox1_PreviewKeyDown(object sender, PreviewKeyDownEventArgs e)
		{
			if (m_curFrame >= 0 && e.KeyCode == Keys.Left)
			{
				this.numericUpDown5.Value--;
				m_frameCoords[m_curFrame].X = (float)this.numericUpDown5.Value;
				e.IsInputKey = true;
			}

			else if (m_curFrame >= 0 && e.KeyCode == Keys.Right)
			{
				this.numericUpDown5.Value++;
				m_frameCoords[m_curFrame].X = (float)this.numericUpDown5.Value;
				e.IsInputKey = true;
			}
			
			else if (m_curFrame >= 0 && e.KeyCode == Keys.Up)
			{
				this.numericUpDown6.Value--;
				m_frameCoords[m_curFrame].Y = (float)this.numericUpDown6.Value;
				e.IsInputKey = true;
			}

			else if (m_curFrame >= 0 && e.KeyCode == Keys.Down)
			{
				this.numericUpDown6.Value++;
				m_frameCoords[m_curFrame].Y = (float)this.numericUpDown6.Value;
				e.IsInputKey = true;
			}

			this.pictureBox1.Invalidate();
			this.panel1.Invalidate();
		}

		private void numericUpDown9_ValueChanged(object sender, EventArgs e)
		{
			if (m_curFrame >= 0)
			{
				m_frameCoords[m_curFrame].cX = (float)this.numericUpDown9.Value;
				this.pictureBox1.Invalidate();
				this.panel1.Invalidate();
			}
		}

		private void numericUpDown10_ValueChanged(object sender, EventArgs e)
		{
			if (m_curFrame >= 0)
			{
				m_frameCoords[m_curFrame].cY = (float)this.numericUpDown10.Value;
				this.pictureBox1.Invalidate();
				this.panel1.Invalidate();
			}
		}
	}
}
