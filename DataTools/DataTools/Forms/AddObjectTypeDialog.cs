﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using GameDataLib;

namespace DataTools
{
	public partial class AddObjectTypeDialog : Form
	{
		private CustomObject m_obj;
		private System.Windows.Forms.TreeView objectTypeTree;
		private long m_curId;

		public AddObjectTypeDialog(CustomObject obj, System.Windows.Forms.TreeView TypeTree)
		{
			m_curId = 0;
			m_obj = obj.Clone(true);
			InitializeComponent();

			this.comboBox1.DataSource = Enum.GetValues(typeof(GameDataLib.DataType));

			//Fill fields with object data
			this.textBox1.Text = m_obj.Name;
			foreach (CustomObjectData data in m_obj.GetData())
			{
				if (data.ID >= m_curId)
					m_curId = data.ID;

				ListViewItem item = new ListViewItem();
				item.Text = data.ToString();
				item.Tag = data;
				this.listView1.Items.Add(item);
			}

			objectTypeTree = TypeTree;

			m_curId++;
		}

		public AddObjectTypeDialog(System.Windows.Forms.TreeView TypeTree)
		{
			m_curId = 0;
			m_obj = new CustomObject("DefaultName", Globals.GetNextID(), "DefaultName");
			InitializeComponent();

			this.comboBox1.DataSource = Enum.GetValues(typeof(GameDataLib.DataType));

			objectTypeTree = TypeTree;
		}

		public CustomObject NewType
		{
			get { return m_obj; }
		}

		private void OKButton_Click(object sender, EventArgs e)
		{
			if(this.textBox1.Text.Length == 0)
			{
				MessageBox.Show("You must enter a name for this type", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
			}
			else
			{
				m_obj.Clear();
				m_obj.Name = this.textBox1.Text;
				m_obj.ParentType = this.textBox1.Text;

				foreach(ListViewItem item in this.listView1.Items)
				{
					CustomObjectData data = item.Tag as CustomObjectData;
					m_obj.AddData(data);
				}
				this.DialogResult = DialogResult.OK;
				this.Close();
			}
		}

		private void AddObjectTypeDialog_KeyDown(object sender, KeyEventArgs e)
		{
			if(e.KeyCode == Keys.Escape)
			{
				e.Handled = true;
				this.DialogResult = DialogResult.Cancel;
				this.Close();
			}
		}

		private void listView1_KeyDown(object sender, KeyEventArgs e)
		{
			if(e.KeyCode == Keys.Delete)
			{
				if (this.listView1.SelectedIndices.Count >= 0)
				{
					foreach (ListViewItem item in this.listView1.SelectedItems)
					{
						this.listView1.Items.Remove(item);
					}
					
				}
			}
		}

		private void addDataTypeButton_Click(object sender, EventArgs e)
		{
			CustomObject customSubType = null;
			if((DataType)this.comboBox1.SelectedValue == DataType.CUSTOM_OBJECT)
			{
				SelectRootTypeDialog type_diag = new SelectRootTypeDialog(this.objectTypeTree.Nodes);
				if (type_diag.ShowDialog() == DialogResult.OK)
				{
					if (type_diag.CustomType != null)
						customSubType = type_diag.CustomType.Clone(true);
				}

				if (customSubType == null)
					return;
			}

			GetStringDialog diag = new GetStringDialog();
			diag.StartPosition = FormStartPosition.CenterParent;
			if (customSubType != null)
				diag.DialogTitle = "Enter " + customSubType.ToString() + " Name";
			else
				diag.DialogTitle = "Enter " + this.comboBox1.SelectedValue.ToString() + " Name";

			if (diag.ShowDialog() == DialogResult.OK)
			{
				if(diag.StringData.Length > 0)
				{
					CustomObjectData newObj = new CustomObjectData((DataType)this.comboBox1.SelectedValue, diag.StringData, m_curId++);
					newObj.SetValue(customSubType);

					ListViewItem item = new ListViewItem();
					item.Text = newObj.ToString();
					item.Tag = newObj;
					this.listView1.Items.Add(item);
				}
				else
				{
					MessageBox.Show("Data name cannot be NULL", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
				}
			}
		}

		private void listView1_AfterLabelEdit(object sender, LabelEditEventArgs e)
		{
			if (e.Label.Length > 0)
			{
				ListViewItem item = this.listView1.Items[e.Item];
				CustomObjectData data = item.Tag as CustomObjectData;
				data.Name = e.Label;
				item.Text = data.ToString();
			}
			e.CancelEdit = true;
			this.listView1.LabelEdit = false;
		}

		private void listView1_DoubleClick(object sender, EventArgs e)
		{
			ListViewItem item = this.listView1.SelectedItems[0];
			CustomObjectData data = item.Tag as CustomObjectData;
			item.Text = data.Name;
			this.listView1.LabelEdit = true;
			item.BeginEdit();
		}

		private void button1_Click(object sender, EventArgs e)
		{
			if(this.listView1.SelectedIndices.Count > 0)
			{
				int index = this.listView1.SelectedIndices[0];

				if(index > 0)
				{
					ListViewItem self = this.listView1.Items[index];
					this.listView1.Items.RemoveAt(index);
					this.listView1.Items.Insert(index-1, self);
				}
			}
		}

		private void button2_Click(object sender, EventArgs e)
		{
			if (this.listView1.SelectedIndices.Count > 0)
			{
				int index = this.listView1.SelectedIndices[0];

				if (index < this.listView1.Items.Count - 1)
				{
					ListViewItem self = this.listView1.Items[index];
					this.listView1.Items.RemoveAt(index);
					this.listView1.Items.Insert(index + 1, self);
				}
			}
		}
	}
}
