﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace DataTools
{
	public partial class GetStringDialog : Form
	{
		public GetStringDialog()
		{
			InitializeComponent();
		}

		public string StringData
		{
			get { return this.textBox1.Text; }
		}

		public string DialogTitle
		{
			set { this.Text = value;  }
			get { return this.Text;  }
		}

		private void textBox1_KeyDown(object sender, KeyEventArgs e)
		{
			if(e.KeyCode == Keys.Return)
			{
				this.DialogResult = DialogResult.OK;
				this.Close();
			}
		}

		private void GetStringDialog_KeyDown(object sender, KeyEventArgs e)
		{
			if (e.KeyCode == Keys.Escape)
			{
				e.Handled = true;
				this.DialogResult = DialogResult.Cancel;
				this.Close();
			}
		}
	}
}
