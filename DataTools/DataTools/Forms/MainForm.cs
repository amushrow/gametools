﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.IO;
using GameDataLib;

namespace DataTools
{
	public partial class MainForm : Form
	{
		private string m_filePath = "";
		private bool m_edited = false;

		public MainForm()
		{
			InitializeComponent();

			MainFormHelper.dodgyVariable = this.objectTypeTree.Nodes;
		}

		private void objectTypeTree_AfterSelect(object sender, TreeViewEventArgs e)
		{
			CustomObject type = e.Node.Tag as CustomObject;
			if (type == null)
				return;

			if(e.Node.Parent == null)
			{
				setPanelForRoot(type);
			}
			else
			{
				MainFormHelper.SetupForChild(this.panel1, type, this, false);
			}
		}

		private void setPanelForRoot(CustomObject type)
		{
			this.panel1.SuspendLayout();
			this.panel1.Controls.Clear();

			Button addNew_button = new Button();
			addNew_button.Name = "AddNew_Button";
			addNew_button.Text = "Add New " + type.Name;
			addNew_button.AutoSize = true;
			addNew_button.Click += new EventHandler(this.addNewChild_click);
			addNew_button.Location = new System.Drawing.Point(49, 46);
			addNew_button.UseVisualStyleBackColor = true;

			TextBox nameEdit = new TextBox();
			nameEdit.Name = "Name_EditBox";
			nameEdit.Location = new System.Drawing.Point(49, 20);
			nameEdit.Size = new System.Drawing.Size(156, 20);
			nameEdit.KeyDown += new KeyEventHandler(this.nameEdit_KeyDown);

			Label nameLabel = new Label();
			nameLabel.Name = "Name_Label";
			nameLabel.Text = "Name:";
			nameLabel.AutoSize = true;
			nameLabel.Location = new System.Drawing.Point(6, 20);
			nameLabel.Size = new System.Drawing.Size(35, 13);

			this.panel1.Controls.Add(addNew_button);
			this.panel1.Controls.Add(nameEdit);
			this.panel1.Controls.Add(nameLabel);

			this.panel1.ResumeLayout();
		}

		private void nameEdit_KeyDown(object sender, KeyEventArgs e)
		{
			if (e.KeyCode == Keys.Return)
			{
				this.addNewChild_click(sender, e);
			}
		}

		private void addNewChild_click(object sender, EventArgs e)
		{
			TextBox box = this.panel1.Controls["Name_EditBox"] as TextBox;
			if(box != null)
			{
				if(box.Text.Length > 0)
				{
					TreeNode newChild = new TreeNode(box.Text, 1, 1);
					CustomObject typeData = this.objectTypeTree.SelectedNode.Tag as CustomObject;
					CustomObject data = typeData.Clone(false);
					//data.InitializeData();
					data.Name = box.Text;
					newChild.Tag = data;

					this.objectTypeTree.SelectedNode.Nodes.Add(newChild);
					this.objectTypeTree.SelectedNode = newChild;
				}
			}
		}

		private void exitToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void saveToolStripMenuItem_Click(object sender, EventArgs e)
        {
			save(false);            
        }

		private void loadToolStripMenuItem_Click(object sender, EventArgs e)
		{
			this.objectTypeTree.SuspendLayout();

			OpenFileDialog diag = new OpenFileDialog();
			diag.Filter = "Game Data File|*.skd";

			if(diag.ShowDialog() == DialogResult.OK)
			{
				FileStream fs = new FileStream(diag.FileName, FileMode.Open);
				Globals.ResetID();
				List<TreeNode> nodes = MainFormHelper.LoadData(fs);

				this.objectTypeTree.Nodes.Clear();
				this.panel1.Controls.Clear();

				foreach (TreeNode node in nodes)
				{
					this.objectTypeTree.Nodes.Add(node);
				}
				
				fs.Close();

				m_filePath = diag.FileName;

				FileInfo fi = new FileInfo(m_filePath);
				this.Text = fi.Name + " - Data Tool";
			}

			this.objectTypeTree.ResumeLayout();
		}

		private void addNewItem_Event(object sender, EventArgs e)
		{
			AddObjectTypeDialog diag = new AddObjectTypeDialog(this.objectTypeTree);
			if (diag.ShowDialog() == DialogResult.OK)
			{
				m_edited = true;
				CustomObject type = diag.NewType;

				TreeNode newRoot = new TreeNode(type.Name.ToUpper(), 0, 0);
				newRoot.Tag = type;
				this.objectTypeTree.Nodes.Add(newRoot);
			}
		}

		private void objectTypeTree_NodeMouseClick(object sender, TreeNodeMouseClickEventArgs e)
		{
			if(e.Button == MouseButtons.Right)
			{
				this.objectTypeTree.SelectedNode = e.Node;
				if (e.Node.Level == 0)
					this.treeRootMenu.Show(Cursor.Position);
				else
					this.treeNodeMenu.Show(Cursor.Position);
			}
		}

		private void objectTypeTree_MouseDown(object sender, MouseEventArgs e)
		{
			if (e.Button == MouseButtons.Right)
			{
				if (this.objectTypeTree.GetNodeAt(e.Location) == null)
				{
					this.treeViewMenu.Show(Cursor.Position);
				}
			}
		}

		private void editTypeToolStripMenuItem_Click(object sender, EventArgs e)
		{
			CustomObject newType = MainFormHelper.EditType(this.objectTypeTree.SelectedNode, this.objectTypeTree);
			typeChanged(newType);
		}

		private void nodeKaboom(TreeNode node)
		{
			if(node.Level == 0 && node.Tag is CustomObject)
			{
				CustomObject type = node.Tag as CustomObject;
				foreach (TreeNode root in this.objectTypeTree.Nodes)
				{
					foreach (TreeNode child in root.Nodes)
					{
						if (child.Tag is CustomObject)
						{
							CustomObject obj = child.Tag as CustomObject;
							foreach (CustomObjectData data in obj.GetData())
							{
								if (data.Type == DataType.CUSTOM_OBJECT)
								{
									CustomObject child_obj = data.GetCustomObject();
									if (child_obj != null && child_obj.ID == type.ID)
									{
										data.SetValue((CustomObject)null);
									}
								}
							}
						}
					}
				}
			}
		}
		
		private void typeChanged(CustomObject newType)
		{
			if (newType == null)
				return;

			//Fix up everything
			foreach (TreeNode root in this.objectTypeTree.Nodes)
			{
				foreach (TreeNode child in root.Nodes)
				{
					if (child.Tag is CustomObject)
					{
						CustomObject obj = child.Tag as CustomObject;
						foreach (CustomObjectData data in obj.GetData())
						{
							if (data.Type == DataType.CUSTOM_OBJECT)
							{
								CustomObject child_obj = data.GetCustomObject();
								if (child_obj != null && child_obj.ID == newType.ID)
								{
									data.SetValue(MainFormHelper.FixType(child_obj, newType));
								}
							}
						}
					}
				}
			}
		}

		private void saveAsToolStripMenuItem_Click(object sender, EventArgs e)
		{
			save(true);
		}

		private void deleteToolStripMenuItem_Click(object sender, EventArgs e)
		{
			TreeNode selected = this.objectTypeTree.SelectedNode;
			DialogResult res;
			if(selected.Level > 0)
				res = MessageBox.Show("Are you sure you want to delete the node \"" + selected.Text + "\"?", "Warning", MessageBoxButtons.YesNo, MessageBoxIcon.Warning);
			else
				res = MessageBox.Show("Are you sure you want to delete the root node \"" + selected.Text + "\"?\nAll child nodes will be removed, and all types referring to this node will affected.", "Warning", MessageBoxButtons.YesNo, MessageBoxIcon.Warning);

			if (res == DialogResult.Yes)
			{
				this.panel1.Controls.Clear();
				nodeKaboom(selected);
				this.objectTypeTree.Nodes.Remove(selected);
			}
		}

		private void objectTypeTree_BeforeSelect(object sender, TreeViewCancelEventArgs e)
		{
			applyChangesToSelectedNode();
		}

		private void newToolStripMenuItem_Click(object sender, EventArgs e)
		{
			this.objectTypeTree.Nodes.Clear();
			this.Text = "Untitled - Data Tool";
			Globals.ResetID();
			this.panel1.Controls.Clear();
		}

		private void applyChangesToSelectedNode()
		{
			TreeNode Node = this.objectTypeTree.SelectedNode;
			if (Node != null && Node.Level > 0 && Node.Tag is CustomObject)
			{
				MainFormHelper.ApplyChanges(this.panel1, Node.Tag as CustomObject, Node);
			}
		}

		public void editSubType(object sender, EventArgs e)
		{
			if (sender is Button)
			{
				Button button_sender = sender as Button;
				SelectRootTypeDialog diag = new SelectRootTypeDialog(this.objectTypeTree.Nodes);
				if(diag.ShowDialog() == DialogResult.OK)
				{
					if (diag.CustomType != null)
						button_sender.Tag = diag.CustomType.Clone(true);
					else
						button_sender.Tag = null;

					applyChangesToSelectedNode();
					CustomObject type = this.objectTypeTree.SelectedNode.Tag as CustomObject;
					MainFormHelper.SetupForChild(this.panel1, type, this, true);
				}
			}
		}

		private void save(bool showDiag)
		{
			bool havePath = true;
			if(showDiag || m_filePath.Length == 0)
			{
				havePath = false;
				SaveFileDialog diag = new SaveFileDialog();
				diag.AddExtension = true;
				diag.DefaultExt = ".skd";
				diag.Filter = "Game Data File|*.skd";
				if (diag.ShowDialog() == DialogResult.OK)
				{
					m_edited = false;
					m_filePath = diag.FileName;
					havePath = true;
				}
			}

			if(havePath)
			{
				applyChangesToSelectedNode();

				FileStream fs = new FileStream(m_filePath, FileMode.Create);
				StreamWrapper sw = new StreamWrapper(fs);

				//Write number of nodes
				sw.WriteInt(MainFormHelper.CountNodes(this.objectTypeTree.Nodes));

				//Write all the nodes
				foreach (TreeNode root in this.objectTypeTree.Nodes)
				{
					MainFormHelper.WriteData(root, fs);
				}

				fs.Close();
			}
		}

		private void editTypeToolStripMenuItem1_Click(object sender, EventArgs e)
		{
			TreeNode selected = this.objectTypeTree.SelectedNode;
			this.objectTypeTree.SelectedNode = null;
			SelectRootTypeDialog diag = new SelectRootTypeDialog(this.objectTypeTree.Nodes);
			if(diag.ShowDialog() == DialogResult.OK)
			{
				foreach (TreeNode root in this.objectTypeTree.Nodes)
				{
					if(root.Tag == diag.CustomType)
					{
						CustomObject newType = MainFormHelper.EditType(root, this.objectTypeTree);
						typeChanged(newType);
						this.objectTypeTree.SelectedNode = selected;
					}
				}
			}
		}
	}
}
