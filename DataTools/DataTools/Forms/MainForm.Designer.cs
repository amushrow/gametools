﻿namespace DataTools
{
	partial class MainForm
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.components = new System.ComponentModel.Container();
			this.objectTypeTree = new System.Windows.Forms.TreeView();
			this.menuStrip1 = new System.Windows.Forms.MenuStrip();
			this.fileToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			this.newToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			this.toolStripMenuItem2 = new System.Windows.Forms.ToolStripSeparator();
			this.loadToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			this.saveToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			this.saveAsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			this.toolStripMenuItem1 = new System.Windows.Forms.ToolStripSeparator();
			this.exitToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			this.editToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			this.addNewTypeToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
			this.editTypeToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
			this.panel1 = new System.Windows.Forms.Panel();
			this.treeViewMenu = new System.Windows.Forms.ContextMenuStrip(this.components);
			this.addNewTypeToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			this.treeRootMenu = new System.Windows.Forms.ContextMenuStrip(this.components);
			this.editTypeToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			this.toolStripMenuItem3 = new System.Windows.Forms.ToolStripMenuItem();
			this.treeNodeMenu = new System.Windows.Forms.ContextMenuStrip(this.components);
			this.deleteToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			this.splitContainer1 = new System.Windows.Forms.SplitContainer();
			this.menuStrip1.SuspendLayout();
			this.treeViewMenu.SuspendLayout();
			this.treeRootMenu.SuspendLayout();
			this.treeNodeMenu.SuspendLayout();
			this.splitContainer1.Panel1.SuspendLayout();
			this.splitContainer1.Panel2.SuspendLayout();
			this.splitContainer1.SuspendLayout();
			this.SuspendLayout();
			// 
			// objectTypeTree
			// 
			this.objectTypeTree.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
			this.objectTypeTree.Dock = System.Windows.Forms.DockStyle.Fill;
			this.objectTypeTree.FullRowSelect = true;
			this.objectTypeTree.HideSelection = false;
			this.objectTypeTree.Location = new System.Drawing.Point(0, 0);
			this.objectTypeTree.Name = "objectTypeTree";
			this.objectTypeTree.Size = new System.Drawing.Size(186, 486);
			this.objectTypeTree.TabIndex = 0;
			this.objectTypeTree.AfterSelect += new System.Windows.Forms.TreeViewEventHandler(this.objectTypeTree_AfterSelect);
			this.objectTypeTree.MouseDown += new System.Windows.Forms.MouseEventHandler(this.objectTypeTree_MouseDown);
			this.objectTypeTree.NodeMouseClick += new System.Windows.Forms.TreeNodeMouseClickEventHandler(this.objectTypeTree_NodeMouseClick);
			this.objectTypeTree.BeforeSelect += new System.Windows.Forms.TreeViewCancelEventHandler(this.objectTypeTree_BeforeSelect);
			// 
			// menuStrip1
			// 
			this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.fileToolStripMenuItem,
            this.editToolStripMenuItem});
			this.menuStrip1.Location = new System.Drawing.Point(0, 0);
			this.menuStrip1.Name = "menuStrip1";
			this.menuStrip1.Size = new System.Drawing.Size(560, 24);
			this.menuStrip1.TabIndex = 1;
			this.menuStrip1.Text = "menuStrip1";
			// 
			// fileToolStripMenuItem
			// 
			this.fileToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.newToolStripMenuItem,
            this.toolStripMenuItem2,
            this.loadToolStripMenuItem,
            this.saveToolStripMenuItem,
            this.saveAsToolStripMenuItem,
            this.toolStripMenuItem1,
            this.exitToolStripMenuItem});
			this.fileToolStripMenuItem.Name = "fileToolStripMenuItem";
			this.fileToolStripMenuItem.Size = new System.Drawing.Size(37, 20);
			this.fileToolStripMenuItem.Text = "File";
			// 
			// newToolStripMenuItem
			// 
			this.newToolStripMenuItem.Name = "newToolStripMenuItem";
			this.newToolStripMenuItem.Size = new System.Drawing.Size(152, 22);
			this.newToolStripMenuItem.Text = "New";
			this.newToolStripMenuItem.Click += new System.EventHandler(this.newToolStripMenuItem_Click);
			// 
			// toolStripMenuItem2
			// 
			this.toolStripMenuItem2.Name = "toolStripMenuItem2";
			this.toolStripMenuItem2.Size = new System.Drawing.Size(149, 6);
			// 
			// loadToolStripMenuItem
			// 
			this.loadToolStripMenuItem.Name = "loadToolStripMenuItem";
			this.loadToolStripMenuItem.Size = new System.Drawing.Size(152, 22);
			this.loadToolStripMenuItem.Text = "Open";
			this.loadToolStripMenuItem.Click += new System.EventHandler(this.loadToolStripMenuItem_Click);
			// 
			// saveToolStripMenuItem
			// 
			this.saveToolStripMenuItem.Name = "saveToolStripMenuItem";
			this.saveToolStripMenuItem.Size = new System.Drawing.Size(152, 22);
			this.saveToolStripMenuItem.Text = "Save";
			this.saveToolStripMenuItem.Click += new System.EventHandler(this.saveToolStripMenuItem_Click);
			// 
			// saveAsToolStripMenuItem
			// 
			this.saveAsToolStripMenuItem.Name = "saveAsToolStripMenuItem";
			this.saveAsToolStripMenuItem.Size = new System.Drawing.Size(152, 22);
			this.saveAsToolStripMenuItem.Text = "Save As...";
			this.saveAsToolStripMenuItem.Click += new System.EventHandler(this.saveAsToolStripMenuItem_Click);
			// 
			// toolStripMenuItem1
			// 
			this.toolStripMenuItem1.Name = "toolStripMenuItem1";
			this.toolStripMenuItem1.Size = new System.Drawing.Size(149, 6);
			// 
			// exitToolStripMenuItem
			// 
			this.exitToolStripMenuItem.Name = "exitToolStripMenuItem";
			this.exitToolStripMenuItem.Size = new System.Drawing.Size(152, 22);
			this.exitToolStripMenuItem.Text = "Exit";
			this.exitToolStripMenuItem.Click += new System.EventHandler(this.exitToolStripMenuItem_Click);
			// 
			// editToolStripMenuItem
			// 
			this.editToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.addNewTypeToolStripMenuItem1,
            this.editTypeToolStripMenuItem1});
			this.editToolStripMenuItem.Name = "editToolStripMenuItem";
			this.editToolStripMenuItem.Size = new System.Drawing.Size(39, 20);
			this.editToolStripMenuItem.Text = "Edit";
			// 
			// addNewTypeToolStripMenuItem1
			// 
			this.addNewTypeToolStripMenuItem1.Name = "addNewTypeToolStripMenuItem1";
			this.addNewTypeToolStripMenuItem1.Size = new System.Drawing.Size(152, 22);
			this.addNewTypeToolStripMenuItem1.Text = "Add New Type";
			this.addNewTypeToolStripMenuItem1.Click += new System.EventHandler(this.addNewItem_Event);
			// 
			// editTypeToolStripMenuItem1
			// 
			this.editTypeToolStripMenuItem1.Name = "editTypeToolStripMenuItem1";
			this.editTypeToolStripMenuItem1.Size = new System.Drawing.Size(152, 22);
			this.editTypeToolStripMenuItem1.Text = "Edit Type";
			this.editTypeToolStripMenuItem1.Click += new System.EventHandler(this.editTypeToolStripMenuItem1_Click);
			// 
			// panel1
			// 
			this.panel1.AutoScroll = true;
			this.panel1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
			this.panel1.Dock = System.Windows.Forms.DockStyle.Fill;
			this.panel1.Location = new System.Drawing.Point(0, 0);
			this.panel1.Name = "panel1";
			this.panel1.Size = new System.Drawing.Size(370, 486);
			this.panel1.TabIndex = 2;
			// 
			// treeViewMenu
			// 
			this.treeViewMenu.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.addNewTypeToolStripMenuItem});
			this.treeViewMenu.Name = "treeViewMenu";
			this.treeViewMenu.Size = new System.Drawing.Size(153, 26);
			// 
			// addNewTypeToolStripMenuItem
			// 
			this.addNewTypeToolStripMenuItem.Name = "addNewTypeToolStripMenuItem";
			this.addNewTypeToolStripMenuItem.Size = new System.Drawing.Size(152, 22);
			this.addNewTypeToolStripMenuItem.Text = "Add New Type";
			this.addNewTypeToolStripMenuItem.Click += new System.EventHandler(this.addNewItem_Event);
			// 
			// treeRootMenu
			// 
			this.treeRootMenu.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.editTypeToolStripMenuItem,
            this.toolStripMenuItem3});
			this.treeRootMenu.Name = "treeNodeMenu";
			this.treeRootMenu.Size = new System.Drawing.Size(124, 48);
			// 
			// editTypeToolStripMenuItem
			// 
			this.editTypeToolStripMenuItem.Name = "editTypeToolStripMenuItem";
			this.editTypeToolStripMenuItem.Size = new System.Drawing.Size(123, 22);
			this.editTypeToolStripMenuItem.Text = "Edit Type";
			this.editTypeToolStripMenuItem.Click += new System.EventHandler(this.editTypeToolStripMenuItem_Click);
			// 
			// toolStripMenuItem3
			// 
			this.toolStripMenuItem3.Name = "toolStripMenuItem3";
			this.toolStripMenuItem3.Size = new System.Drawing.Size(123, 22);
			this.toolStripMenuItem3.Text = "Delete";
			this.toolStripMenuItem3.Click += new System.EventHandler(this.deleteToolStripMenuItem_Click);
			// 
			// treeNodeMenu
			// 
			this.treeNodeMenu.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.deleteToolStripMenuItem});
			this.treeNodeMenu.Name = "treeNodeMenu";
			this.treeNodeMenu.Size = new System.Drawing.Size(108, 26);
			// 
			// deleteToolStripMenuItem
			// 
			this.deleteToolStripMenuItem.Name = "deleteToolStripMenuItem";
			this.deleteToolStripMenuItem.Size = new System.Drawing.Size(107, 22);
			this.deleteToolStripMenuItem.Text = "Delete";
			this.deleteToolStripMenuItem.Click += new System.EventHandler(this.deleteToolStripMenuItem_Click);
			// 
			// splitContainer1
			// 
			this.splitContainer1.Dock = System.Windows.Forms.DockStyle.Fill;
			this.splitContainer1.Location = new System.Drawing.Point(0, 24);
			this.splitContainer1.Name = "splitContainer1";
			// 
			// splitContainer1.Panel1
			// 
			this.splitContainer1.Panel1.Controls.Add(this.objectTypeTree);
			// 
			// splitContainer1.Panel2
			// 
			this.splitContainer1.Panel2.Controls.Add(this.panel1);
			this.splitContainer1.Size = new System.Drawing.Size(560, 486);
			this.splitContainer1.SplitterDistance = 186;
			this.splitContainer1.TabIndex = 3;
			// 
			// MainForm
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(560, 510);
			this.Controls.Add(this.splitContainer1);
			this.Controls.Add(this.menuStrip1);
			this.MainMenuStrip = this.menuStrip1;
			this.Name = "MainForm";
			this.Text = "Untitled - Data Tool";
			this.menuStrip1.ResumeLayout(false);
			this.menuStrip1.PerformLayout();
			this.treeViewMenu.ResumeLayout(false);
			this.treeRootMenu.ResumeLayout(false);
			this.treeNodeMenu.ResumeLayout(false);
			this.splitContainer1.Panel1.ResumeLayout(false);
			this.splitContainer1.Panel2.ResumeLayout(false);
			this.splitContainer1.ResumeLayout(false);
			this.ResumeLayout(false);
			this.PerformLayout();

		}

		#endregion

		private System.Windows.Forms.TreeView objectTypeTree;
		private System.Windows.Forms.MenuStrip menuStrip1;
		private System.Windows.Forms.ToolStripMenuItem fileToolStripMenuItem;
		private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.ToolStripMenuItem saveToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem exitToolStripMenuItem;
		private System.Windows.Forms.ToolStripMenuItem loadToolStripMenuItem;
		private System.Windows.Forms.ToolStripMenuItem saveAsToolStripMenuItem;
		private System.Windows.Forms.ToolStripSeparator toolStripMenuItem1;
		private System.Windows.Forms.ContextMenuStrip treeViewMenu;
		private System.Windows.Forms.ToolStripMenuItem addNewTypeToolStripMenuItem;
		private System.Windows.Forms.ContextMenuStrip treeRootMenu;
		private System.Windows.Forms.ToolStripMenuItem editTypeToolStripMenuItem;
		private System.Windows.Forms.ToolStripMenuItem editToolStripMenuItem;
		private System.Windows.Forms.ToolStripMenuItem addNewTypeToolStripMenuItem1;
		private System.Windows.Forms.ToolStripMenuItem editTypeToolStripMenuItem1;
		private System.Windows.Forms.ContextMenuStrip treeNodeMenu;
		private System.Windows.Forms.ToolStripMenuItem deleteToolStripMenuItem;
		private System.Windows.Forms.ToolStripMenuItem newToolStripMenuItem;
		private System.Windows.Forms.ToolStripSeparator toolStripMenuItem2;
		private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem3;
		private System.Windows.Forms.SplitContainer splitContainer1;
	}
}