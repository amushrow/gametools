﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using GameDataLib;

namespace DataTools
{
	public partial class SelectRootTypeDialog : Form
	{
		private CustomObject m_customType;

		public SelectRootTypeDialog(TreeNodeCollection col)
		{
			InitializeComponent();

			foreach (TreeNode node in col)
			{
				this.listBox1.Items.Add(node.Tag as CustomObject);
			}
			this.listBox1.Items.Add("[None]");
		}

		private void listBox1_SelectedIndexChanged(object sender, EventArgs e)
		{
			m_customType = this.listBox1.SelectedItem as CustomObject;
		}

		public CustomObject CustomType
		{
			get { return m_customType; }
		}
	}
}
