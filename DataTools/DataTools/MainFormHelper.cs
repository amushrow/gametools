﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.IO;
using System.Drawing;

using GameDataLib;

namespace DataTools
{
	class FormBuilder
	{
		private int m_x, m_y;
		private int m_padding;
		private Control m_parent;

		public FormBuilder(int padding, Control parent) 
		{ 
			m_x = m_y = m_padding = padding;

			if (m_x < parent.Padding.Left)
				m_x = parent.Padding.Left;
			if (m_y < parent.Padding.Top)
				m_y = parent.Padding.Top;

			m_parent = parent;
		}

		public void AddControl(string labelText, Control control)
		{
			//Add the label
			int height = 0;
			Label someLabel = null;
			if (labelText.Length > 0)
			{
				someLabel = new Label();
				someLabel.Text = labelText;
				someLabel.AutoSize = false;
				someLabel.Size = new Size(100, 13);
				someLabel.TextAlign = ContentAlignment.TopRight;
				someLabel.Location = new System.Drawing.Point(m_x, m_y);
				m_parent.Controls.Add(someLabel);
				height = someLabel.Height;
			}

			if(control != null)
			{
				if (labelText.Length > 0)
					control.Location = new Point(m_x + someLabel.Width + m_padding, m_y);
				else
					control.Location = new Point(m_x, m_y);

				m_parent.Controls.Add(control);
			}

			m_x = m_padding;

			if (control.Height > height)
				height = control.Height;

			m_y += height + m_padding;
		}
	}

	static class MainFormHelper
	{
		public static TreeNodeCollection dodgyVariable;

		public static int CountNodes(TreeNodeCollection nodeCollection)
		{
			int count = 0;
			foreach (TreeNode node in nodeCollection)
			{
				count += countNodes(node);
			}

			return count;
		}

		private static int countNodes(TreeNode node)
		{
			int count = 0;

			//Count all the children
			foreach(TreeNode child in node.Nodes)
			{
				count += countNodes(child);
			}

			//Count ourselves
			count++;

			return count;
		}

		public static void WriteData(TreeNode node, Stream output)
		{
			if (node.Tag is CustomObject)
			{
				StreamWrapper sw = new StreamWrapper(output);
				CustomObject type = node.Tag as CustomObject;

				//Is root
				bool isRoot = (node.Level == 0);
				sw.WriteBool(isRoot);
				sw.WriteString(type.Name);
				sw.WriteLong(type.ID);
				sw.WriteString(type.ParentType);

				sw.WriteInt(type.GetData().Count);

				foreach (CustomObjectData data in type.GetData())
				{
					//If we're a root, just save the type information
					// otherwise save data values as well
					if (isRoot)
					{
						sw.WriteString(data.Name);
						sw.WriteInt((int)data.Type);
						sw.WriteLong(data.ID);
						if (data.Type == DataType.CUSTOM_OBJECT)
						{
							CustomObject obj = data.GetCustomObject();
							if (obj != null)
								sw.WriteLong(obj.ID);
							else
								sw.WriteLong(-1);
						}
					}
					else
					{
						data.WriteToStream(output);
					}
				}

				foreach(TreeNode child in node.Nodes)
				{
					WriteData(child, output);
				}
			}
		}

		public static List<TreeNode> LoadData(Stream input)
		{
			long maxId = 0;
			StreamWrapper sw = new StreamWrapper(input);
			List<TreeNode> allNodes = new List<TreeNode>();
			TreeNode currentRoot = null;

			int numNodes = sw.ReadInt();
			for (int nodeCount = 0; nodeCount < numNodes; nodeCount++)
			{
				bool isRoot = sw.ReadBool();
				string name = sw.ReadString();
				long id = sw.ReadLong();
				string type_name = sw.ReadString();

				if (id > maxId)
					maxId = id;

				CustomObject newType = new CustomObject(name, id, type_name);

				int numData = sw.ReadInt();
				for (int i = 0; i < numData; i++)
				{
					if (isRoot)
					{
						string dataName = sw.ReadString();
						DataType type = (DataType)sw.ReadInt();
						long data_id = sw.ReadLong();
						if (type == DataType.CUSTOM_OBJECT)
						{
							long typeId = sw.ReadLong();
							CustomObjectData tmpObj = newType.AddData(dataName, type, data_id);
							tmpObj.SetValue(typeId);
						}
						else
							newType.AddData(dataName, type, data_id);

					}
					else
					{
						CustomObjectData newData = CustomObjectData.ReadFromStream(input);
						newType.AddData(newData);
					}
				}


				TreeNode newNode;
				if (isRoot)
				{
					newNode = new TreeNode(name.ToUpper(), 0, 0);
					newNode.Tag = newType;

					allNodes.Add(newNode);
					currentRoot = newNode;
				}
				else if (currentRoot != null)
				{
					newNode = new TreeNode(name, 1, 1);
					newNode.Tag = newType;

					currentRoot.Nodes.Add(newNode);
				}
			}

			maxId++;
			Globals.SetID(maxId);

			//Set up custom objects
			foreach(TreeNode node in allNodes)
			{
				CustomObject tmpObj = node.Tag as CustomObject;
				foreach(CustomObjectData data in tmpObj.GetData())
				{
					if(data.Type == DataType.CUSTOM_OBJECT)
					{
						long id = data.GetLong();
						foreach (TreeNode node2 in allNodes)
						{
							CustomObject tmpObj2 = node2.Tag as CustomObject;
							if (tmpObj2.ID == id)
								data.SetValue(tmpObj2);
						}
					}
				}
			}

			return allNodes;
		}

		public static CustomObject EditType(TreeNode node, TreeView typeTree)
		{
			if (node.Level == 0 && node.Tag is CustomObject)
			{
				AddObjectTypeDialog diag = new AddObjectTypeDialog(node.Tag as CustomObject, typeTree);
				if (diag.ShowDialog() == DialogResult.OK)
				{
					node.Tag = diag.NewType;

					//Fix up the children
					foreach (TreeNode child in node.Nodes)
					{
						if (child.Tag is CustomObject)
						{
							child.Tag = FixType(child.Tag as CustomObject, diag.NewType);
						}
					}

					return diag.NewType;
				}
			}

			return null;
		}

		public static CustomObject FixType(CustomObject old_obj, CustomObject newType)
		{
			CustomObject new_obj = new CustomObject(old_obj.Name, old_obj.ID, newType.ParentType);

			foreach (CustomObjectData new_data in newType.GetData())
			{
				bool foundInOld = false;
				foreach (CustomObjectData old_data in old_obj.GetData())
				{
					DataType type = new_data.Type;
					if (new_data.ID == old_data.ID)
					{
						foundInOld = true;

						CustomObjectData data = new CustomObjectData(type, new_data.Name, new_data.ID);
						switch (type)
						{
							case DataType.BOOL:
								data.SetValue(old_data.GetBool());
								break;

							case DataType.FLOAT:
								data.SetValue(old_data.GetFloat());
								break;

							case DataType.INT:
								data.SetValue(old_data.GetInt());
								break;

							case DataType.SPRITE:
								data.SetValue(old_data.GetSprite());
								break;

							case DataType.STRING:
								data.SetValue(old_data.GetString());
								break;

							case DataType.LONG:
								data.SetValue(old_data.GetLong());
								break;

							case DataType.CUSTOM_OBJECT:
								CustomObject obj = old_data.GetCustomObject();
								if (obj != null && obj.ID == newType.ID)
								{
									obj = FixType(obj, newType);
								}
								data.SetValue(obj);
								break;

							case DataType.VECTOR3:
								data.SetValue(old_data.GetVector());
								break;
						}
						new_obj.AddData(data);
					}
				}

				if (!foundInOld)
				{
					new_obj.AddData(new_data.Name, new_data.Type, new_data.ID);
				}
			}

			return new_obj;
		}

		public static void ApplyChanges(Control parent, CustomObject type, TreeNode selected_node)
		{
			TextBox tb = parent.Controls["Name_EditBox"] as TextBox;
			if (tb != null)
			{
				type.Name = tb.Text;
				if(selected_node != null)
					selected_node.Text = tb.Text;
			}

			foreach (CustomObjectData data in type.GetData())
			{
				switch (data.Type)
				{
					case DataType.BOOL:
						CheckBox cb = parent.Controls[data.Name + "_bool"] as CheckBox;
						if (cb != null)
						{
							data.SetValue(cb.Checked);
						}
						break;

					case DataType.FLOAT:
						NumericUpDown nup = parent.Controls[data.Name + "_float"] as NumericUpDown;
						if (nup != null)
						{
							data.SetValue((float)nup.Value);
						}
						break;

					case DataType.INT:
						nup = parent.Controls[data.Name + "_int"] as NumericUpDown;
						if (nup != null)
						{
							data.SetValue((int)nup.Value);
						}
						break;

					case DataType.SPRITE:
						Button but = parent.Controls[data.Name + "_sprite"] as Button;
						if (but != null && but.Tag is SpriteData)
						{
							data.SetValue(but.Tag as SpriteData);
						}
						break;

					case DataType.STRING:
						tb = parent.Controls[data.Name + "_string"] as TextBox;
						if (tb != null)
						{
							data.SetValue(tb.Text);
						}
						break;

					case DataType.LONG:
						nup = parent.Controls[data.Name + "_long"] as NumericUpDown;
						if (nup != null)
						{
							data.SetValue((long)nup.Value);
						}
						break;

					case DataType.CUSTOM_OBJECT:
						GroupBox container = parent.Controls[data.Name + "_custom"] as GroupBox;
						if (container != null && container.Tag is CustomObject)
						{
							ApplyChanges(container, container.Tag as CustomObject, null);
							data.SetValue(container.Tag as CustomObject);
						}
						break;

					case DataType.VECTOR3:
						VectorControl vec = parent.Controls[data.Name + "_vector3"] as VectorControl;
						if(vec != null)
						{
							data.SetValue(vec.Vector);
						}
						break;
				}
			}
		}

		public static void SetupForChild(Control parent, CustomObject type, MainForm owner, bool subType)
		{
			parent.Controls.Clear();

			FormBuilder helperyThing = new FormBuilder(6, parent);

			if(subType)
			{
				Label label = new Label();
				label.AutoSize = false;
				label.Size = new Size(100, 18);
				label.TextAlign = ContentAlignment.TopLeft;
				label.Text = type.Name;
				helperyThing.AddControl("SubType:", label);
			}
			else
			{
				TextBox nameEdit = new TextBox();
				nameEdit.Text = type.Name;
				nameEdit.Name = "Name_EditBox";
				nameEdit.Size = new System.Drawing.Size(160, 20);
				helperyThing.AddControl("Name:", nameEdit);
				Label label = new Label();
				label.AutoSize = false;
				label.Size = new Size(100, 18);
				label.TextAlign = ContentAlignment.TopLeft;
				label.Text = type.ID.ToString();
				helperyThing.AddControl("ID:", label);
			}

			foreach (CustomObjectData data in type.GetData())
			{
				switch (data.Type)
				{
					case DataType.STRING:
						TextBox stringEdit = new TextBox();
						stringEdit.Text = data.GetString();
						stringEdit.Name = data.Name + "_string";
						stringEdit.Width = 160;
						helperyThing.AddControl(data.Name + ":", stringEdit);
						break;

					case DataType.INT:
						NumericUpDown number = new NumericUpDown();
						number.Minimum = Int32.MinValue;
						number.Maximum = Int32.MaxValue;
						number.Name = data.Name + "_int";
						number.Increment = 1;
						number.DecimalPlaces = 0;
						number.Value = data.GetInt();			
						helperyThing.AddControl(data.Name + ":", number);
						break;

					case DataType.FLOAT:
						number = new NumericUpDown();
						number.Name = data.Name + "_float";
						number.Increment = 0.1m;
						number.DecimalPlaces = 0;
						number.Minimum = decimal.MinValue;
						number.Maximum = decimal.MaxValue;
						number.ValueChanged += new EventHandler(numericUpDownChanged);
						number.Value = (decimal)data.GetFloat();
						helperyThing.AddControl(data.Name + ":", number);
						break;

					case DataType.LONG:
						number = new NumericUpDown();
						number.Name = data.Name + "_long";
						number.Increment = 1;
						number.DecimalPlaces = 0;
						number.Minimum = Int64.MinValue;
						number.Maximum = Int64.MaxValue;
						number.Value = (decimal)data.GetFloat();
						helperyThing.AddControl(data.Name + ":", number);
						break;

					case DataType.BOOL:
						CheckBox checkBox = new CheckBox();
						checkBox.Name = data.Name + "_bool";
						checkBox.Checked = data.GetBool();
						checkBox.Size = new Size(15, 14);
						helperyThing.AddControl(data.Name + ":", checkBox);
						break;

					case DataType.SPRITE:
						Button button = new Button();
						button.Name = data.Name + "_sprite";
						button.Text = "Edit Sprite";
						button.AutoSize = true;
						button.Click += new EventHandler(editSpriteClick);
						button.UseVisualStyleBackColor = true;
						button.Tag = data.GetSprite().Clone();
						helperyThing.AddControl(data.Name + ":", button);
						break;

					case DataType.CUSTOM_OBJECT:
						CustomObject custom = data.GetCustomObject();
						/*button = new Button();
						button.Name = data.Name + "_custom";
						button.Text = "Change SubType";
						button.AutoSize = true;
						button.Click += new EventHandler(owner.editSubType);
						button.UseVisualStyleBackColor = true;
						if (custom != null)
							button.Tag = data.GetCustomObject().Clone(true);

						helperyThing.AddControl(data.Name + ":", button);*/

						GroupBox groupBox = new GroupBox();
						groupBox.AutoSize = true;
						groupBox.AutoSizeMode = AutoSizeMode.GrowAndShrink;
						groupBox.Padding = new Padding(0, 16, 0, 0);
						groupBox.Name = data.Name + "_custom";
						groupBox.Text = data.Name;
						if (custom != null)
							groupBox.Tag = data.GetCustomObject().Clone(true);

						if (custom != null)
						{
							SetupForChild(groupBox, data.GetCustomObject(), owner, true);
						}
						helperyThing.AddControl("", groupBox);
						
						break;

					case DataType.VECTOR3:
						VectorControl vec = new VectorControl();
						vec.Name = data.Name + "_vector3";
						vec.Text = data.Name;
						vec.Vector = data.GetVector();
						helperyThing.AddControl("Vector:", vec);
						break;
				}
			}
		}

		private static void editSpriteClick(object sender, EventArgs e)
		{
			if (sender is Button)
			{
				Button button_sender = sender as Button;
				if (button_sender.Tag is SpriteData)
				{
					SpriteData data = button_sender.Tag as SpriteData;
					SpriteEditor diag = new SpriteEditor(data);
					if (diag.ShowDialog() == DialogResult.OK)
						button_sender.Tag = diag.Sprite;
				}
			}
		}

		private static void numericUpDownChanged(object sender, EventArgs e)
		{
			NumericUpDown ud = sender as NumericUpDown;
			if(ud != null)
			{
				string val = ud.Value.ToString("G");
				int decPlace = val.IndexOf('.');
				int numPlaces = 0;
				if(decPlace > -1)
				{
					numPlaces = (val.Length - decPlace) -1;
				}

				ud.DecimalPlaces = numPlaces;
			}
		}
	}
}
