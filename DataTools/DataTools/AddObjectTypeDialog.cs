﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace DataTools
{
	public partial class AddObjectTypeDialog : Form
	{
		private ObjectType m_obj;

		public AddObjectTypeDialog(ObjectType obj)
		{
			m_obj = obj;
			InitializeComponent();
		}

		public AddObjectTypeDialog()
		{
			m_obj = new ObjectType("DefaultName", Globals.GetNextID());
			InitializeComponent();
		}

		public ObjectType NewType
		{
			get { return m_obj; }
		}

		private void addIntButton_Click(object sender, EventArgs e)
		{
			GetStringDialog diag = new GetStringDialog();
			diag.DialogTitle = "Enter Int Name";

			if (diag.ShowDialog() == DialogResult.OK)
			{
				ObjectData newObj = new ObjectData(DataType.INT, diag.StringData);
				this.listBox1.Items.Add(newObj);
			}
		}

		private void button5_Click(object sender, EventArgs e)
		{
			GetStringDialog diag = new GetStringDialog();
			diag.DialogTitle = "Enter Sprite Name";

			if (diag.ShowDialog() == DialogResult.OK)
			{
				ObjectData newObj = new ObjectData(DataType.SPRITE, diag.StringData);
				this.listBox1.Items.Add(newObj);
			}

		}

		private void addStringButton_Click(object sender, EventArgs e)
		{
			GetStringDialog diag = new GetStringDialog();
			diag.DialogTitle = "Enter String Name";

			if (diag.ShowDialog() == DialogResult.OK)
			{
				ObjectData newObj = new ObjectData(DataType.STRING, diag.StringData);
				this.listBox1.Items.Add(newObj);
			}
		}

		private void addFloatButton_Click(object sender, EventArgs e)
		{
			GetStringDialog diag = new GetStringDialog();
			diag.DialogTitle = "Enter Float Name";

			if (diag.ShowDialog() == DialogResult.OK)
			{
				ObjectData newObj = new ObjectData(DataType.FLOAT, diag.StringData);
				this.listBox1.Items.Add(newObj);
			}
		}

		private void button1_Click(object sender, EventArgs e)
		{
			if(this.textBox1.Text.Length == 0)
			{
				MessageBox.Show("You must enter a name for this type", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
			}
			else
			{
				m_obj.Clear();
				m_obj.Name = this.textBox1.Text;

				foreach(ObjectData data in this.listBox1.Items)
				{
					m_obj.AddData(data);
				}
				this.DialogResult = DialogResult.OK;
				this.Close();
			}
		}

		private void AddObjectTypeDialog_KeyDown(object sender, KeyEventArgs e)
		{
			if(e.KeyCode == Keys.Escape)
			{
				e.Handled = true;
				this.DialogResult = DialogResult.Cancel;
				this.Close();
			}
		}

		private void listBox1_KeyDown(object sender, KeyEventArgs e)
		{
			if(e.KeyCode == Keys.Delete)
			{
				if (this.listBox1.SelectedIndex >= 0)
					this.listBox1.Items.RemoveAt(this.listBox1.SelectedIndex);
			}
		}

		private void button2_Click(object sender, EventArgs e)
		{
			GetStringDialog diag = new GetStringDialog();
			diag.DialogTitle = "Enter Bool Name";

			if (diag.ShowDialog() == DialogResult.OK)
			{
				ObjectData newObj = new ObjectData(DataType.BOOL, diag.StringData);
				this.listBox1.Items.Add(newObj);
			}
		}
	}
}
