﻿namespace DataTools
{
	partial class AddObjectTypeDialog
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.textBox1 = new System.Windows.Forms.TextBox();
			this.label1 = new System.Windows.Forms.Label();
			this.label2 = new System.Windows.Forms.Label();
			this.addIntButton = new System.Windows.Forms.Button();
			this.addStringButton = new System.Windows.Forms.Button();
			this.addFloatButton = new System.Windows.Forms.Button();
			this.listBox1 = new System.Windows.Forms.ListBox();
			this.button1 = new System.Windows.Forms.Button();
			this.button4 = new System.Windows.Forms.Button();
			this.button5 = new System.Windows.Forms.Button();
			this.button2 = new System.Windows.Forms.Button();
			this.SuspendLayout();
			// 
			// textBox1
			// 
			this.textBox1.Location = new System.Drawing.Point(53, 6);
			this.textBox1.Name = "textBox1";
			this.textBox1.Size = new System.Drawing.Size(139, 20);
			this.textBox1.TabIndex = 0;
			// 
			// label1
			// 
			this.label1.AutoSize = true;
			this.label1.Location = new System.Drawing.Point(12, 9);
			this.label1.Name = "label1";
			this.label1.Size = new System.Drawing.Size(35, 13);
			this.label1.TabIndex = 1;
			this.label1.Text = "Name";
			// 
			// label2
			// 
			this.label2.AutoSize = true;
			this.label2.Location = new System.Drawing.Point(12, 37);
			this.label2.Name = "label2";
			this.label2.Size = new System.Drawing.Size(50, 13);
			this.label2.TabIndex = 3;
			this.label2.Text = "Variables";
			// 
			// addIntButton
			// 
			this.addIntButton.Location = new System.Drawing.Point(197, 53);
			this.addIntButton.Name = "addIntButton";
			this.addIntButton.Size = new System.Drawing.Size(75, 23);
			this.addIntButton.TabIndex = 4;
			this.addIntButton.Text = "Add Int";
			this.addIntButton.UseVisualStyleBackColor = true;
			this.addIntButton.Click += new System.EventHandler(this.addIntButton_Click);
			// 
			// addStringButton
			// 
			this.addStringButton.Location = new System.Drawing.Point(197, 82);
			this.addStringButton.Name = "addStringButton";
			this.addStringButton.Size = new System.Drawing.Size(75, 23);
			this.addStringButton.TabIndex = 4;
			this.addStringButton.Text = "Add String";
			this.addStringButton.UseVisualStyleBackColor = true;
			this.addStringButton.Click += new System.EventHandler(this.addStringButton_Click);
			// 
			// addFloatButton
			// 
			this.addFloatButton.Location = new System.Drawing.Point(197, 111);
			this.addFloatButton.Name = "addFloatButton";
			this.addFloatButton.Size = new System.Drawing.Size(75, 23);
			this.addFloatButton.TabIndex = 4;
			this.addFloatButton.Text = "Add Float";
			this.addFloatButton.UseVisualStyleBackColor = true;
			this.addFloatButton.Click += new System.EventHandler(this.addFloatButton_Click);
			// 
			// listBox1
			// 
			this.listBox1.FormattingEnabled = true;
			this.listBox1.Location = new System.Drawing.Point(12, 53);
			this.listBox1.Name = "listBox1";
			this.listBox1.Size = new System.Drawing.Size(177, 225);
			this.listBox1.TabIndex = 2;
			this.listBox1.KeyDown += new System.Windows.Forms.KeyEventHandler(this.listBox1_KeyDown);
			// 
			// button1
			// 
			this.button1.Location = new System.Drawing.Point(197, 255);
			this.button1.Name = "button1";
			this.button1.Size = new System.Drawing.Size(75, 23);
			this.button1.TabIndex = 5;
			this.button1.Text = "OK";
			this.button1.UseVisualStyleBackColor = true;
			this.button1.Click += new System.EventHandler(this.button1_Click);
			// 
			// button4
			// 
			this.button4.DialogResult = System.Windows.Forms.DialogResult.Cancel;
			this.button4.Location = new System.Drawing.Point(197, 226);
			this.button4.Name = "button4";
			this.button4.Size = new System.Drawing.Size(75, 23);
			this.button4.TabIndex = 6;
			this.button4.Text = "Cancel";
			this.button4.UseVisualStyleBackColor = true;
			// 
			// button5
			// 
			this.button5.Location = new System.Drawing.Point(197, 169);
			this.button5.Name = "button5";
			this.button5.Size = new System.Drawing.Size(75, 23);
			this.button5.TabIndex = 4;
			this.button5.Text = "Add Sprite";
			this.button5.UseVisualStyleBackColor = true;
			this.button5.Click += new System.EventHandler(this.button5_Click);
			// 
			// button2
			// 
			this.button2.Location = new System.Drawing.Point(197, 140);
			this.button2.Name = "button2";
			this.button2.Size = new System.Drawing.Size(75, 23);
			this.button2.TabIndex = 7;
			this.button2.Text = "Add Bool";
			this.button2.UseVisualStyleBackColor = true;
			this.button2.Click += new System.EventHandler(this.button2_Click);
			// 
			// AddObjectTypeDialog
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(284, 286);
			this.Controls.Add(this.button2);
			this.Controls.Add(this.button4);
			this.Controls.Add(this.button1);
			this.Controls.Add(this.button5);
			this.Controls.Add(this.addFloatButton);
			this.Controls.Add(this.addStringButton);
			this.Controls.Add(this.addIntButton);
			this.Controls.Add(this.label2);
			this.Controls.Add(this.listBox1);
			this.Controls.Add(this.label1);
			this.Controls.Add(this.textBox1);
			this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
			this.KeyPreview = true;
			this.Name = "AddObjectTypeDialog";
			this.Text = "AddObjectType";
			this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.AddObjectTypeDialog_KeyDown);
			this.ResumeLayout(false);
			this.PerformLayout();

		}

		#endregion

		private System.Windows.Forms.TextBox textBox1;
		private System.Windows.Forms.Label label1;
		private System.Windows.Forms.Label label2;
		private System.Windows.Forms.Button addIntButton;
		private System.Windows.Forms.Button addStringButton;
		private System.Windows.Forms.Button addFloatButton;
		private System.Windows.Forms.ListBox listBox1;
		private System.Windows.Forms.Button button1;
		private System.Windows.Forms.Button button4;
		private System.Windows.Forms.Button button5;
		private System.Windows.Forms.Button button2;
	}
}