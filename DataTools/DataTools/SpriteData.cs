﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Drawing;

namespace DataTools
{
	public struct Shape
	{
		public float X;
		public float Y;
		public float Width;
		public float Height;
		public float cX;
		public float cY;
	}

	public class SpriteData
	{
		private Shape[] m_frameCoords;
		private int m_numFrames;
		private float m_frameInterval;
		private float m_width;
		private float m_height;
		private string m_name;
		private string m_texName;
		private string m_texPath;

		public SpriteData()
		{
			m_width = 0;
			m_height = 0;
			m_numFrames = 0;
			m_frameInterval = 0.0166667f;
			m_frameCoords = null;
			m_name = "DefaultName";
			m_texName = "";
			m_texPath = "";
		}

		public SpriteData Clone()
		{
			SpriteData data = new SpriteData();
			data.Name = m_name;
			data.TexName = m_texName;
			data.FrameInterval = m_frameInterval;
			data.Height = m_height;
			data.Width = m_width;
			data.SetFrameCoords(m_numFrames, m_frameCoords);

			return data;
		}

		public string Name
		{
			get { return m_name;  }
			set { m_name = value; }
		}

		public string TexName
		{
			get { return m_texName;  }
			set { m_texName = value; }
		}

		public string TexPath
		{
			get { return m_texPath; }
			set { m_texPath = value; }
		}

		public float Width
		{
			get { return m_width;  }
			set { m_width = value; }
		}

		public float Height
		{
			get { return m_height; }
			set { m_height = value; }
		}

		public float FrameInterval
		{
			get { return m_frameInterval; }
			set { m_frameInterval = value; }
		}

		public int NumFrames
		{
			get { return m_numFrames; }
		}

		public Shape[] FrameCoords
		{
			get { return m_frameCoords; }
		}

		public void SetFrameCoords(int numFrames, Shape[] coords)
		{
			m_numFrames = numFrames;
			m_frameCoords = new Shape[numFrames];
			for(int i=0; i<numFrames; i++)
			{
				m_frameCoords[i] = coords[i];
			}
		}
	}
}
