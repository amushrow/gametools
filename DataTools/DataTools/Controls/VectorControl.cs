﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using GameDataLib;

namespace DataTools
{
	public partial class VectorControl : UserControl
	{
		public VectorControl()
		{
			InitializeComponent();

			this.numericUpDown1.Minimum = Decimal.MinValue;
			this.numericUpDown2.Minimum = Decimal.MinValue;
			this.numericUpDown3.Minimum = Decimal.MinValue;

			this.numericUpDown1.Maximum = Decimal.MaxValue;
			this.numericUpDown2.Maximum = Decimal.MaxValue;
			this.numericUpDown3.Maximum = Decimal.MaxValue;
		}

		public override string Text
		{
			get { return this.groupBox1.Text;  }
			set { this.groupBox1.Text = value; }
		}

		public Vector3 Vector
		{
			get { return new Vector3((float)this.numericUpDown1.Value, (float)this.numericUpDown2.Value, (float)this.numericUpDown3.Value); }
			set
			{
				this.numericUpDown1.Value = (decimal)value.X;
				this.numericUpDown2.Value = (decimal)value.Y;
				this.numericUpDown3.Value = (decimal)value.Z;
			}
		}

		private void numericUpDown_ValueChanged(object sender, EventArgs e)
		{
			NumericUpDown ud = sender as NumericUpDown;
			if (ud != null)
			{
				string val = ud.Value.ToString("G");
				int decPlace = val.IndexOf('.');
				int numPlaces = 0;
				if (decPlace > -1)
				{
					numPlaces = (val.Length - decPlace) - 1;
				}

				ud.DecimalPlaces = numPlaces;
			}
		}
	}
}
