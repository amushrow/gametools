﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;

namespace DataTools
{

	public static class Globals
	{
		private static long CurID = 0;

		public static long GetNextID()
		{
			return CurID++;
		}
	}

	public enum DataType
	{
		STRING,
		INT,
		FLOAT,
		SPRITE,
		BOOL
	}

	public class ObjectData
	{
		private	string m_name;
		private DataType m_type;
		private float m_fValue;
		private string m_sValue;
		private int m_iValue;
		private bool m_bValue;
		private SpriteData m_spValue;

		public ObjectData(DataType type, string name)
		{
			m_name = name;
			m_type = type;
		}

		public ObjectData Clone()
		{
			ObjectData data = new ObjectData(m_type, m_name);
			
			switch(m_type)
			{
				case DataType.INT:
					data.SetValue(m_iValue);
					break;

				case DataType.FLOAT:
					data.SetValue(m_fValue);
					break;

				case DataType.STRING:
					data.SetValue(m_sValue);
					break;

				case DataType.SPRITE:
					if(m_spValue != null)
						data.SetValue(m_spValue.Clone());
					break;

				case DataType.BOOL:
					data.SetValue(m_bValue);
					break;
			}

			return data;
		}

		public DataType Type
		{
			get { return m_type; }
		}

		public string Name
		{
			get { return m_name;  }
			set { m_name = value; }
		}

		public void SetValue(float val)
		{
			m_fValue = val;
		}

		public void SetValue(int val)
		{
			m_iValue = val;
		}

		public void SetValue(string val)
		{
			m_sValue = val;
		}

		public void SetValue(SpriteData val)
		{
			m_spValue = val;
		}

		public void SetValue(bool val)
		{
			m_bValue = val;
		}

		public string GetString()
		{
			return m_sValue;
		}

		public int GetInt()
		{
			return m_iValue;
		}

		public float GetFloat()
		{
			return m_fValue;
		}

		public bool GetBool()
		{
			return m_bValue;
		}

		public SpriteData GetSprite()
		{
			return m_spValue;
		}

		public override string ToString()
		{
			string type = "";
			switch(m_type)
			{
				case DataType.INT:
					type = " : INT";
					break;

				case DataType.FLOAT:
					type = " : FLOAT";
					break;

				case DataType.STRING:
					type = " : STRING";
					break;

				case DataType.SPRITE:
					type = " : SPRITE";
					break;

				case DataType.BOOL:
					type = " : BOOL";
					break;
			}

			return m_name + type;
		}
	}

	public class ObjectType
	{
		private string m_name;
		private long m_id;
		private List<ObjectData> m_data;

		public ObjectType(string name, long id)
		{
			m_name = name;
			m_id = id;
			m_data = new List<ObjectData>();
		}

		public ObjectType Clone()
		{
			ObjectType clone = new ObjectType(m_name, Globals.GetNextID());
			foreach(ObjectData data in m_data)
			{
				clone.AddData(data.Clone());
			}

			return clone;
		}

		public void InitializeData()
		{
			foreach (ObjectData data in m_data)
			{
				switch (data.Type)
				{
					case DataType.INT:
						data.SetValue(0);
						break;

					case DataType.FLOAT:
						data.SetValue(0f);
						break;

					case DataType.STRING:
						data.SetValue("");
						break;

					case DataType.SPRITE:
						data.SetValue(new SpriteData());
						break;

					case DataType.BOOL:
						data.SetValue(false);
						break;
				}
			}
		}

        public long ID
        {
            get { return m_id; }
        }

		public void AddData(ObjectData obj)
		{
			m_data.Add(obj);
		}

		public string Name
		{
			get { return m_name; }
			set { m_name = value; }
		}

		public ReadOnlyCollection<ObjectData> GetData()
		{
			return m_data.AsReadOnly();
		}

		public void Clear()
		{
			m_name = "";
			m_data.Clear();
		}
	}
}
