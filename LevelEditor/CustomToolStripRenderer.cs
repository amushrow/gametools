﻿using System;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Windows.Forms;

namespace MapEditor
{
	/// <summary>
	/// Just to add a gradient to the toolstrip, make it look nice y'know?
	/// </summary>
	public class CustomToolStripRenderer : ToolStripProfessionalRenderer
	{
		private Color backcolour;
		
		protected override void OnRenderToolStripBackground(ToolStripRenderEventArgs e)
		{
			//I check IsEmpty becuase if the connected area contains
			// values, then the menu has been opened i we need to draw the
			// background for the menu items instead of the actual toolstrip
			if(e.ConnectedArea.IsEmpty)
			{	
				backcolour = e.BackColor;
				Color dark = Darken(e.BackColor, 30);
				LinearGradientBrush myBrush = new LinearGradientBrush(e.AffectedBounds, dark, e.BackColor, LinearGradientMode.Vertical);
				e.Graphics.FillRectangle(myBrush, e.AffectedBounds);
				myBrush.Dispose();
			} else {
				Brush myBrush = new SolidBrush(Color.White);
				e.Graphics.FillRectangle(myBrush, e.AffectedBounds);
				myBrush.Dispose();
			}
		}
		
		protected override void OnRenderImageMargin(ToolStripRenderEventArgs e)
		{
			//When you see a menu with a margin it goes from white-gray, this is going
			// to go gray-white so that it fades across. It just makes it stand out less,
			// especially since i'm noy using any images on the menu
			Rectangle rect = e.AffectedBounds;
			LinearGradientBrush myBrush = new LinearGradientBrush(rect, backcolour, Color.White, LinearGradientMode.Horizontal);
			e.Graphics.FillRectangle(myBrush, rect);
			myBrush.Dispose();
		}
		
		Color Darken(Color baseCol, int darkness)
		{
			//If the ControlPaint.Dark method worked properly i would use it, but
			// i kept on getting strange results
			int r,g,b;
			r = baseCol.R;
			g = baseCol.G;
			b = baseCol.B;
			
			
			r -= darkness; g -= darkness; b -= darkness;
			if(r<0)
				r=0;
			if(g<0)
				g=0;
			if(b<0)
				b=0;
			
			return Color.FromArgb(r,g,b);
		}
	}
}
