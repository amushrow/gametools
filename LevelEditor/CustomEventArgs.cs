﻿using System;
using System.Drawing;
using MapEditorLib;

/// <summary>
/// This is a collection of custom EventArgs used in the editor, each with
/// relevant names and the required data
/// </summary>
namespace MapEditor
{
	public delegate void TileChangedEventHandler(object sender, TileChangedEventArgs e);
	public class TileChangedEventArgs : EventArgs
	{
		private Tile[,] m_tiles;
        public TileChangedEventArgs(Tile[,] tiles)
		{
            m_tiles = tiles;
		}
				
		public Tile[,] Tiles
		{
            get { return m_tiles; }
		}
	}
		
	public delegate void FontChangedEventHandler(object sender, FontChangedEventArgs e);
	public class FontChangedEventArgs : EventArgs
	{
		private Font m_font;
		
		public Font Font
		{
			get { return m_font; }
		}
		
		public FontChangedEventArgs(Font font)
		{
			m_font = font;
		}
	}
	
	public delegate void FlagChangedEventHandler(object sender, FlagChangedEventArgs e);
	public class FlagChangedEventArgs : EventArgs
	{
		private bool m_set;
		
		public bool IsSet
		{
			get { return m_set; }
		}
		
		public FlagChangedEventArgs(bool set)
		{
			m_set = set;
		}
	}
	
	public delegate void ColourChangedEventHandler(object sender, ColourChangedEventArgs e);
	public class ColourChangedEventArgs : EventArgs
	{
		private Color m_colour;
		public Color Colour
		{
			get { return m_colour; }
		}
		public ColourChangedEventArgs(Color colour)
		{
			m_colour = colour;
		}
	}
	
	public delegate void TileSizeChangedEventHandler(object sender, TileSizeChangedEventArgs e);
	public delegate void TilesetsChangedEventHandler(object sender, EventArgs e);
	public class TileSizeChangedEventArgs : EventArgs
	{
		private int m_tilesize;
		public TileSizeChangedEventArgs(int TileSize)
		{
			m_tilesize = TileSize;
		}
		
		public int TileSize
		{
			get { return m_tilesize; }
		}
	}
}

