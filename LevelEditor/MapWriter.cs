﻿using System;
using System.Drawing;
using System.IO;
using System.Collections.Generic;
using System.Windows.Forms;

namespace MapEditorLib
{
	/// <summary>
	/// Description of MapWriter.
	/// </summary>
	public class MapWriter
	{
		/*public static void Save(List<Tileset> TileSets, List<MapLayer> MapLayers, string Path)
		{
			//Get each block of data
            MemoryStream s_tilesets = WriteTilesets(m_tilesets);
			MemoryStream s_tiles = WriteTiles();

			FileStream FS = new FileStream(Path, FileMode.Create);
			
			//Write data
			byte[] name = System.Text.ASCIIEncoding.ASCII.GetBytes("MapEditorFile");
			FS.Write(name, 0, name.Length);
			
			string[] s_version = Application.ProductVersion.Split('.');
			float f_version = 0;
			float.TryParse(s_version[0]+"."+s_version[1], out f_version);
			
			byte[] version = System.BitConverter.GetBytes(f_version);
			FS.Write(version, 0, 4);
			
			byte[] size = System.BitConverter.GetBytes(s_tilesets.Length);
			FS.Write(size, 0, 8);
			s_tilesets.WriteTo(FS);
			
			size = System.BitConverter.GetBytes(s_tiles.Length);
			FS.Write(size, 0, 8);
			s_tiles.WriteTo(FS);
			
			FS.Close();
		}

		private MemoryStream WriteTiles()
		{
			MemoryStream s_tiles = new MemoryStream();	
			MemoryStream tempStream = new MemoryStream();
						
			int width = m_mapTiles.GetLength(0);
            int height = m_mapTiles.GetLength(1);
			
			//Write the width and height
			byte[] size = System.BitConverter.GetBytes(width);
			tempStream.Write(size, 0, 4);
			size = System.BitConverter.GetBytes(height);
			tempStream.Write(size, 0, 4);
			
			//Write the data from each Tile
			for(int x=0; x<width; x++) {
				for(int y=0; y<height; y++) {
					//Xpos
                    size = System.BitConverter.GetBytes(m_mapTiles[x, y].SourceX);
					tempStream.Write(size, 0, 4);
					//Ypos
                    size = System.BitConverter.GetBytes(m_mapTiles[x, y].SourceY);
					tempStream.Write(size, 0, 4);

                    //Solid
                    size = System.BitConverter.GetBytes(m_mapTiles[x, y].IsSolid);
					tempStream.Write(size, 0, 1);
					
                    //Find what tileset we are using on this tile
                    int index = -1;
					for(int i=0;i<m_tilesets.Count; i++) {
                        if (m_mapTiles[x, y] != null && m_mapTiles[x, y].IsInitialized && m_tilesets[i] == m_mapTiles[x, y].Tileset)
                        {
							index = i;
							break;
						}
					}
					
					//Tileset number
					size = System.BitConverter.GetBytes(index);
					tempStream.Write(size, 0, 4);
				}
			}
			
			tempStream.WriteTo(s_tiles);
			
			return s_tiles;
		}
		
		private MemoryStream WriteTilesets(List<Tileset> tileset)
		{
			MemoryStream s_tilesets = new MemoryStream();
			MemoryStream tempStream = new MemoryStream();
			
			foreach(Tileset tSet in tileset)
			{	
				MemoryStream image = new MemoryStream();
				
				//Tile size
				byte[] size = System.BitConverter.GetBytes(tSet.TileSize);
				tempStream.Write(size, 0, 4);
				
				//Name
				byte[] name = System.Text.ASCIIEncoding.ASCII.GetBytes(tSet.ToString());
				size = System.BitConverter.GetBytes(name.Length);
				tempStream.Write(size, 0, 4);
				tempStream.Write(name, 0, name.Length);
				
				//Image
				tSet.Image.Save(image, System.Drawing.Imaging.ImageFormat.Bmp);
				size = System.BitConverter.GetBytes(image.Length);
				tempStream.Write(size, 0, 8);
				image.Position = 0;
				image.WriteTo(tempStream);			
			}
			
			tempStream.WriteTo(s_tilesets);
			
			return s_tilesets;
		}*/
	}
}
