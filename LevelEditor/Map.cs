﻿
using System;
using System.Collections.Generic;
using System.Text;
using System.IO;
using System.Windows.Forms;
using System.Drawing;
using GameDataLib;

namespace MapEditorLib
{
	public class GameObject
	{
		public int X;
		public int Y;
		public CustomObject Obj;
		public long ID;
	}

	public class GameObjectList
	{
		private string m_text;
		public List<GameObject> List;

		public GameObjectList()
		{
			m_text = "Game Objects";
			List = new List<GameObject>();
		}

		public bool Visible = true;

		public string Text
		{
			get { return m_text;  }
			set { m_text = value; }
		}

		public override string ToString()
		{
			return m_text;
		}
	}

	/// <summary>
	/// Represents a map which consists of different
	/// layers of tiles. Has functions to load a map
	/// from File / Memory
	/// </summary>
	public class Map
	{
		#region Variables
		protected List<MapLayer> m_mapLayers;
		protected List<Tileset> m_tileSets;
		protected GameObjectList m_gameObjects;
		protected int m_width, m_height;
		protected int m_tileSize;
		private string m_name;

		public delegate Image RequestTexture(string Name);
		#endregion

		public Map()
		{
			m_width = 0;
			m_height = 0;
			m_tileSize = 0;
			m_mapLayers = new List<MapLayer>(0);
			m_tileSets = new List<Tileset>(0);
			m_gameObjects = new GameObjectList();
			m_name = "";
		}

		#region Properties
		public List<MapLayer> MapLayers
		{
			get { return m_mapLayers; }
		}

		public GameObjectList GameObjectsLayer
		{
			get { return m_gameObjects; }
		}

		public int RealWidth
		{
			get { return m_tileSize * m_width; }
		}

		public int RealHeight
		{
			get { return m_tileSize * m_height; }
		}

		public int Width
		{
			get { return m_width; }
		}

		public int Height
		{
			get { return m_height; }
		}

		public virtual int TileSize
		{
			get { return m_tileSize; }
			set { m_tileSize = value; }
		}

		public List<Tileset> TileSets
		{
			get { return m_tileSets; }
		}

		public string Name
		{
			get { return m_name; }
			set { m_name = value; }
		}
		#endregion

		#region Public Functions
		public void Load(string Path, RequestTexture requestTexture)
		{
			FileStream fs = File.Open(Path, FileMode.Open);
			Load(fs, requestTexture);
			fs.Close();
		}

		public virtual void Load(Stream dataStream, RequestTexture requestTexture)
		{
			m_mapLayers = new List<MapLayer>(4);
			m_tileSets = new List<Tileset>(4);
			m_gameObjects = new GameObjectList();

			byte[] buffer = new byte[128];
			
			//Check the first 4 bytes to make sure they match our
			// id
			bool correctFileType = true;
			dataStream.Read(buffer, 0, 4);
			for (int i = 0; i < 4; i++)	{
				if (buffer[i] != LibData.ID[i])
					correctFileType = false;
			}
			if (!correctFileType)
				throw new InvalidFileTypeException("Not a valid MapEditor file");

			//Get the version number
			dataStream.Read(buffer, 0, 4);
			float version = BitConverter.ToSingle(buffer, 0);
			if(version > LibData.FILEVERSION_NUM)
				throw new InvalidFileTypeException("The file is from a newer version of MapEditor");

			//Get name
			if (version >= 0.11f)
				m_name = readString(dataStream);
			else
				m_name = "Default";

			//Get the width, height and tilesize
			dataStream.Read(buffer, 0, 12);
			m_width = BitConverter.ToInt32(buffer, 0);
			m_height = BitConverter.ToInt32(buffer, 4);
			m_tileSize = BitConverter.ToInt32(buffer, 8);

			//Load up the tilesets
			dataStream.Read(buffer, 0, 4);
			int count = BitConverter.ToInt32(buffer, 0);
			for (int i = 0; i < count; i++)
			{
				dataStream.Read(buffer, 0, 1);
				bool readOn = BitConverter.ToBoolean(buffer, 0);
				if(readOn)
				{
					dataStream.Read(buffer, 0, 4);
					int id = BitConverter.ToInt32(buffer, 0);
					string name = readString(dataStream);

					//I love delegates
					Image texture = requestTexture(name);

					if (texture != null)
					{
						Tileset newSet = new Tileset(texture, name, id);
						m_tileSets.Add(newSet);
					}
				}
			}

			//Load up the map layers
			dataStream.Read(buffer, 0, 4);
			count = BitConverter.ToInt32(buffer, 0);
			for (int i = 0; i < count; i++)
			{
				dataStream.Read(buffer, 0, 8);
				int ID = BitConverter.ToInt32(buffer, 0);
				float Z_Value = BitConverter.ToSingle(buffer, 4);

				dataStream.Read(buffer, 0, 2);
				bool IsBG = BitConverter.ToBoolean(buffer, 0);
				bool Visible = BitConverter.ToBoolean(buffer, 1);
				string Name = readString(dataStream);

				MapLayer newLayer = new MapLayer(Name, ID, Z_Value, IsBG);
				newLayer.Visible = Visible;
				newLayer.Resize(m_width, m_height);

				for(int x=0; x<m_width; x++) {
					for( int y=0; y<m_height; y++) {
						dataStream.Read(buffer, 0, 1);
						bool readOn = BitConverter.ToBoolean(buffer, 0);
						if(readOn)
						{
							dataStream.Read(buffer, 0, 20);
							int SrcX = BitConverter.ToInt32(buffer, 0);
							int SrcY = BitConverter.ToInt32(buffer, 4);
							int XPos = BitConverter.ToInt32(buffer, 8);
							int YPos = BitConverter.ToInt32(buffer, 12);
							int TsID = BitConverter.ToInt32(buffer, 16);

							dataStream.Read(buffer, 0, 1);
							bool Solid = BitConverter.ToBoolean(buffer, 0);

							foreach (Tileset ts in m_tileSets)
							{
								if(ts.ID == TsID)
								{
									newLayer.Tiles[x, y] = new Tile(ts, SrcX, SrcY, Solid);
									newLayer.Tiles[x, y].XPos = XPos;
									newLayer.Tiles[x, y].YPos = YPos;
								}
							}
						}
					}
				}

				m_mapLayers.Add(newLayer);
			}

			if(version >= 0.2f)
			{
				dataStream.Read(buffer, 0, 4);
				int numObjects = BitConverter.ToInt32(buffer, 0);

				for(int i=0; i<numObjects; i++)
				{
					GameObject newObj = new GameObject();
					dataStream.Read(buffer, 0, 8);
					newObj.ID = BitConverter.ToInt64(buffer, 0);
					dataStream.Read(buffer, 0, 8);
					newObj.X = BitConverter.ToInt32(buffer, 0);
					newObj.Y = BitConverter.ToInt32(buffer, 4);

					m_gameObjects.List.Add(newObj);
				}
			}

			int check = dataStream.ReadByte();
			if (check != 0xFF)
				throw new InvalidFileTypeException("Error parsing file");
		}
		#endregion

		//Just for convenience
		private string readString(Stream dataStream)
		{
			byte[] buffer = new byte[128];
			dataStream.Read(buffer, 0, 4);
			int length = BitConverter.ToInt32(buffer, 0);
			dataStream.Read(buffer, 0, length);

			return System.Text.ASCIIEncoding.ASCII.GetString(buffer, 0, length);
		}
	}
}
