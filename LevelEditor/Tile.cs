﻿using System;

namespace MapEditorLib
{
	/// <summary>
	///  The Tile class represents an individual tile in the map
	/// Its stores contains the position of the tile in the map
	/// if the tile is solid or not and the tileset that it is
	/// using.
	///  If the tile is not marked as NULL it MUST have an
	/// associated Tileset.
	/// </summary>
    public class Tile : IEquatable<Tile>
	{
		#region Variables
		private Tileset m_tileSet;
        private int m_sourcex;
        private int m_sourcey;
		private int m_xpos;
		private int m_ypos;
        private bool m_solid;
        private bool m_init;
		#endregion

		#region Properties
		public bool IsInitialized
        {
            get { return m_init; }
        }

        public Tileset Tileset
        {
            get { return m_tileSet; }
            set { m_tileSet = value; m_init = true; }
        }

        public int SourceX
        {
            get { return m_sourcex; }
            set { m_sourcex = value; }
        }

        public int SourceY
        {
            get { return m_sourcey; }
            set { m_sourcey = value; }
        }

		public int XPos
		{
			get { return m_xpos; }
			set { m_xpos = value; }
		}

		public int YPos
		{
			get { return m_ypos; }
			set { m_ypos = value; }
		}

        public bool IsSolid
        {
            get { return m_solid; }
            set { m_solid = value; }
		}
		#endregion

		public Tile(Tileset tileSet, int srcX, int srcY, bool solid)
        {
            m_tileSet = tileSet;
            m_sourcex = srcX;
            m_sourcey = srcY;
            m_solid = solid;
            m_init = true;
        }

        public Tile()
        {
            m_sourcex = 0;
            m_sourcey = 0;
            m_solid = false;
            m_init = false;
		}

		#region Public Functions
		public void Clear()
        {
            m_sourcex = 0;
            m_sourcey = 0;
            m_tileSet = null;
            m_solid = false;
            m_init = false;
        }

		public Tile Clone()
		{
			Tile newTile = new Tile();
			newTile.m_tileSet = m_tileSet;
			newTile.m_sourcex = m_sourcex;
			newTile.m_sourcey = m_sourcey;
			newTile.m_solid = m_solid;
			newTile.m_init = m_init;

			return newTile;
		}

		public bool Equals(Tile t)
		{
			if (t.m_tileSet != m_tileSet)
				return false;

			if (t.IsSolid != IsSolid)
				return false;
			
			if (t.SourceX != SourceX)
				return false;

			if (t.SourceY != SourceY)
				return false;

			return true;
		}
		#endregion
	}
}