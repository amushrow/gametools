﻿using System;
using System.Drawing;
using System.IO;

namespace MapEditorLib
{
	/// <summary>
	/// Simple stores the Bitmap and Texture2D (if using XnaMapRenderer)
	/// of a tile set. It also has a unique ID so that a tileset can be
	/// easily identified.
	/// </summary>
	public class Tileset
	{
		#region Variables
		private Microsoft.Xna.Framework.Graphics.Texture2D m_texture;
		private Bitmap m_image;
		private IntPtr m_pImage;
		private int m_width, m_height;
		private string m_name;
		private int m_id;
		#endregion

		#region Properties
		public Microsoft.Xna.Framework.Graphics.Texture2D Texture
		{
			get { return m_texture; }
			set { m_texture = value; }
		}

		public IntPtr pImage
		{
			get { return m_pImage; }
			set { m_pImage = value; }
		}
			
		public int Width
		{
			get { return m_width; }
		}
		
		public int Height
		{
			get { return m_height; }
		}

		public Bitmap Image
		{
			get { return m_image; }
		}

		public int ID
		{
			get { return m_id; }
		}

		public string Name
		{
			get { return m_name; }
		}
		#endregion
		
		public Tileset(Image image, string Name, int ID)
		{
			m_name = Name;

			m_image = new Bitmap(image);
			m_width = m_image.Width;
			m_height = m_image.Height;
			m_id = ID;
		}

		public override string ToString()
		{
			return m_name;
		}

		public void Dispose()
		{
			if (m_texture != null)
				m_texture.Dispose();
			if (m_image != null)
				m_image.Dispose();			
		}
	}
}
