﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Windows.Forms;

namespace MapEditor.Controls
{
	/// <summary>
	/// Basically just an empty control, in which you can use an
	/// external function to do any rendering
	/// </summary>
	public partial class MapPanel : UserControl
	{
		public delegate void MapRenderer(PaintEventArgs e);

		private MapRenderer m_renderMethod;

		public MapPanel()
		{
			InitializeComponent();
		}

		#region Public Functions
		public void SetRenderMethod(MapRenderer renderer)
		{
			m_renderMethod = renderer;
		}
		#endregion

		#region Events
		protected override void OnPaintBackground(PaintEventArgs e)
		{
			//Nothing
			if (m_renderMethod == null)
				base.OnPaintBackground(e);

		}

		protected override void OnPaint(PaintEventArgs e)
		{
			//Call our custom renderer
			if (m_renderMethod != null)
				m_renderMethod.Invoke(e);
			else
				base.OnPaint(e);
		}
		#endregion
	}
}
