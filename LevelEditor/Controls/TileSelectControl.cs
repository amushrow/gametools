﻿using System;
using System.ComponentModel;
using System.Drawing;
using System.Windows.Forms;
using System.Collections.Generic;
using System.IO;
using MapEditorLib;

namespace MapEditor
{
	public partial class TileSelectControl : UserControl
	{
		#region Variables
		private Color m_gridColour;
		private Color m_overlayColour;
		private Tileset m_currentTileset;
		private List<Tileset> m_tilesets;
		private Point m_startSelection;
		private Point m_endSelection;
		private Rectangle m_selectingArea;
		private Rectangle m_selected;
		private bool m_isSelecting;
		private int m_tileSize;
		#endregion

		#region Properties
		public event TilesetsChangedEventHandler OnTilesetsChanged;
		public event TileChangedEventHandler OnTileSelectionChanged;

		public List<Tileset> Tilesets
		{
			get { return m_tilesets; }
		}

		[Browsable(false)]
		public int TileSize
		{
			get { return m_tileSize; }
			set
			{
				m_tileSize = value;
				this.pictureBox.Invalidate();
			}
		}

		public Color GridColour
		{
			get { return m_gridColour; }
			set { m_gridColour = value; this.pictureBox.Invalidate(); }
		}
		
		public Color OverlayColour
		{
			get { return m_overlayColour; }
			set { m_overlayColour = value;  this.pictureBox.Invalidate(); }
		}
		#endregion
		
		public TileSelectControl()
		{
			InitializeComponent();
			m_selected = new Rectangle(0,0,0,0);
			m_selectingArea = new Rectangle(0,0,0,0);
			m_startSelection = new Point(0,0);
			m_endSelection = new Point(0,0);
			m_isSelecting = false;
			m_tilesets = new List<Tileset>(0);
			m_gridColour = Color.Gray;
			m_overlayColour = Color.Red;
			m_tileSize = 32;
		}
		
		public void SetTileSets(List<Tileset> TileSets)
		{
			m_tilesets = TileSets;
			//Clear - refill the combobox with the new list
			this.comboBox.SuspendLayout();
			this.comboBox.Items.Clear();

			foreach (Tileset tileset in m_tilesets)
				this.comboBox.Items.Add(tileset);

			this.comboBox.Items.Add("Add...");
			if (m_tilesets.Count > 0)
				this.comboBox.SelectedIndex = 0;
			else
			{
				this.comboBox.Text = "";
				m_currentTileset = null;
				System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(TileSelectControl));
				this.pictureBox.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox.Image")));
				this.pictureBox.Size = new System.Drawing.Size(154, 154);
			}
			this.comboBox.ResumeLayout();
		}

		#region Private Functions
		private Tile[,] getSelectedTiles()
		{
			//Return the currently selected tile
			Tile[,] myTiles;
			if(m_currentTileset != null) {
				
				int tsize = m_tileSize;
				int startX = m_selected.X;
				int startY = m_selected.Y;
				int width = m_selected.Width;
				int height = m_selected.Height;
				myTiles = new Tile[width, height];
				width += startX;
				height += startY;
				
				for(int x=startX; x<width; x++) {
					for(int y=startY; y<height; y++) {
						myTiles[x-startX,y-startY] = new Tile(m_currentTileset, x*tsize, y*tsize, this.checkBox1.Checked);
					}
				}
				
			}
			else {
				myTiles = new Tile[1,1];
				myTiles[0,0] = new Tile();
			}
			
			return myTiles;
		}

		private void addTileSet()
		{
			DialogResult res = openImage.ShowDialog();
			if (res == DialogResult.OK)
			{
				//Create a new tileset and add it to the list
				int index = 0;
				foreach (Tileset ts in m_tilesets)
				{
					if (index <= ts.ID)
						index = ts.ID + 1;
				}

				//Load up the image
				Bitmap img = new Bitmap(openImage.FileName);
				
				//Change the pixel format of the image to 32-bit, for use with XnaMapRenderer
				//Also, it's nice to have all images in the same format
				if (img.PixelFormat != System.Drawing.Imaging.PixelFormat.Format32bppArgb)
				{
					Bitmap tempImg = new Bitmap(img.Width, img.Height, System.Drawing.Imaging.PixelFormat.Format32bppArgb);
					Graphics g = Graphics.FromImage(tempImg);
					g.DrawImage(img, 0, 0, img.Width, img.Height);
					g.Dispose();

					img.Dispose();
					img = tempImg;
				}

				//Clear out the colour that matches our transparency key
				img.MakeTransparent(Color.Magenta);

				//Grab the name of the file (sans extension)
				FileInfo fi = new FileInfo(openImage.FileName);
				string name = fi.Name.Substring(0, fi.Name.Length - fi.Extension.Length);

				//Make the tileset
				Tileset newSet = new Tileset(img, name, index);
				m_tilesets.Add(newSet);

				//Clear - refill the comboboc with the new list
				this.comboBox.SuspendLayout();
				this.comboBox.Items.Clear();

				foreach (Tileset tileset in m_tilesets)
					this.comboBox.Items.Add(tileset);

				this.comboBox.Items.Add("Add...");

				//Set variables
				m_currentTileset = newSet;
				this.comboBox.SelectedItem = m_currentTileset;
				this.pictureBox.Image = m_currentTileset.Image;
				this.pictureBox.Width = m_currentTileset.Image.Width + 1;
				this.pictureBox.Height = m_currentTileset.Image.Height + 1;
				this.comboBox.ResumeLayout();

				//And fire the event
				onTilesetsChange(new EventArgs());
			}
		}
		#endregion

		#region Events
		private void onTilesetsChange(EventArgs e)
		{
			if(OnTilesetsChanged != null)
				OnTilesetsChanged(this, e);
		}

        private void onTileSelectionChanged(TileChangedEventArgs e)
        {
            if (OnTileSelectionChanged != null)
                OnTileSelectionChanged(this, e);
        }
		
		void ComboBoxSelectedIndexChanged(object sender, EventArgs e)
		{
			m_selected = new Rectangle(0,0,0,0);
			
			//If the type is a string, then the user clicked 'Add...'
			if(this.comboBox.SelectedItem.GetType() == typeof(string)) {
				addTileSet();

			} else {
				m_currentTileset = this.comboBox.SelectedItem as Tileset;
				this.pictureBox.Image = m_currentTileset.Image;
				this.pictureBox.Size = m_currentTileset.Image.Size;
			}
		}
	
		void PictureBoxMouseClick(object sender, MouseEventArgs e)
		{
			m_isSelecting = true;
			m_startSelection = new Point(e.X, e.Y);
		}
		
		void PictureBoxPaint(object sender, PaintEventArgs e)
		{
			//Draw grid and highlight
			if(m_currentTileset != null)
			{
				//Draw Grid
				Pen myPen = new Pen(m_gridColour);
				for(int y=0; y<this.pictureBox.Height; y += m_tileSize)
					e.Graphics.DrawLine(myPen, 0, y, this.pictureBox.Width, y);
				for(int x=0; x<this.pictureBox.Width; x += m_tileSize)
					e.Graphics.DrawLine(myPen, x, 0, x, this.pictureBox.Height);
				
				//Draw selection rectangles
				myPen = new Pen(m_overlayColour, 2);
				e.Graphics.DrawRectangle(myPen, m_selected.X * m_tileSize + 1, m_selected.Y * m_tileSize + 1, m_selected.Width * m_tileSize - 1.5f, m_selected.Height * m_tileSize - 1.5f);

				myPen = new Pen(m_overlayColour, 1);
				if(m_isSelecting && m_selectingArea.Width != 0 && m_selectingArea.Height != 0)
				{
					e.Graphics.DrawRectangle(myPen, m_selectingArea);
				}
			}
		}
		
		void Button1Click(object sender, EventArgs e)
		{
			if(this.comboBox.SelectedItem is Tileset)
			{
				Tileset item = this.comboBox.SelectedItem as Tileset;
				
				//Remove tileset from list
				if(DialogResult.Yes == MessageBox.Show("Are you sure you want to remove \"" + item.ToString() +"\"?\n Removing this Tileset may remove tiles from your map.", "Warning", MessageBoxButtons.YesNo, MessageBoxIcon.Warning))
				{
					m_tilesets.Remove(item);
					this.comboBox.SuspendLayout();
					this.comboBox.Items.Clear();
						
					foreach(Tileset tileset in m_tilesets)
						this.comboBox.Items.Add(tileset);
					if(m_tilesets.Count == 0) {
						m_currentTileset = null;
						this.pictureBox.Image = null;
					}
						
					this.comboBox.Items.Add("Add...");
					this.comboBox.SelectedIndex = 0;
					this.comboBox.ResumeLayout();
									
					onTilesetsChange(new EventArgs());
				}
			}
		}
		
		void PictureBoxMouseMove(object sender, MouseEventArgs e)
		{
			if(m_currentTileset != null)
			{
				int mouseX = (int)Math.Floor((float)e.X/m_tileSize);
				int mouseY = (int)Math.Floor((float)e.Y/m_tileSize);
				
				if(m_isSelecting)
				{
					m_endSelection = new Point(e.X, e.Y);
					
					//Make sure our rectangle goes the right way
					// (If the user start to select that drags to the
					// left or upwards, you would get a negative width /
					// height)
					int startX,	startY, width, height;
					if(e.X < m_startSelection.X) {
						startX = e.X;
						width = m_startSelection.X-e.X;
					} else {
						startX = m_startSelection.X;
						width = e.X-m_startSelection.X;
					}
					if(e.Y < m_startSelection.Y) {
						startY = e.Y;
						height = m_startSelection.Y-e.Y;
					} else {
						startY = m_startSelection.Y;
						height = e.Y-m_startSelection.Y;
					}

					//Make sure we don't select anything off the edge of the map
					int overlapX = (startX+width)-this.pictureBox.Width;
					if (overlapX > 0) {
						width -= overlapX;
					}

					int overlapY = (startY + height) - this.pictureBox.Height;
					if (overlapY > 0) {
						height -= overlapY;
					}

					//Set the final selecting area
					m_selectingArea = new Rectangle(startX, startY, width, height);
					
					//Find out what tiles have actually been selected
					int x1 = (int)Math.Floor((double)m_selectingArea.X/m_tileSize);
					int y1 = (int)Math.Floor((double)m_selectingArea.Y/m_tileSize);
					int x2 = (int)Math.Ceiling((double)m_selectingArea.Right/m_tileSize);
					int y2 = (int)Math.Ceiling((double)m_selectingArea.Bottom/m_tileSize);
					m_selected = new Rectangle(x1,y1, x2-x1, y2-y1);
				
					this.pictureBox.Invalidate();
				}

				//Set the status bar text
				this.mousePositionLabel.Text = mouseX.ToString() + "," + mouseY.ToString();
			}
		}
		
		void PictureBoxMouseUp(object sender, MouseEventArgs e)
		{
			//Not selecting anymore, send off the newly selected
			// tiles
			m_isSelecting = false;
            if (m_currentTileset != null)
            {
				//Sort out the case where the user simple clicked
				// rather than dragging across multiple tiles
                if(e.X == m_startSelection.X &&
                    e.Y == m_startSelection.Y )
                {
                    int x1 = (int)Math.Floor((double)e.X/m_tileSize);
					int y1 = (int)Math.Floor((double)e.Y/m_tileSize);
                    m_selected = new Rectangle(x1, y1, 1, 1);
                }

				//Fire off the event to say we have got some new tiles
                Tile[,] tiles = getSelectedTiles();
                TileChangedEventArgs eventArgs = new TileChangedEventArgs(tiles);
                onTileSelectionChanged(eventArgs);

				this.pictureBox.Invalidate();
            }
		}
		#endregion
	}
}
