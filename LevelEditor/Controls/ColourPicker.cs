﻿using System;
using System.ComponentModel;
using System.Drawing;
using System.Windows.Forms;
using MapEditor;

namespace Settings
{
	/// <summary>
	/// I really do have to do everything myself don't I? *sigh*
	/// Anyway, this is a simple colour picker, its so amazing you
	/// can even pick colours with it
	/// </summary>
	public partial class ColourPicker : UserControl
	{

		#region Properties
		public event ColourChangedEventHandler ColourChanged;
		
		[Browsable(true)]
		public override string Text
		{
			get { return this.comboBox1.Text; }
			set { this.comboBox1.Text = value; }
		}
		
		public Color SelectedColor
		{
			get { return (Color)this.comboBox1.SelectedItem; }
			set { this.comboBox1.SelectedItem = value; }
		}
		
		public int SelectedIndex
		{
			get { return this.comboBox1.SelectedIndex; }
			set { this.comboBox1.SelectedIndex = value; }
		}
		#endregion

		public ColourPicker()
		{
			InitializeComponent();
			
			//Set the draw mode so we can do some drawing ourselves
			this.comboBox1.DrawMode = DrawMode.OwnerDrawFixed;
			this.comboBox1.DrawItem += new DrawItemEventHandler(this.drawItem);
			
			//Build a list of available colours and populate the combo box
			string[] colours = System.Enum.GetNames(typeof(KnownColor));
			foreach(string colour in colours) {
				Color newColour = Color.FromName(colour);
				if(!newColour.IsSystemColor && newColour != Color.Transparent)
					this.comboBox1.Items.Add(newColour);
			}
		}

		#region Private Functions
		void colourChanged(ColourChangedEventArgs e)
		{
			if(ColourChanged != null)
				ColourChanged(this, e);
		}

		void drawItem(object sender, DrawItemEventArgs e)
		{
			//Get the colour and use it to set up the brush
			Color itemColour = (Color)this.comboBox1.Items[e.Index];
			Brush myBrush = new SolidBrush(SystemColors.Window);
			Pen myPen = new Pen(Color.Black);
			int X, Y;
			Color textCol = SystemColors.ControlText;
			X = e.Bounds.X;
			Y = e.Bounds.Y;

			//Clear the area
			e.Graphics.FillRectangle(myBrush, e.Bounds);

			//If the item is selected draw the blue 'selected' rectangle
			if((e.State & DrawItemState.Selected) == DrawItemState.Selected)
			{
				myBrush = new SolidBrush(SystemColors.Highlight);
				e.Graphics.FillRectangle(myBrush, e.Bounds);
				textCol = SystemColors.HighlightText;
			}

			//Draw a coloured square before the text
			e.Graphics.DrawRectangle(myPen, X + 1, Y + 1, 17, 12);
			myBrush = new SolidBrush(itemColour);
			e.Graphics.FillRectangle(myBrush, X + 2, Y + 2, 16, 11);

			myBrush = new SolidBrush(textCol);
			e.Graphics.DrawString(itemColour.Name, this.Font, myBrush, X + 20, Y + 2);
			
		}
		
		void ComboBox1SelectedIndexChanged(object sender, EventArgs e)
		{
			colourChanged(new ColourChangedEventArgs((Color)this.comboBox1.SelectedItem));
		}
		#endregion
	}
}
