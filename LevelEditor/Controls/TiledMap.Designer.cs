﻿
namespace MapEditor
{
	partial class TiledMap
	{
		/// <summary>
		/// Designer variable used to keep track of non-visual components.
		/// </summary>
		private System.ComponentModel.IContainer components = null;
		
		/// <summary>
		/// Disposes resources used by the control.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing) {
				if (components != null) {
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}
		
		/// <summary>
		/// This method is required for Windows Forms designer support.
		/// Do not change the method contents inside the source code editor. The Forms designer might
		/// not be able to load this method if it was changed manually.
		/// </summary>
		private void InitializeComponent()
		{
			this.widthUpDown = new System.Windows.Forms.NumericUpDown();
			this.heightUpDown = new System.Windows.Forms.NumericUpDown();
			this.label1 = new System.Windows.Forms.Label();
			this.label2 = new System.Windows.Forms.Label();
			this.setsize_button = new System.Windows.Forms.Button();
			this.statusStrip1 = new System.Windows.Forms.StatusStrip();
			this.tileStatusLabel = new System.Windows.Forms.ToolStripStatusLabel();
			this.tileInfoStatusLabel = new System.Windows.Forms.ToolStripStatusLabel();
			this.openImage = new System.Windows.Forms.OpenFileDialog();
			this.layerSelectBox = new System.Windows.Forms.ComboBox();
			this.label3 = new System.Windows.Forms.Label();
			this.zvalueUpDown = new System.Windows.Forms.NumericUpDown();
			this.label4 = new System.Windows.Forms.Label();
			this.layerVisChkBox = new System.Windows.Forms.CheckBox();
			this.tileSizeUpDown = new System.Windows.Forms.NumericUpDown();
			this.label5 = new System.Windows.Forms.Label();
			this.hScrollBar1 = new System.Windows.Forms.HScrollBar();
			this.vScrollBar1 = new System.Windows.Forms.VScrollBar();
			this.panel1 = new System.Windows.Forms.Panel();
			this.container_panel = new System.Windows.Forms.Panel();
			this.mainGrid_panel = new MapEditor.Controls.MapPanel();
			this.textBox1 = new System.Windows.Forms.TextBox();
			this.label6 = new System.Windows.Forms.Label();
			((System.ComponentModel.ISupportInitialize)(this.widthUpDown)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.heightUpDown)).BeginInit();
			this.statusStrip1.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.zvalueUpDown)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.tileSizeUpDown)).BeginInit();
			this.panel1.SuspendLayout();
			this.container_panel.SuspendLayout();
			this.SuspendLayout();
			// 
			// widthUpDown
			// 
			this.widthUpDown.Location = new System.Drawing.Point(3, 20);
			this.widthUpDown.Maximum = new decimal(new int[] {
            1000,
            0,
            0,
            0});
			this.widthUpDown.Name = "widthUpDown";
			this.widthUpDown.Size = new System.Drawing.Size(75, 20);
			this.widthUpDown.TabIndex = 0;
			this.widthUpDown.ThousandsSeparator = true;
			this.widthUpDown.Value = new decimal(new int[] {
            10,
            0,
            0,
            0});
			// 
			// heightUpDown
			// 
			this.heightUpDown.Location = new System.Drawing.Point(87, 20);
			this.heightUpDown.Maximum = new decimal(new int[] {
            1000,
            0,
            0,
            0});
			this.heightUpDown.Name = "heightUpDown";
			this.heightUpDown.Size = new System.Drawing.Size(75, 20);
			this.heightUpDown.TabIndex = 1;
			this.heightUpDown.ThousandsSeparator = true;
			this.heightUpDown.Value = new decimal(new int[] {
            10,
            0,
            0,
            0});
			// 
			// label1
			// 
			this.label1.Location = new System.Drawing.Point(3, 2);
			this.label1.Name = "label1";
			this.label1.Size = new System.Drawing.Size(53, 15);
			this.label1.TabIndex = 2;
			this.label1.Text = "Width";
			// 
			// label2
			// 
			this.label2.Location = new System.Drawing.Point(84, 2);
			this.label2.Name = "label2";
			this.label2.Size = new System.Drawing.Size(53, 15);
			this.label2.TabIndex = 3;
			this.label2.Text = "Height";
			// 
			// setsize_button
			// 
			this.setsize_button.Location = new System.Drawing.Point(168, 19);
			this.setsize_button.Name = "setsize_button";
			this.setsize_button.Size = new System.Drawing.Size(75, 22);
			this.setsize_button.TabIndex = 4;
			this.setsize_button.Text = "Set";
			this.setsize_button.UseVisualStyleBackColor = true;
			this.setsize_button.Click += new System.EventHandler(this.setsize_buttonClick);
			// 
			// statusStrip1
			// 
			this.statusStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tileStatusLabel,
            this.tileInfoStatusLabel});
			this.statusStrip1.LayoutStyle = System.Windows.Forms.ToolStripLayoutStyle.Flow;
			this.statusStrip1.Location = new System.Drawing.Point(0, 356);
			this.statusStrip1.Name = "statusStrip1";
			this.statusStrip1.RenderMode = System.Windows.Forms.ToolStripRenderMode.ManagerRenderMode;
			this.statusStrip1.Size = new System.Drawing.Size(470, 24);
			this.statusStrip1.SizingGrip = false;
			this.statusStrip1.TabIndex = 5;
			// 
			// tileStatusLabel
			// 
			this.tileStatusLabel.AutoSize = false;
			this.tileStatusLabel.BorderSides = System.Windows.Forms.ToolStripStatusLabelBorderSides.Right;
			this.tileStatusLabel.BorderStyle = System.Windows.Forms.Border3DStyle.Etched;
			this.tileStatusLabel.Name = "tileStatusLabel";
			this.tileStatusLabel.Overflow = System.Windows.Forms.ToolStripItemOverflow.Never;
			this.tileStatusLabel.Size = new System.Drawing.Size(60, 17);
			this.tileStatusLabel.Text = "0,0";
			// 
			// tileInfoStatusLabel
			// 
			this.tileInfoStatusLabel.BorderSides = System.Windows.Forms.ToolStripStatusLabelBorderSides.Right;
			this.tileInfoStatusLabel.BorderStyle = System.Windows.Forms.Border3DStyle.Etched;
			this.tileInfoStatusLabel.Name = "tileInfoStatusLabel";
			this.tileInfoStatusLabel.Size = new System.Drawing.Size(40, 19);
			this.tileInfoStatusLabel.Text = "None";
			// 
			// openImage
			// 
			this.openImage.Filter = "Image Files|*.bmp;*.jpg;*.png;*.gif;*.tif;";
			// 
			// layerSelectBox
			// 
			this.layerSelectBox.FormattingEnabled = true;
			this.layerSelectBox.Items.AddRange(new object[] {
            "Add..."});
			this.layerSelectBox.Location = new System.Drawing.Point(6, 67);
			this.layerSelectBox.Name = "layerSelectBox";
			this.layerSelectBox.Size = new System.Drawing.Size(156, 21);
			this.layerSelectBox.TabIndex = 8;
			this.layerSelectBox.SelectedIndexChanged += new System.EventHandler(this.comboBox_SelectedIndexChanged);
			// 
			// label3
			// 
			this.label3.AutoSize = true;
			this.label3.Location = new System.Drawing.Point(3, 51);
			this.label3.Name = "label3";
			this.label3.Size = new System.Drawing.Size(36, 13);
			this.label3.TabIndex = 9;
			this.label3.Text = "Layer:";
			// 
			// zvalueUpDown
			// 
			this.zvalueUpDown.BackColor = System.Drawing.SystemColors.Window;
			this.zvalueUpDown.DecimalPlaces = 2;
			this.zvalueUpDown.Increment = new decimal(new int[] {
            1,
            0,
            0,
            65536});
			this.zvalueUpDown.Location = new System.Drawing.Point(168, 67);
			this.zvalueUpDown.Maximum = new decimal(new int[] {
            0,
            0,
            0,
            0});
			this.zvalueUpDown.Minimum = new decimal(new int[] {
            10,
            0,
            0,
            -2147483648});
			this.zvalueUpDown.Name = "zvalueUpDown";
			this.zvalueUpDown.Size = new System.Drawing.Size(75, 20);
			this.zvalueUpDown.TabIndex = 10;
			this.zvalueUpDown.ValueChanged += new System.EventHandler(this.zvalueUpDown_ValueChanged);
			// 
			// label4
			// 
			this.label4.AutoSize = true;
			this.label4.Location = new System.Drawing.Point(165, 51);
			this.label4.Name = "label4";
			this.label4.Size = new System.Drawing.Size(50, 13);
			this.label4.TabIndex = 11;
			this.label4.Text = "Z-Value: ";
			// 
			// layerVisChkBox
			// 
			this.layerVisChkBox.AutoSize = true;
			this.layerVisChkBox.Location = new System.Drawing.Point(259, 67);
			this.layerVisChkBox.Name = "layerVisChkBox";
			this.layerVisChkBox.Size = new System.Drawing.Size(56, 17);
			this.layerVisChkBox.TabIndex = 12;
			this.layerVisChkBox.Text = "Visible";
			this.layerVisChkBox.UseVisualStyleBackColor = true;
			this.layerVisChkBox.CheckedChanged += new System.EventHandler(this.layerVisChkBox_CheckedChanged);
			// 
			// tileSizeUpDown
			// 
			this.tileSizeUpDown.Location = new System.Drawing.Point(259, 20);
			this.tileSizeUpDown.Maximum = new decimal(new int[] {
            256,
            0,
            0,
            0});
			this.tileSizeUpDown.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
			this.tileSizeUpDown.Name = "tileSizeUpDown";
			this.tileSizeUpDown.Size = new System.Drawing.Size(56, 20);
			this.tileSizeUpDown.TabIndex = 13;
			this.tileSizeUpDown.Value = new decimal(new int[] {
            32,
            0,
            0,
            0});
			this.tileSizeUpDown.ValueChanged += new System.EventHandler(this.tileSizeUpDown_ValueChanged);
			// 
			// label5
			// 
			this.label5.Location = new System.Drawing.Point(256, 2);
			this.label5.Name = "label5";
			this.label5.Size = new System.Drawing.Size(53, 15);
			this.label5.TabIndex = 3;
			this.label5.Text = "Tilesize";
			// 
			// hScrollBar1
			// 
			this.hScrollBar1.Dock = System.Windows.Forms.DockStyle.Bottom;
			this.hScrollBar1.Enabled = false;
			this.hScrollBar1.LargeChange = 32;
			this.hScrollBar1.Location = new System.Drawing.Point(0, 339);
			this.hScrollBar1.Name = "hScrollBar1";
			this.hScrollBar1.Size = new System.Drawing.Size(453, 17);
			this.hScrollBar1.SmallChange = 4;
			this.hScrollBar1.TabIndex = 14;
			this.hScrollBar1.ValueChanged += new System.EventHandler(this.hScrollBar1_ValueChanged);
			// 
			// vScrollBar1
			// 
			this.vScrollBar1.Dock = System.Windows.Forms.DockStyle.Right;
			this.vScrollBar1.Enabled = false;
			this.vScrollBar1.LargeChange = 32;
			this.vScrollBar1.Location = new System.Drawing.Point(0, 0);
			this.vScrollBar1.Name = "vScrollBar1";
			this.vScrollBar1.Size = new System.Drawing.Size(17, 243);
			this.vScrollBar1.SmallChange = 4;
			this.vScrollBar1.TabIndex = 15;
			this.vScrollBar1.ValueChanged += new System.EventHandler(this.vScrollBar1_ValueChanged);
			// 
			// panel1
			// 
			this.panel1.Controls.Add(this.vScrollBar1);
			this.panel1.Dock = System.Windows.Forms.DockStyle.Right;
			this.panel1.Location = new System.Drawing.Point(453, 96);
			this.panel1.Name = "panel1";
			this.panel1.Padding = new System.Windows.Forms.Padding(0, 0, 0, 17);
			this.panel1.Size = new System.Drawing.Size(17, 260);
			this.panel1.TabIndex = 16;
			// 
			// container_panel
			// 
			this.container_panel.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
			this.container_panel.Controls.Add(this.mainGrid_panel);
			this.container_panel.Dock = System.Windows.Forms.DockStyle.Fill;
			this.container_panel.Location = new System.Drawing.Point(0, 96);
			this.container_panel.Name = "container_panel";
			this.container_panel.Padding = new System.Windows.Forms.Padding(2);
			this.container_panel.Size = new System.Drawing.Size(453, 243);
			this.container_panel.TabIndex = 17;
			// 
			// mainGrid_panel
			// 
			this.mainGrid_panel.Dock = System.Windows.Forms.DockStyle.Fill;
			this.mainGrid_panel.Location = new System.Drawing.Point(2, 2);
			this.mainGrid_panel.Name = "mainGrid_panel";
			this.mainGrid_panel.Size = new System.Drawing.Size(445, 235);
			this.mainGrid_panel.TabIndex = 0;
			this.mainGrid_panel.MouseMove += new System.Windows.Forms.MouseEventHandler(this.mainGrid_panelMouseMove);
			this.mainGrid_panel.MouseDown += new System.Windows.Forms.MouseEventHandler(this.tileClick);
			this.mainGrid_panel.MouseUp += new System.Windows.Forms.MouseEventHandler(this.mainGrid_panelMouseUp);
			this.mainGrid_panel.SizeChanged += new System.EventHandler(this.mainGrid_panel_SizeChanged);
			// 
			// textBox1
			// 
			this.textBox1.Location = new System.Drawing.Point(333, 21);
			this.textBox1.Name = "textBox1";
			this.textBox1.Size = new System.Drawing.Size(134, 20);
			this.textBox1.TabIndex = 18;
			this.textBox1.TextChanged += new System.EventHandler(this.textBox1_TextChanged);
			// 
			// label6
			// 
			this.label6.AutoSize = true;
			this.label6.Location = new System.Drawing.Point(330, 2);
			this.label6.Name = "label6";
			this.label6.Size = new System.Drawing.Size(35, 13);
			this.label6.TabIndex = 19;
			this.label6.Text = "Name";
			// 
			// TiledMap
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.Controls.Add(this.label6);
			this.Controls.Add(this.textBox1);
			this.Controls.Add(this.container_panel);
			this.Controls.Add(this.hScrollBar1);
			this.Controls.Add(this.panel1);
			this.Controls.Add(this.tileSizeUpDown);
			this.Controls.Add(this.layerVisChkBox);
			this.Controls.Add(this.label4);
			this.Controls.Add(this.zvalueUpDown);
			this.Controls.Add(this.label3);
			this.Controls.Add(this.layerSelectBox);
			this.Controls.Add(this.statusStrip1);
			this.Controls.Add(this.setsize_button);
			this.Controls.Add(this.label5);
			this.Controls.Add(this.label2);
			this.Controls.Add(this.label1);
			this.Controls.Add(this.heightUpDown);
			this.Controls.Add(this.widthUpDown);
			this.DoubleBuffered = true;
			this.Name = "TiledMap";
			this.Padding = new System.Windows.Forms.Padding(0, 96, 0, 0);
			this.Size = new System.Drawing.Size(470, 380);
			((System.ComponentModel.ISupportInitialize)(this.widthUpDown)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.heightUpDown)).EndInit();
			this.statusStrip1.ResumeLayout(false);
			this.statusStrip1.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.zvalueUpDown)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.tileSizeUpDown)).EndInit();
			this.panel1.ResumeLayout(false);
			this.container_panel.ResumeLayout(false);
			this.ResumeLayout(false);
			this.PerformLayout();

		}
		private System.Windows.Forms.OpenFileDialog openImage;
		private System.Windows.Forms.ToolStripStatusLabel tileInfoStatusLabel;
		private System.Windows.Forms.ToolStripStatusLabel tileStatusLabel;
		private System.Windows.Forms.StatusStrip statusStrip1;
		private System.Windows.Forms.Button setsize_button;
		private System.Windows.Forms.Label label2;
		private System.Windows.Forms.Label label1;
		private System.Windows.Forms.NumericUpDown heightUpDown;
		private System.Windows.Forms.NumericUpDown widthUpDown;
        private System.Windows.Forms.ComboBox layerSelectBox;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.NumericUpDown zvalueUpDown;
		private System.Windows.Forms.Label label4;
		private System.Windows.Forms.CheckBox layerVisChkBox;
		private System.Windows.Forms.NumericUpDown tileSizeUpDown;
		private System.Windows.Forms.Label label5;
		private System.Windows.Forms.HScrollBar hScrollBar1;
		private MapEditor.Controls.MapPanel mainGrid_panel;
		private System.Windows.Forms.VScrollBar vScrollBar1;
		private System.Windows.Forms.Panel panel1;
		private System.Windows.Forms.Panel container_panel;
		private System.Windows.Forms.TextBox textBox1;
		private System.Windows.Forms.Label label6;
	}
}
