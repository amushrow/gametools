﻿using System;

namespace MapEditorLib
{
	/// <summary>
	/// Represents one layer of a map. MapLayer holds the actual
	/// array of tiles
	/// </summary>
	public class MapLayer : IComparable<MapLayer>
	{
		#region Variables
		private Tile[,] m_tiles;
		private string m_name;
		private int m_id;
		private bool m_bg;
		private float m_zvalue;
		private bool m_visible;
		#endregion

		#region Properties
		public string Name
		{
			get { return m_name; }
		}

		public int ID
		{
			get { return m_id; }
		}

		public bool IsBackground
		{
			get { return m_bg; }
		}

		public float Z_Value
		{
			set { m_zvalue = value; }
			get { return m_zvalue; }
		}

		public bool Visible
		{
			set { m_visible = value; }
			get { return m_visible; }
		}

		public Tile[,] Tiles
		{
			get { return m_tiles; }
		}
		#endregion

		public MapLayer(string Name, int ID, float ZValue, bool Background)
		{
			m_name = Name;
			m_id = ID;
			m_bg = Background;
			m_zvalue = ZValue;
			m_visible = true;
			m_tiles = new Tile[0, 0];
		}

		#region Public Functions
		public override string ToString()
		{
			return m_name;
		}

		public int CompareTo(MapLayer other)
		{
			int retval = 0;
			if (this.m_zvalue > other.m_zvalue)
			{
				retval = 1;
			}
			else if (this.m_zvalue < other.m_zvalue)
			{
				retval = -1;
			}

			return retval;
		}

		public void Resize(int width, int height)
		{
			//Set the new size of the map
			Tile[,] temp = new Tile[width, height];

			for (int x = 0; x < width; x++)
			{
				for (int y = 0; y < height; y++)
				{
					if (x < m_tiles.GetLength(0) && y < m_tiles.GetLength(1))
						temp[x, y] = m_tiles[x, y];
				}
			}

			m_tiles = temp;
		}

		public bool TileIsAlive(int X, int Y)
		{
			if(m_tiles[X,Y] != null && m_tiles[X,Y].IsInitialized)
				return true;
			return false;
		}
		#endregion
	}
}