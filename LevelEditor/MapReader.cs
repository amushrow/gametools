﻿using System;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Collections.Generic;


namespace MapEditorLib
{
	/// <summary>
	/// You can use this class in your game/whatever to read the file and get
	/// the set of tiles, and the the tilesets, but you will also need to include
	/// the 'Tile' and 'Tileset' classes
	/// </summary>
	/*public class MapReader
	{
		private string m_path;
		private MemoryStream m_stream;
		private List<Tileset> m_tilesetList;
		private Tile[,] m_tiles;
		private int m_tileSize;
		private float m_version;
		private long m_start;
		
		public MapReader(string Path)
		{
			m_tileSize = 0;
			m_version = 0;
			m_path = Path;
			FileStream fs = File.Open(Path, FileMode.Open);
			m_stream = new MemoryStream();
			
			//Copy the file to a memory stream so that we don't keep hold of
			// the file itself
			byte[] buffer = new byte[1024];
			int read = 0;
			while(fs.Position < fs.Length) {
				read = fs.Read(buffer, 0, 1024);
				m_stream.Write(buffer, 0, read);
			}
			
			fs.Close();
			
			//Get filetype string
			m_stream.Position = 0;
			m_stream.Read(buffer, 0, 13);
			string text = System.Text.ASCIIEncoding.ASCII.GetString(buffer, 0, 13);
			if(text != "MapEditorFile")
				throw new InvalidFileTypeException("You totaly didn't try to open a proper MapEditor file");
			
			m_stream.Read(buffer, 0, 4);
			m_version = BitConverter.ToSingle(buffer, 0);
			
			m_start = m_stream.Position;			
			m_stream.Position = 0;
		}
		
		public Tile[,] ReadTiles()
		{
			if(m_tiles != null)
				return m_tiles;
			
			//We need to read the tilesets first so that we can set them to
			// the tiles
			if(m_tilesetList == null)
				ReadTilesets();
			
			m_stream.Position = m_start;
			
			byte[] buffer = new byte[1024];
			
			//Skip ahead to the Tiles section of the file
			byte[] size = new byte[8];
			m_stream.Read(size, 0, 8);
			long length = BitConverter.ToInt64(size, 0);
			
			m_stream.Position += length;
			
			//Get length of data block
			m_stream.Read(size, 0, 8);
			length = BitConverter.ToInt64(size, 0);
			
			//Get the width and height of the map
			int Width, Height;
			m_stream.Read(size, 0, 4);
			Width = BitConverter.ToInt32(size, 0);
			m_stream.Read(size, 0, 4);
			Height = BitConverter.ToInt32(size, 0);
			
			Tile[,] tiles = new Tile[Width, Height];
			int xPos, yPos;
			bool solid;
			int index;
			
			//Read each of the tiles from left-right top-bottom
			for(int x=0; x<Width; x++) {
				for(int y=0; y<Height; y++) {
					m_stream.Read(size, 0, 4);
					xPos = BitConverter.ToInt32(size, 0);		
					
					m_stream.Read(size, 0, 4);
					yPos = BitConverter.ToInt32(size, 0);
					
					m_stream.Read(size, 0, 1);
					solid = BitConverter.ToBoolean(size, 0);
					
					m_stream.Read(size, 0, 4);
					index = BitConverter.ToInt32(size, 0);
					
					if(index >= 0)
						tiles[x,y] = new Tile(m_tilesetList[index], xPos, yPos, solid);
					else {
						tiles[x,y] = new Tile();
						tiles[x,y].IsSolid = solid;
					}
				}
			}
			
			m_tiles = tiles;
			return tiles;
		}
			
		public List<Tileset> ReadTilesets()
		{
			if(m_tilesetList != null)
				return m_tilesetList;
			
			m_stream.Position = m_start;
			
			byte[] buffer = new byte[1024];
			MemoryStream s_tilesets = new MemoryStream();
			
			//Get the length of this block
			byte[] size = new byte[8];
			m_stream.Read(size, 0, 8);
			long length = BitConverter.ToInt64(size, 0);
			long endOfBlock = m_stream.Position+length;
			
			List<Tileset> list = new List<Tileset>();

			//Keep going until we reach the end of the block
			while(m_stream.Position < endOfBlock) {
				//Tileset data = readTileSet(m_stream);
				//list.Add(data);
			}
			
			m_tilesetList = list;
			return list;
		}
		
		public int GetTileSize()
		{
			//Return the tilesize of the Tilesets
			if(m_tileSize == 0)
				ReadTilesets();
			return m_tileSize;
		}
		
		//This is in a separate method because we also need to
		// read the bitmap file, and it would have made the main
		// method a bit messy
		private Tileset readTileSet(Stream stream)
		{
			byte[] size = new byte[8];
				
			//Get the tile size
			stream.Read(size, 0, 4);
			int TileSize = BitConverter.ToInt32(size, 0);
			m_tileSize = TileSize;
			
			//get the length of the string
			stream.Read(size, 0, 4);
			int textLength = BitConverter.ToInt32(size, 0);
			
			//Get the string
			byte[] buffer = new byte[1024];
			stream.Read(buffer, 0, textLength);
			string Name = System.Text.ASCIIEncoding.ASCII.GetString(buffer, 0, textLength);
			
			//Get the length of the image		
			stream.Read(size, 0, 8);
			long length = BitConverter.ToInt64(size, 0);
			
			//Copy the image to a separate stream
			MemoryStream image = new MemoryStream();
			int read = 0;
			while(image.Position < length) {
				int count = 1024;
				if(image.Position+1024 > length)
					count = (int)(length-image.Position);
				
				read = stream.Read(buffer, 0, count);
				image.Write(buffer, 0, read);
			}
			
			image.Position = 0;
			Image img = Image.FromStream(image);
			
			//Finally make the tileset
			Tileset data = new Tileset(img, Name);
			return data;
		}
			
		public void Close()
		{
			m_stream.Close();
		}
	}*/
}

