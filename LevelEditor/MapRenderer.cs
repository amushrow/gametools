﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Windows.Forms;

namespace MapEditor
{
	interface IMapRenderer
	{
		void SetClearColour(byte R, byte G, byte B);
		void SetGridColour(byte R, byte G, byte B);
		void SetOverlayColour(byte R, byte G, byte B);
		void SetGridVisible(bool value);
		void SetSolidsVisible(bool value);
		void SetOffset(int x, int y);
		void ResetGraphicsDevice(int BufferWidth, int BufferHeight);
		void RenderMap(PaintEventArgs e);
	}
}
