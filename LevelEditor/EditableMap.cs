﻿using System;
using System.Collections.Generic;
using System.Text;
using System.IO;
using System.Drawing;
using GameDataLib;

namespace MapEditorLib
{
	/// <summary>
	/// Inherits from Map and adds options to change tiles
	/// on the map as well as query values within the map
	/// </summary>
	public class EditableMap : Map
	{
		private MapLayer m_workingLayer;
		private Tile[,] m_currentTiles;
		private CustomObject m_currentObject;
		private bool m_useObjectLayer;

		#region CustomEvents
		public delegate void MapChangedEventHandler(object sender, MapChangedEventArgs e);
		public class MapChangedEventArgs : EventArgs
		{
			private Rectangle m_area;

			public Rectangle Area
			{
				get { return m_area; }
			}

			public MapChangedEventArgs(Rectangle Area)
			{
				m_area = Area;
			}
		}
		public event MapChangedEventHandler OnMapChanged;
		#endregion

		public EditableMap(int NumLayers, int Width, int Height, int TileSize)
		{
			initialize(NumLayers, Width, Height, TileSize);
		}

		public EditableMap()
		{
			initialize(4, 10, 10, 32);
		}

		#region Properties
		public MapLayer WorkingLayer
		{
			get { return m_workingLayer; }
			
			set
			{
				if(m_mapLayers.Contains(value))
				{
					m_workingLayer = value;
				}
				else
				{
					throw new System.ArgumentException();
				}
			}
		}

		public bool UseObjectLayer
		{
			get { return m_useObjectLayer; }
			set { m_useObjectLayer = value;  }
		}

		public override int TileSize
		{
			get { return m_tileSize; }
			set
			{
				m_tileSize = value;
				mapUpdated(0, 0, RealWidth, RealHeight);
			}
		}
		#endregion

		#region Public Functions
		public void SetGameObjects(CustomObject obj)
		{
			foreach(GameObject thing in m_gameObjects.List)
			{
				if(thing.ID == obj.ID)
				{
					thing.Obj = obj;
				}
			}
		}

		public void SetSize(int Width, int Height)
		{
			m_width = Width;
			m_height = Height;

			foreach (MapLayer layer in m_mapLayers)
            {
                layer.Resize(m_width, m_height);
            }

			mapUpdated(0, 0, RealWidth, RealHeight);
		}

		public void SetSelectedTiles(Tile[,] Tiles)
		{
			m_currentTiles = Tiles;
		}

		public void SetSelectedObject(CustomObject obj)
		{
			m_currentObject = obj;
		}

		public void SetTiles(int PosX, int PosY)
		{
			if (m_useObjectLayer)
			{
				if (m_currentObject == null)
					return;

				GameObject newObj = null;
				foreach (GameObject obj in m_gameObjects.List)
				{
					if (obj.X == PosX && obj.Y == PosY)
					{
						newObj = obj;
					}
				}

				if(newObj == null)
				{
					newObj = new GameObject();
					newObj.X = PosX;
					newObj.Y = PosY;
					newObj.ID = m_currentObject.ID;
					newObj.Obj = m_currentObject;
					m_gameObjects.List.Add(newObj);
				}
				else
				{
					newObj.ID = m_currentObject.ID;
					newObj.Obj = m_currentObject;
				}		

				mapUpdated(PosX * m_tileSize, PosY * m_tileSize, m_tileSize, m_tileSize);
			}
			else
			{
				//Get the width and height of the block of
				// tiles we are placing
				int width = m_currentTiles.GetLength(0);
				int height = m_currentTiles.GetLength(1);

				//Place the selected tiles onto our current
				// layer
				for (int x = 0; x < width; x++)
				{
					for (int y = 0; y < height; y++)
					{
						int realX = PosX + x;
						int realY = PosY + y;

						if (realX < m_width && realY < m_height)
						{
							m_workingLayer.Tiles[realX, realY] = m_currentTiles[x, y].Clone();
							m_workingLayer.Tiles[realX, realY].XPos = realX;
							m_workingLayer.Tiles[realX, realY].YPos = realY;
						}
					}
				}

				mapUpdated(PosX * m_tileSize, PosY * m_tileSize, width * m_tileSize, height * m_tileSize);
			}
		}

		public bool TileIsSolid(int X, int Y)
		{
			if (m_workingLayer.Tiles[X, Y] != null && m_workingLayer.Tiles[X, Y].IsSolid)
				return true;
			return false;
		}

		public void ToggleSolid(int X, int Y)
		{
			if(m_workingLayer.Tiles[X,Y] != null)
				m_workingLayer.Tiles[X, Y].IsSolid = !m_workingLayer.Tiles[X, Y].IsSolid;

			mapUpdated(X * m_tileSize, Y * m_tileSize, m_tileSize, m_tileSize);
		}

		public void ClearTile(int X, int Y)
		{
			if (UseObjectLayer)
			{
				GameObject found = null;
				foreach(GameObject obj in m_gameObjects.List)
				{
					if(obj.X == X && obj.Y == Y)
					{
						found = obj;
					}
				}

				if(found != null)
				{
					m_gameObjects.List.Remove(found);
				}
			}
			else
			{
				m_workingLayer.Tiles[X, Y] = null;
			}

			mapUpdated(X * m_tileSize, Y * m_tileSize, m_tileSize, m_tileSize);
		}

		public string GetTileName(int X, int Y)
		{
			if(UseObjectLayer)
			{
				foreach(GameObject obj in m_gameObjects.List)
				{
					if(obj.X == X && obj.Y == Y)
					{
						string data = obj.ID.ToString();
						if(obj.Obj != null)
						{
							data += " - " + obj.Obj.Name;
						}
						return data;
					}
				}

				return "None";
			}
			else if (m_workingLayer.Tiles[X, Y] != null)
				return m_workingLayer.Tiles[X, Y].Tileset.ToString();
			else
				return "None";
		}

		public bool LayerNameInUse(string Name)
		{
			foreach(MapLayer layer in m_mapLayers)
			{
				if (layer.Name == Name)
					return true;
			}

			return false;
		}

		public bool LayerZInUse(float Z_Value)
		{
			foreach (MapLayer layer in m_mapLayers)
			{
				if (layer.Z_Value == Z_Value)
					return true;
			}

			return false;
		}

		public void AddLayer(string Name, float Z_Value, bool Background)
		{
			int nextId = 0;
			foreach (MapLayer layer in m_mapLayers)
			{
				if (layer.ID <= nextId)
					nextId = layer.ID + 1;
			}
			MapLayer newLayer = new MapLayer(Name, nextId, Z_Value, Background);
			newLayer.Resize(m_width, m_height);
			m_mapLayers.Add(newLayer);
			m_mapLayers.Sort();
			
			//Set the working layer to the newly created one
			m_workingLayer = newLayer;
		}

		public void SetLayerVisible(bool Visible)
		{
			if (!UseObjectLayer)
				m_workingLayer.Visible = Visible;
			else
				m_gameObjects.Visible = Visible;

			mapUpdated(0, 0, RealWidth, RealHeight);
		}

		public void SetLayerZValue(float Z_Value)
		{
			m_workingLayer.Z_Value = Z_Value;
			m_mapLayers.Sort();
			mapUpdated(0, 0, RealWidth, RealHeight);
		}

		public void Save(string Path)
		{
			FileStream fs = File.Open(Path, FileMode.Create);
			Save(fs);
			fs.Close();
		}

		public void Save(Stream outputData)
		{
			writeFileHeader(outputData);
			writeString(Name, outputData);
			writeInt(m_width, outputData);
			writeInt(m_height, outputData);
			writeInt(m_tileSize, outputData);

			writeInt(m_tileSets.Count, outputData);
			foreach(Tileset ts in m_tileSets)
			{
				//if(!ts.IsDisposed)
				//{
					writeBool(true, outputData);
					writeInt(ts.ID, outputData);
					writeString(ts.Name, outputData);
				//}
				//else
				//{
				//	writeBool(false, outputData);
				//}
			}

			writeInt(m_mapLayers.Count, outputData);
			foreach(MapLayer layer in m_mapLayers)
			{
				writeInt(layer.ID, outputData);
				writeFloat(layer.Z_Value, outputData);
				writeBool(layer.IsBackground, outputData);
				writeBool(layer.Visible, outputData);
				writeString(layer.Name, outputData);

				//Write out all of the tile data
				for(int x=0; x<m_width; x++) {
					for(int y=0; y<m_height; y++) {
						if (layer.Tiles[x, y] == null || !layer.Tiles[x,y].IsInitialized)
						{
							writeBool(false, outputData);
						}
						else
						{
							writeBool(true, outputData);
							writeInt(layer.Tiles[x, y].SourceX, outputData);
							writeInt(layer.Tiles[x, y].SourceY, outputData);
							writeInt(layer.Tiles[x, y].XPos, outputData);
							writeInt(layer.Tiles[x, y].YPos, outputData);
							writeInt(layer.Tiles[x, y].Tileset.ID, outputData);
							writeBool(layer.Tiles[x, y].IsSolid, outputData);
						}
					}
				}
			}

			writeInt(m_gameObjects.List.Count, outputData);
			foreach(GameObject obj in m_gameObjects.List)
			{
				writeLong(obj.ID, outputData);
				writeInt(obj.X, outputData);
				writeInt(obj.Y, outputData);
			}

			writeInt(0xFF, outputData);
		}

		public void SaveTextures(string  outputFolder)
		{
			string destinationFolder = outputFolder + "\\MapData\\";
			Directory.CreateDirectory(destinationFolder);
			foreach (Tileset ts in m_tileSets)
			{
				ts.Image.Save(destinationFolder + ts.Name, System.Drawing.Imaging.ImageFormat.Png);
			}
		}

		public override void Load(Stream dataStream, RequestTexture requestTexture)
		{
			base.Load(dataStream, requestTexture);
			m_workingLayer = m_mapLayers[0];
		}

		public void Reset()
		{
			initialize(4, 14, 10, 32);
		}

		public void Optimize()
		{
			List<Tile> usedTiles = new List<Tile>();

			//Find out which tiles are being used
			foreach(Tileset ts in m_tileSets)
			{
				foreach(MapLayer layer in m_mapLayers)
				{
					//Nested loops, you love it.
					for (int x = 0; x < m_width; x++) {
						for (int y = 0; y < m_height; y++) {
							if(layer.TileIsAlive(x,y)  && layer.Tiles[x, y].Tileset == ts)
							{
								if(!usedTiles.Contains(layer.Tiles[x,y]))
								{
									usedTiles.Add(new Tile(layer.Tiles[x, y].Tileset, layer.Tiles[x,y].SourceX, layer.Tiles[x,y].SourceY,  layer.Tiles[x,y].IsSolid));
								}
							}
						}
					}
				}
			}

			const int _64 = 64*64;
			const int _128 = 128*128;
			const int _256 = 256*256;
			int blockArea = 0;
			int texSize = 0;
			int lengthPerTex = 0;
			int tilesPerTex = 0;

			List<Tileset> tempTilesets = new List<Tileset>();
			Bitmap newTex = null;
			Graphics g = null;
			Tileset newTileset = null;
			
			int blockX = 0;
			int blockY = 0;
			int texNum = 0;

			bool firstRun = true;
			for(int i=0; i<usedTiles.Count; i++)
			{
				if (firstRun)
				{
					blockArea = (m_tileSize * m_tileSize) * (usedTiles.Count-i);
					if (blockArea <= _64)
						texSize = 64;
					else if (blockArea <= _128)
						texSize = 128;
					else if (blockArea <= _256)
						texSize = 256;
					else
						texSize = 512;

					lengthPerTex = texSize / m_tileSize;
					tilesPerTex = lengthPerTex * lengthPerTex;

					if(newTex != null)
						newTex.Dispose();

					newTex = new Bitmap(texSize, texSize);
					newTileset = new Tileset(newTex, Name + "_tex" + texNum.ToString(), texNum);
					g = Graphics.FromImage(newTileset.Image);
					g.Clear(Color.Transparent);
					tempTilesets.Add(newTileset);

					firstRun = false;
				}
				
				Rectangle destRect = new Rectangle(blockX * m_tileSize, blockY * m_tileSize, m_tileSize, m_tileSize);
				Rectangle srcRect = new Rectangle(usedTiles[i].SourceX, usedTiles[i].SourceY, m_tileSize, m_tileSize);
				g.DrawImage(usedTiles[i].Tileset.Image, destRect, srcRect, GraphicsUnit.Pixel);

				foreach (MapLayer layer in m_mapLayers)
				{
					//Nested loops, you love it.
					for (int x = 0; x < m_width; x++)
					{
						for (int y = 0; y < m_height; y++)
						{
							if (layer.TileIsAlive(x, y))
							{
								if(layer.Tiles[x,y].Equals(usedTiles[i]))
								{
									layer.Tiles[x, y].SourceX = destRect.Left;
									layer.Tiles[x, y].SourceY = destRect.Top;
									layer.Tiles[x, y].Tileset = newTileset;
								}
							}
						}
					}
				}
				
				usedTiles[i].SourceY = -1;
				usedTiles[i].SourceX = -1;

				blockX++;
				if(blockX >= lengthPerTex)
				{
					blockY++;
					blockX = 0;
				}
				if(blockY >= lengthPerTex)
				{
					blockY = 0;
					blockX = 0;

					newTileset.Image.MakeTransparent(Color.Magenta);
					g.Dispose();
					
					texNum++;
					firstRun = true;
				}
			}

			newTileset.Image.MakeTransparent(Color.Magenta);
			g.Dispose();
			newTex.Dispose();

			foreach (Tileset ts in m_tileSets)
				ts.Dispose();

			m_tileSets.Clear();
			m_tileSets = tempTilesets;
		}
		#endregion

		#region Private Functions
		private void initialize(int NumLayers, int Width, int Height, int TileSize)
		{
			m_currentTiles = new Tile[0, 0];
			m_tileSets = new List<Tileset>(4);
			m_mapLayers = new List<MapLayer>(NumLayers);
			MapLayer defaultLayer = new MapLayer("Default", 0, -5, false);
            m_mapLayers.Add(defaultLayer);
			m_gameObjects = new GameObjectList();
			m_workingLayer = defaultLayer;
			m_width = Width;
			m_height = Height;
			m_tileSize = TileSize;

			foreach (MapLayer layer in m_mapLayers)
            {
                layer.Resize(m_width, m_height);
            }
		}

		private void writeFileHeader(Stream outputData)
		{
			byte[] version = BitConverter.GetBytes(LibData.FILEVERSION_NUM);

			outputData.Write(LibData.ID, 0, LibData.ID.Length);
			outputData.Write(version, 0, version.Length);
		}

		private void writeInt(int value, Stream outputData)
		{
			byte[] buffer = BitConverter.GetBytes(value);
			outputData.Write(buffer, 0, buffer.Length);
		}

		private void writeLong(long value, Stream outputData)
		{
			byte[] buffer = BitConverter.GetBytes(value);
			outputData.Write(buffer, 0, buffer.Length);
		}

		private void writeFloat(float value, Stream outputData)
		{
			byte[] buffer = BitConverter.GetBytes(value);
			outputData.Write(buffer, 0, buffer.Length);
		}

		private void writeBool(bool value, Stream outputData)
		{
			byte[] buffer = BitConverter.GetBytes(value);
			outputData.Write(buffer, 0, buffer.Length);
		}

		private void writeString(string value, Stream outputData)
		{
			byte[] buffer = System.Text.ASCIIEncoding.ASCII.GetBytes(value);
			writeInt(buffer.Length, outputData);
			outputData.Write(buffer, 0, buffer.Length);
		}

		private void mapUpdated(int x, int y, int width, int height)
		{
			if(OnMapChanged != null)
			{
				MapChangedEventArgs e = new MapChangedEventArgs(new Rectangle(x,y,width,height));
				OnMapChanged(this, e);
			}
		}
		#endregion
	}
}
