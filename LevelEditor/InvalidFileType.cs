﻿using System;

namespace MapEditorLib
{
	/// <summary>
	/// This is to be thrown when the user tries to load
	/// a file that isn't a map editor file, rather than throwing
	/// a bog-standard exception that could be anything
	/// </summary>
	public class InvalidFileTypeException : Exception
	{
		public InvalidFileTypeException(string message) : base(message)
		{
		}
	}
}
