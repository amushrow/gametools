﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Windows.Forms;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using MapEditorLib;

namespace MapEditor
{
	/// <summary>
	/// A renderer for the EditableMap that uses DirectX
	/// through XNA, rendering is hardware accelerated so
	/// performance is greatly improved and flickering is
	/// obliterated
	/// </summary>
	class XnaMapRenderer : IMapRenderer
	{
		#region Variables
		private EditableMap m_Map;
		private GraphicsDevice m_device;
		private Matrix m_projectionMatrix;
		private VertexPositionColor[] m_gridVertices;
		private VertexPositionColor[] m_circleVertices;
		private int[] m_circleIndices;
		private int[] m_gridIndices;
		BasicEffect m_basicEffect;
		VertexDeclaration colouredLines;
		VertexDeclaration texturedQuads;

		private int x_offset, y_offset;
		private int m_bufferWidth, m_bufferHeight;
		
		private Color m_clearColour;
		private bool m_showGrid;
		private bool m_showSolids;
		private Color m_gridColour;
		private Color m_overlayColour;
		#endregion

		public XnaMapRenderer(EditableMap Map, IntPtr window_handle, int BufferWidth, int BufferHeight)
		{
			m_Map = Map;
			m_bufferWidth = BufferWidth;
			m_bufferHeight = BufferHeight;
			initialize_directx(window_handle);
		}

		#region Public Function
		public void RenderMap(PaintEventArgs e)
		{
			m_basicEffect.Projection = m_projectionMatrix;
			m_basicEffect.World = Matrix.Identity;
			m_device.Clear(m_clearColour);

			m_basicEffect.View = Matrix.CreateTranslation(-x_offset, -y_offset, 0);

			renderTiles();


			if (m_Map.GameObjectsLayer.Visible)
			{
				foreach (GameObject obj in m_Map.GameObjectsLayer.List)
				{
					m_device.VertexDeclaration = colouredLines;
					m_basicEffect.TextureEnabled = false;
					m_basicEffect.VertexColorEnabled = false;
					m_basicEffect.AmbientLightColor = new Vector3(0, 0, 0);
					m_basicEffect.Alpha = 0.75f;
					m_basicEffect.World = Matrix.CreateTranslation((obj.X * m_Map.TileSize) + (m_Map.TileSize / 2.0f), (obj.Y * m_Map.TileSize) + (m_Map.TileSize / 2.0f), 0);
					m_basicEffect.Begin();
					m_basicEffect.CurrentTechnique.Passes[0].Begin();
					m_device.DrawUserIndexedPrimitives<VertexPositionColor>(
								PrimitiveType.TriangleFan, m_circleVertices, 0, m_circleVertices.Length,
								m_circleIndices, 0, m_circleIndices.Length - 2);
					m_basicEffect.CurrentTechnique.Passes[0].End();
					m_basicEffect.End();
				}
			}
			
			//Draw the grid
			if (m_showGrid)
			{
				m_device.VertexDeclaration = colouredLines;
				m_basicEffect.TextureEnabled = false;
				m_basicEffect.VertexColorEnabled = true;
				m_basicEffect.AmbientLightColor = new Vector3(1, 1, 1);
				m_basicEffect.Alpha = 1;
				m_basicEffect.World = Matrix.Identity;
				m_basicEffect.Begin();
				m_basicEffect.CurrentTechnique.Passes[0].Begin();
				m_device.DrawUserIndexedPrimitives<VertexPositionColor>(
					PrimitiveType.LineList, m_gridVertices, 0, m_gridVertices.Length,
					m_gridIndices, 0, m_gridIndices.Length / 2);
				m_basicEffect.CurrentTechnique.Passes[0].End();
				m_basicEffect.End();
			}


			//Draw any text / marks
			//Brush myBrush = new SolidBrush(Color.FromArgb(128, m_overlayColour));
			//Pen myPen = new Pen(Color.FromArgb(128, m_overlayColour));
			if (m_showSolids)
			{
				m_device.VertexDeclaration = colouredLines;
				m_basicEffect.TextureEnabled = false;
				m_basicEffect.VertexColorEnabled = true;
				m_basicEffect.AmbientLightColor = new Vector3(1, 1, 1);
				m_basicEffect.Alpha = 1;

				for (int x = 0; x < m_Map.Width; x++)
				{
					for (int y = 0; y < m_Map.Height; y++)
					{
						if (m_showSolids && m_Map.TileIsSolid(x, y))
						{
							m_basicEffect.World = Matrix.CreateTranslation((x * m_Map.TileSize) + (m_Map.TileSize / 2.0f), (y * m_Map.TileSize) + (m_Map.TileSize / 2.0f), 0);
							m_basicEffect.Begin();
							m_basicEffect.CurrentTechnique.Passes[0].Begin();
							m_device.DrawUserIndexedPrimitives<VertexPositionColor>(
										PrimitiveType.TriangleFan, m_circleVertices, 0, m_circleVertices.Length,
										m_circleIndices, 0, m_circleIndices.Length - 2);
							m_basicEffect.CurrentTechnique.Passes[0].End();
							m_basicEffect.End();
						}
					}
				}
			}

			m_device.Present();
		}

		public void ResetGraphicsDevice(int BufferWidth, int BufferHeight)
		{
			if (m_device == null || m_Map.RealWidth == 0 || m_Map.RealHeight == 0)
				return;

			if (m_device.PresentationParameters.BackBufferHeight != BufferHeight ||
				m_device.PresentationParameters.BackBufferWidth != BufferWidth)
			{
				m_device.PresentationParameters.BackBufferHeight = BufferHeight;
				m_device.PresentationParameters.BackBufferWidth = BufferWidth;
				m_bufferWidth = BufferWidth;
				m_bufferHeight = BufferHeight;
				m_device.Reset(m_device.PresentationParameters);
			}

			setupRenderingStuff();
		}

		public void SetClearColour(byte R, byte G, byte B)
		{
			m_clearColour.A = 0xFF;
			m_clearColour.R = R;
			m_clearColour.G = G;
			m_clearColour.B = B;
		}

		public void SetGridColour(byte R, byte G, byte B)
		{
			m_gridColour.A = 0xFF;
			m_gridColour.R = R;
			m_gridColour.G = G;
			m_gridColour.B = B;
		}

		public void SetOverlayColour(byte R, byte G, byte B)
		{
			m_overlayColour.A = 0x7F;
			m_overlayColour.R = R;
			m_overlayColour.G = G;
			m_overlayColour.B = B;
		}

		public void SetOffset(int x, int y)
		{
			x_offset = x;
			y_offset = y;
		}

		public void SetGridVisible(bool value)
		{
			m_showGrid = value;
		}

		public void SetSolidsVisible(bool value)
		{
			m_showSolids = value;
		}
		#endregion

		#region Private Functions
		private void initialize_directx(IntPtr window_handle)
		{
			PresentationParameters pp = new PresentationParameters();
			pp.BackBufferCount = 1;
			pp.IsFullScreen = false;
			pp.SwapEffect = SwapEffect.Copy;
			pp.BackBufferWidth = m_Map.RealWidth;
			pp.BackBufferHeight = m_Map.RealHeight;
			//pp.AutoDepthStencilFormat = DepthFormat.Depth24Stencil8;
			//pp.EnableAutoDepthStencil = true;
			pp.PresentationInterval = PresentInterval.One;
			pp.BackBufferFormat = SurfaceFormat.Color;
			pp.MultiSampleType = MultiSampleType.None;

			m_device = new GraphicsDevice(GraphicsAdapter.DefaultAdapter, DeviceType.Hardware, window_handle, pp);

			m_basicEffect = new BasicEffect(m_device, null);
			m_basicEffect.VertexColorEnabled = true;
			m_basicEffect.TextureEnabled = true;
			m_basicEffect.EnableDefaultLighting();
			m_basicEffect.AmbientLightColor = new Vector3(1, 1, 1);

			colouredLines = new VertexDeclaration(
								m_device, VertexPositionColor.VertexElements);
			texturedQuads = new VertexDeclaration(
								m_device, VertexPositionTexture.VertexElements);

			x_offset = y_offset = 0;
			m_clearColour = Color.Black;
			setupRenderingStuff();
		}

		private void setupRenderingStuff()
		{
			m_device.RenderState.AlphaBlendEnable = true;
			m_device.RenderState.SourceBlend = Blend.SourceAlpha;
			m_device.RenderState.DestinationBlend = Blend.InverseSourceAlpha;
			m_device.RenderState.DepthBufferEnable = false;
			m_projectionMatrix = Matrix.CreateOrthographicOffCenter(0, m_bufferWidth, m_bufferHeight, 0, -1, 10);

			List<VertexPositionColor> points = new List<VertexPositionColor>();

			//Add vertical lines
			for (int x = 0; x < m_Map.Width - 1; x++)
			{
				points.Add(new VertexPositionColor(new Vector3((x + 1) * m_Map.TileSize, 0, 1), m_gridColour));
				points.Add(new VertexPositionColor(new Vector3((x + 1) * m_Map.TileSize, m_Map.RealHeight, 1), m_gridColour));
			}

			//Add horizontal lines
			for (int y = 0; y < m_Map.Height - 1; y++)
			{
				points.Add(new VertexPositionColor(new Vector3(0, (y + 1) * m_Map.TileSize, 1), m_gridColour));
				points.Add(new VertexPositionColor(new Vector3(m_Map.RealWidth, (y + 1) * m_Map.TileSize, 1), m_gridColour));
			}

			//Add lines for the border. 4 sides; 2 lines on each; 16 points
			points.Add(new VertexPositionColor(new Vector3(0, 0, 1), m_gridColour));
			points.Add(new VertexPositionColor(new Vector3(m_Map.RealWidth, 0, 1), m_gridColour));
			points.Add(new VertexPositionColor(new Vector3(0, 1, 1), m_gridColour));
			points.Add(new VertexPositionColor(new Vector3(m_Map.RealWidth, 1, 1), m_gridColour));
			points.Add(new VertexPositionColor(new Vector3(0, m_Map.RealHeight - 1, 1), m_gridColour));
			points.Add(new VertexPositionColor(new Vector3(m_Map.RealWidth, m_Map.RealHeight - 1, 1), m_gridColour));
			points.Add(new VertexPositionColor(new Vector3(0, m_Map.RealHeight - 2, 1), m_gridColour));
			points.Add(new VertexPositionColor(new Vector3(m_Map.RealWidth, m_Map.RealHeight - 2, 1), m_gridColour));
			points.Add(new VertexPositionColor(new Vector3(0, 0, 1), m_gridColour));
			points.Add(new VertexPositionColor(new Vector3(0, m_Map.RealHeight, 1), m_gridColour));
			points.Add(new VertexPositionColor(new Vector3(1, 0, 1), m_gridColour));
			points.Add(new VertexPositionColor(new Vector3(1, m_Map.RealHeight, 1), m_gridColour));
			points.Add(new VertexPositionColor(new Vector3(m_Map.RealWidth - 1, 0, 1), m_gridColour));
			points.Add(new VertexPositionColor(new Vector3(m_Map.RealWidth - 1, m_Map.RealHeight, 1), m_gridColour));
			points.Add(new VertexPositionColor(new Vector3(m_Map.RealWidth - 2, 0, 1), m_gridColour));
			points.Add(new VertexPositionColor(new Vector3(m_Map.RealWidth - 2, m_Map.RealHeight, 1), m_gridColour));

			//I'm not sure why we need to do this, surely if we didn't pass one it it would just work it's
			// way through the vertices?
			m_gridIndices = new int[points.Count];
			for (int i = 0; i < points.Count; i++)
			{
				m_gridIndices[i] = i;
			}

			m_gridVertices = points.ToArray();

			//Setup our circle
			points.Clear();

			points.Add(new VertexPositionColor(new Vector3(0, 0, 0), m_overlayColour));

			double max = 2 * Math.PI;
			double step = max / 10;
			double radius = (m_Map.TileSize - 6) / 2.0;

			for (double theta = 0; theta < max; theta += step)
			{
				float x = (float)(radius * Math.Cos(theta));
				float y = (float)(radius * Math.Sin(theta));
				points.Add(new VertexPositionColor(new Vector3(x, y, 1), m_overlayColour));
			}

			points.Add(new VertexPositionColor(new Vector3((float)radius, 0, 1), m_overlayColour));

			m_circleIndices = new int[points.Count];
			for (int i = 0; i < points.Count; i++)
			{
				m_circleIndices[i] = i;
			}

			m_circleVertices = points.ToArray();

		}

		private void renderTiles()
		{
			//Draw layer by layer to make sure transparency is done
			// correctly
			foreach (MapLayer layer in m_Map.MapLayers)
			{
				if (!layer.Visible)
					continue;

				List<VertexPositionTexture> points = new List<VertexPositionTexture>();

				//Send all the tiles for the same texture at once,
				// to minimize texture changes
				foreach (Tileset ts in m_Map.TileSets)
				{
					for (int x = 0; x < m_Map.Width; x++) {
						for (int y = 0; y < m_Map.Height; y++) {
							if (layer.TileIsAlive(x, y) && layer.Tiles[x, y].Tileset == ts)
							{
								//We've found a tile using the current texture

								//Get the texel coords
								float top, bottom, left, right;
								left = (float)layer.Tiles[x, y].SourceX / layer.Tiles[x, y].Tileset.Width;
								right = left + ((float)m_Map.TileSize / layer.Tiles[x, y].Tileset.Width);
								top = (float)layer.Tiles[x, y].SourceY / layer.Tiles[x, y].Tileset.Height;
								bottom = top + ((float)m_Map.TileSize / layer.Tiles[x, y].Tileset.Height);

								VertexPositionTexture[] quad = new VertexPositionTexture[6];

								float xPos = (x * m_Map.TileSize) - 0.5f;
								float yPos = (y * m_Map.TileSize) - 0.5f;

								//Setup the two triangles that maketh this quad
								quad[0] = new VertexPositionTexture(new Vector3(xPos, yPos, layer.Z_Value), new Vector2(left, top));
								quad[1] = new VertexPositionTexture(new Vector3(xPos + m_Map.TileSize, yPos + m_Map.TileSize, layer.Z_Value), new Vector2(right, bottom));
								quad[2] = new VertexPositionTexture(new Vector3(xPos, yPos + m_Map.TileSize, layer.Z_Value), new Vector2(left, bottom));
								quad[3] = new VertexPositionTexture(new Vector3(xPos + m_Map.TileSize, yPos, layer.Z_Value), new Vector2(right, top));
								quad[4] = quad[1];
								quad[5] = quad[0];

								points.AddRange(quad);
							}
						}
					}

					if (points.Count > 3)
					{
						int[] indices = new int[points.Count];
						for (int i = 0; i < points.Count; i++)
							indices[i] = i;

						if (ts.Texture == null)
						{
							createTextureForTileset(ts);
						}
						m_device.VertexDeclaration = texturedQuads;
						m_basicEffect.Texture = ts.Texture;
						m_basicEffect.TextureEnabled = true;
						m_basicEffect.Begin();
						m_basicEffect.CurrentTechnique.Passes[0].Begin();

						m_device.DrawUserIndexedPrimitives<VertexPositionTexture>(
								PrimitiveType.TriangleList, points.ToArray(), 0,
								points.Count, indices, 0, points.Count / 3);

						m_basicEffect.CurrentTechnique.Passes[0].End();
						m_basicEffect.End();
					}

					points.Clear();
				}
			}
		}

		private void createTextureForTileset(Tileset ts)
		{
			Texture2D newTex = new Texture2D(m_device, ts.Width, ts.Height, 0, TextureUsage.None, SurfaceFormat.Color);

			System.Drawing.Imaging.BitmapData data = ts.Image.LockBits(
				new System.Drawing.Rectangle(0, 0, ts.Width, ts.Height),
				System.Drawing.Imaging.ImageLockMode.ReadOnly, ts.Image.PixelFormat);

			int bufferSize = data.Height * data.Stride;
			byte[] bytes = new byte[bufferSize];
			System.Runtime.InteropServices.Marshal.Copy(data.Scan0, bytes, 0, bytes.Length);
			ts.Image.UnlockBits(data);

			newTex.SetData<byte>(bytes);
			ts.Texture = newTex;
		}
		#endregion
	}
}
