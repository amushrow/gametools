﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Drawing;
using System.Runtime.InteropServices;
using MapEditorLib;

namespace MapEditor
{
	/// <summary>
	/// A renderer for EditableMap that uses GDI and GDI+
	/// It is quite slow, XnaMapRenderer is recommended
	/// </summary>
	class GdiMapRenderer : IMapRenderer, IDisposable
	{
		#region Variables
		private EditableMap m_Map;
		private Color m_clearColour;
		private Color m_gridColour;
		private Color m_overlayColour;
		private bool m_showGrid;
		private bool m_showSolids;
		private int x_offset, y_offset;
		private int m_width, m_height;
		#endregion

		public GdiMapRenderer(EditableMap Map)
		{
			m_Map = Map;
		}

		~GdiMapRenderer()
		{
			Dispose();
		}

		#region Public Functions
		public void RenderMap(System.Windows.Forms.PaintEventArgs e)
		{
			e.Graphics.Clear(m_clearColour);

			IntPtr target = e.Graphics.GetHdc();
			IntPtr source = Win32.CreateCompatibleDC(target);

			//Draw the tiles
			foreach (MapLayer layer in m_Map.MapLayers)
			{
				if (layer.Visible)
				{
					foreach (Tileset ts in m_Map.TileSets)
					{
						if (ts.pImage == IntPtr.Zero)
							getHBitmap(ts);

						Win32.SelectObject(source, ts.pImage);

						for (int x = 0; x < m_Map.Width; x++)
						{
							for (int y = 0; y < m_Map.Height; y++)
							{
								if (layer.TileIsAlive(x, y) && layer.Tiles[x, y].Tileset == ts)
								{
									int posX = x * m_Map.TileSize - x_offset;
									int posY = y * m_Map.TileSize - y_offset;
									if (posX + m_Map.TileSize >= e.ClipRectangle.Left && posX < e.ClipRectangle.Right
										&& posY + m_Map.TileSize >= e.ClipRectangle.Top && posY < e.ClipRectangle.Bottom)
									{
										Win32.BitBlt(target, posX, posY, m_Map.TileSize, m_Map.TileSize, source, layer.Tiles[x, y].SourceX, layer.Tiles[x, y].SourceY, TernaryRasterOperations.SRCCOPY);
									}
								}
							}
						}
					}
				}
			}

			Win32.DeleteDC(source);
			e.Graphics.ReleaseHdc(target);

			//Draw any text / marks
			Brush myBrush = new SolidBrush(Color.FromArgb(128, m_overlayColour));
			Pen myPen = new Pen(Color.FromArgb(128, m_overlayColour));
			if (m_showSolids)
			{
				e.Graphics.SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.HighSpeed;
				for (int x = 0; x < m_Map.Width; x++)
				{
					for (int y = 0; y < m_Map.Height; y++)
					{
						int xPos = x * m_Map.TileSize + 1 -x_offset;
						int yPos = y * m_Map.TileSize + 1 -y_offset;
						if (m_Map.TileIsSolid(x, y))
							e.Graphics.FillEllipse(myBrush, new Rectangle(xPos, yPos, m_Map.TileSize - 2, m_Map.TileSize - 2));
					}
				}
			}

			//Draw the grid
			myPen = new Pen(m_gridColour);
			if (m_showGrid)
			{
				for (int y = 0; y < m_Map.RealHeight; y += m_Map.TileSize)
					e.Graphics.DrawLine(myPen, 0, y-y_offset, m_width, y-y_offset);
				for (int x = 0; x < m_Map.RealWidth; x += m_Map.TileSize)
					e.Graphics.DrawLine(myPen, x-x_offset, 0, x-x_offset, m_height);
			}

			myPen = new Pen(Color.FromArgb(75, 75, 100), 2);
			e.Graphics.DrawRectangle(myPen, 1-x_offset, 1-y_offset, m_Map.RealWidth, m_Map.RealHeight);
		}

		public void SetClearColour(byte R, byte G, byte B)
		{
			m_clearColour = Color.FromArgb(R, G, B);
		}

		public void SetGridColour(byte R, byte G, byte B)
		{
			m_gridColour = Color.FromArgb(R, G, B);
		}

		public void SetOverlayColour(byte R, byte G, byte B)
		{
			m_overlayColour = Color.FromArgb(R, G, B);
		}

		public void SetGridVisible(bool value)
		{
			m_showGrid = value;
		}

		public void SetSolidsVisible(bool value)
		{
			m_showSolids = value;
		}

		public void SetOffset(int x, int y)
		{
			x_offset = x;
			y_offset = y;
		}

		public void ResetGraphicsDevice(int BufferWidth, int BufferHeight)
		{
			m_width = (m_Map.RealWidth < BufferWidth) ? m_Map.RealWidth : BufferWidth;
			m_height = (m_Map.RealHeight < BufferHeight) ? m_Map.RealHeight : BufferHeight;
		}

		public void Dispose()
		{
			foreach(Tileset ts in m_Map.TileSets)
			{
				if(ts.pImage != IntPtr.Zero)
				{
					Win32.DeleteObject(ts.pImage);
				}
			}
		}
		#endregion

		private void getHBitmap(Tileset ts)
		{
			ts.pImage = ts.Image.GetHbitmap();
		}
	}


	/// <summary>
	/// Exposes standard GDI functions for GdiMapRenderer
	/// </summary>
	public enum TernaryRasterOperations : uint
	{
		SRCCOPY = 0x00CC0020,
		SRCPAINT = 0x00EE0086,
		SRCAND = 0x008800C6,
		SRCINVERT = 0x00660046,
		SRCERASE = 0x00440328,
		NOTSRCCOPY = 0x00330008,
		NOTSRCERASE = 0x001100A6,
		MERGECOPY = 0x00C000CA,
		MERGEPAINT = 0x00BB0226,
		PATCOPY = 0x00F00021,
		PATPAINT = 0x00FB0A09,
		PATINVERT = 0x005A0049,
		DSTINVERT = 0x00550009,
		BLACKNESS = 0x00000042,
		WHITENESS = 0x00FF0062
	}

	public static class Win32
	{
		[DllImport("gdi32.dll")]
		public static extern bool BitBlt(IntPtr hObject, int nXDest, int nYDest, int nWidth,
		   int nHeight, IntPtr hObjSource, int nXSrc, int nYSrc, TernaryRasterOperations dwRop);

		[DllImport("gdi32.dll", ExactSpelling = true, SetLastError = true)]
		public static extern IntPtr CreateCompatibleDC(IntPtr hdc);

		[DllImport("gdi32.dll", ExactSpelling = true, SetLastError = true)]
		public static extern bool DeleteDC(IntPtr hdc);

		[DllImport("gdi32.dll", ExactSpelling = true, SetLastError = true)]
		public static extern IntPtr SelectObject(IntPtr hdc, IntPtr hgdiobj);

		[DllImport("gdi32.dll", ExactSpelling = true, SetLastError = true)]
		public static extern bool DeleteObject(IntPtr hObject);
	}
}
