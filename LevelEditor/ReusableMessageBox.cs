﻿using System;
using System.Windows.Forms;

namespace MapEditor
{
	/// <summary>
	/// A wrapper for the messagebox class, that stores the values
	/// so that you can prepare a messagebox and re-use it later
	/// 
	/// Keeps the code a little tidier eh?
	/// </summary>
	public class ReusableMessageBox
	{
		#region Variables
		private string m_text;
		private string m_caption;
		private MessageBoxButtons m_buttons;
		private MessageBoxIcon m_icon;
		private MessageBoxDefaultButton m_defButton;
		#endregion

		#region Properties
		public string Text
		{
			get { return m_text; }
			set { m_text = value; }
		}

		public string Caption
		{
			get { return m_caption; }
			set { m_caption = value; }
		}
		
		public MessageBoxButtons Buttons
		{
			get { return m_buttons; }
			set { m_buttons = value; }
		}
		
		public MessageBoxIcon Icon
		{
			get { return m_icon; }
			set { m_icon = value; }
		}
		
		public MessageBoxDefaultButton DefaultButton
		{
			get { return m_defButton; }
			set { m_defButton = value; }
		}
		#endregion

		#region All o' thems constructors
		public ReusableMessageBox(string Text)
		{
			m_text = Text;
			m_caption = "";
			m_buttons = MessageBoxButtons.OK;
			m_icon = MessageBoxIcon.None;
			m_defButton = MessageBoxDefaultButton.Button1;
		}
		
		public ReusableMessageBox(string Text, string Caption)
		{
			m_text = Text;
			m_caption = Caption;
			m_buttons = MessageBoxButtons.OK;
			m_icon = MessageBoxIcon.None;
			m_defButton = MessageBoxDefaultButton.Button1;
		}
		
		public ReusableMessageBox(string Text, string Caption, MessageBoxIcon Icon)
		{
			m_text = Text;
			m_caption = Caption;
			m_buttons = MessageBoxButtons.OK;
			m_icon = Icon;
			m_defButton = MessageBoxDefaultButton.Button1;
		}
		
		public ReusableMessageBox(string Text, string Caption, MessageBoxButtons Buttons)
		{
			m_text = Text;
			m_caption = Caption;
			m_buttons = Buttons;
			m_icon = MessageBoxIcon.None;
			m_defButton = MessageBoxDefaultButton.Button1;
		}
		
		public ReusableMessageBox(string Text, string Caption, MessageBoxIcon Icon, MessageBoxButtons Buttons)
		{
			m_text = Text;
			m_caption = Caption;
			m_buttons = Buttons;
			m_icon = Icon;
			m_defButton = MessageBoxDefaultButton.Button1;
		}
		
		public ReusableMessageBox(string Text, string Caption, MessageBoxIcon Icon, MessageBoxButtons Buttons, MessageBoxDefaultButton DefaultButton)
		{
			m_text = Text;
			m_caption = Caption;
			m_buttons = Buttons;
			m_icon = Icon;
			m_defButton = DefaultButton;
		}
		#endregion

		public DialogResult Show()
		{
			return MessageBox.Show(m_text, m_caption, m_buttons, m_icon, m_defButton);
		}
	}
}
