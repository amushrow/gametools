﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace MapEditor
{
	/// <summary>
	/// Simple dialog to add a new layer to the map.
	/// It has two options, a name and a z-value
	/// </summary>
	public partial class NewLayerDialog : Form
	{
		#region Variables
		private string m_name;
		private float m_zvalue;
		private bool m_bg;
		#endregion

		#region Properties
		public string LayerName
		{
			get { return m_name; }
		}

		public float Z_Value
		{
			get { return m_zvalue; }
		}

		public bool Background
		{
			get { return m_bg; }
		}
		#endregion

		public NewLayerDialog()
		{
			InitializeComponent();

			m_name = "";
			m_zvalue = 0;
			m_bg = false;
		}

		#region Events
		private void button1_Click(object sender, EventArgs e)
		{
			if (this.nameField.Text.Length > 0)
			{
				m_name = this.nameField.Text;
				m_zvalue = (float)this.zvalueField.Value;
				this.DialogResult = DialogResult.OK;
				this.Close();
			}
			else
			{
				MessageBox.Show("You must enter a name for the layer", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
			}
		}

		private void button2_Click(object sender, EventArgs e)
		{
			this.DialogResult = DialogResult.Cancel;
			this.Close();
		}
		#endregion
	}
}
