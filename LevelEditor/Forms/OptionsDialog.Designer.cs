﻿
namespace Settings
{
	partial class OptionsDialog
	{
		/// <summary>
		/// Designer variable used to keep track of non-visual components.
		/// </summary>
		private System.ComponentModel.IContainer components = null;
		
		/// <summary>
		/// Disposes resources used by the form.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing) {
				if (components != null) {
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}
		
		/// <summary>
		/// This method is required for Windows Forms designer support.
		/// Do not change the method contents inside the source code editor. The Forms designer might
		/// not be able to load this method if it was changed manually.
		/// </summary>
		private void InitializeComponent()
		{
			this.label1 = new System.Windows.Forms.Label();
			this.label2 = new System.Windows.Forms.Label();
			this.groupBox1 = new System.Windows.Forms.GroupBox();
			this.grid_colourPicker = new Settings.ColourPicker();
			this.overlay_colourPicker = new Settings.ColourPicker();
			this.groupBox2 = new System.Windows.Forms.GroupBox();
			this.numberCheckBox = new System.Windows.Forms.CheckBox();
			this.solidCheckBox = new System.Windows.Forms.CheckBox();
			this.gridCheckBox = new System.Windows.Forms.CheckBox();
			this.fontDialog1 = new System.Windows.Forms.FontDialog();
			this.font_textbox = new System.Windows.Forms.TextBox();
			this.fontbrowse_button = new System.Windows.Forms.Button();
			this.label3 = new System.Windows.Forms.Label();
			this.groupBox1.SuspendLayout();
			this.groupBox2.SuspendLayout();
			this.SuspendLayout();
			// 
			// label1
			// 
			this.label1.Location = new System.Drawing.Point(6, 16);
			this.label1.Name = "label1";
			this.label1.Size = new System.Drawing.Size(80, 16);
			this.label1.TabIndex = 0;
			this.label1.Text = "Overlay Colour";
			// 
			// label2
			// 
			this.label2.Location = new System.Drawing.Point(6, 59);
			this.label2.Name = "label2";
			this.label2.Size = new System.Drawing.Size(80, 16);
			this.label2.TabIndex = 2;
			this.label2.Text = "Grid Colour";
			// 
			// groupBox1
			// 
			this.groupBox1.Controls.Add(this.label1);
			this.groupBox1.Controls.Add(this.grid_colourPicker);
			this.groupBox1.Controls.Add(this.overlay_colourPicker);
			this.groupBox1.Controls.Add(this.label2);
			this.groupBox1.Location = new System.Drawing.Point(12, 12);
			this.groupBox1.Name = "groupBox1";
			this.groupBox1.Size = new System.Drawing.Size(163, 106);
			this.groupBox1.TabIndex = 4;
			this.groupBox1.TabStop = false;
			this.groupBox1.Text = "Colours";
			// 
			// grid_colourPicker
			// 
			this.grid_colourPicker.AutoSize = true;
			this.grid_colourPicker.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
			this.grid_colourPicker.Location = new System.Drawing.Point(6, 75);
			this.grid_colourPicker.Margin = new System.Windows.Forms.Padding(0);
			this.grid_colourPicker.Name = "grid_colourPicker";
			this.grid_colourPicker.SelectedIndex = -1;
			this.grid_colourPicker.Size = new System.Drawing.Size(150, 21);
			this.grid_colourPicker.TabIndex = 2;
			// 
			// overlay_colourPicker
			// 
			this.overlay_colourPicker.AutoSize = true;
			this.overlay_colourPicker.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
			this.overlay_colourPicker.Location = new System.Drawing.Point(6, 32);
			this.overlay_colourPicker.Margin = new System.Windows.Forms.Padding(0);
			this.overlay_colourPicker.Name = "overlay_colourPicker";
			this.overlay_colourPicker.SelectedIndex = -1;
			this.overlay_colourPicker.Size = new System.Drawing.Size(150, 21);
			this.overlay_colourPicker.TabIndex = 1;
			// 
			// groupBox2
			// 
			this.groupBox2.Controls.Add(this.numberCheckBox);
			this.groupBox2.Controls.Add(this.solidCheckBox);
			this.groupBox2.Controls.Add(this.gridCheckBox);
			this.groupBox2.Location = new System.Drawing.Point(12, 124);
			this.groupBox2.Name = "groupBox2";
			this.groupBox2.Size = new System.Drawing.Size(163, 107);
			this.groupBox2.TabIndex = 5;
			this.groupBox2.TabStop = false;
			this.groupBox2.Text = "Overlay";
			// 
			// numberCheckBox
			// 
			this.numberCheckBox.Enabled = false;
			this.numberCheckBox.Location = new System.Drawing.Point(6, 79);
			this.numberCheckBox.Name = "numberCheckBox";
			this.numberCheckBox.Size = new System.Drawing.Size(150, 24);
			this.numberCheckBox.TabIndex = 5;
			this.numberCheckBox.Text = "Show Grid Numbers (slow)";
			this.numberCheckBox.UseVisualStyleBackColor = true;
			this.numberCheckBox.CheckedChanged += new System.EventHandler(this.NumberCheckBoxCheckedChanged);
			// 
			// solidCheckBox
			// 
			this.solidCheckBox.Location = new System.Drawing.Point(6, 49);
			this.solidCheckBox.Name = "solidCheckBox";
			this.solidCheckBox.Size = new System.Drawing.Size(104, 24);
			this.solidCheckBox.TabIndex = 4;
			this.solidCheckBox.Text = "Show Solids";
			this.solidCheckBox.UseVisualStyleBackColor = true;
			this.solidCheckBox.CheckedChanged += new System.EventHandler(this.SolidCheckBoxCheckedChanged);
			// 
			// gridCheckBox
			// 
			this.gridCheckBox.Checked = true;
			this.gridCheckBox.CheckState = System.Windows.Forms.CheckState.Checked;
			this.gridCheckBox.Location = new System.Drawing.Point(6, 19);
			this.gridCheckBox.Name = "gridCheckBox";
			this.gridCheckBox.Size = new System.Drawing.Size(104, 24);
			this.gridCheckBox.TabIndex = 3;
			this.gridCheckBox.Text = "ShowGrid";
			this.gridCheckBox.UseVisualStyleBackColor = true;
			this.gridCheckBox.CheckedChanged += new System.EventHandler(this.GridCheckBoxCheckedChanged);
			// 
			// font_textbox
			// 
			this.font_textbox.BackColor = System.Drawing.Color.White;
			this.font_textbox.Location = new System.Drawing.Point(12, 250);
			this.font_textbox.Name = "font_textbox";
			this.font_textbox.ReadOnly = true;
			this.font_textbox.Size = new System.Drawing.Size(132, 20);
			this.font_textbox.TabIndex = 7;
			// 
			// fontbrowse_button
			// 
			this.fontbrowse_button.Location = new System.Drawing.Point(146, 248);
			this.fontbrowse_button.Name = "fontbrowse_button";
			this.fontbrowse_button.Size = new System.Drawing.Size(29, 23);
			this.fontbrowse_button.TabIndex = 6;
			this.fontbrowse_button.Text = "...";
			this.fontbrowse_button.UseVisualStyleBackColor = true;
			this.fontbrowse_button.Click += new System.EventHandler(this.Button1Click);
			// 
			// label3
			// 
			this.label3.Location = new System.Drawing.Point(12, 234);
			this.label3.Name = "label3";
			this.label3.Size = new System.Drawing.Size(100, 13);
			this.label3.TabIndex = 8;
			this.label3.Text = "Font";
			// 
			// OptionsDialog
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.AutoSize = true;
			this.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
			this.ClientSize = new System.Drawing.Size(190, 282);
			this.Controls.Add(this.label3);
			this.Controls.Add(this.fontbrowse_button);
			this.Controls.Add(this.font_textbox);
			this.Controls.Add(this.groupBox2);
			this.Controls.Add(this.groupBox1);
			this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
			this.KeyPreview = true;
			this.Name = "OptionsDialog";
			this.Padding = new System.Windows.Forms.Padding(10);
			this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
			this.Text = "Settings";
			this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.OptionsDialogFormClosing);
			this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.OptionsDialogKeyDown);
			this.groupBox1.ResumeLayout(false);
			this.groupBox1.PerformLayout();
			this.groupBox2.ResumeLayout(false);
			this.ResumeLayout(false);
			this.PerformLayout();

		}
		private System.Windows.Forms.Label label3;
		private System.Windows.Forms.Button fontbrowse_button;
		private System.Windows.Forms.TextBox font_textbox;
		private System.Windows.Forms.FontDialog fontDialog1;
		private System.Windows.Forms.CheckBox numberCheckBox;
		private System.Windows.Forms.CheckBox solidCheckBox;
		private System.Windows.Forms.CheckBox gridCheckBox;
		private System.Windows.Forms.GroupBox groupBox2;
		private System.Windows.Forms.GroupBox groupBox1;
		private System.Windows.Forms.Label label2;
		private Settings.ColourPicker grid_colourPicker;
		private Settings.ColourPicker overlay_colourPicker;
		private System.Windows.Forms.Label label1;
	}
}
