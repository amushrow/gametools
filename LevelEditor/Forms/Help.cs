﻿

using System;
using System.Drawing;
using System.Windows.Forms;

namespace MapEditor
{
	/// <summary>
	/// This will eventually show a help file
	/// Update 06/10/2009: I bet it never shows a help file. :-P
	/// </summary>
	public partial class Help : Form
	{
		public Help()
		{
			InitializeComponent();
			this.label1.Text = "There is no help...\n\nI really must get around to doing that.";
		}
	}
}
