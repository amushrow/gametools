﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Windows.Forms;
using Settings;
using System.IO;
using System.Text;
using MapEditorLib;
using GameDataLib;

namespace MapEditor
{
	/// <summary>
	/// The main form provides a link between the TiledMap control
	/// and the TileSelect control. Both controls can be used 
	/// independently, although to what purpose I am not sure.
	/// </summary>
	public partial class MainForm : Form
	{
		#region Variables
		//Options dialog is stored as a member so that we can just
		// show/hide the same window rather than opening a new one
		private OptionsDialog m_options;
		private ReusableMessageBox m_saveMessage;
		
		//m_path stores the path to the current file, and m_edited
		// is used so we know when the file has been edited since
		// it was last saved
		private string m_path;
		private bool m_edited;

		//Tree view for placing objects
		private TreeView m_treeViewControl;
		#endregion

		public MainForm()
		{
			InitializeComponent();

			initialize();
		}
		
		//Start form and load a file
		public MainForm(string Path)
		{
			InitializeComponent();

			initialize();

			//Load file
			FileInfo fi = new FileInfo(Path);
			if(fi.Exists && fi.Extension == ".skm")
			{
				load(Path);
			}
		}

		private void initialize()
		{
			//Set up the events
			this.tiledMap1.OnTileSizeChanged += new TileSizeChangedEventHandler(this.tileSizeChanged);
			this.tiledMap1.OnWorkingLayerChanged += new EventHandler(this.tiledMapLayerChanged);
			this.tileSelect1.OnTilesetsChanged += new TilesetsChangedEventHandler(this.tilesetsChanged);
			this.tileSelect1.OnTileSelectionChanged += new TileChangedEventHandler(this.tileSelectionChanged);

			//Default values
			m_path = String.Empty;
			m_edited = false;
			ToolStripManager.Renderer = new CustomToolStripRenderer();
			m_saveMessage = new ReusableMessageBox("Do you want to save any changes?\nAny unsaved changes will be lost", "Save", MessageBoxIcon.Warning, MessageBoxButtons.YesNoCancel);

			//Link up the tileset select control, with the actual map
			this.tileSelect1.SetTileSets(this.tiledMap1.GetTilesets());
			this.tileSelect1.TileSize = this.tiledMap1.TileSize;


			m_treeViewControl = new TreeView();
			m_treeViewControl.Dock = DockStyle.Fill;
			m_treeViewControl.Name = "objectTreeView";
			m_treeViewControl.HideSelection = false;
			m_treeViewControl.AfterSelect += new TreeViewEventHandler(this.treeView_AfterSelect);
		}
		
		#region External Events
		void tilesetsChanged(object sender, EventArgs e)
		{
			m_edited = true;
			
			//This just adds a start to the title of the window,
			// so that you can easily see if you need to save
			if(!this.Text.EndsWith("*"))
				this.Text += "*";
		}

        void tileSelectionChanged(object sender, TileChangedEventArgs e)
        {
            this.tiledMap1.SetSelectedTiles(e.Tiles);
        }

		void treeView_AfterSelect(object sender, TreeViewEventArgs e)
		{
			TreeNode node = m_treeViewControl.SelectedNode;
			if(node.Level > 0 && node.Tag is CustomObject)
			{
				CustomObject obj = node.Tag as CustomObject;
				this.tiledMap1.SetSelectedObject(obj);
			}
			else
			{
				this.tiledMap1.SetSelectedObject(null);
			}
		}
		
		void overlayColourChanged(object sender, ColourChangedEventArgs e)
		{
			this.tiledMap1.OverlayColour = e.Colour;
			this.tileSelect1.OverlayColour = e.Colour;
		}

		void gridColourChanged(object sender, ColourChangedEventArgs e)
		{
			this.tiledMap1.GridColour = e.Colour;
			this.tileSelect1.GridColour = e.Colour;
		}
		
		void showGridChanged(object sender, FlagChangedEventArgs e)
		{
			this.tiledMap1.ShowGrid = e.IsSet;
		}
		
		void showNumbersChanged(object sender, FlagChangedEventArgs e)
		{
			this.tiledMap1.ShowNumbers = e.IsSet;
		}
		
		void showSolidsChanged(object sender, FlagChangedEventArgs e)
		{
			this.tiledMap1.ShowSolids = e.IsSet;
		}
		
		void tileSizeChanged(object sender, TileSizeChangedEventArgs e)
		{
			m_edited = true;
			if(!this.Text.EndsWith("*"))
				this.Text += "*";
			this.tileSelect1.TileSize = e.TileSize;
		}

		void tiledMapLayerChanged(object sender, EventArgs e)
		{
			this.splitContainer1.Panel1.Controls.Clear();
			switch(this.tiledMap1.CurrentLayerType)
			{
				case LayerTypes.TILES:
					this.splitContainer1.Panel1.Controls.Add(this.tileSelect1);
					break;

				case LayerTypes.OBJECTS:
					this.splitContainer1.Panel1.Controls.Add(m_treeViewControl);
					break;
			}
		}

		void fontChanged(object sender, FontChangedEventArgs e)
		{
			this.tiledMap1.MapFont = e.Font;
		}
		#endregion
		
		#region Form Events
		void OptionsToolStripMenuItemClick(object sender, EventArgs e)
		{
			//Show the options dialog, if its not already open then we need
			// to set all the events etc.
			if(m_options == null || m_options.IsDisposed)
			{
				m_options = new OptionsDialog();
				m_options.OverlayColourChanged += new ColourChangedEventHandler(this.overlayColourChanged);
				m_options.GridColourChanged += new ColourChangedEventHandler(this.gridColourChanged);
				m_options.ShowGridChanged += new FlagChangedEventHandler(this.showGridChanged);
				m_options.ShowSolidsChanged += new FlagChangedEventHandler(this.showSolidsChanged);
				m_options.ShowNumbersChanged += new FlagChangedEventHandler(this.showNumbersChanged);
				m_options.FontChanged += new FontChangedEventHandler(this.fontChanged);
				m_options.Show(this);
			} else
				m_options.Visible = !m_options.Visible;
		}
		
		void MainFormFormClosing(object sender, FormClosingEventArgs e)
		{
			DialogResult dr = checkForSave();
			if(dr == DialogResult.No) {
				if(m_options != null && !m_options.IsDisposed)
					m_options.Close();
			} else if(dr == DialogResult.Cancel)
				e.Cancel = true;
		}
		
		void MainFormKeyDown(object sender, KeyEventArgs e)
		{
			if(e.KeyCode == Keys.ControlKey)
				this.tiledMap1.CtrlKey = true;
		}
		
		void MainFormKeyUp(object sender, KeyEventArgs e)
		{
			if(e.KeyCode == Keys.ControlKey)
				this.tiledMap1.CtrlKey = false;
		}
		
		void ExitToolStripMenuItemClick(object sender, EventArgs e)
		{
			this.Close();
		}
		
		void SaveToolStripMenuItemClick(object sender, EventArgs e)
		{
			save();
		}
		
		void SaveAsToolStripMenuItemClick(object sender, EventArgs e)
		{
			//Show the save file dialog to get a path to save to
			DialogResult dr = this.saveFileDialog1.ShowDialog();
			if(dr == DialogResult.OK)
			{
				m_path = this.saveFileDialog1.FileName;
				FileInfo fi = new FileInfo(m_path);
				this.Text = "MapEditor - " + fi.Name;

				this.tiledMap1.SaveMap(m_path);
				m_edited = false;
			}
		}
		
		void MainFormDragDrop(object sender, DragEventArgs e)
		{
			//See whats being droped, if its a map file, then open it
			if(e.Data.GetDataPresent(DataFormats.FileDrop))
			{
				string[] data = (string[])e.Data.GetData(DataFormats.FileDrop);
				FileInfo fi = new FileInfo(data[0]);
				if(fi.Extension == ".skm") {
					DialogResult dr = checkForSave();
			
					if(dr != DialogResult.Cancel) {
						try {
							load(fi.FullName);
						} catch(InvalidFileTypeException) {
							MessageBox.Show("File is not a valid MapEditor file", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
						}
					}
				}
			}
		}
		
		void MainFormDragEnter(object sender, DragEventArgs e)
		{
			//When user drags something onto the form check 
			// for a map file and then show the right effect
			e.Effect = DragDropEffects.None;
			if(e.Data.GetDataPresent(DataFormats.FileDrop)) {
				string[] data = (string[])e.Data.GetData(DataFormats.FileDrop);
				FileInfo fi = new FileInfo(data[0]);
				if(fi.Extension == ".skm")
					e.Effect = DragDropEffects.Move;
			}				
		}
		
		void NewToolStripMenuItemClick(object sender, EventArgs e)
		{
			//Reset the maps
			DialogResult dr = checkForSave();
			if(dr == DialogResult.Cancel)
				return;

			this.tiledMap1.Clear();
			this.tileSelect1.SetTileSets(this.tiledMap1.GetTilesets());
			this.tileSelect1.TileSize = this.tiledMap1.TileSize;

			m_edited = false;
			m_path = "";
			this.Text = "MapEditor";
		}
		
		void OpenToolStripMenuItemClick(object sender, EventArgs e)
		{
			DialogResult dr = checkForSave();
			
			if(dr != DialogResult.Cancel)
			{
				dr = this.openFileDialog1.ShowDialog();
				if(dr == DialogResult.OK)
				{
					//Try to open the new file
					try {
						load(this.openFileDialog1.FileName);
					}
					//Catch the error if the file is not a valid map file
					catch(InvalidFileTypeException) {
						MessageBox.Show("File is not a valid MapEditor file", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
					}
				}
			}
		}
		
		//For some reason, if the menu has focus, it's shortcuts don't work >:(
		void MenuStrip1KeyDown(object sender, KeyEventArgs e)
		{
			if(e.KeyData == Keys.F7)
			{
				if(m_options == null || m_options.IsDisposed)
				{
					m_options = new OptionsDialog();
					m_options.OverlayColourChanged += new ColourChangedEventHandler(this.overlayColourChanged);
					m_options.GridColourChanged += new ColourChangedEventHandler(this.gridColourChanged);
					m_options.ShowGridChanged += new FlagChangedEventHandler(this.showGridChanged);
					m_options.ShowSolidsChanged += new FlagChangedEventHandler(this.showSolidsChanged);
					m_options.ShowNumbersChanged += new FlagChangedEventHandler(this.showNumbersChanged);
					m_options.FontChanged += new FontChangedEventHandler(this.fontChanged);
					m_options.Visible = false;
					m_options.Show(this);
				} else
					m_options.Visible = !m_options.Visible;
			}
		}
		
		void ReloadFileToolStripMenuItemClick(object sender, EventArgs e)
		{
			//The same as opening any other file, but we use the path stored
			// in m_path
			if(m_path != "") {
				DialogResult dr = checkForSave();
				
				if(dr != DialogResult.Cancel) {
					try {
						load(m_path);
					} catch(InvalidFileTypeException) {
						MessageBox.Show("File is not a valid MapEditor file", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
					}
				}
			}
		}
		
		//And a little gradient here to match the toolstrip, Mmm shiny
		void SplitContainer1Paint(object sender, PaintEventArgs e)
		{
			int darkness = 30;
			int r,g,b;
			r = this.BackColor.R;
			g = this.BackColor.G;
			b = this.BackColor.B;
			
			//If the ControlPaint.Dark method worked properly, I would use it
			r -= darkness; g -= darkness; b -= darkness;
			if(r<0)
				r=0;
			if(g<0)
				g=0;
			if(b<0)
				b=0;
			Color dark = Color.FromArgb(r,g,b);
			
			LinearGradientBrush myBrush = new LinearGradientBrush(e.ClipRectangle, this.BackColor, dark, LinearGradientMode.Vertical);
			e.Graphics.FillRectangle(myBrush, e.ClipRectangle);
		}
		
		void AboutToolStripMenuItemClick(object sender, EventArgs e)
		{
			About info = new About();
			info.ShowDialog();
			info.Dispose();
		}
		
		void HelpToolStripMenuItem1Click(object sender, EventArgs e)
		{
			//One day, there will be help... maybe.
			Help help = new Help();
			help.ShowDialog();
			help.Dispose();
		}
		
		private void optimizeToolStripMenuItem_Click(object sender, EventArgs e)
		{
			this.tiledMap1.OptimizeMap();
			this.tileSelect1.SetTileSets(this.tiledMap1.GetTilesets());
			m_edited = true;
			if (!this.Text.EndsWith("*"))
				this.Text += "*";
		}
		#endregion
		
		#region Private Methods
		private DialogResult save()
		{
			if(m_path != String.Empty)
			{
				this.tiledMap1.SaveMap(m_path);
				m_edited = false;
				this.Text = this.Text.TrimEnd('*');
			} 
			else if(this.saveFileDialog1.ShowDialog() == DialogResult.OK)
			{
				m_path = this.saveFileDialog1.FileName;
				FileInfo fi = new FileInfo(m_path);
				this.Text = "MapEditor - " + fi.Name;
				this.tiledMap1.SaveMap(m_path);

				m_edited = false;
				return DialogResult.OK;
			}
            return DialogResult.Cancel;
		}

		private void load(string path)
		{
			m_treeViewControl.Nodes.Clear();
			this.tiledMap1.SetSelectedObject(null);

			this.tiledMap1.LoadMap(path);

			this.tileSelect1.SetTileSets(this.tiledMap1.GetTilesets());
			this.tileSelect1.TileSize = this.tiledMap1.TileSize;

			FileInfo fi = new FileInfo(path);
			this.Text = "MapEditor - " + fi.Name;
			m_path = path;

			m_edited = false;
		}

		private DialogResult checkForSave()
		{
			DialogResult dr = DialogResult.None;
			if(m_edited) 
			{
				dr = m_saveMessage.Show();
				if(dr == DialogResult.Yes)
					dr = save();
			}
			
			return dr;
		}
		#endregion

		private void loadDataFileToolStripMenuItem_Click(object sender, EventArgs e)
		{
			OpenFileDialog diag = new OpenFileDialog();
			diag.Filter = "Game Data File|*.skd";

			if (diag.ShowDialog() == DialogResult.OK)
			{
				m_treeViewControl.Nodes.Clear();

				FileStream fs = new FileStream(diag.FileName, FileMode.Open);

				StreamWrapper sw = new StreamWrapper(fs);
				TreeNode currentRoot = null;

				int numNodes = sw.ReadInt();
				for (int nodeCount = 0; nodeCount < numNodes; nodeCount++)
				{
					bool isRoot = sw.ReadBool();
					string name = sw.ReadString();
					long id = sw.ReadLong();
                    string type_name = sw.ReadString();

					CustomObject newType = new CustomObject(name, id, type_name);

					int numData = sw.ReadInt();
					for (int i = 0; i < numData; i++)
					{
						if (isRoot)
						{
							string dataName = sw.ReadString();
							DataType type = (DataType)sw.ReadInt();
							long data_id = sw.ReadLong();
							newType.AddData(dataName, type, data_id);
						}
						else
						{
							CustomObjectData newData = CustomObjectData.ReadFromStream(fs);
							newType.AddData(newData);
						}
					}

					this.tiledMap1.SetGameObjects(newType);

					TreeNode newNode;
					if (isRoot)
					{
						newNode = new TreeNode(name.ToUpper(), 0, 0);
						newNode.Tag = newType;

						m_treeViewControl.Nodes.Add(newNode);
						currentRoot = newNode;
					}
					else if (currentRoot != null)
					{
						newNode = new TreeNode(name, 1, 1);
						newNode.Tag = newType;

						currentRoot.Nodes.Add(newNode);
					}
				}
			}
		}
	}
}
