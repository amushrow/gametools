﻿namespace MapEditor
{
	partial class NewLayerDialog
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.label1 = new System.Windows.Forms.Label();
			this.nameField = new System.Windows.Forms.TextBox();
			this.zvalueField = new System.Windows.Forms.NumericUpDown();
			this.label2 = new System.Windows.Forms.Label();
			this.ok_button = new System.Windows.Forms.Button();
			this.cancle_button = new System.Windows.Forms.Button();
			((System.ComponentModel.ISupportInitialize)(this.zvalueField)).BeginInit();
			this.SuspendLayout();
			// 
			// label1
			// 
			this.label1.AutoSize = true;
			this.label1.Location = new System.Drawing.Point(12, 9);
			this.label1.Name = "label1";
			this.label1.Size = new System.Drawing.Size(38, 13);
			this.label1.TabIndex = 0;
			this.label1.Text = "Name:";
			// 
			// nameField
			// 
			this.nameField.Location = new System.Drawing.Point(15, 25);
			this.nameField.Name = "nameField";
			this.nameField.Size = new System.Drawing.Size(165, 20);
			this.nameField.TabIndex = 1;
			// 
			// zvalueField
			// 
			this.zvalueField.DecimalPlaces = 2;
			this.zvalueField.Increment = new decimal(new int[] {
            1,
            0,
            0,
            65536});
			this.zvalueField.Location = new System.Drawing.Point(186, 25);
			this.zvalueField.Maximum = new decimal(new int[] {
            0,
            0,
            0,
            0});
			this.zvalueField.Minimum = new decimal(new int[] {
            10,
            0,
            0,
            -2147483648});
			this.zvalueField.Name = "zvalueField";
			this.zvalueField.Size = new System.Drawing.Size(86, 20);
			this.zvalueField.TabIndex = 2;
			// 
			// label2
			// 
			this.label2.AutoSize = true;
			this.label2.Location = new System.Drawing.Point(183, 9);
			this.label2.Name = "label2";
			this.label2.Size = new System.Drawing.Size(44, 13);
			this.label2.TabIndex = 3;
			this.label2.Text = "Z-Value";
			// 
			// ok_button
			// 
			this.ok_button.Location = new System.Drawing.Point(197, 51);
			this.ok_button.Name = "ok_button";
			this.ok_button.Size = new System.Drawing.Size(75, 23);
			this.ok_button.TabIndex = 3;
			this.ok_button.Text = "OK";
			this.ok_button.UseVisualStyleBackColor = true;
			this.ok_button.Click += new System.EventHandler(this.button1_Click);
			// 
			// cancle_button
			// 
			this.cancle_button.Location = new System.Drawing.Point(116, 51);
			this.cancle_button.Name = "cancle_button";
			this.cancle_button.Size = new System.Drawing.Size(75, 23);
			this.cancle_button.TabIndex = 4;
			this.cancle_button.Text = "Cancle";
			this.cancle_button.UseVisualStyleBackColor = true;
			this.cancle_button.Click += new System.EventHandler(this.button2_Click);
			// 
			// NewLayerDialog
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(283, 83);
			this.Controls.Add(this.cancle_button);
			this.Controls.Add(this.ok_button);
			this.Controls.Add(this.label2);
			this.Controls.Add(this.zvalueField);
			this.Controls.Add(this.nameField);
			this.Controls.Add(this.label1);
			this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
			this.Name = "NewLayerDialog";
			this.Text = "NewLayerDialog";
			((System.ComponentModel.ISupportInitialize)(this.zvalueField)).EndInit();
			this.ResumeLayout(false);
			this.PerformLayout();

		}

		#endregion

		private System.Windows.Forms.Label label1;
		private System.Windows.Forms.TextBox nameField;
		private System.Windows.Forms.NumericUpDown zvalueField;
		private System.Windows.Forms.Label label2;
		private System.Windows.Forms.Button ok_button;
		private System.Windows.Forms.Button cancle_button;
	}
}