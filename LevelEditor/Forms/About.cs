﻿using System;
using System.Drawing;
using System.Windows.Forms;

namespace MapEditor
{
	/// <summary>
	/// Simple form, just to show a teeny bit of info
	/// </summary>
	public partial class About : Form
	{
		public About()
		{
			InitializeComponent();
			
			//Center the labels
			this.version.Text = Application.ProductVersion + "/CP";
			int width = this.label1.Width+this.version.Width;
			int xPos = (this.Width-width)/2;
			int yPos = 75;
			this.label1.Location = new Point(xPos, yPos);
			this.version.Location = new Point(xPos+this.label1.Width, yPos);
		}
		
		void LinkLabel1LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
		{
			System.Diagnostics.Process.Start("http://www.codeproject.com/info/cpol10.aspx");
		}
	}
}
