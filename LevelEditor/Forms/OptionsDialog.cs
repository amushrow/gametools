﻿using System;
using System.Drawing;
using System.Windows.Forms;
using MapEditor;

namespace Settings
{
	/// <summary>
	/// Options dialog for the main form, lets the user select
	/// overlay colours for the map, and an option to turn the
	/// grid on and off.
	/// 
	/// The effects of changing an option are immediate, whenever
	/// a value is changed an event is sent to the main form
	/// </summary>
	public partial class OptionsDialog : Form
	{
		#region Properties
		public new event FontChangedEventHandler FontChanged;
		public event FlagChangedEventHandler ShowGridChanged;
		public event FlagChangedEventHandler ShowSolidsChanged;
		public event FlagChangedEventHandler ShowNumbersChanged;
				
		public event ColourChangedEventHandler OverlayColourChanged
		{
			add { this.overlay_colourPicker.ColourChanged += value; }
			remove { this.overlay_colourPicker.ColourChanged -= value; }
		}
		
		public event ColourChangedEventHandler GridColourChanged
		{
			add { this.grid_colourPicker.ColourChanged += value; }
			remove { this.grid_colourPicker.ColourChanged -= value; }
		}
		#endregion

		public OptionsDialog()
		{
			InitializeComponent();
			this.overlay_colourPicker.SelectedColor = Color.Red;
			this.grid_colourPicker.SelectedColor = Color.Gray;
		}

		#region Events
		void OptionsDialogKeyDown(object sender, KeyEventArgs e)
		{
			if(e.KeyData == Keys.F7)
				this.Hide();
		}
		
		void Button1Click(object sender, EventArgs e)
		{
			this.fontDialog1.ShowDialog();
			Font font = this.fontDialog1.Font;
			this.font_textbox.Text = font.Name + ", " + font.Size.ToString();
			if(font.Bold)
				this.font_textbox.Text += ", Bold";
			if(font.Italic)
				this.font_textbox.Text += ", Italic";
			FontChangedEventArgs fea = new FontChangedEventArgs(font);
			if(FontChanged != null)
				FontChanged(this, fea);
		}
		
		void GridCheckBoxCheckedChanged(object sender, EventArgs e)
		{
			bool b = this.gridCheckBox.Checked;
			if(ShowGridChanged != null)
				ShowGridChanged(this, new FlagChangedEventArgs(b));
		}
		
		void SolidCheckBoxCheckedChanged(object sender, EventArgs e)
		{
			bool b = this.solidCheckBox.Checked;
			if(ShowSolidsChanged != null)
				ShowSolidsChanged(this, new FlagChangedEventArgs(b));
		}
		
		void NumberCheckBoxCheckedChanged(object sender, EventArgs e)
		{
			bool b = this.numberCheckBox.Checked;
			if(ShowNumbersChanged != null)
				ShowNumbersChanged(this, new FlagChangedEventArgs(b));
		}
		
		void OptionsDialogFormClosing(object sender, FormClosingEventArgs e)
		{
			if(e.CloseReason != CloseReason.FormOwnerClosing) {
				e.Cancel = true;
				this.Hide();
			}
		}
		#endregion
	}
}
